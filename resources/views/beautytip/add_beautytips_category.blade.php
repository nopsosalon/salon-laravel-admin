
@extends('service-mgmt.base')
@section('action-content')

@push("css")
    <style type="text/css">
        input[type="file"]
            {
                /*color: transparent;*/
            }
    </style>
@endpush

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Beauty Tips Category </h3><br>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Add Beauty Tips Category</h3>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="{{ url('store_beautytips_category') }}" method="POST" role="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group col-sm-6">
                        <label for="exampleInputEmail1">Beauty Tips Name</label>
                        <input required="" type="text" name="btc_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="exampleInputPassword1">Beauty Tips Category Status</label>
                        <select name="btc_status" class="form-control">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                   
                    <div class="form-group col-sm-6">
                        <label for="exampleInputFile">Add Image</label>
                        <input name="btc_image" type="file" id="exampleInputFile" class="form-control" onclick="fileClicked(event)" onchange="fileChanged(event)">
                    </div>
                </div>
                <!-- /.box-body -->
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                      <a class="btn btn-primary" style="margin-left: 10px;" href="{{ url($returnback) }}">Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection

@push('script')
    <script type="text/javascript">
        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })
    </script>



      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

    <script>
        
      @if(Session::has('success'))
            toastr.success("{{ Session::get('success') }}");
      @endif
      @if(Session::has('info'))
            toastr.info("{{ Session::get('info') }}");
      @endif
      @if(Session::has('warning'))
            toastr.warning("{{ Session::get('warning') }}");
      @endif
      @if(Session::has('error'))
            toastr.error("{{ Session::get('error') }}");
      @endif


    </script>

    <script>
    //This is All Just For Logging:
    var debug = true;//true: add debug logs when cloning
    var evenMoreListeners = true;//demonstrat re-attaching javascript Event Listeners (Inline Event Listeners don't need to be re-attached)
    if (evenMoreListeners) {
        var allFleChoosers = $("input[type='file']");
        addEventListenersTo(allFleChoosers);
        function addEventListenersTo(fileChooser) {
            fileChooser.change(function (event) { console.log("file( #" + event.target.id + " ) : " + event.target.value.split("\\").pop()) });
            fileChooser.click(function (event) { console.log("open( #" + event.target.id + " )") });
        }
    }
    (function () {
        var old = console.log;
        var logger = document.getElementById('log');
        console.log = function () {
            for (var i = 0; i < arguments.length; i++) {
                if (typeof arguments[i] == 'object') {
                    logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
                } else {
                    logger.innerHTML += arguments[i] + '<br />';
                }
            }
            old.apply(console, arguments);
        }
    })();

    var clone = {};

    // FileClicked()
    function fileClicked(event) {
        var fileElement = event.target;
        if (fileElement.value != "") {
            // if (debug) { console.log("Clone( #" + fileElement.id + " ) : " + fileElement.value.split("\\").pop()) }
            clone[fileElement.id] = $(fileElement).clone(); //'Saving Clone'
        }
        //What ever else you want to do when File Chooser Clicked
    }

    // FileChanged()
    function fileChanged(event) {
        var fileElement = event.target;
        if (fileElement.value == "") {
            // if (debug) { console.log("Restore( #" + fileElement.id + " ) : " + clone[fileElement.id].val().split("\\").pop()) }
            clone[fileElement.id].insertBefore(fileElement); //'Restoring Clone'
            $(fileElement).remove(); //'Removing Original'
            if (evenMoreListeners) { addEventListenersTo(clone[fileElement.id]) }//If Needed Re-attach additional Event Listeners
        }
        //What ever else you want to do when File Chooser Changed
    }



        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })
    </script>

@endpush