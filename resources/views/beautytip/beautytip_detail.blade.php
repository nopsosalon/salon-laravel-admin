
{{-- @php dd($beautytip); @endphp --}}
@extends('service-mgmt.base')
@section('action-content')

@push("css")
    <style type="text/css">

        .content-header{
          margin-top: 30px !important;
        }

        input[type="file"]
            {
               
            }
        mark{
          background-color: lightgrey;
          color:black;
        }

        pre{
          padding-left: 20px;
          background-color: black;
          color: grey;
        }

        .pageNav a, .pageNav a:active{
          font-family: "", sans-serif, monospace;
          font-size: 18px;
          text-decoration: none;
          color: black;
        }
        .urdu{
            font-family: 'Noto Nastaliq Urdu Draft', serif !important;
            font-family: 'Noto Naskh Arabic', serif;
            font-size: 1.5em;
            direction: rtl;
        }

    </style>
@endpush

<section class="content" style="margin-top: 50px;">
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    
                </div>
            </div>
        </div>
     
        <div class="box-body">
            <article>
                <div class="container">
                    <div class="row">
                        <div class ="col-sm-12">
                            <p class="pull-right">
                               <span style="font-size: 16px;"> Last Modified Date: <strong>
                                {{date('d-m-Y',strtotime($beautytip->bt_modified_date))}} &nbsp;
                                {{ date("g:i a", strtotime($beautytip->bt_modified_date))}}  </strong>
                                <br>
                                Status: <strong>@if($beautytip->bt_status === 1)Active @else Inactive @endif </strong>
                                </span>
                                <div class="clearfix"></div>
                                <h3 class="pull-right" style="margin-right: 100px;">
                                    <a href="{{$return_back}}">
                                        Back
                                    </a>
                                </h3>
                            </p>
                          <br/>
                          <br/>
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="{{$beautytip->bt_imageUrl}}" class="img-rounded float-left" alt="..." width="100%" height="300px" style="margin-top: -60px;">
                            </div>
                            <div class="col-sm-9">
                                <h1><b>{{$beautytip->bt_title}}</b></h1>
                                  <br/>
                                  <p>{{$beautytip->bt_short_description_english}}</p><br>
                                  <p>{!! $beautytip->bt_description !!}</p>
                            </div>
                        </div>
                        <hr><br><br>
                        <div class="row">
                            <div class="col-sm-3 pull-right">
                                <img src="{{$beautytip->bt_imageUrl}}" class="img-rounded float-right" alt="..." width="100%" height="300px" style="margin-top: -50px;">
                            </div>
                            <div class="col-sm-9 pull-right">
                                <h1 class="pull-right urdu"><b>@if($beautytip->bt_title_urdu !== null and $beautytip->bt_title_urdu !== ''){{$beautytip->bt_title_urdu}}@else <span style="color: red;"> Urdu Title Not Available</span> @endif</b></h1>
                                <div class="clearfix"></div>
                                  <br/>
                                  <p class="pull-right urdu">@if($beautytip->bt_short_description_urdu !== null and $beautytip->bt_short_description_urdu !== ''){!! $beautytip->bt_short_description_urdu !!} @else <span style="color: red;"> Urdu Short Description Not Available </span> @endif</p><br>
                                  <p class="pull-right urdu">@if($beautytip->bt_description_urdu !== null and $beautytip->bt_description_urdu !== ''){!! $beautytip->bt_description_urdu !!} @else <span style="color: red;"> Urdu Description Not Available </span> @endif</p>
                            </div>
                        </div>
                    </div>
                      
                      </div>
                  
                </div>
                </article>
  
        </div>
         </div>
</section>
@endsection

@push("script")

@endpush