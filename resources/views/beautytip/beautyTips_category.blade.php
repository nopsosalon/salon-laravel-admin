@extends('service-mgmt.base')
@section('action-content')


<section class="content">
  <div class="row">
        <div class="col-sm-12">
            <h3>Beauty Tips Categories </h3><br>
        </div>
    </div>

    <div style="margin-bottom: 30px;">
        <form role="form" method="get" action="{{ url('beautyTips_category') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Beauty Tip Category
                          </label>
                          <input placeholder="Service category"  type="text" class="form-control" @isset($btc_name)  value="{{$btc_name}}" @endisset name="btc_name" maxlength="128">
                        </div>
                      </div>


                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>
                            Is Active  
                        </label>
                        <select class="form-control" name="btc_status" id="btc_status">
                          <option value='all_active' @if($btc_status == "all_active") selected="checked" @endif >All</option>
                          <option value='1' @if($btc_status == 1) selected="checked" @endif>Yes</option>
                          <option value='2' @if($btc_status == 2) selected="checked" @endif>No</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>

    @php $urls = ''; @endphp
    @if($count !== 0)
        @if(!empty($btc_name) OR !empty($btc_status))
                    @php

                      if(is_null($btc_name))
                        {
                          $btc_name = "";
                        } 
                        if(is_null($btc_status))
                        {
                          $btc_status = "";
                        }
                        $urls = ('beautyTips_category?'.'btc_name'.'='.$btc_name.'&'.'btc_status'.'='.$btc_status.'&'.'page='.$beauty_tip_cat->currentPage());

                    @endphp
                  @else
                    @php
                       $urls = ('beautyTips_category?'.'page='.$beauty_tip_cat->currentPage());
                    @endphp
                @endif
       @endif         

    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Beauty Tips Category</h3>
                </div>
                <div class="col-sm-4">
                    <a class="btn btn-info" href="{{ url('admin/add_beautyTips_category'.'/'.$urls) }}">Add new Category</a>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody>
                  @if($count !== 0)
                    <tr>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Is Active</th>
                        <th>Action</th>
                    </tr>
                    
                     @foreach ($beauty_tip_cat as $btc) 
                        <tr>
                            <td><?php echo $btc->btc_name ?></td>

                            <?php // $backslash = "/"; ?>
                            <td><img src="{{$cate_images}}{{$btc->btc_image ? $btc->btc_image : 'placeholder.png'}}" width="100" height="100"></td>
                          
                            <td><?php echo $btc->btc_status == 1 ? 'Yes' : 'No'; ?></td>
                            <td> 
                              <a href="{{ url('admin/edit_beautytip_cat', ['id' => $btc->btc_id,'urls'=>$urls])}}" class="btn btn-info">
                                <i class="glyphicon glyphicon-edit"></i>
                              </a> 
                                <a style="margin-left: 10px;" href="{{ url('admin/beauti_tips', ['id' => $btc->btc_id,'urls'=>$urls])}}" class="btn btn-info">Manage Beauty Tips</a>
                            </td>
                        </tr>
                    @endforeach
                    @else
                      <tr>
                        <td colspan="4">
                           <h3 style="text-align: center;"> Record Not Found</h3>
                        </td>
                      </tr>
                    @endif
                </tbody>
            </table>
            @if(count($beauty_tip_cat) !== 0)
            {!! $beauty_tip_cat->appends(['btc_name'=>isset($btc_name)?$btc_name:'','btc_status'=>isset($btc_status)?$btc_status:'','page'=>$beauty_tip_cat->currentPage()])->links() !!}
            @endif
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection


@push('script')
  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

  <script>
      
    @if(Session::has('success'))
          toastr.success("{{ Session::get('success') }}");
    @endif
    @if(Session::has('info'))
          toastr.info("{{ Session::get('info') }}");
    @endif
    @if(Session::has('warning'))
          toastr.warning("{{ Session::get('warning') }}");
    @endif
    @if(Session::has('error'))
          toastr.error("{{ Session::get('error') }}");
    @endif


  </script>

@endpush
