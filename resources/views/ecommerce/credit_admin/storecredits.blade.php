@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $display_cust_img = $helper->display_cust_img();
  
@endphp

@extends('service-mgmt.base')
@section('action-content')
<style type="text/css">
    .checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
</style>

<section class="content" style="margin-top: -40px;">
  <div class="row">
        <div class="col-sm-12">
            <h2>Store Credit</h2><br>
        </div>
    </div>
    {{-- <div style="margin-bottom: 30px;">
        <form method="get" action="{{url('admin/credit_admin')}}">
            <div style="margin:0 auto; width:100%; height:50px;" class="div1" >
                <div style="margin:0 auto; height:50px;" class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                        <div class="form-group">
                            <label></label>
                            <select name="" id="" class="form-control">
                                <option value="">Select Order</option>
                                @foreach($orders as $key => $value)
                                    <option value="{{$value->o_id}}">{{$value->o_city}}</option>
                                @endforeach
                            </select>
                        
                        </div>
                      </div>
                                 
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>
 --}}

    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h2 class="">Store Credit Details </h2>
                </div>
                <div class="col-sm-4">
                    {{-- <a class="btn btn-info" href="">Add new Collection</a> --}}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                 @if (\Session::has('flash_message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 23px !important; margin: -tio; margin-top: -12px; margin-right: -14px;"><span aria-hidden="true">&times;</span></button> 
                        {{ \Session::get('flash_message') }}
                    </div> 
                @endif
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row" style="padding: 0px 100px;">
                <div class="col-md-6"> 
                    <h3>Name : {{$cust->cust_name}}</h3> 
                </div>
                <div class="col-md-6">
                    <h3>Email : {{$cust->cust_email}}</h3> 
                </div>
                <div class="col-md-6"> 
                    <h3>Phone : {{$cust->cust_phone}}</h3> 
                </div>
                <div class="col-md-6" >
                    <h3>City : {{$cust->cust_city}}</h3> 
                </div>
            </div>
            <div class="row" style="padding-top: 50px;">
                
            </div>
            <div class="row">
                <div class="col-md-12" style="padding: 30px;">
                    <h3>
                        Existing Store Credit : {{$cust->cust_store_credit}}
                    </3>
                </div>
            </div>
            <div class="row" style="padding:20px;">
                <form method="post" action="{{route('store_cust_credits')}}">
                    <div class="col-md-12" style="padding-bottom: 20px; margin-left: -20px; ">
                        <div class="col-md-6">
                            <label>Order Id</label>
                            <select required="" name="order_id" id="order_id" class="form-control">
                                <option value="">Select Order Id</option>
                                @foreach($orders as $key => $value)
                                    <option value="{{$value->o_id}}">{{$value->o_id}}, dated({{$value->o_modified_date}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{csrf_field()}}
                    <input type="hidden" name="cust_id" id="cust_id" value="{{$cust->cust_id}}">
                    <input type="hidden" name="o_id" id="o_id" value="">
                    <input type="hidden" name="cust_phone" id="cust_phone" value="{{$cust->cust_phone}}">
                    
                    <div class="col-md-6">
                        <label>New Credit</label>
                        <input required="" type="number"  name="csc_credits" id="csc_credits" class="form-control">
                    </div>
                    <div class="col-sm-6">
                        <div class="checkbox" style="margin-top: 22px;">
                            <label style="font-size: 2em">
                                <input type="checkbox" value="0" name="csc_send_sms" id="csc_send_sms"  onclick="$(this).val(this.checked ? 1 : 0)">
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                Send sms to user
                            </label>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label>Comments</label>
                        <textarea required="" name="csc_comments" id="csc_comments" class="form-control" rows="8"></textarea>
                    </div>

                    <div class="col-md-12" style="padding:20px;">
                        <input type="submit" class="btn btn-primary btn-lg" value="Save">
                    </div>
                </form>
            </div>

            <div class="row" style="margin-top: 50px; padding: 20px;">
                <table class="table table-hover">
                <tbody>
                    @if(count($cs)>0)
                    <tr>
                        <th>#</th>
                        <th>Credits</th>
                        <th>Order No</th>
                        <th>SMS </th>
                        <th>Comments</th>
                        <th>Datetime</th>
                        {{-- <th>Action</th> --}}
                    </tr>
                    @foreach($cs as $key => $value)
                    <tr>
                        <td>{{ $key += 1  }}</td>
                        <td>{{$value->csc_credits}}</td>
                        <td>{{$value->o_id}}</td>
                        <td>@if($value->csc_send_sms == 1) Yes @else No @endif</td>
                        <td>{{$value->csc_comments}}</td>
                        <td>{{$value->csc_datetime }}</td>
                        <td>
                         {{-- <a href="{{url('admin/storecredits'.'/'.$value->csc_id)}}" class="btn btn-info">Manage Credits</a>  --}}
                        </td>
                    </tr>
                    @endforeach
                    @endif
                
                </tbody>
            </table>
            </div>
        </div>
    </div>
</section>
@endsection
@push('script')
<script type="text/javascript">
    $("#order_id").on('change',function(){
        var order_id = $("#order_id").val();
        document.getElementById('o_id').value = order_id; 
    })
</script>

@endpush