@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $display_cust_img = $helper->display_cust_img();
  
@endphp

@extends('service-mgmt.base')
@section('action-content')

<section class="content" style="margin-top: -40px;">
  <div class="row">
        <div class="col-sm-12">
            <h3>Coupons</h3><br>
        </div>
    </div>
    {{-- <div style="margin-bottom: 30px;">
        <form method="get" action="{{url('admin/credit_admin')}}">
            <div style="margin:0 auto; width:100%; height:50px;" class="div1" >
                <div style="margin:0 auto; height:50px;" class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Find User
                          </label>
                          <input required="" minlength="4" placeholder="Find By Name or Phone Number"  type="text" class="form-control" value="@isset($find_user) {{$find_user}}  @endisset" name="find_user" maxlength="128">
                        </div>
                      </div>
                                 
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>
 --}}

    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Coupons Details </h3>
                </div>
                <div class="col-sm-4" style="text-align: right;">
                    <a class="btn btn-info" href="{{route('add_coupons')}}">Add new Coupons</a>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody>
                    @if(!empty($coupons))
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        {{-- <th>Code </th> --}}
                        <th>Percentage</th>
                        <th>Expiry Date</th>
                        <th>Maximum Value</th>
                        <th>Total</th>
                        <th>Used</th>
                        <th>Balance</th>
                        
                        {{-- <th>Action</th> --}}
                    </tr>
                    @foreach($coupons as $key => $value)
                    <tr>
                        <td>{{ ++$key}}</td>
                        <td>{{$value->pc_title}}</td>
                        
                        {{-- <td>{{$value->pc_code}}</td> --}}
                        <td>{{$value->pc_percent}}</td>
                        <td>{{$value->pc_expiry }}</td>
                        <td>{{$value->pc_max_value }}</td>
                        <td>{{$value->pc_total}}</td>
                        <td>{{$value->pc_used}}</td>
                        <td>{{$value->pc_balance}}</td>
                        
                        <td>
                         <a href="{{url('admin/send_coupon'.'/'.$value->pc_id)}}" class="btn btn-info">Manage Coupons</a> 
                        </td>
                    </tr>
                    @endforeach
                    @endif
                
                </tbody>
            </table>

            {{$coupons->links()}}

            
        </div>
    </div>
</section>
@endsection