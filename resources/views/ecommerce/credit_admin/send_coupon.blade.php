@php
    
    use App\Helpers\Helper;

    $helper = new Helper();
    $display_cust_img = $helper->display_cust_img();

  
@endphp

@extends('service-mgmt.base')
@section('action-content')
<style type="text/css">
    .checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
</style>

<section class="content" style="margin-top: -40px;">
  <div class="row">
        <div class="col-sm-12">
            <h2>Send Coupons</h2><br>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h2 class="">Coupons </h2>
                </div>
                <div class="col-sm-4">
                    {{-- <a class="btn btn-info" href="">Add new Collection</a> --}}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                 @if (\Session::has('flash_message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 23px !important; margin: -tio; margin-top: -12px; margin-right: -14px;"><span aria-hidden="true">&times;</span></button> 
                        {{ \Session::get('flash_message') }}
                    </div> 
                @endif
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            
            <div class="row" style="padding:20px;">
                <form method="post" action="{{url('admin/send_coupons'.'/'.$id)}}">
                    {{csrf_field()}}
                    <div class="col-md-6">
                        <label>Phone No</label>
                        <input type="tel" name="sal_phone" id="sal_phone" class="form-control" placeholder="Phone" maxlength="11" min="0" required="">
                    </div>
                    

                    <div class="col-md-12">
                        <label>Description</label>
                        <textarea class="form-control" name="send_description" id="send_description" rows="8" required="">{CouponNo}</textarea>
                    </div>
            
                    <div class="col-md-12" style="padding:20px;">
                        <input type="submit" id="save" class="btn btn-primary btn-lg" value="Save">
                    </div>
                </form>
            </div>

            <div class="row" style="margin-top: 50px; padding: 20px;">
                
            </div>
        </div>
    </div>
</section>
@endsection
@push('script')
<script type="text/javascript">
    $("#order_id").on('change',function(){
        var order_id = $("#order_id").val();
        document.getElementById('o_id').value = order_id; 
    })



    // $("#sal_phone").hide();
         $("#sal_phone").on('input', function() {  //this is use for every time input change.
                var inputValue = getInputValue(); //get value from input and make it usefull number
                var length = inputValue.length; //get lenth of input

                if(inputValue.trim().length !== 0 && inputValue.trim() != '0'){
                  var first = inputValue.toString()[0]
                  var second = inputValue.toString()[1]
                    if(first !== '0')
                    {
                      document.getElementById('sal_phone').value = ""; 
                      alert("Phone number must start with 03 ");
                       $("#sal_phone").focus();
                      
                      return false;
                    }          
                  else if(second !== '3')
                  {
                 
                      document.getElementById('sal_phone').value = "0";
                      alert("Phone number must start with 03 ");
                       $("#sal_phone").focus();
                  
                      return false;
                  }

                }
              
                $("#sal_phone").val(inputValue); //correct value entered to your input.
                inputValue = getInputValue();//get value again, becuase it changed, this one using for changing color of input border
               if ((inputValue > 2000000000) && (inputValue < 9999999999))
              {
                  $("#sal_phone").css("border","solid 1px rgba(197, 214, 222, .7)");//if it is valid phone number than border will be black.
              }else
              {
                  $("#sal_phone").css("border","red solid 1px");//if it is invalid phone number than border will be red.
              }
          });

         function getInputValue() {
         var inputValue = $("#sal_phone").val().replace(/\D/g,'');  //remove all non numeric character
                if (inputValue.charAt(0) == 1) // if first character is 1 than remove it.
                {
                    var inputValue = inputValue.substring(1, inputValue.length);

                }
                return inputValue;
        }

 // $("#save").mouseenter(function(){
 //    // alert("oka");
 //       $a = $("#send_description").val();
 //        if (strpos($a,'xxxxxxxx') !== false) {
 //            echo 'true';
 //        }else{
 //            echo "xxxxxxxx not exist in description please enter promo code in drescription field";
 //            return false;
 //        }

 // })

</script>

@endpush