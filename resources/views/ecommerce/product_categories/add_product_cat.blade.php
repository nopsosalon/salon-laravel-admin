@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $pc_image_path = $helper->pc_image_display();
   
@endphp


@extends('service-mgmt.base')
@section('action-content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<link rel="stylesheet" type="text/css" href="{{asset('css/multiple_selected.css')}}">
<style type="text/css">
    .filter-option{
        border:1px solid #ccc !important;
    }
    .btn-group{
        width: 100% !important;
    }
    .multiselect{
        width: 100% !important;
    }
    .multiselect-container{
    background-color: rgb(234, 234, 234) !important;
    height: 200px !important;
    overflow-y: scroll !important;
    scroll-behavior: smooth !important;
    width: 100% !important;
    }
    .multiselect-all{
        display: none;
    }
    .content-header{
        margin-top: 50px !important;
    }
</style>
<section class="content" style="">
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    {{-- <h3 class="box-title">Add new post</h3> --}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('toasts'))
                      @foreach(Session::get('toasts') as $toast)
                        <div class="alert alert-{{ $toast['level'] }}">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          {{ $toast['message'] }}
                        </div>
                      @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if(isset($pCategories))
                 <form action="{{route('pc_update',['id' => $pCategories->pc_id])}}" method="POST" role="form" enctype="multipart/form-data">
            @else
                <form action="{{route('store_pcate')}}" method="POST" role="form" enctype="multipart/form-data">
            @endif
                {{ csrf_field() }}

                <input type="hidden" name="bt_id" @isset($pCategories->pc_id) value="{{$pCategories->pc_id}}" @endisset>
                <div class="box-body">

                    <div class="col-sm-6">
                         <div class="form-group">
                            <label for="exampleInputPassword1">Category Name</label>
                            <input required="" type="text" placeholder="Product Category Name" class="form-control" name="pc_name" @isset($pCategories->pc_name) value="{{$pCategories->pc_name}}" @endisset>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Category Status</label>
                            <select name="pc_status" class="form-control" required="">
                                <option value="1" @isset($pCategories->pc_status) @if($pCategories->pc_status == 1) selected="checked" @endif @endisset >Active</option>
                                <option value="0" @isset($pCategories->pc_status) @if($pCategories->pc_status == 0) selected="checked" @endif @endisset >Inactive</option>
                            </select>
                        </div>
                    </div> 
                   <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Category Image</label>
                            <input @if(isset($pCategories->pc_image)) @else required="" @endif type="file" name="pc_image" class="form-control" id="imgInp" placeholder="Product Category Image">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <img id="blah" src="@if(isset($pCategories->pc_image)) {{$pc_image_path}}{{$pCategories->pc_image}} @else {{$pc_image_path}}/default.png @endif" width="100px" height="100px">
                        </div>
                    </div>
                     <div class="col-sm-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Category Description</label>
                            <textarea required="" placeholder="Product Category Description" rows="7" class="form-control" id="summary-ckeditor" name="pc_description">@isset($pCategories->pc_description) {{$pCategories->pc_description}} @endisset</textarea>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="returnback" value="{{$return_back}}">
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box-footer">
                    
                    @if(isset($pCategories->pc_id))
                        <input type="submit" name="save" class="btn btn-primary" value="Update">
                    @else
                        <input name="save" type="submit" class="btn btn-primary" value="Submit">
                    @endif
                    <input name="save" type="reset" class="btn btn-primary" value="Clear">
                    <a style="margin-left: 10px;" href="{{url($return_back)}}" class="btn btn-primary" >Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection

@push("script")
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="{{asset('js/multi_selected.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('ckeditor/samples/js/sample.js')}}"></script>
{{-- <script>
    CKEDITOR.replace( 'summary-ckeditor' );

        $("form").submit( function(e) {
            var messageLength = CKEDITOR.instances['summary-ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
            if( !messageLength ) {
                alert( 'Please enter BeautyTips Description' );
                    $("#summary-ckeditor").focus();
                    return false;
                e.preventDefault();
            }
        });
</script>
 --}}
<script>
    initSample();



    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
}
    $("#imgInp").change(function() {
      readURL(this);
    });
</script>




@endpush