@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $pc_image_path = $helper->pc_image_display();
   
@endphp

@extends('salon-magmt.base') 
@section('action-content')
@push("css")
    <style type="text/css">
        .incons{
            border: none; 
            color: white; 
            padding: 5px 10px;
            font-size: 20px;
            margin-left:10px;
            cursor: pointer;"

        }

        input[type="file"]{
                    /*color: transparent;*/
                }
    .collapsing {
          transition: none !important;
          -webkit-transition: none;
            transition: none;
            display: none;
        }
    </style>

@endpush
<section class="content">
<section class="content" >
  <div class="row">
    <div class="col-sm-12">
      <h3>Manage Products</h3>
    </div>
  </div>

<div id="success"></div>
    <div class="row">
      <div class="col-lg-12" style="margin-bottom: 30px;">
        <form method="get" action="{{route('p_search',['id'=>$pc_id])}}">
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label>Product Name</label>
                <input type="text" name="p_name" placeholder="Product Name" class="form-control" @isset($p_name) value="{{$p_name}}" @endisset>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label>Product Status</label>
                <select name="p_status" class="form-control">
                  <option value="all" @isset($p_status) @if($p_status == 'all') selected="selected" @endif @endisset>All</option>
                  <option value="1" @isset($p_status) @if($p_status == 1) selected="selected" @endif @endisset>Active</option>
                  <option value="2" @isset($p_status) @if($p_status == 2) selected="selected" @endif @endisset>Inactive</option>
                </select>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label></label>
                <input type="submit" class="btn btn-info" style="margin-top: 24px;" value="Search">
              </div>
            </div>
          </div>
        </form>
      </div>
        <div class="col-sm-12 col-lg-12">
            <div class="box">
                <div class="nav-tabs-custom">
                    
                        <div class="tab-pane" id="tab_1">
                            <div class="box-header">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h3 class="box-title"> {{$pc_name}}  </h3>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="{{route('add-product',['id'=>$pc_id])}}" class="btn btn-primary">Add Product</a>
                                    </div>
                                </div>
                            </div>
                            <table id="salon_tabel"  class="table table-hover">
                                @if(count($product) > 0)
                                <tr>
                                  <td>#</td>
                                  <th>Name</th>
                                  <th>Vendor Name</th>
                                  <th>Image</th>
                                  <th>Weight</th>
                                  <th>Product Cost</th>
                                  <th>Retail Price</th>
                                  <th>Product Price</th>
                                  <th>Product Dimention</th>
                                  <td>Status </td>
                                  <th>
                                    <span>Action</span>
                                  </th>
                                </tr>
                                
                                @foreach($product as $key => $value)
                                @php $key += 1; @endphp
                                <tr>
                                  <td>{{$key}}</td>
                                  <td>{{$value->p_name}}</td>
                                   <td>{{$value->p_vendor_name}}</td>
                                  <td>
                                    <img src="{{$pc_image_path}}{{$value->p_image}}" width="100px" height="100px">
                                    
                                  </td>
                                  <td>{{$value->p_weight}}</td>
                                  <td>{{$value->p_cost}}</td>
                                  <td>{{$value->p_rrp}}</td>
                                  <td>{{$value->p_price}}</td>
                                  <td>{{$value->p_dimention}}</td>
                                  <td>@if($value->p_status == 1) Active @else Inactive @endif</td>
                                  <td>
                                    <a href="{{route('edit_product',['id'=>$value->p_id,'pc_id'=>$pc_id])}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i></a>
                                    <a href="{{route('delete_product',['id'=>$value->p_id])}}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                                  </td>
                                </tr>
                                @endforeach
                                @else
                                <tr><td>Record Not found !</td></tr>
                                @endif
                            </table>
                        </div>
         
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    

       
       
    </section>
@endsection

@push('script')


<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


<script>
    
  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>


@endpush