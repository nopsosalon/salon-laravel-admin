@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $pc_image_path = $helper->display_cust_img();
   
@endphp

@extends('salon-magmt.base')
@section('action-content')

@push("css")
    <style type="text/css">
        .incons
        {
            border: none; 
            color: white; 
            padding: 5px 10px;
            font-size: 20px;
            margin-left:10px;
            cursor: pointer;"
        }
        body{
          font-size: 1.5rem !important;
      }
      .btn-group-vertical>.btn-group:after, .btn-group-vertical>.btn-group:before, .btn-toolbar:after, .btn-toolbar:before, .clearfix:after, .clearfix:before, .container-fluid:after, .container-fluid:before, .container:after, .container:before, .dl-horizontal dd:after, .dl-horizontal dd:before, .form-horizontal .form-group:after, .form-horizontal .form-group:before, .modal-footer:after, .modal-footer:before, .modal-header:after, .modal-header:before, .nav:after, .nav:before, .navbar-collapse:after, .navbar-collapse:before, .navbar-header:after, .navbar-header:before, .navbar:after, .navbar:before, .pager:after, .pager:before, .panel-body:after, .panel-body:before, .row:after, .row:before {
            display: table;
            content: normal !important;
        }
    </style>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

@endpush

<section class="content">
  <div class="container" style="margin-top: 40px;">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row p-5">
                        <div class="col-md-6">
                            @if($order->cust_pic!='')
                                <img src="{{$pc_image_path}}{{$order->cust_pic}}" width="200px" height="150px">
                            @else
                                <img src="http://via.placeholder.com/400x90?text=logo">
                            @endif
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-1">Invoice #{{$order->o_id}}  </p>
                            <p class="text-muted">Due to: {{-- {{date("F j, Y, g:i a",$order_detail->o_datetime)}} --}} {{date("jS F, Y", strtotime($order->o_created_at))}}</p>
                        </div>
                    </div>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-6">
                            <p class="font-weight-bold mb-4">Customer Information</p>
                            <p class="mb-1">{{$order->cust_name}}</p>
                            <p>{{$order->o_address}}</p>
                            <p class="mb-1">{{$order->o_city}}</p>
                            <p class="mb-1">{{$order->cust_phone}}</p>
                        </div>

                        {{-- <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-4">Payment Details</p>
                            <p class="mb-1"><span class="text-muted">VAT: </span> 1425782</p>
                            <p class="mb-1"><span class="text-muted">VAT ID: </span> 10253642</p>
                            <p class="mb-1"><span class="text-muted">Payment Type: </span> Root</p>
                            <p class="mb-1"><span class="text-muted">Name: </span> John Doe</p>
                        </div> --}}
                    </div>

                    <div class="row p-5">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>  
                                    @php 
                                        $total_amount = 0; 
                                        $net_amount = 0;
                                        $delivery_charges = 0;
                                        $discount = 0;
                                    @endphp
                                @if(count($order_details) >0 )
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">Serial No</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Item</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Unit Cost</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach($order_details as $key => $value)

                                    @php 
                                        $total_amount = $total_amount + $value->p_price; 
                                        $delivery_charges = $value->o_delivery_charges; 
                                    @endphp
                                    <tr>
                                        <td>{{$value->od_id}}</td>
                                        <td>{{$value->p_name}}</td>
                                        <td style="text-align: center;">{{$value->od_quantity}}</td>
                                        <td style="text-align: center;">{{$value->od_cost}}</td>
                                        <td style="text-align: center;">{{$value->p_price}}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="5">Record Not Found !</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                            @php 
                                $net_amount = $total_amount + $delivery_charges;
                            @endphp
                    <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Rs.{{$total_amount}}</div>
                            <div class="">Rs.{{$delivery_charges}}</div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2"><p style="color: red;"></p></div>
                            <div class=""></div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Sub - Total amount</div>
                            <div class="">Delivery Charges</div>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white p-4" style="margin-top: -30px;">
                        <div class="py-3 px-5 text-right">
                            <div class="mb-2"><p style="color: red;">(Rs.{{$discount}})</p></div>
                            <div class="">Rs.{{$net_amount}}</div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2"><p style="color: red;"></p></div>
                            <div class=""></div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Discount</div>
                            <div class="">Net Total</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>


</section>
@endsection
@push("script")

@endpush