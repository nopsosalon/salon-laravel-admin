@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $pc_image_path = $helper->pc_image_display();
   
@endphp


@extends('service-mgmt.base')
@section('action-content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<link rel="stylesheet" type="text/css" href="{{asset('css/multiple_selected.css')}}">
<style type="text/css">
    .filter-option{
        border:1px solid #ccc !important;
    }
    .btn-group{
        width: 100% !important;
    }
    .multiselect{
        width: 100% !important;
    }
    .multiselect-container{
    background-color: rgb(234, 234, 234) !important;
    height: 200px !important;
    overflow-y: scroll !important;
    scroll-behavior: smooth !important;
    width: 100% !important;
    }
    .multiselect-all{
        display: none;
    }
    .content-header{
        margin-top: 50px !important;
    }
</style>
<section class="content" style="">
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    {{-- <h3 class="box-title">Add new post</h3> --}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('toasts'))
                      @foreach(Session::get('toasts') as $toast)
                        <div class="alert alert-{{ $toast['level'] }}">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          {{ $toast['message'] }}
                        </div>
                      @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if(isset($pb))
                 <form action="{{route('update_pbrand',['id' => $pb->pb_id])}}" method="POST" role="form" enctype="multipart/form-data">
            @else
                <form action="{{route('store_pbrand')}}" method="POST" role="form" enctype="multipart/form-data">
            @endif
                {{ csrf_field() }}
                <input type="hidden" name="return_back" value="{{$return_back}}">
                <div class="box-body">
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label for="exampleInputPassword1">Product brand Name</label>
                            <input required="" type="text" placeholder="Product brand Name" class="form-control" name="pb_name" @isset($pb->pb_name) value="{{$pb->pb_name}}" @endisset>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Product Brand Status</label>
                            <select name="pb_status" class="form-control" required="">
                                <option value="1" @isset($pb->pb_status) @if($pb->pb_status == 1) selected="checked" @endif @endisset >Active</option>
                                <option value="0" @isset($pv->pb_status) @if($pb->pb_status == 0) selected="checked" @endif @endisset >Inactive</option>
                            </select>
                        </div>
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Product Vendor</label>
                            <select name="pv_id" class="form-control" required="">
                                @isset($pv)
                                @foreach($pv as $key => $value)
                                    <option value="{{$value->pv_id}}" @isset($pv_id) @if($value->pv_id == $pv_id) selected="checked" @endif @endisset >{{$value->pv_name}}</option>
                                @endforeach
                                @endisset
                            </select>
                        </div>
                    </div> 
                   <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Product Brand Manufacturer</label>
                            <input required="" type="text" name="pb_manufacturer" class="form-control"  placeholder="Product Brand Manufacturer" @isset($pb->pb_manufacturer) value="{{$pb->pb_manufacturer}}" @endisset>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Product Brand Image</label>
                            <input @if(!empty($pb)) @else required="" @endif type="file" name="pb_image" class="form-control"  placeholder="Product Brand Image">
                        </div>
                    </div>
                    
                </div>
                <input type="hidden" name="returnback" value="{{$return_back}}">
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box-footer">
                    
                    @if(isset($pv->pv_id))
                        <input type="submit" name="save" class="btn btn-primary" value="Update">
                    @else
                        <input name="save" type="submit" class="btn btn-primary" value="Submit">
                    @endif
                    <input name="save" type="reset" class="btn btn-primary" value="Clear">
                    <a style="margin-left: 10px;" href="{{url($return_back)}}" class="btn btn-primary" >Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection

@push("script")
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="{{asset('js/multi_selected.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('ckeditor/samples/js/sample.js')}}"></script>
{{-- <script>
    CKEDITOR.replace( 'summary-ckeditor' );

        $("form").submit( function(e) {
            var messageLength = CKEDITOR.instances['summary-ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
            if( !messageLength ) {
                alert( 'Please enter BeautyTips Description' );
                    $("#summary-ckeditor").focus();
                    return false;
                e.preventDefault();
            }
        });
</script>
 --}}
<script>
    initSample();



    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
}
    $("#imgInp").change(function() {
      readURL(this);
    });
</script>




@endpush