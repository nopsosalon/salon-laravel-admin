@extends('service-mgmt.base')
@section('action-content')

<style type="text/css">
  .small-img {
      height: 80px;
      width: 80px;
      border-radius: 50%;
    }
</style>

<section class="content">
  <div class="row">
        <div class="col-sm-12">
            <h3>Customers Detail</h3><br>
        </div>
    </div>

    <div style="margin-bottom: 30px;">
        <form role="form" method="get" action="{{ url('admin/customers') }}">
            {{-- {{ csrf_field() }} --}}
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Custer Name
                          </label>
                          <input placeholder="Customer Name"  type="text" class="form-control" value="@isset($cust_name) {{$cust_name}}  @endisset" name="cust_name" maxlength="128">
                        </div>
                      </div>
                    
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>

        @if(!empty($cust_name))
                    @php

                      if(is_null($cust_name))
                        {
                          $cust_name = "";
                        } 
                        
                        $urls = ('admin/customers?'.'cust_name'.'='.$cust_name.'&'.'page='.$customers->currentPage());

                    @endphp
                  @else
                    @php
                       $urls = ('admin/customers?'.'page='.$customers->currentPage());
                    @endphp
                @endif


    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Customers</h3>
                </div>
                <div class="col-sm-4">
                    {{-- <a class="btn btn-info" href="{{ url('addcategory') }}">Add new Category</a> --}}
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody><tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        {{-- <th>Image</th> --}}
                        <th>Phone</th>
                        <th>Credits</th>
                        <th>Creation Date</th>
                        <th>Status</th>
                    </tr>
                    <?php foreach ($customers as $key => $cust) {  $key += 1;  ?>
                        <tr>
                            <td>{{$key}}</td>
                            <td>
                              {{-- <img class="small-img" src="{{$display_cust_img}}{{$cust->cust_pic ? $cust->cust_pic : 'placeholder.png'}}"> --}}
                              <?php echo $cust->cust_name ?>
                            </td>

                            <td>{{$cust->cust_email}}</td>
                           
                            <td>
                              {{$cust->cust_phone}}
                            </td>
                            <td>{{$cust->cust_credits}}</td>
                            <td>
                              {{date('d-m-Y',strtotime($cust->cust_datetime))}}<br>
                              {{date('g:i a',strtotime($cust->cust_datetime))}}
                             
                            </td>
                            <td>
                                    @if( $cust->cust_status ==0)
                                      <a href="{{ url('admin/enable_cust', ['id' => $cust->cust_id]) }}"
                                         class="btn btn-danger incons" title="Inactive">   
                                          <i class="glyphicon glyphicon-remove"></i> 
                                      </a> 
                                  @else
                                      <a href="{{ url('admin/disable_cust', ['id' => $cust->cust_id]) }}"
                                         class="btn incons" style="background: #52C1F0;" title="Active">   
                                          <i class="glyphicon glyphicon-ok"></i> 
                                      </a> 
                                  @endif
                            </td>
                           
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

            {!! $customers->appends(['cust_name'=>isset($cust_name)?$cust_name:'','page'=>$customers->currentPage()])->links() !!}
            
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection