@php 
    $db_conn_name = '';
    if(request()->getSchemeAndHttpHost() == "https://www.beautyapp.qwpcorp.com"){
        $db_conn_name = '4';
    }elseif(request()->getSchemeAndHttpHost() == "https://www.beautyapp.pk"){
        $db_conn_name = '3';
    }

@endphp


@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                       {{--  <div class="form-group">
                            <label class="col-md-4 control-label" for="change_db_con">Select Database Connection</label>
                            <select class="form-control" name="change_db_con" id="change_db_con">
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                        </div> --}}


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            @if($db_conn_name !== '' && !empty($db_conn_name))
                                <input type="hidden" name="change_db_con" value="{{$db_conn_name}}">
                            @else
                            <label for="email" class="col-md-4 control-label">Select Country</label>
                            <div class="col-md-6">
                                <select class="form-control" name="change_db_con" id="change_db_con">
                                    <option value="1">UK</option>
                                    <option value="2">PK Dev</option>
                                    <option value="3">PK Live</option>
                                </select>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
