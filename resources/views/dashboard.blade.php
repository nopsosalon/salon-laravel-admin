<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Salon App Super Admin</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" type="images/ico" href="{{asset('images')}}/favicon.png">
        <link rel="shortcut icon" type="images/ico" href="{{asset('images')}}/favicon.ico">
        <!-- Bootstrap 3.3.6 -->
        <link href="{{ asset("/bower_components/AdminLTE/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link href="{{ asset("/bower_components/AdminLTE/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    
        <link href="{{ asset("/bower_components/AdminLTE/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="{{ asset ('/bower_components/AdminLTE/plugins/datepicker/datepicker3.css')}}">
        <style type="text/css">
            .datepicker,
                .timepicker,
                .datetimepicker {
                    .form-control {
                        background: #fff;
                    }
                }
        </style>


    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <!-- Main Header -->
            @include('layouts.header')
            <!-- Sidebar -->
            @include('layouts.sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        
                        <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3><?php echo $total_salons; ?></h3>

                                    <p>Salons</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="{{url('managesalons')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>{{$totech}}</h3>

                                    <p>
                                        @if(Session::get('change_db_connection')=="2" || Session::get('change_db_connection')=="4") Staff @elseif(Session::get('change_db_connection')=="3") Staff @elseif(Session::get('change_db_connection')=="1") Technicians @endif

                                        
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{url('admin/technicians')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>{{$t_bt}}</h3>

                                    <p>Active Beauty Tips. <b>{{count($active_bt)}}</b>  in Last 24 Hours</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{url('admin/beauty_tips')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <!-- ./col -->
                        <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>{{$customers}}</h3>

                                    <p>Customers</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="{{url('admin/customers')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->

                        <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    @php $totalpins = 0; @endphp
                                    @foreach($pins as $key => $pin)
                                        @php $totalpins = $totalpins + $pin->count @endphp
                                    @endforeach
                                    <h3>{{$totalpins}}</h3>
                                    <p>Posts in Last 24 Hours</p>
                                </div>

                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                    <div class="card">

                                      <div class="card-body" style="padding: 5px;">
                                        <table class="table table-bordered">
                                          <tbody>
                                        @if(count($pins)>0)
                                            <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Name</th>
                                            <th>Number of post</th>
                                          </tr>
                                          @foreach($pins as $key => $pin)
                                          @php $key += 1; @endphp
                                          <tr>
                                            <td>{{$key}}</td>
                                            <td>{{$pin->ssc_name}}</td>
                                            <td style="text-align: center; margin-right: 30px;">
                                              {{$pin->count}}
                                            </td>
                                           
                                          </tr>
                                          @endforeach
                                          @endif
                                        </tbody>
                                    </table>
                                      </div>
                                     
                                    </div>

                                <a href="{{url('admin/advancesearch')}}" class="small-box-footer" id="moreinfo">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>{{$customers}}</h3>

                                    <p>Sample</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>{{count($total_posts)}}</h3>
                                    <p>Service Sub-Category Style</p>
                                </div>

                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                    <div class="card" style="padding: 5px;">
                                      
                                      <!-- /.card-header -->
                                      <div class="card-body"> 
                                        <table class="table table-bordered">
                                          <tbody>
                                          @php $ssc_ids = 0; @endphp
                                        @if(count($posts)>0)
                                            <tr>
                                            <th>Sub-category</th>
                                            <th>Approved</th>
                                            <th>Un-APproved</th>
                                          </tr>
                                              @if(count($myfinalCategories)!=0)
                                                  @foreach($myfinalCategories as $key => $req)
                                                  @if($ssc_ids != $req->ssc_id)
                                                  <tr>
                                                    <td style="width: 100px;">{{$req->ssc_name}}</td>
                                                    <td style="width: 20px;">
                                                    @if(!empty($req->ApprovedPost))
                                                        {{$req->ApprovedPost}}
                                                    @else
                                                        0
                                                    @endif
                                                    </td>
                                                    <td style="width: 20px; text-align: center; margin-right: 0px;">
                                                    @if(!empty($req->unApprovedPost))
                                                        {{$req->unApprovedPost}}
                                                    @else
                                                        0
                                                    @endif
                                                    </td>
                                                  </tr>
                                                  @php $ssc_ids = $req->ssc_id; @endphp
                                                  @endif
                                                  @endforeach
                                              @else
                                              <tr>
                                                <td colspan="3">
                                                    <h2>Record Not found</h2>
                                                </td>
                                            </tr>
                                              @endif
                                          @endif
                                        </tbody>
                                    </table>
                                      </div>
                                     
                                    </div>

                                <a href="{{url('admin/service-subcategory-posts')}}" class="small-box-footer" id="moreinfo">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        
                        
                        <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>{{$totaol_apr}}</h3>
                                    <p>Appointments</p>
                                </div>

                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                    <div class="card" style="padding: 5px;">
                                      
                                      <!-- /.card-header -->
                                      <div class="card-body"> 
                                        <table class="table table-bordered">
                                          <tbody>
                                        @if(count($app_requests)>0)
                                            <tr>
                                            <th>Salon</th>
                                            <th>Customer</th>
                                            <th>DateTime</th>
                                          </tr>
                                              @if(count($app_requests)!=0)
                                                  @foreach($app_requests as $key => $req)
                                                  <tr>
                                                    <td style="width: 100px;">{{$req->sal_name}}</td>
                                                    <td style="width: 20px;">{{$req->cust_name}}</td>
                                                    <td style="width: 20px; text-align: center; margin-right: 0px;">
                                                      {{date('d-m-Y',strtotime($req->app_created))}}<br>
                                                        {{ date("g:i a", strtotime($req->app_created))}}
                                                    </td>
                                                  </tr>
                                                  @endforeach
                                              @else
                                              <tr>
                                                <td colspan="3">
                                                    <h2>Record Not found</h2>
                                                </td>
                                            </tr>
                                              @endif
                                          @endif
                                        </tbody>
                                    </table>
                                      </div>
                                     
                                    </div>

                                <a href="{{url('admin/appointments')}}" class="small-box-footer" id="moreinfo">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        
                        <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>UserApp Activities</h3>
                                    <br><br>
                                    <p> <b></b>  </p>
                                    <!-- /.card-header -->
                                      <div class="card-body"> 
                                        <table class="table table-bordered">
                                          <tbody>
                                            @if(isset($public_user))
                                                <tr>
                                                <th style="width: 100px;">Date</th>
                                                <th>Public User</th>
                                                <th>Customer</th>
                                                
                                              </tr>
                                                  @if(isset($public_user))
                                                      @foreach($public_user as $key => $value)
                                                      <tr>
                                                        <td style="width: 20px; text-align: center; margin-right: 0px;">
                                                          {{$value['datetime']}}<br>
                                                        </td>
                                                        <td style="width: 100px;">{{$value['pu_count']}}</td>
                                                        <td style="width: 20px;">{{$value['cu_count']}}</td>
                                                        
                                                      </tr>
                                                      @endforeach
                                                  @else
                                                  <tr>
                                                    <td colspan="3">
                                                        <h2>Record Not found</h2>
                                                    </td>
                                                </tr>
                                                  @endif
                                              @endif
                                            </tbody>
                                        </table>
                                      </div>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="{{url('admin/userapp-activity')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    
                </section>
            </div>
        </div>

               
            <!-- /.content-wrapper -->

            <!-- Footer -->
            @include('layouts.footer')

            <!-- ./wrapper -->

            <!-- REQUIRED JS SCRIPTS -->

            <!-- jQuery 2.1.3 -->
            <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>

            <!-- Bootstrap 3.3.2 JS -->
            <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>

            <!-- AdminLTE App -->
            <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>

            <script type="text/javascript" src="{{ asset ('/bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js')}}"></script>

    </body>
</html>
