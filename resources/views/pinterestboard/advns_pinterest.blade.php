@extends('salon-magmt.base')
@section('action-content')

@push("css")
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css">
    

    {{-- scripts for data tables  --}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<style type="text/css">
  .unApproved{
        background-color: transparent;
        text-decoration: none;
        border: none;
        color: #337ab7;
    }
</style>

@endpush

<section class="content" >
    
    
    <div id="success"></div>
    @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif

    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
          @if(Session::has('flash_message'))
              {{ Session::get("flash_message") }}
          @endif

          <div class="col-lg-12">
                            <div id="advance" style="display:;">
                                <form method="get" action="{{url('admin/post')}}">
                                    <div class="row">
                                      

                                        <div class="col-xs-3">
                                            <label for="startDate">Start Date</label>
                                            <input id="startDate" name="startDate" type="text" value="{{$startDate}}" class="form-control" />
                                        </div>
                                        <div class="col-xs-3">
                                            <label for="endDate">End Date</label>
                                            <input id="endDate" name="endDate" type="text" value="{{$endDate}}" class="form-control" />
                                        </div>
                                        <div class="col-xs-3">
                                            <label>Service Sub-Category</label>
                                            <div class="">
                                              @php 
                                                  $ssc = DB::table("service_sub_categories")
                                                            ->where('ssc_status',1)
                                                            ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                                            ->where('service_categories.sc_status',1)
                                                             ->orderBy("ssc_name","ASC")->get(); 
                                                @endphp
                                              <select class="form-control" name="ssc" id="ssc_id">
                                                  <option value="">All</option>
                                                  @isset($ssc)
                                                  <optgroup label="Female">
                                                    @foreach($ssc as $key => $sec)
                                                      @if($sec->ssc_gender === 2)
                                                      <option value="{{$sec->ssc_id}}">
                                                        {{$sec->ssc_name}} (F)
                                                      </option>
                                                      @endif
                                                    @endforeach
                                                  </optgroup>
                                                  <optgroup label="Male">
                                                    @foreach($ssc as $key => $sec)
                                                      @if($sec->ssc_gender === 1)
                                                      <option value="{{$sec->ssc_id}}">
                                                        {{$sec->ssc_name}} (M)
                                                      </option>
                                                      @endif
                                                    @endforeach
                                                  </optgroup>
                                                  <optgroup label="Both">
                                                    @foreach($ssc as $key => $sec)
                                                      @if($sec->ssc_gender === 3)
                                                      <option value="{{$sec->ssc_id}}">
                                                        {{$sec->ssc_name}} (B)
                                                      </option>
                                                      @endif
                                                    @endforeach
                                                  </optgroup>
                                                  
                                                  @endisset
                                              </select>
      
                                            </div>
                                        </div>  
                                        <div class="col-xs-3">
                                          <label>Post Status</label>
                                          <div class="">
                                            <select class="form-control" name="p_status" id="p_status">
                                              <option value="">All</option>
                                              <option value="1">Approved</option>
                                              <option value="0">Un-approved</option>
                                            </select>
                                          </div>
                                        </div>   
                                        @push("script")
                                              <script type="text/javascript">
                                                $("#ssc_id").val("{{$sscs}}");
                                                $("#p_status").val("{{$p_status}}");
                                              </script>
                                              @endpush
                                        <div class="col-xs-3">
                                            <div class=""> <br>
                                                <input type="submit" class="btn btn-primary btn-lg" value="Search">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>

           {{-- data table start code here  --}}
              <div class="row">
                <div class="col-md-12" style="margin-top: 50px;">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Total Number of Post</th>
                              <th>Last Fetch Date</th>
                              <th>Status</th>
                          </tr>
                      </thead>
                      <tbody>
                        @php $pbids = 0; $i = 0; @endphp
                      @if(count($pins) > 0 )
                      @foreach($pins as $key => $pin) 

                    @if($pin->p_status != "2")
                                                     
                      <tr> 
                          <td>
                            @php $i += 1;  @endphp
                            {{$i}}
                          </td>
                          <td>
                            {{$pin->ssc_name}}
                          </td>
                          
                          <td style="text-align: center;">
                            {{$pin->count}}
                              
                          </td>
                          <td>
                            {{$pin->p_fetch_date}}
                          </td>
                          <td>
                            @if($pin->p_status == '1')
                                    <form method="get" action="{{url('admin/postmanagement')}}">
                                        <input type="hidden" name="ssc_id" value="{{$pin->ssc_id}}">
                                        <input type="hidden" name="p_status" value="{{$pin->p_status}}">
                                        
                                        <button type="submit" class="unApproved">
                                            {{-- {{$post->unApprovedPost}} --}} Approved
                                        </button>
                                    </form>
                              
                            @elseif($pin->p_status == '0')
                                   <form method="get" action="{{url('admin/postmanagement')}}">
                                        <input type="hidden" name="ssc_id" value="{{$pin->ssc_id}}">
                                        <input type="hidden" name="p_status" value="{{$pin->p_status}}">
                                        
                                        <button type="submit" class="unApproved">
                                            {{-- {{$post->unApprovedPost}} --}} Un-approved
                                        </button>
                                    </form>
                              
                            @endif
                          </td>
                      </tr>
                    @endif
                      @endforeach
                      @endif
                      </tbody>
                      <tfoot>
                          <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Total Number of Post</th>
                              <th>Last Fetch Date</th>
                              <th>Status</th>
                          
                          </tr>
                      </tfoot>
                  </table>
                </div>
              </div>
              {{-- end data table code here  --}}

     {{--  <div class="box" style=" margin-top: 40px; margin-left: 0px; padding-left: 5px;">
        <form role="form" method="POST" a enctype="multipart/form-data">
          
            <div class="form-group">
  
                <table id="salon_tabel" class="table table-hover">
                    <tbody>
                      <tr>
                        <th>
                          # 
                        </th>
                        <th>
                          Name   
                        </th>
                        <th style="text-align: center;">
                          Total Number of Post
                        </th>
                        <th>
                          Last Fetch Date
                        </th>
                        <th>Status</th>
                      </tr>   
                      @php $pbids = 0; $i = 0; @endphp
                      @if(count($pins) > 0 )
                      @foreach($pins as $key => $pin) 
                                                     
                      <tr> 
                          <td>
                            @php $i += 1;  @endphp
                            {{$i}}
                          </td>
                          <td>
                            {{$pin->ssc_name}}
                          </td>
                          
                          <td style="text-align: center;">
                            {{$pin->count}}
                              
                          </td>
                          <td>
                            {{$pin->p_fetch_date}}
                          </td>
                          <td>
                            @if($pin->p_status == '1')
                              Approved
                            @else
                              Un-approved
                            @endif
                          </td>
                      </tr>
                      @endforeach
                      @else
                        <tr>
                          <td colspan="5">
                            <h4 style="text-align: center;">
                              No posts found for selected carateria!
                            </h4>
                          </td>
                        </tr>
                      @endif

                    </tbody>
                </table>
            </div>
        </form>
      </div> --}}

@endsection
@push("script")

  
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>

<script  src="{{asset('/')}}js/index.js"></script>
  

      <script type="text/javascript">
          jQuery("#is_active").on("change",function(){
          // var id = jQuery(this).val();
          alert(id);
        });
      </script>
    @isset($is_reviewed)
      <script type="text/javascript">
        $("#sal_is_reviewed").val("{{$is_reviewed}}");
      </script>
    @endisset

    @isset($is_active)
      <script type="text/javascript">
        $("#is_active").val("{{$is_active}}");
      </script>
    @endisset

     <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

  <script type="text/javascript">  
      $(".ssc_id").on("change",function(){
          var ssc_id = $(this).val();
          var array = ssc_id.split(",");
          var s_id = array[0];
          var pb_id = array[1];
          $.post('{{ url('admin/changessc') }}' , {s_id: s_id,pb_id:pb_id, _token: '{{ csrf_token() }}' } , function(data){
                  $("#success").html(data);
                  $('#me').fadeIn('fast').delay(1000).fadeOut('slow');
          }); 
        });

    $(document).ready(function(){
            $("#moreinfo").click(function(){
                $("#advance").toggle();
            });
        });

     

     $(document).ready(function() {
        $('#example').DataTable();
      } );
      
</script>
@endpush