@extends('salon-magmt.base')
@section('action-content')

@push("css")
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@php $ar_id = null; @endphp

@endpush
<section class="content">
    
    <div id="success"></div>
    @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif

    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
          @if(Session::has('flash_message'))
              {{ Session::get("flash_message") }}
          @endif
          <br><br>
          <div class="container" style="width: 100% !important;">
            @isset($ar_details)
            @foreach($ar_details as $ar_detail)
              {{-- @php $app_id = $ar_detail->app_id; @endphp --}}
            <div class="row">
              <div class="col-md-6 pull-left">
                <p>Salon Name: {{$ar_detail->sal_name}} </p>
                <p>Phone: {{$ar_detail->sal_phone}}</p>
                <p>Email: {{$ar_detail->sal_email}}</p>
              </div>
              <div class="col-md-3"></div>
              <div class="col-md-3 pull-right">
                <p>
                  Req. Date: 
                    {{date('d-m-Y',strtotime($ar_detail->app_created))}} &nbsp;
                    {{ date("g:i a", strtotime($ar_detail->app_created))}} </a>
                </p>
                <p>Customer: {{$ar_detail->cust_name}}</p>
              </div>   
            </div>
            @endforeach
            @endisset   
            <div class="clearfix"></div>
            <br>
            <div class="row">
              
                <div class="col-md-12">
                  <p>Conversation:</p>
                </div>
                @if(count($conversations) !== 0)
                @foreach($conversations as $conv)
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #148CBF; margin-bottom: 20px;">
                    <div style="padding-top: 11px;">
                      <p>
                        {{date('d-m-Y',strtotime($conv->ara_datetime))}} &nbsp;
                        {{ date("g:i a", strtotime($conv->ara_datetime))}}
                      </p> 
                        
                            <p>Talk with: <strong>{{$conv->ara_talkwith}}</strong>. {{$conv->ara_comments}}.</p>
                          
                          <div class="row">
                          <div class="col-md-6">
                              <p>Status: {{$conv->ara_status}} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                              </p>
                          </div>

                          <div class="col-md-6">

                                @if(!empty($conv->ara_follow_date))
                                  <p> Follow up date:&nbsp; &nbsp;
                                    {{date('d-m-Y',strtotime($conv->ara_follow_date))}} &nbsp;
                                    {{ date("g:i a", strtotime($conv->ara_follow_date))}}
                                  </p>
                                @endif
                            
                          </div>
                        </div>
                      
                    </div>
                    </div>
                  @endforeach
                  @else

                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #148CBF; margin-bottom: 20px;">
                    <div style="padding-top:">
                         <h5>Conversation not available now! </h5>
                      
                    </div>
                  </div>
                  
                  @endif
                
              
                  
              {{-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #148CBF; margin-bottom: 20px;">
                <div style="padding-top: 11px;">
                  <p>2018-06-20 10:00 am</p>
                  <p>Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow.</p>
                  <p>Status: Closed</p>
                </div>
              </div>
                  
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #148CBF; margin-bottom: 20px;">
                <div style="padding-top: 11px;">
                  <p>2018-06-20 10:00 am</p>
                  <p>Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow.</p>
                  <p>Status: Closed</p>
                </div>
              </div>
                  
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 2px solid #148CBF; margin-bottom: 20px;">
                <div style="padding-top: 11px;">
                  <p>2018-06-20 10:00 am</p>
                  <p>Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow. Talk with David. Sent demo video on whatsapp. He is interested in app. Scheduled call tomorrow.</p>
                  <p>Status: Closed</p>
                </div>
              </div> --}}

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 1px solid #148CBF; margin-bottom: 30px;">
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px;">
                <div class="row">
                  <form method="post" action="{{url('store_conversation'.'/'.$app_id)}}">
                    {{csrf_field()}}
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label for="">Talked with:</label>
                        <input type="text" class="form-control" name="ara_talkwith" style="border: 1px solid #148CBF;" required="">
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label for="">Status:</label>
                        <input type="text" class="form-control" name="ara_status" style="border: 1px solid #148CBF;" required="">
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label for="">Follow up date:</label>
                        <input type="text" id="start" class="form-control" name="ara_follow_date" style="border: 1px solid #148CBF;" >
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label for="">Comment:</label>
                        <textarea class="form-control" name="ara_comments" rows="7" style="border: 1px solid #148CBF;" required=""></textarea>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                      <input type="submit" class="btn btn-primary btn-lg" value="Save" style="padding: 10px 35px;">
                    </div>
                  </form>
                </div>
              </div>

            </div>
          </div>


      
@endsection
@push("script")
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>

  

 
  {{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}

    <script type="text/javascript">
     $( "#start" ).datetimepicker(
     
      );
   
    </script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


<script>


  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>

@endpush