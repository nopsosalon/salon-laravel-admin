
@extends('service-mgmt.base')
@section('action-content')
<style type="text/css">
    .content-header{
        margin-top: 50px !important;
    }
</style>
<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Add new post</h3>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="{{ url('addnew_post') }}" method="POST" role="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label for="exampleInputPassword1">Service sub-category</label>
                            <select name="ssc_id" class="form-control" id="cat_name">
                                 <optgroup label="Female">
                                 @foreach($sscs as $ssc)
                                    @if($ssc->ssc_gender == '2')
                                        <option value="{{ $ssc->ssc_id }}"> {{ $ssc->ssc_name }} (F)</option>
                                    @endif
                                    
                                 @endforeach
                                 </optgroup>
                                 <optgroup label="Male">
                                 @foreach($sscs as $ssc)
                                    
                                    @if($ssc->ssc_gender == '1')
                                        <option value="{{ $ssc->ssc_id }}"> {{ $ssc->ssc_name }} (M)</option>
                                    @endif
                                    
                                 @endforeach
                                 </optgroup>
                            </select>
                        </div>
                        @push("script")
                        <script type="text/javascript">
                            $("#cat_name").val("{{$ssc_id}}");
                        </script>
                        @endpush
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Post status</label>
                            <select name="p_status" class="form-control">
                                <option value="1">Approved</option>
                                <option value="0">Un-approved</option>
                            </select>
                        </div>
                    </div>
                    {{-- <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Post creator</label>
                            <input required="" type="text" name="pp_creator" class="form-control" id="exampleInputEmail1" placeholder="Post creator">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Posts URL</label>
                            <input required="" type="text" name="p_url" class="form-control" id="exampleInputEmail1" placeholder="Posts URL">
                        </div>
                    </div> --}}
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Post link</label>
                            <input required="" type="text" name="p_link" class="form-control" id="exampleInputEmail1" placeholder="Post link">
                        </div>
                    </div>
                    
                   <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Posts image URL</label>
                            <input required="" type="text" name="p_imageUrl" class="form-control" id="exampleInputEmail1" placeholder="Post image URL">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Posts metadata</label>
                            <input required="" type="text" name="p_metadata" class="form-control" id="exampleInputEmail1" placeholder="Posts metadata">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Posts keywords</label>
                            <input required="" type="text" name="p_keywords" class="form-control" id="exampleInputEmail1" placeholder="Posts keywords">
                        </div>
                    </div>
                    
                </div>
                <!-- /.box-body -->
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                   
                     <button style="margin-left: 10px;" type="reset" class="btn btn-primary">Reset</button>
                     
                    <a style="margin-left: 10px;" href="{{ url('admin/postmanagement')}}" class="btn btn-primary" >Cancel</a>
                    
                   
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection

@push("script")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


<script>


  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>
@endpush