@extends('salon-magmt.base')
@section('action-content')

@push("css")
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    

<style type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></style>
<style type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"></style>
    

@endpush
<section class="content">
    
    
    <div id="success"></div>
    @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif

    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
          @if(Session::has('flash_message'))
              {{ Session::get("flash_message") }}
          @endif

          <div class="col-lg-12">
                            
                        </div>
                        <div class="clearfix"></div>

              {{-- data table start code here  --}}
              <div class="row">
                <div class="col-md-12" style="margin-top: 50px;">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>Email </th>
                              <th>Status</th>
                              
                          </tr>
                      </thead>
                      <tbody>
                          @if(!empty($re))
                          @foreach($re as $key => $ar)  
                            @php $key += 1; @endphp
                              <tr>
                                <td>{{$key}}</td>
                                <td>
                                    {{$ar['email_address']}}
                                </td>
                                <td>
                                    {{$ar['status']}}
                                </td>
                            </tr>
                    @endforeach
                  @endif

                         
                      </tbody>
                      <tfoot>
                          <tr>
                              <th>#</th>
                              <th>Email </th>
                              <th>Status</th>
                          </tr>
                      </tfoot>
                  </table>
                </div>
              </div>


@endsection
@push("script")
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>

<script  src="{{asset('/')}}js/index.js"></script>


      <script type="text/javascript">
          jQuery("#is_active").on("change",function(){
          // var id = jQuery(this).val();
          alert(id);
        });
      </script>
    @isset($is_reviewed)
      <script type="text/javascript">
        $("#sal_is_reviewed").val("{{$is_reviewed}}");
      </script>
    @endisset

    @isset($is_active)
      <script type="text/javascript">
        $("#is_active").val("{{$is_active}}");
      </script>
    @endisset


<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


  <script type="text/javascript">  
      $(".ssc_id").on("change",function(){
          var ssc_id = $(this).val();
          var array = ssc_id.split(",");
          var s_id = array[0];
          var pb_id = array[1];
          $.post('{{ url('admin/changessc') }}' , {s_id: s_id,pb_id:pb_id, _token: '{{ csrf_token() }}' } , function(data){
                  $("#success").html(data);
                  $('#me').fadeIn('fast').delay(1000).fadeOut('slow');

                  
          }); 
        });

    $(document).ready(function(){
            $("#moreinfo").click(function(){
                $("#advance").toggle();
            });
        });

     

     $(document).ready(function() {
        $('#example').DataTable();
      } );
      
</script>
@endpush