@extends('salon-magmt.base')
@section('action-content')

@push("css")
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    

<style type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></style>
<style type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"></style>
    

@endpush
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Appointments </h3><br>
        </div>
    </div>
    
    <div id="success"></div>
    @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif

    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
          @if(Session::has('flash_message'))
              {{ Session::get("flash_message") }}
          @endif

          <div class="col-lg-12">
                            <div id="advance" style="display:;">
                                <form method="post" action="{{url('admin/search-appointments')}}">
                                  {!! csrf_field() !!}
                                    <div class="row">
                                      

                                        <div class="col-xs-2">
                                            <label for="startDate">Start Date</label>
                                            <input id="startDate" name="startDate" type="text" value="{{$startDate}}" class="form-control" />
                                        </div>
                                        <div class="col-xs-2">
                                            <label for="endDate">End Date</label>
                                            <input id="endDate" name="endDate" type="text" value="{{$endDate}}" class="form-control" />
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="form-group">
                                            <label>Status</label>
                                            <select name="app_status" id="app_status" class="form-control">
                                              <option value="">All Status</option>
                                              <option value="accepted">Accepted</option>
                                              <option value="deleted">Deleted</option>
                                              <option value="finished">Finished</option>
                                              <option value="pending">Pending</option>
                                              <option value="serving">Serving</option>
                                              <option value="rejected">Rejected</option>
                                              
                                            </select>
                                          </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                            <label>Salon</label>
                                            <select name="sal_id" id="sal_id" class="form-control">
                                              <option value="">All salons</option>
                                              @foreach($salons as $key => $value)
                                              <option value="{{$value->sal_id}}">{{$value->sal_name}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        
                                        <div class="col-xs-3">
                                            <div class=""> <br>
                                                <input type="submit" class="btn btn-info btn-lg" value="Search" style="padding: 3px 30px !important; margin-top: 5px;">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>

              {{-- data table start code here  --}}
              <div class="row">
                <div class="col-md-12" style="margin-top: 50px;">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                          <tr>
                              <th>Salon</th>
                              <th>Email</th>
                              <th>Customer</th>
                              <th>Phone</th>
                              <th>Req. Date</th>
                              <th>Status</th>
                              <th>Last Update</th>
                              {{-- <th>Follow up date</th> --}}
                          </tr>
                      </thead>
                      <tbody>
                          @if(!empty($app_request))
                          @foreach($app_request as $key => $ar)  
                            @if($ara_follow_date == "on")
                              @if(!empty($ar->ara_follow_date))
                              <tr>
                                <td>
                                  <a href="{{url('appointment-request-detail'.'/'.$ar->app_id)}}"> {{$ar->sal_name}}</a>
                                </td>
                                <td>
                                  <a href="{{url('appointment-request-detail'.'/'.$ar->app_id)}}">
                                    {{$ar->ar_email}} &nbsp; @if(!empty($ar->mc_status)) ( {{$ar->mc_status}} )@else @endif
                                  </a>
                                </td>
                                <td>
                                  <a href="{{url('appointment-request-detail'.'/'.$ar->app_id)}}">
                                    {{$ar->cust_name}} &nbsp; @if(!empty($ar->mc_status)) ( {{$ar->mc_status}} )@else @endif
                                  </a>
                                </td>
                                
                                <td style="text-align: center;">
                                  <a href="{{url('appointment-request-detail'.'/'.$ar->app_id)}}">{{$ar->sal_phone}}</a>
                                    
                                </td>
                                <td>
                                  <a href="{{url('appointment-request-detail'.'/'.$ar->app_id)}}">
                                  {{date('d-m-Y',strtotime($ar->app_created))}} &nbsp;
                                  {{ date("g:i a", strtotime($ar->app_created))}} </a>
                                </td>
                                <td>
                                  <a href="{{url('appointment-request-detail'.'/'.$ar->app_id)}}"></a>
                                </td>
                                <td>
                                  <a href="{{url('appointment-request-detail'.'/'.$ar->app_id)}}">
                                  {{date('d-m-Y',strtotime($ar->app_last_modified))}} &nbsp;
                                  {{ date("g:i a", strtotime($ar->app_last_modified))}} </a>
                                </td>

                                <td>
                                  <a href="{{url('appointment-request-detail'.'/'.$ar->app_id)}}">
                                    @if(!empty($ar->ara_follow_date))
                                      {{date('d-m-Y',strtotime($ar->ara_follow_date))}} &nbsp;
                                      {{ date("g:i a", strtotime($ar->ara_follow_date))}} 
                                    @else
                                      Not Available
                                    @endif
                                </a>
                                </td>
                              </tr>
                              @endif
                            @else
                              <tr>
                                <td>
                                  <a href="{{url('appointment-detail'.'/'.$ar->app_id)}}"> {{$ar->sal_name}}</a>
                                </td>
                                <td>
                                  <a href="{{url('appointment-detail'.'/'.$ar->app_id)}}">
                                     {{$ar->sal_email}}
                                  </a>
                                </td>
                                <td>
                                  <a href="{{url('appointment-detail'.'/'.$ar->app_id)}}">
                                     {{$ar->cust_name}}
                                  </a>
                                </td>
                                
                                <td style="text-align: center;">
                                  <a href="{{url('appointment-detail'.'/'.$ar->app_id)}}">{{$ar->sal_phone}}</a>
                                    
                                </td>
                                <td>
                                  <a href="{{url('appointment-detail'.'/'.$ar->app_id)}}">
                                  {{date('d-m-Y',strtotime($ar->app_created))}} &nbsp;
                                  {{ date("g:i a", strtotime($ar->app_created))}} </a>
                                </td>
                                <td>
                                  <a href="{{url('appointment-detail'.'/'.$ar->app_id)}}">{{$ar->app_status}}</a> 
                                </td>
                                <td>
                                  <a href="{{url('appointment-detail'.'/'.$ar->app_id)}}">
                                  {{date('d-m-Y',strtotime($ar->app_last_modified))}} &nbsp;
                                  {{ date("g:i a", strtotime($ar->app_last_modified))}} </a>
                                </td>
                            </tr>
                            @endif
                        @endforeach
                        @endif

                         
                      </tbody>
                      <tfoot>
                          <tr>
                              <th>Salon</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Req. Date</th>
                              <th>Status</th>
                              <th>Last Update</th>
                          </tr>
                      </tfoot>
                  </table>
                </div>
              </div>
@endsection
@push("script")
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>

<script  src="{{asset('/')}}js/index.js"></script>


      <script type="text/javascript">
          jQuery("#is_active").on("change",function(){
          // var id = jQuery(this).val();
          alert(id);
        });
      </script>
    @isset($is_reviewed)
      <script type="text/javascript">
        $("#sal_is_reviewed").val("{{$is_reviewed}}");
      </script>
    @endisset

    @isset($is_active)
      <script type="text/javascript">
        $("#is_active").val("{{$is_active}}");
      </script>
    @endisset


<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


  <script type="text/javascript">  
      $(".ssc_id").on("change",function(){
          var ssc_id = $(this).val();
          var array = ssc_id.split(",");
          var s_id = array[0];
          var pb_id = array[1];
          $.post('{{ url('admin/changessc') }}' , {s_id: s_id,pb_id:pb_id, _token: '{{ csrf_token() }}' } , function(data){
                  $("#success").html(data);
                  $('#me').fadeIn('fast').delay(1000).fadeOut('slow');

                  
          }); 
        });

    $(document).ready(function(){
            $("#moreinfo").click(function(){
                $("#advance").toggle();
            });
        });

     

     $(document).ready(function() {
        $('#example').DataTable();
      } );

     $("#sal_id").val('{{$sal_id}}');
     $("#app_status").val('{{$app_status}}');
      
</script>
@endpush