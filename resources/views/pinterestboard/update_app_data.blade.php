@extends('salon-magmt.base')
@section('action-content')

@push("css")
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    

<style type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></style>
<style type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"></style>
    
<style>
    #unApproved{
        background-color: transparent;
        text-decoration: none;
        border: none;
        color: #337ab7;
    }
</style>
@endpush
<section class="content">
    
    
    <div id="success"></div>
    @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif

    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
          @if(Session::has('flash_message'))
              {{ Session::get("flash_message") }}
          @endif

          <div class="col-lg-12">
                            <div id="advance" style="display:;">
                            @if (Session::has('message'))
                            <div class="flash-message">
                                <div class="alert alert-success">
                                {{ Session::get('message') }}
                                </div>
                            </div>
                                
                            @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>

            


@endsection
@push("script")
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>

<script  src="{{asset('/')}}js/index.js"></script>


      <script type="text/javascript">
          jQuery("#is_active").on("change",function(){
          // var id = jQuery(this).val();
          alert(id);
        });
      </script>
    @isset($is_reviewed)
      <script type="text/javascript">
        $("#sal_is_reviewed").val("{{$is_reviewed}}");
      </script>
    @endisset

    @isset($is_active)
      <script type="text/javascript">
        $("#is_active").val("{{$is_active}}");
      </script>
    @endisset


        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


<script>


  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>


<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


  <script type="text/javascript">  
      $(".ssc_id").on("change",function(){
          var ssc_id = $(this).val();
          var array = ssc_id.split(",");
          var s_id = array[0];
          var pb_id = array[1];
          $.post('{{ url('admin/changessc') }}' , {s_id: s_id,pb_id:pb_id, _token: '{{ csrf_token() }}' } , function(data){
                  $("#success").html(data);
                  $('#me').fadeIn('fast').delay(1000).fadeOut('slow');

                  
          }); 
        });

    $(document).ready(function(){
            $("#moreinfo").click(function(){
                $("#advance").toggle();
            });
        });

     

     $(document).ready(function() {
        $('#example').DataTable();
        
      } );


     $('#example').dataTable( {
        "pageLength": 25
    } );
      
</script>
@endpush