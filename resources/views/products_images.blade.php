@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $pc_image_path = $helper->fashion_image_display();
  
@endphp


@extends('service-mgmt.base')
@section('action-content')

@push("css")
  <link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
    <style type="text/css">
        input[type="file"]
            {
                /*color: transparent;*/
            }
        .checkbox label:after, 
        .radio label:after {
            content: '';
            display: table;
            clear: both;
        }

        .checkbox .cr,
        .radio .cr {
            position: relative;
            display: inline-block;
            border: 1px solid #a9a9a9;
            border-radius: .25em;
            width: 1.3em;
            height: 1.3em;
            float: left;
            margin-right: .5em;
        }

        .radio .cr {
            border-radius: 50%;
        }

        .checkbox .cr .cr-icon,
        .radio .cr .cr-icon {
            position: absolute;
            font-size: .8em;
            line-height: 0;
            top: 50%;
            left: 20%;
        }

        .radio .cr .cr-icon {
            margin-left: 0.04em;
        }

        .checkbox label input[type="checkbox"],
        .radio label input[type="radio"] {
            display: none;
        }

        .checkbox label input[type="checkbox"] + .cr > .cr-icon,
        .radio label input[type="radio"] + .cr > .cr-icon {
            transform: scale(3) rotateZ(-20deg);
            opacity: 0;
            transition: all .3s ease-in;
        }

        .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
        .radio label input[type="radio"]:checked + .cr > .cr-icon {
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        .checkbox label input[type="checkbox"]:disabled + .cr,
        .radio label input[type="radio"]:disabled + .cr {
            opacity: .5;
        }
        #checked_featured{
            display: none;
        }
        #blah{
            display: none;
        }

        /*/////////////*/

    </style>
@endpush

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Products Category</h3><br>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Products Category</h3>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form id="addform" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Products Category </label>
                            <select name="pc_id" class="form-control" id="pc_id">
                              <option value="">Please Select Brand Collection</option>
                              @foreach($main_pro as $key => $value)
                                <option value="{{$value->pc_id}}">{{$value->pc_name}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>
                    @push('script')
                      <script type="text/javascript">
                        $("#pc_id").val("{{$id}}");
                      </script>
                    @endpush
                    @isset($data)
                    @foreach($data as $key => $value)
                      <div class="col-xs-12 col-md-6 col-lg-3">
                        <div class="card">
                          <img class="card-img-top" width="225px" height="200px" src="{{$pc_image_path}}{{$value->fbci_name}}" alt="Card image cap">
                            <div class="card-block">
                              <h4 class="card-title text-center">
                                <button class="btn btn-danger" id="{{$value->fbci_id}}" onClick="reply_click(this.id)">
                                  Remove
                                </button>
                              </h4>
                            </div>
                        </div>
                      </div>
                    @endforeach
                    @endisset

                    @isset($pro)
                    @foreach()
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Product Name</label>
                        <input type="text" name="p_name" value="{{$value->p_name}}">
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label></label>
                        <img src="">
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Product Images</label>
                        <input type="text" name="p_name" value="{{$value->p_name}}">
                      </div>
                    </div>

                    {{-- <div class="col-sm-12">
                      <div class="form-group gallery">
                        <label>Brand Collection Images</label>
                          <div class="form-group">      
                          <div class="form-group dropzone" id="dropzone">
                          </div>       
                          </div>
                      </div>
                    </div> --}}
                </div>
                

                <!-- /.box-body -->
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                   
                     <button style="margin-left: 10px;" type="reset" class="btn btn-primary">Reset</button>
                     
                    {{-- <a style="margin-left: 10px;" href="{{$returnback}}" class="btn btn-primary" >Cancel</a> --}}
                    
                </div>  

                <input type="hidden" name="fash_bc_id" id="fash_bc_id" value="">
            </form>



        </div>
        <!-- /.box-body -->
    </div>
</section>

    
@endsection

@push('script')

<link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
<script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>


<script type="text/javascript">
  function reply_click(id)
  {
      // alert(clicked_id);
      window.location.href="{{url('/admin/delete_catalog_image')}}/"+id;
  }
</script>

<script type="text/javascript">
 

  $('#pc_id').on('change',function(e){
    var id = $(this).val();
    // alert(id);
        // document.getElementById('fash_bc_id').value = id;
        window.location.href="{{url('/product_image')}}/"+id;
    });


    // $('#addform').submit(function(e){
    //     var button = $('#submit');
    //     button.prop('disabled', true);
    //     e.preventDefault();
    //         myDropzone.processQueue();
    //   });
    //    if(typeof Dropzone != 'undefined')
    //    {
    //        Dropzone.autoDiscover = false;
    //        var myDropzone = new Dropzone("#dropzone", {
    //          url: "{{url('/admin/add_fbc_images')}}",
    //          autoProcessQueue:false,
    //          required:true,
    //          acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
    //          addRemoveLinks: true,
    //          maxFiles:8,
    //          parallelUploads : 100,
    //          maxFilesize:5,
    //          init: function() {
    //             // Update the total progress bar
    //             this.on("totaluploadprogress", function(progress) {
    //                // document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
    //              });
    //              this.on("queuecomplete", function(progress) {
    //                // document.querySelector("#total-progress").style.opacity = "0";
    //              });
    //         this.on("sending", function(file, xhr, formData)
    //          {
    //           // document.querySelector("#total-progress").style.opacity = "1";
    //           formData.append("fbc_id",'{{ $id }}'); 
    //           formData.append('_token', '{{ csrf_token() }}'); 
    //          });
    //            this.on('success', function(file, responseText)
    //             {                   

    //             });

    //          this.on("complete", function (file)
    //                      {
    //                   if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) 
    //                       {    

    //                          swal({
    //                           title: 'Property Updated',
    //                           type:'success'
            
    //                          }).then(function(){
    //                           window.location.href='{{url('/')}}';
    //                          });    

    //                       }
    //                     });   
    //         }

    //     });
    //   };
</script>

<script type="text/javascript">
  $('#fb_featured').click(function() {
            if( $(this).is(':checked')) {
                $("#checked_featured").show();
            } else {
                $("#checked_featured").hide();
            }
        }); 
</script>

@endpush
