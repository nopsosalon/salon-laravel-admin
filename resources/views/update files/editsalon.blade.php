@extends('salon-magmt.base') 
@section('action-content')
-<section class="content">

    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <div class="box">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li id="one" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Services</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Salon Info</a></li>
                        <li id="three" class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">
                            Salon Services</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-header">
                                <h3 class="box-title"> 
                                    {{ $salon->sal_name }}
                                </h3>
                                <!--<p>All fields are required* </p>-->

                            </div>
                            <div class="box-body">
                                <div>

                                    <div class="">

                                        <form role="form" method="POST" action="{{ url('typeservices') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="returnback" value="{{$returnback}}">

                                            <div class="form-group">

                                                <label>
                                                    <input type="hidden" value="<?php echo $salon->sal_id; ?>" name="sal_id">
                                                </label>
                                                <label>Salon Type</label>
                                                <select id="salon_type" name="salon_type" class="form-control">
                                                    <?php foreach ($salon_types as $salon_type) { ?>

                                                        <?php $type_id = session('type_id') ? session('type_id') : 1; ?>

                                                        <option <?php if ($type_id == $salon_type->sty_id) echo "selected"; ?>  value="<?php echo $salon_type->sty_id; ?>"> <?php echo $salon_type->sty_name; ?>
                                                        </option>;
                                                    <?php }
                                                    ?>

                                                </select>
                                                <input style="display: none;" type="submit" value="submit">
                                            </div>
                                        </form>

                                    </div>
                                    <?php
//                                $attributes = array("name" => "services", "id" => "service_form");
//                                echo form_open("admin/update_services/", $attributes);
                                    ?>

                                    <form role="form" method="POST" action="{{ url('type_services_update') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="">
                                                <input type="hidden" name="returnback" value="{{$returnback}}">
                                            <div class="form-group">
                                                <label>Services</label>
                                                <div class="row">
                                                    <input type="hidden" value="<?php echo $salon->sal_id; ?>" name="sal_id">
                                                    <input type="hidden" value="<?php echo $salon->sal_name; ?>" name="sal_name">


                                                    <?php
                                                    foreach ($type_services as $value) {
//
                                                        if (array_key_exists("checked", $value)) {

                                                            $isChecked = $value->checked;
                                                        } else {
                                                            $isChecked = '';
                                                            // 
                                                        }
                                                        ?>
                                                        <div class="col-md-3">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input hidden  name="sal_type_name[]" 
                                                                           value=" <?php echo $value->ser_name ?>" 
                                                                           <?php echo $isChecked; ?>
                                                                           type="text">
                                                                </label>
                                                                <label>



                                                                    <input  name="sal_type_status[]" 
                                                                            value=" <?php echo $value->ser_id ?>" 
                                                                            <?php echo $isChecked; ?>
                                                                            type="checkbox">

                                                                    <?php echo $value->ser_name; ?>

                                                                </label>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>



                                            </div>
                                            <div class="form-group">

                                                <button name="submit" type="submit" class="btn btn-primary">Update</button>
                                                <button style=" margin-left: 14px; margin-right: 10px;" type="reset" class="btn btn-primary">Reset</button>
                                                <a style=" margin-left: 8px; margin-right: 8px;" class="btn btn-primary" href="{{ $returnback }}">
                                                    {{-- {{ decrypt($returnback) }} --}}
                                                    Cancel
                                                </a>

                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->

                        <div class="tab-pane" id="tab_2">
                            <form role="form" method="POST" action="{{ url('updateSalon') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}


                                <table class="table table-hover">
                                    <tbody>
                                        <!-- style= "background-color: #84ed86; color: #761a9b; margin-top: 200px;" -->
                                        <tr>
                                            <th>Name </th>
                                            <th>Email</th>
                                        </tr>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_name }}" name="sal_name" style="width: 60%;">
                                    </td>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_email }}" name="sal_email" style="width: 60%;">
                                    </td>
                                    <tr>
                                        <th>Phone</th>
                                        <th>Status</th>
                                    </tr>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_phone }}" name="sal_phone" style="width: 60%;">
                                    </td>
                                    <td>
                                        <?php if ($salon->is_active == 1) { ?>
                                            <span class="label label-success">Active</span>
                                        <?php } else { ?>
                                            <span class="label label-danger">InActive</span>
                                        <?php } ?>
                                    </td>

                                    <tr>
                                        <th>City</th>
                                        <th>Addresss</th>
                                    </tr>

                                    <td>
                                        <input type="text" value="{{ $salon->sal_city }}" name="sal_city" style="width: 60%;">
                                    </td>

                                    <td>
                                        <input type="text" value="{{ $salon->sal_address }}" name="sal_address" style="width: 60%;">
                                    </td>

                                    <tr>
                                        <th>Postcode</th>
                                        <th>Speciality</th>
                                    </tr>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_zip }}" name="sal_zip" style="width: 60%;">
                                    </td>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_specialty }}" name="sal_specialty" style="width: 60%;">
                                    </td>

                                    <tr>
                                        <th>Facebook</th>
                                        <th>Instagram</th>
                                    </tr>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_facebook }}" name="sal_facebook" style="width: 60%;">
                                    </td>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_instagram }}" name="sal_instagram" style="width: 60%;">
                                    </td>

                                    <tr>
                                        <th>Sal_24clock</th>
                                        <th>Sal_Hours</th>

                                    </tr>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_24clock }}" name="sal_24clock" style="width: 60%;">
                                    </td>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_hours }}" name="sal_hours" style="width: 60%;">
                                    </td>
                                    <tr>

                                        <th>Website</th>
                                    </tr>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_website }}" name="sal_website" style="width: 60%;">
                                    </td>

                                    <tr>
                                        <th>Biography</th>
                                    </tr>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_biography }}" name="sal_biography" style="width: 60%;height:80px;">
                                    </td>
                                    <td>
                                        <input type="hidden" value="{{ $salon->sal_id }}" name="sal_id" style="width: 60%;height:80px;">
                                    </td>
                                    </tbody>
                                </table>

                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button style=" margin-left: 14px; margin-right: 10px;" type="reset" class="btn btn-primary">Reset</button>
                                <a style=" margin-left: 8px; margin-right: 8px;" class="btn btn-primary" href="{{ $returnback }}">
                                    {{-- {{ decrypt($returnback) }} --}}
                                    Cancel
                                </a>

                            </form>
                        </div>

                        <!-- close form here -->



                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <div class="box-header">
                                <h3 class="box-title"> {{ $salon->sal_name }} </h3>
                                <!--<p>All fields are required* </p>-->
                            </div>
                            <table id="salon_tabel"  class="table table-hover">

                                <tr>

                                    <th>Service</th>

                                    <th>Price</th>
                                    <th>Duration</th>
                                    <th>Status </th>
                                    <th>featured</th>

                                    <th>Action</th>
                                </tr>
                                <?php
                                foreach ($salon_services_salon as $value) {
                                    // $status=$value->sser_name;
                                    if ($value->sser_enabled == '0') {
                                        $status = 'InActive';
                                    } else {
                                        $status = 'Active';
                                    }
                                    $sser_featured='';
                                    if ($value->sser_featured == '0') {
                                        $sser_featured = 'No';
                                    } else {
                                        $sser_featured = 'Yes';
                                    }
                                    
                                    
                                    $rate="";
                                    if ($value->sser_rate==null) {
                                        $rate="Null";
                                    }else{
                                       $rate =$value->sser_rate;
                                    }
                                    
                                    $duration="";
                                    if ($value->sser_time==null) {
                                        $duration="Null";
                                    }else{
                                       $duration =$value->sser_time;
                                    }
                                    
                                    ?>
                                    <tr>
                                        <td style="; border:1pt;">
                                            <?php echo $value->sser_name; ?>
                                        </td>
                                        <td style="; border:1pt;">
                                              <?php echo $rate ?>
                                          
                                        </td>
                                        <td style="; border:1pt;">
                                              <?php echo $duration  ?> 
                                        </td>
                                        <td style=" border:1pt;"><?php echo $status ?></td>
                                        <td style="; border:1pt;">
                                            <?php echo $sser_featured ?> 
                                        </td>

                                        <td style="width: 10%; border:1pt;"> 
                                            <a href="{{ url('editsalonservices', ['id' => $value->sser_id,  'salId' => $salon->sal_id ]) }}" class="btn btn-info"> 
                                                Edit 
                                            </a>



                                        </td>


                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>


    -</section>
-@endsection