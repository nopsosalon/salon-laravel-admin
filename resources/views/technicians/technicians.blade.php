@extends('service-mgmt.base')
@section('action-content')

<style type="text/css">
  .small-img {
      height: 80px;
      width: 80px;
      border-radius: 50%;
    }
</style>

<section class="content">
  <div class="row">
        <div class="col-sm-12">
            <h3> @if(Session::get('change_db_connection')=="2") Staff @elseif(Session::get('change_db_connection')=="3") Staff @elseif(Session::get('change_db_connection')=="1") Technicians @endif Detail</h3><br>
        </div>
    </div>


    <div style="margin-bottom: 30px;">
        <form role="form" method="get" action="{{ url('admin/technicians') }}">
            {{-- {{ csrf_field() }} --}}
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            @if(Session::get('change_db_connection')=="2") Staff @elseif(Session::get('change_db_connection')=="3") Staff @elseif(Session::get('change_db_connection')=="1") Technicians @endif Name
                          </label>
                          <input placeholder="Technician Name"  type="text" class="form-control" value="@isset($tech_name) {{$tech_name}}  @endisset" name="tech_name" maxlength="128">
                        </div>
                      </div>

                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>
                            Select Salon
                        </label>
                        <select class="form-control" name="sal_id" id="sal_id">
                          <option value='all_active' @if($tech_status == "all_active") selected="checked" @endif >All</option>
                          @foreach($salons as $key => $salon)
                            <option value='{{$salon->sal_id}}' @if($salon->sal_id == $sal_id) selected="checked" @endif>{{$salon->sal_name}}</option>
                          @endforeach
                          {{-- <option value='2' @if($tech_status == 2) selected="checked" @endif>In-Active</option> --}}
                        </select>
                      </div>
                    </div>

                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>
                            @if(Session::get('change_db_connection')=="2") Staff @elseif(Session::get('change_db_connection')=="3") Staff @elseif(Session::get('change_db_connection')=="1") Technician @endif Status  
                        </label>
                        <select class="form-control" name="tech_status" id="tech_status">
                          <option value='all_active' @if($tech_status == "all_active") selected="checked" @endif >All</option>
                          <option value='1' @if($tech_status == 1) selected="checked" @endif>Active</option>
                          <option value='2' @if($tech_status == 2) selected="checked" @endif>In-Active</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>

        @if(!empty($tech_name) OR !empty($tech_status) OR !empty($sal_id))
                    @php

                      if(is_null($tech_name))
                        {
                          $tech_name = "";
                        }
                      if(is_null($tech_status))
                        {
                          $tech_status = "";
                        }
                        if(is_null($sal_id))
                        {
                          $sal_id = "";
                        }  
                        
                        $urls = ('admin/technicians?'.'tech_name'.'='.$tech_name.'&'.'tech_status'.'='.$tech_status.'&'.'sal_id'.'='.$sal_id.'&'.'page='.$technicians->currentPage());

                    @endphp
                  @else
                    @php
                       $urls = ('admin/technicians?'.'page='.$technicians->currentPage());
                    @endphp
                @endif


    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">
                      @if(Session::get('change_db_connection')=="2") Staff @elseif(Session::get('change_db_connection')=="3") Staff @elseif(Session::get('change_db_connection')=="1") Technicians @endif
                    </h3>
                </div>
                <div class="col-sm-4">
                    {{-- <a class="btn btn-info" href="{{ url('addcategory') }}">Add new Category</a> --}}
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody>
                  @if(count($technicians) > 0 ) 
                  <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Salon Name</th>
                        <th>Email</th>
                        {{-- <th>Image</th> --}}
                        <th>Phone</th>
                        <th>Status</th>
                        <th>Is Active</th>
                        <th>@if(Session::get('change_db_connection')=="2") Staff @elseif(Session::get('change_db_connection')=="3") Staff @elseif(Session::get('change_db_connection')=="1") Technician @endif DateTime</th>
                        {{-- <th>Action</th> --}}
                    </tr>
                   
                    <?php foreach ($technicians as $key => $tech) {  $key += 1;  ?>
                        <tr>
                            <td>{{$key}}</td>
                            <td>
                              <img class="small-img" src="{{$display_tech_img}}{{$tech->tech_pic ? $tech->tech_pic : 'placeholder.png'}}">
                              <?php echo $tech->tech_name ?>
                            </td>
                            <td>{{$tech->sal_name}}</td>
                            <td>{{$tech->sal_email}}</td>
                            {{-- <td>
                              <img class="small-img" src="{{$display_tech_img}}{{$tech->tech_pic ? $tech->tech_pic : 'placeholder.png'}}" width="100" height="100">
                            </td> --}}
                            <td>
                              {{$tech->tech_phone}}
                            </td>
                            <td>
                              @if($tech->tech_status==1)
                                Active
                              @else
                                In-Active
                              @endif  
                            </td>
                            <td>
                              @if($tech->tech_is_active==1)
                                Yes
                              @else
                                No
                              @endif  
                            </td>
                            <td>
                              {{date('d-m-Y',strtotime($tech->tech_modify_datetime))}}<br>
                              {{date('g:i a',strtotime($tech->tech_modify_datetime))}}
                            </td>
                            {{-- <td> <a href="{{ url('#', ['id' => $cust->cust_id,'urls'=>$urls])}}" class="btn btn-warning">Edit</a> 
                                <a style="margin-left: 10px;" href="{{ url('#', ['id' => $cust->cust_id,'urls'=>$urls])}}" class="btn btn-info">Manage Sub Categories</a>
                            </td> --}}
                        </tr>
                    <?php } ?>
                    @else
                      
                        <h3 style="text-align: center;">Record Not Found</h2>
                      
                    @endif
                </tbody>
            </table>

            {!! $technicians->appends(['tech_name'=>isset($tech_name)?$tech_name:'','tech_status'=>isset($tech_status)?$tech_status:'','sal_id'=>isset($sal_id)?$sal_id:'','page'=>$technicians->currentPage()])->links() !!}
            
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection