@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $pc_image_path = $helper->fashion_image_display();
    // dd($fash_brand);
@endphp
@extends('service-mgmt.base')
@section('action-content')

@push("css")
  <link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
    <style type="text/css">
        input[type="file"]
            {
                /*color: transparent;*/
            }
        .checkbox label:after, 
        .radio label:after {
            content: '';
            display: table;
            clear: both;
        }

        .checkbox .cr,
        .radio .cr {
            position: relative;
            display: inline-block;
            border: 1px solid #a9a9a9;
            border-radius: .25em;
            width: 1.3em;
            height: 1.3em;
            float: left;
            margin-right: .5em;
        }

        .radio .cr {
            border-radius: 50%;
        }

        .checkbox .cr .cr-icon,
        .radio .cr .cr-icon {
            position: absolute;
            font-size: .8em;
            line-height: 0;
            top: 50%;
            left: 20%;
        }

        .radio .cr .cr-icon {
            margin-left: 0.04em;
        }

        .checkbox label input[type="checkbox"],
        .radio label input[type="radio"] {
            display: none;
        }

        .checkbox label input[type="checkbox"] + .cr > .cr-icon,
        .radio label input[type="radio"] + .cr > .cr-icon {
            transform: scale(3) rotateZ(-20deg);
            opacity: 0;
            transition: all .3s ease-in;
        }

        .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
        .radio label input[type="radio"]:checked + .cr > .cr-icon {
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        .checkbox label input[type="checkbox"]:disabled + .cr,
        .radio label input[type="radio"]:disabled + .cr {
            opacity: .5;
        }
        #checked_featured{
            display: none;
        }
        #blah{
            display: none;
        }

        /*/////////////*/

    </style>
    <style id="css">
  @media (min-width: 576px)
{
.card-columns {
    -webkit-column-count: 3;
    column-count: 4 !important;
    -webkit-column-gap: 1.25rem;
    column-gap: 3.25rem;
  }

}
</style>
@endpush

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Add Fashion Brand Collection</h3><br>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-6">
                    <h3 class="box-title">Add Brand Collection</h3>
                </div>
                <div class="col-sm-6">
                  <div class="pull-right">
                    @isset($fb_name)
                      Brand Name : <strong><b>{{$fb_name}}</b></strong><br>
                    @endisset
                    @isset($totalimages)
                      Total Images : <strong><b>{{$totalimages}}</b></strong>
                    @endisset
                  </div>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form id="addform" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Brand Collection</label>
                            <select name="fbc_id" class="form-control" id="fbc_id">
                              <option value="">Please Select Brand Collection</option>
                              @isset($fash_brand)
                                @foreach($fash_brand as $key => $value) 
                                    @php $fbc = $value['fash_brand_collection']; @endphp
                                      <optgroup label="{{$value['fb_name']}} @if($value['fb_gender'] == 2)    
                                            (F)
                                          @elseif($value['fb_gender'] == 1)
                                            (M)
                                          @elseif($value['fb_gender'] == 3)
                                             (B)
                                          @endif
                                           ">
                                        @isset($fbc)
                                        @foreach($fbc as $key => $f)
                                        <option value="{{$f['fbc_id']}}">
                                          @if($f['fbc_gender'] == 2)    
                                            {{$f['fbc_name']}} (F)
                                          @elseif($f['fbc_gender'] == 1)
                                            {{$f['fbc_name']}} (M)
                                          @elseif($f['fbc_gender'] == 3)
                                            {{$f['fbc_name']}} (B)
                                          @endif
                                        </option>
                                        @endforeach
                                        @endisset
                                      </optgroup>
                                 @endforeach
                              @endisset
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group gallery">
                        <label>Brand Collection Images</label>
                          <div class="form-group">      
                          <div class="form-group dropzone" id="dropzone">
                          </div>       
                          </div>
                      </div>
                    </div>

                    @push('script')
                      <script type="text/javascript">
                        $("#fbc_id").val("{{$id}}");
                      </script>
                    @endpush
                    <div class="box-footer">
                      <button type="submit" id="btn_submit" class="btn btn-primary">Submit</button>
                      <button style="margin-left: 10px;" type="reset" class="btn btn-primary">Reset</button>
                    </div>  

                    <!-- /.box-body -->
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                

                <input type="hidden" name="fbci_id" id="fbci_id" value="">
            </form>
                      <div class="row">
                        <form method="post" action="{{url('admin/delete_images_fbci')}}" id="devel-generate-content-form">
                          {{csrf_field()}}
                          <div class="col-12">
                            <p style="float: right;">
                              <input type="submit" id="delete_images" class="btn btn-danger" value="Delete Multiple Images"><br><br>
                              <input style="
                              display: none;" type="checkbox" class="selectall" style="zoom:1.5; margin-top: -5px;" id="select_all" onclick="$(this).val(this.checked ? 1 : 0)" value="0" /> 

                          
                          {{-- <span style="margin-top: 5px; font-size: 18px;">Select All</span> --}}
                              <div class="pull-right" style="margin-top: -50px; padding: 20px; margin-right: -80px;">
                                <span id="count-checked-checkboxes">0</span> checked
                              </div>
                            </p><br><br>
                          </div>
                          <div class="card-columns" style="padding-left: 20px; padding-right: 20px;">
                            @isset($data)
                              @foreach($data as $key => $value)
                              <div class="card card-pin change_font_size" style="border: none;">
                                {{-- <img src=""> --}}
                                <a href="{{url('admin/fashionBrandCollection'.'/'.$fb_id)}}">
                                <img class="card-img-top" width="225px" src="{{$pc_image_path}}{{$value->fbci_name}}" alt="Card image cap">
                              </a>
                              <br>
                                  <span style="text-align: center;">
                                       <button class="btn btn-danger" id="{{$value->fbci_id}}" onClick="reply_click(this.id)">
                                      Remove
                                    </button>
                                  </span>
                                  <span style="float: right; margin-right: -5px;">
                                       <input type="checkbox" class="checkbox" name="fbci_id[]" value="{{$value->fbci_id}}" style="zoom:1.5;" id="checkboxid" />
                                       <input type="hidden" name="fbci_name[]" value="{{$value->fbci_name}}">
                                  </span>
                            </div>
                              @endforeach
                            @endisset
                          </div>
                        </form>
                      </div>    
                </div>
                
        </div>
    </div>
</section>

    
@endsection

@push('script')

<link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
<script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>


<script type="text/javascript">
  function reply_click(id)
  {
      // alert(clicked_id);
      window.location.href="{{url('/admin/delete_catalog_image')}}/"+id;
  }
</script>

<script type="text/javascript">
  
  // alert($("#fbc_id").val())

  $('#fbc_id').on('change',function(e){
    var id = $(this).val();
    // alert(id);
    // false;
        // document.getElementById('fbci_id').value = id;
        window.location.href="{{url('/admin/get_catalogs')}}/"+id;
    });


    $('#addform').submit(function(e){
        var button = $('#btn_submit');
        button.prop('disabled', true);
        e.preventDefault();
            myDropzone.processQueue();
      });
       if(typeof Dropzone != 'undefined')
       {
           Dropzone.autoDiscover = false;
           var myDropzone = new Dropzone("#dropzone", {
             url: "{{url('/admin/add_fbc_images')}}/"+$("#fbc_id").val(),
             method:"POST",
             autoProcessQueue:false,
             required:true,
             acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
             addRemoveLinks: true,
             maxFiles:30,
             parallelUploads : 100,
             maxFilesize:10,
             init: function() {
                // Update the total progress bar
                this.on("totaluploadprogress", function(progress) {
                   // document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
                 });
                 this.on("queuecomplete", function(progress) {
                   // document.querySelector("#total-progress").style.opacity = "0";
                 });
            this.on("sending", function(file, xhr, formData)
             {
              // document.querySelector("#total-progress").style.opacity = "1";
              formData.append("data",$("#fbc_id").val()); 
              formData.append('_token', '{{ csrf_token() }}'); 
             });
               this.on('success', function(file, responseText)
                {                   

                });

             this.on("complete", function (file)
                 {
              if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) 
                  {    
                    window.location.href='{{url('admin/catalog'.'/'.$id)}}';
                  }
                });   
            }

        });
      };
</script>

<script type="text/javascript">
  $('#fb_featured').click(function() {
            if( $(this).is(':checked')) {
                $("#checked_featured").show();
            } else {
                $("#checked_featured").hide();
            }
        }); 




    $("#delete_images").click(function() {
       var count = $(":checkbox:checked").length;
       if(count==0)
       {
        alert("please select image ");
        return false;
       }
    });

    $(document).ready(function(){

    var $checkboxes = $('#devel-generate-content-form input[type="checkbox"]');
        
    $checkboxes.change(function(){
        var select_all = 0;
       var selectall = document.getElementById("select_all").checked;
       if(selectall == true){
        select_all = 1;
       }
     
        
        var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
          if(countCheckedCheckboxes == 53)
          {
            countCheckedCheckboxes = eval(countCheckedCheckboxes) - eval(select_all);
            $('#count-checked-checkboxes').text(countCheckedCheckboxes);
            $('#edit-count-checked-checkboxes').val(countCheckedCheckboxes);
            // alert(countCheckedCheckboxes);
          }
          else
          {
            countCheckedCheckboxes = eval(countCheckedCheckboxes);
            if(countCheckedCheckboxes == -1){
              countCheckedCheckboxes = eval(countCheckedCheckboxes) - eval(select_all);
              $('#count-checked-checkboxes').text(countCheckedCheckboxes);
            $('#edit-count-checked-checkboxes').val(countCheckedCheckboxes);
            }
            else{
              countCheckedCheckboxes = eval(countCheckedCheckboxes) - eval(select_all);
              $('#count-checked-checkboxes').text(countCheckedCheckboxes);
            $('#edit-count-checked-checkboxes').val(countCheckedCheckboxes);
            }
            
          }
        // $('#count-checked-checkboxes').text(countCheckedCheckboxes);
        
          // $('#edit-count-checked-checkboxes').val(countCheckedCheckboxes);
      });

  });


</script>

@endpush
