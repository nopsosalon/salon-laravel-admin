@extends('service-mgmt.base')

@section('action-content')

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Manage Brand Collection</h3><br>
        </div>
    </div>

    <div class="box box-primary">
        <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="{{$cate_images}}{{$fb->fb_image ? $fb->fb_image : 'placeholder.png' }}"  alt="User profile picture">
            <h3 class="profile-username text-center">{{ $fb->fb_name }}</h3>
            <div class="col-sm-2 pull-right">
                <h1 style="margin-top: -100px;">
                    <a href="{{url($returnback)}}">Back</a>
                </h1>
            </div>
        </div>
    </div>

    <div style="margin-bottom: 30px;">
        <form role="form" method="get" action="{{ url('admin/fashionBrandCollection'.'/'.$id) }}" enctype="multipart/form-data">
            {{-- {{ csrf_field() }} --}}
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Brand Collection
                          </label>
                          <input placeholder="Brand Collection Name"  type="text" class="form-control" @isset($fbc_name) value="{{$fbc_name}}" @endisset name="fbc_name" maxlength="128">
                        </div>
                      </div>


                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>
                            Is Active  
                        </label>
                        <select class="form-control" name="fbc_status" id="fbc_status">
                          <option value='all_active' @isset($fbc_status) @if($fbc_status == "all_active") selected="checked" @endif @endisset>All</option>
                          <option value='1' @isset($fbc_status) @if($fbc_status == "1") selected="checked" @endif @endisset>Yes</option>
                          <option value='2' @isset($fbc_status) @if($fbc_status == "2") selected="checked" @endif @endisset>No</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Brand Collection</h3>
                </div>
                <div class="col-sm-4">
                    <a class="btn btn-info" href="{{ url('admin/addFashBrandSubcate', ['fb_id' => $id]) }}">Add new Brand Collection</a>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody>
                @isset($fbc)
                    @if(!empty($fbc))
                    <tr>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Gender</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                          @foreach($fbc as $value)
                    <tr>
                        <td>{{$value->fbc_name}}</td>
                        <td>
                            
                            <img src="{{$cate_images}}{{$value->fbc_image ? $value->fbc_image : 'placeholder.png'}}" width="100" height="100">
                        </td>
                        <td>
                            @if($value->fbc_gender==1)
                                <p>Male</p>
                            @elseif($value->fbc_gender==2)
                                <p>Female</p>
                            @elseif($value->fbc_gender==3)
                                Both
                            @endif
                        </td>
                        <td>
                            @if($value->fbc_status==1)
                                yes
                            @elseif($value->fbc_status==0)
                                No
                            @endif
                        </td>
                        <td>
                            <a   href="{{ url('admin/update_fashbrandc' , [ 'fbc_id' =>  $value->fbc_id, 'fb_id' => $id] )}}" class="btn btn-info"> <i class="glyphicon glyphicon-edit"></i>  </a> 
                            {{-- <a style="margin-left: 10px;" href="{{ url('category_association' , ['sc_id' => $value->fbc_id, 'fb_id' =>  $value->fbc_id, 'sc_name' => $value->fbc_name ] )}}" class="btn btn-info">Category for association</a> --}}
                            {{-- <a style="margin-left: 10px;" href="{{ url('manageservices' , ['sc_id' => $value->fbc_id, 'ssc_id' =>  $value->fbc_id] )}}" class="btn btn-info">manageservices</a> --}}
                        </td>
                    </tr>
                    @endforeach
                    @else
                        <h2></h2>Data not found!
                    @endif
                    @endisset
                </tbody>
            </table>
            {!! $fbc->appends(['fbc_name'=>isset($fbc_name)?$fbc_name:'','is_active'=>isset($fbc_status)?$fbc_status:'','page'=>$fbc->currentPage()])->links() !!} 
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection