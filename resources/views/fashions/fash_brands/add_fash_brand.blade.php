
@extends('service-mgmt.base')
@section('action-content')

@push("css")
    <style type="text/css">
        input[type="file"]
            {
                /*color: transparent;*/
            }
        .checkbox label:after, 
        .radio label:after {
            content: '';
            display: table;
            clear: both;
        }

        .checkbox .cr,
        .radio .cr {
            position: relative;
            display: inline-block;
            border: 1px solid #a9a9a9;
            border-radius: .25em;
            width: 1.3em;
            height: 1.3em;
            float: left;
            margin-right: .5em;
        }

        .radio .cr {
            border-radius: 50%;
        }

        .checkbox .cr .cr-icon,
        .radio .cr .cr-icon {
            position: absolute;
            font-size: .8em;
            line-height: 0;
            top: 50%;
            left: 20%;
        }

        .radio .cr .cr-icon {
            margin-left: 0.04em;
        }

        .checkbox label input[type="checkbox"],
        .radio label input[type="radio"] {
            display: none;
        }

        .checkbox label input[type="checkbox"] + .cr > .cr-icon,
        .radio label input[type="radio"] + .cr > .cr-icon {
            transform: scale(3) rotateZ(-20deg);
            opacity: 0;
            transition: all .3s ease-in;
        }

        .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
        .radio label input[type="radio"]:checked + .cr > .cr-icon {
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        .checkbox label input[type="checkbox"]:disabled + .cr,
        .radio label input[type="radio"]:disabled + .cr {
            opacity: .5;
        }
        #checked_featured{
            display: none;
        }
        #blah{
            display: none;
        }
    </style>
@endpush

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Add New Brands</h3><br>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Add Brands</h3>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if(isset($fb))
                 <form action="{{url('admin/updatedBrand',['id' => $fb->fb_id])}}" method="POST" role="form" enctype="multipart/form-data"> 
            @else
                <form action="{{ url('admin/store_factionBrand') }}" method="POST" role="form" enctype="multipart/form-data">
            @endif
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group col-sm-6">
                        <label for="exampleInputEmail1">Name</label>
                        <input required="" type="text" name="fb_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Name" value="@isset($fb) {{$fb->fb_name}} @endisset">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="exampleInputPassword1">Status</label>
                        <select name="fb_status" class="form-control">
                            <option value="1" @isset($fb) @if($fb->fb_status == '1') selected="checked" @endif  @endisset >Active</option>
                            <option value="0" @isset($fb) @if($fb->fb_status == '0') selected="checked" @endif  @endisset >In Active</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="exampleInputPassword1">Gender</label>
                        <select name="fb_gender" class="form-control">
                            <option value="1" @isset($fb) @if($fb->fb_gender == '1') selected="checked" @endif  @endisset>Male</option>
                            <option value="2" @isset($fb) @if($fb->fb_gender == '2') selected="checked" @endif  @endisset>Female</option>
                            <option value="3" @isset($fb) @if($fb->fb_gender == '3') selected="checked" @endif  @endisset>Both</option>
                        </select>
                    </div>
                   {{--  <div class="form-group col-sm-6">
                        <label for="exampleInputPassword1">Type</label>
                        <select name="fb_type" class="form-control">
                            <option value="1" @isset($fb) @if($fb->fb_status) sellected="selected" @endif  @endisset>Trend</option>
                            <option value="2" @isset($fb) @if($fb->fb_status) sellected="selected" @endif  @endisset>Fashion</option>
                        </select>
                    </div> --}}
                    <div class="form-group col-sm-6">
                        <label for="exampleInputFile">Add Image</label>
                        <input name="fb_image" type="file" id="imgInp" class="form-control" onclick="fileClicked(event)" onchange="fileChanged(event)"><br>

                        <img id="blah" src="#" alt="your image" width="200px" height="150px" />
                    </div>
                    <div class="col-sm-6">
                        <div class="checkbox">
                            <label style="font-size: 1.5em">
                                <input type="checkbox" value="0" name="fb_featured" id="fb_featured" onclick="$(this).attr('value', this.checked ? 1 : 0)" >
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                Featured  <span style="font-size: 16px !important;" id="checked_featured">[Recommended image size 350px x 150px]</span>
                            </label> 
                        </div>
                    </div>
                    
                </div>
                <!-- /.box-body -->
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                      <a class="btn btn-primary" style="margin-left: 10px;" href="{{ $returnback }}">Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection

@push('script')
    <script type="text/javascript">
        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })


        $('#fb_featured').click(function() {
            if( $(this).is(':checked')) {
                $("#checked_featured").show();
            } else {
                $("#checked_featured").hide();
            }
        }); 
    </script>

    <script>


        function readURL(input) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
          }
        }
        $("#imgInp").change(function() {
          readURL(this);
        });
        $('#imgInp').on("change",function(){
            $("#blah").show();
        });
        
         

    //This is All Just For Logging:
    var debug = true;//true: add debug logs when cloning
    var evenMoreListeners = true;//demonstrat re-attaching javascript Event Listeners (Inline Event Listeners don't need to be re-attached)
    if (evenMoreListeners) {
        var allFleChoosers = $("input[type='file']");
        addEventListenersTo(allFleChoosers);
        function addEventListenersTo(fileChooser) {
            fileChooser.change(function (event) { console.log("file( #" + event.target.id + " ) : " + event.target.value.split("\\").pop()) });
            fileChooser.click(function (event) { console.log("open( #" + event.target.id + " )") });
        }
    }
    (function () {
        var old = console.log;
        var logger = document.getElementById('log');
        console.log = function () {
            for (var i = 0; i < arguments.length; i++) {
                if (typeof arguments[i] == 'object') {
                    logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
                } else {
                    logger.innerHTML += arguments[i] + '<br />';
                }
            }
            old.apply(console, arguments);
        }
    })();

    var clone = {};

    // FileClicked()
    function fileClicked(event) {
        var fileElement = event.target;
        if (fileElement.value != "") {
            // if (debug) { console.log("Clone( #" + fileElement.id + " ) : " + fileElement.value.split("\\").pop()) }
            clone[fileElement.id] = $(fileElement).clone(); //'Saving Clone'
        }
        //What ever else you want to do when File Chooser Clicked
    }

    // FileChanged()
    function fileChanged(event) {
        var fileElement = event.target;
        if (fileElement.value == "") {
            // if (debug) { console.log("Restore( #" + fileElement.id + " ) : " + clone[fileElement.id].val().split("\\").pop()) }
            clone[fileElement.id].insertBefore(fileElement); //'Restoring Clone'
            $(fileElement).remove(); //'Removing Original'
            if (evenMoreListeners) { addEventListenersTo(clone[fileElement.id]) }//If Needed Re-attach additional Event Listeners
        }
        //What ever else you want to do when File Chooser Changed
    }



        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })
    </script>

@endpush