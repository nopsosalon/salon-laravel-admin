@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $fb_image_path = $helper->fashion_image_display();
  
@endphp

@extends('service-mgmt.base')
@section('action-content')

<section class="content">
  <div class="row">
        <div class="col-sm-12">
            <h3>Fashion Brands</h3><br>
        </div>
    </div>
    <div style="margin-bottom: 30px;">
        <form role="form" method="get" action="{{ url('admin/fashionBrand') }}" enctype="multipart/form-data">
            {{-- {{ csrf_field() }} --}}
          <div style="margin:0 auto; width:100%; height:50px;" class="div1">
                <div style="margin:0 auto; height:50px;" class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Brands
                          </label>
                          <input placeholder="Brand Name"  type="text" class="form-control" @isset($fb_name) value="{{$fb_name}}" @endisset name="fb_name" maxlength="128">
                        </div>
                      </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>
                            Is Active  
                        </label>
                        <select class="form-control" name="fb_status" id="fb_status">
                          <option value='all_active' @isset($fb_status) @if($fb_status == "all_active") selected="checked" @endif @endisset>All</option>
                          <option value='1' @isset($fb_status) @if($fb_status == 1) selected="checked" @endif @endisset>Yes</option>
                          <option value='2' @isset($fb_status) @if($fb_status == 2) selected="checked" @endif @endisset>No</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div> 
        </form>
    </div>

       {{--  @if(!empty($service_cat) OR !empty($is_active))
                    @php

                      if(is_null($service_cat))
                        {
                          $service_cat = "";
                        } 
                        if(is_null($is_active))
                        {
                          $is_active = "";
                        }
                        $urls = ('servicecategories?'.'service_cat'.'='.$service_cat.'&'.'is_active'.'='.$is_active.'&'.'page='.$categories->currentPage());

                    @endphp
                  @else
                    @php
                       $urls = ('servicecategories?'.'page='.$categories->currentPage());
                    @endphp
                @endif --}}


    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Brands</h3>
                </div>
                <div class="col-sm-4">
                    <a class="btn btn-info" href="{{ url('admin/addFashBrand') }}">Add new Brand</a>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody><tr>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Gender</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                    <?php foreach ($fb as $key => $value) { ?>
                        <tr>
                            <td><?php echo $value->fb_name ?></td>

                            <?php // $backslash = "/"; ?>
                            <td><img src="{{$fb_image_path}}{{$value->fb_image ? $value->fb_image : 'placeholder.png'}}" width="100" height="100"></td>
                            <td>
                              @if($value->fb_gender == 2)
                                Female
                              @elseif($value->fb_gender == 1)
                                Male
                              @elseif($value->fb_gender == 3)
                                Both
                              @endif
                            </td>
                            <td><?php echo $value->fb_status == 1 ? 'Yes' : 'No'; ?></td>
                            <td> <a href="{{ url('admin/edit_brand', ['id' => $value->fb_id])}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i></a> 
                                <a style="margin-left: 10px;" href="{{ url('admin/fashionBrandCollection', ['id' => $value->fb_id])}}" class="btn btn-info">Manage Brand Collection</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

            {!! $fb->appends(['fb_name'=>isset($fb_name)?$fb_name:'','is_active'=>isset($fb_status)?$fb_status:'','page'=>$fb->currentPage()])->links() !!} 
            
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection