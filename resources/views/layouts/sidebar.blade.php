<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('/bower_components/AdminLTE/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name}}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <br>
       

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li class=" @if(Request::is('dashboard') || Request::is('admin/advancesearch')) active @endif">
                <a href="{{ url('dashboard') }}">
                    <i class="fa fa-link"></i>
                    <span>Dashboard</span>
                </a> 
            </li>     
            <li class="treeview @if(Request::is('sarch_salons*') || Request::is('add-new-post/*') || Request::is('admin/pc-filter*') || Request::is('post-category') || Request::is('editserviceinfo/*') || Request::is('addNewService') || Request::is('allServices') || Request::is('category_association/*') || Request::is('edit_sub_category/*') || Request::is('add_subcategory/*') || Request::is('manage_subcategores/*') || Request::is('edit_category/*') || Request::is('addcategory') || Request::is('editsalon/*') || Request::is('admin/add_beautytips*')||Request::is('edit_beautyTips*')||Request::is('admin/beauti_tips*')||Request::is('admin/add_beautyTips_category*')||Request::is('admin/edit_beautytip_cat*') || Request::is('beautyTips_category')||Request::is('admin/technicians') || Request::is('admin/customers') || Request::is('admin/update-app-data') || Request::is('service-subcategory-post-detail') || Request::is('admin/service-subcategory-posts') || Request::is('managesalons') || Request::is('admin/appointments') || Request::is('admin/postmanagement') || Request::is('servicecategories') || Request::is('salon_types') || Request::is('allServices') || Request::is('post-category') || Request::is('fileupload') || Request::is('editsalon') || Session::get('activemenu')=='salon_types' || Session::get('activemenu')=='fileupload' ) || Session::get('activemenu')=='save_file') || Session::get('activemenu')=='save_file') || Session::get('activemenu')=='save_file_services' || Session::get('activemenu')=='temp_save_file' || Session::get('activemenu')=='temp_salon_file_services' || Session::get('activemenu')=='service_subcategory_posts' || Session::get('activemenu')=='service_subcategory_posts' || Session::get('activemenu')=='beautyTips_category' active @endif ">
                <a href="#">
                    <i class="fa fa-link"></i> 
                    <span>Main Menu</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(Request::is('sarch_salons*') || Request::is('managesalons') || Request::is('editsalon') || Session::get('activemenu') == 'managesalons' ) active @endif">
                        <a href="{{ url('managesalons') }}">
                            Salon Management
                        </a>
                    </li>
                    <li class="@if(Request::is('fileupload') || Session::get('activemenu')=='fileupload' ) active @endif">
                        <a href="{{ url('fileupload') }}">
                            Salon Data upload
                        </a>
                    </li> 

                    <li class="@if(Request::is('admin/appointments') || Session::get('activemenu')=='appointment_request' ) active @endif">
                        <a href="{{ url('admin/appointments') }}">
                            Appointments
                        </a>
                    </li> 

                    <li class="@if(Request::is('servicecategories') || Session::get('activemenu') == 'servicecategories' ) active @endif">
                        <a href="{{ url('servicecategories') }}">
                            Service Categories
                        </a>
                    </li>

                    <li class="@if(Request::is('allServices') || Session::get('activemenu')=='allServices' ) active @endif">
                        <a href="{{ url('allServices') }}">
                            Services
                        </a>
                    </li>

                     <li class="@if(Request::is('admin/service-subcategory-posts') || Session::get('activemenu')=='service_subcategory_posts' ) active @endif">
                        <a href="{{ url('admin/service-subcategory-posts') }}">
                            Service Sub-Category Posts
                        </a>
                    </li> 
                   
                      
                    

                    <li class="@if(Request::is('post-category') || Session::get('activemenu')=='pinterest-board' ) active @endif">
                        <a href="{{ url('post-category') }}">
                            Post Category
                        </a>
                    </li>  

                    <li class="@if(Request::is('admin/postmanagement') || Request::is('add-new-post/*')) active @endif">
                        <a href="{{ url('admin/postmanagement') }}">
                            Post Management
                        </a>
                    </li> 

                    <li class="@if(Request::is('admin/customers')) active @endif">
                        <a href="{{ url('admin/customers') }}">
                            Customers
                        </a>
                    </li> 
                     <li class="@if(Request::is('admin/technicians')) active @endif">
                        <a href="{{ url('admin/technicians') }}">
                            @if(Session::get('change_db_connection')=="2") Staff @elseif(Session::get('change_db_connection')=="3") Staff @elseif(Session::get('change_db_connection')=="1") Technicians @endif
                        </a>
                    </li>

                    <li class="@if(Request::is('beautyTips_category') || Session::get('activemenu')=='beautyTips_category' ) active @endif">
                        <a href="{{ url('beautyTips_category') }}">
                            Beauty Tips
                        </a>
                    </li>

                    <li class="@if(Request::is('admin/update-app-data')) active @endif">
                        <a href="{{ url('admin/update-app-data') }}">
                            Update App Data
                        </a>
                    </li>  
                </ul>
            </li>     
            <li class="treeview @if(Request::is('admin/featured-products')||Request::is('admin/store_products_import_data')||Request::is('admin/product_data_import')||Request::is('admin/order_detail*')|| Request::is('admin/order_detail/id')||Request::is('admin/orders*')||Request::is('admin/edit_pbrand/*') || Request::is('admin/add_pbrand/*') || Request::is('admin/pb_search/*') || Request::is('admin/manage_brands/*') || Request::is('admin/edit_vendor/*') || Request::is('admin/add_vendor') || Request::is('admin/pv_search*') || Request::is('admin/product-vendors') || Request::is('admin/edit_product/*') || Request::is('admin/add-product/*') || Request::is('admin/p_search/*') || Request::is('admin/products/*') || Request::is('admin/pc_search*') || Request::is('admin/product_categories') || Request::is('admin/pc_edit*') || Request::is('admin/add_product_cat*')) active @endif">
                <a href="#">
                    <i class="fa fa-link"></i> 
                    <span>E-Commerce</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">   
                    <li class="@if(Request::is('admin/edit_product/*') || Request::is('admin/add-product/*') || Request::is('admin/p_search/*') || Request::is('admin/products/*') || Request::is('admin/pc_search*') || Request::is('admin/product_categories') || Request::is('admin/add_product_cat*') || Request::is('admin/pc_edit*') || Session::get('activemenu') == 'product_categories' ) active @endif">
                        <a href="{{ url('admin/product_categories') }}">
                            Products
                        </a>
                    </li>

                    <li class="@if(Request::is('admin/store_products_import_data')||Request::is('admin/product_data_import')) active @endif">
                        <a href="{{ url('admin/product_data_import') }}">
                             Product Data Import
                        </a>
                    </li> 

                    {{-- <li class="@if(Request::is('admin/order_detail*')||Request::is('admin/order_detail/id')||Request::is('admin/orders')) active @endif">
                        <a href="{{ url('admin/orders') }}">
                             Orders
                        </a>
                    </li> --}}
                    <li class="@if(Request::is('admin/edit_pbrand/*') || Request::is('admin/add_pbrand/*') || Request::is('admin/pb_search/*') || Request::is('admin/manage_brands/*') || Request::is('admin/edit_vendor/*') || Request::is('admin/add_vendor') || Request::is('admin/pv_search*') || Request::is('admin/product-vendors*')) active @endif">
                        <a href="{{ url('admin/product-vendors') }}">
                             Vendors
                        </a>
                    </li> 
                    <li class="@if(Request::is('admin/featured-products')) active @endif">
                        <a href="{{ url('admin/featured-products') }}">
                             Featured Products
                        </a>
                    </li> 
                </ul>
            </li>    
            <li class="treeview @if(Request::is('admin/update_fashbrandc*')||Request::is('admin/get_catalogs*')||Request::is('admin/catalog')||Request::is('admin/addFashBrandSubcate*')||Request::is('admin/fashionBrandCollection*')||Request::is('admin/edit_brand*')||Request::is('admin/addFashBrand')||Request::is('admin/fashionBrand')||Request::is('admin/fashionCollection') || Request::is('admin/addFashCollection') || Request::is('admin/update_collection*') || Request::is('admin/addFashBrand') ) active @endif">
                <a href="#"> 
                    <i class="fa fa-link"></i> 
                    <span>Fashion</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a> 
                <ul class="treeview-menu">   
                    <li class="@if(Request::is('admin/fashionCollection') || Request::is('admin/addFashCollection') || Request::is('admin/update_collection*') ) active @endif">
                        <a href="{{ url('admin/fashionCollection') }}">
                            Collections
                        </a>
                    </li>   
                    <li class="@if(Request::is('admin/update_fashbrandc*')||Request::is('admin/addFashBrandSubcate*')||Request::is('admin/fashionBrandCollection*')||Request::is('admin/edit_brand*')||Request::is('admin/addFashBrand')||Request::is('admin/fashionBrand')||Request::is('admin/addFashBrand')) active @endif">
                        <a href="{{ url('admin/fashionBrand') }}">
                            Brands
                        </a>
                    </li> 
                    <li class="@if(Request::is('admin/get_catalogs*')||Request::is('admin/catalog')) active @endif">
                        <a href="{{ url('admin/catalog') }}">
                            Catalogs
                        </a>
                    </li>    
                </ul>
            </li>
            <li class="treeview @if(Request::is('admin/credit_admin') || Request::is('admin/storecredits/*') || Request::is('admin/coupons') || Request::is('admin/add_coupons') || Request::is('admin/send_coupon/*')) active @endif">
                <a href="#"> 
                    <i class="fa fa-link"></i> 
                    <span>Credit Admin</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a> 
                <ul class="treeview-menu">   
                    <li class="@if(Request::is('admin/credit_admin') || Request::is('admin/storecredits/*')) active @endif">
                        <a href="{{ url('admin/credit_admin') }}">
                            Store Credit
                        </a>
                    </li>   
                    <li class="@if(Request::is('admin/coupons') || Request::is('admin/add_coupons') || Request::is('admin/send_coupon/*')) active @endif">
                        <a href="{{ url('admin/coupons') }}">
                            Coupons
                        </a>
                    </li> 
                    <li class="@if(Request::is('admin/get_catalogs*')||Request::is('admin/catalog')) active @endif">
                        <a href="{{ url('admin/catalog') }}">
                            Campaingns
                        </a>
                    </li>    
                </ul>
            </li>
            <li class="@if(Request::is('province') || Request::is('edit_province/*') || Request::is('cities/*') || Request::is('edit_cities/*') || Request::is('add-cities/*')) active @endif">
                <a href="{{ url('province') }}">
                    <i class="fa fa-link"></i> 
                    <span>Province</span>
                </a>
            </li>
            <li class="@if(Request::is('salon-areas') || Request::is('add-area') || Request::is('edit_area/*')) active @endif">
                <a href="{{ url('salon-areas') }}">
                    <i class="fa fa-link"></i> 
                    <span>Salon Area Description</span>
                </a>
            </li>
            <li class="@if(Request::is('admin/add-seo') || Request::is('admin/seo_data') || Request::is('admin/edit_seo/*')) active @endif">
                <a href="{{ url('admin/seo_data') }}">
                    <i class="fa fa-link"></i> 
                    <span>SEO Data</span>
                </a>
            </li>
            <li class="@if(Request::is('user-management') || Session::get('activemenu')=='user-management') active @endif">
                <a href="{{ route('user-management.index') }}">
                    <i class="fa fa-link"></i> 
                    <span>User Management</span>
                </a>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>