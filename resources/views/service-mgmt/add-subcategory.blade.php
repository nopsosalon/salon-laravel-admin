
@extends('service-mgmt.base')
@section('action-content')

@push("css")
    <style type="text/css">

        input[type="file"]{
                    /*color: transparent;*/
                }
    </style>
@endpush

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Add Service Sub-Category</h3><br>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Add Sub Category</h3>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="{{ url('save_subcategory') }}" method="POST" role="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1"> Name</label>
                            <input required="" type="text" name="sub_cat_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Sub Category Name">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Keywords</label>
                            <input required="" type="text" name="ssc_keywords" class="form-control" id="exampleInputEmail1" placeholder="Enter Sub Category Keywords">
                        </div>
                    </div>
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label for="exampleInputPassword1">Select Category</label>
                            <select name="cat_name" class="form-control" id="cat_name">
                                 @foreach($categories as $category)
                                <option value="{{ $category->sc_id }}"> {{ $category->sc_name }}</option>
                                 @endforeach
                            </select>
                        </div>
                        @push("script")
                        <script type="text/javascript">
                            $("#cat_name").val("{{$sc_id}}");
                        </script>
                        @endpush
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Status</label>
                            <select name="cat_status" class="form-control">
                                <option value="1">Active</option>
                                <option value="0">In Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Posts URL</label>
                            <input required="" type="text" name="ssc_link" class="form-control" id="exampleInputEmail1" placeholder="Enter Posts URL">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Gender</label>
                            <select class="form-control" name="ssc_gender" id="ssc_gender">
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                                <option value="3">Both</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="exampleInputFile">Add Image</label>
                            <input name="cat_image" type="file" id="exampleInputFile" class="form-control" required="" onclick="fileClicked(event)" onchange="fileChanged(event)">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                   
                     <button style="margin-left: 10px;" type="reset" class="btn btn-primary">Reset</button>
                     
                    <a style="margin-left: 10px;" href="{{ url('manage_subcategores', ['id' => $sc_id])}}" class="btn btn-primary" >Cancel</a>
                    
                   
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection

@push("script")

    <script type="text/javascript">
        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })
    </script>

    <script>
    //This is All Just For Logging:
    var debug = true;//true: add debug logs when cloning
    var evenMoreListeners = true;//demonstrat re-attaching javascript Event Listeners (Inline Event Listeners don't need to be re-attached)
    if (evenMoreListeners) {
        var allFleChoosers = $("input[type='file']");
        addEventListenersTo(allFleChoosers);
        function addEventListenersTo(fileChooser) {
            fileChooser.change(function (event) { console.log("file( #" + event.target.id + " ) : " + event.target.value.split("\\").pop()) });
            fileChooser.click(function (event) { console.log("open( #" + event.target.id + " )") });
        }
    }
    (function () {
        var old = console.log;
        var logger = document.getElementById('log');
        console.log = function () {
            for (var i = 0; i < arguments.length; i++) {
                if (typeof arguments[i] == 'object') {
                    logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
                } else {
                    logger.innerHTML += arguments[i] + '<br />';
                }
            }
            old.apply(console, arguments);
        }
    })();

    var clone = {};

    // FileClicked()
    function fileClicked(event) {
        var fileElement = event.target;
        if (fileElement.value != "") {
            // if (debug) { console.log("Clone( #" + fileElement.id + " ) : " + fileElement.value.split("\\").pop()) }
            clone[fileElement.id] = $(fileElement).clone(); //'Saving Clone'
        }
        //What ever else you want to do when File Chooser Clicked
    }

    // FileChanged()
    function fileChanged(event) {
        var fileElement = event.target;
        if (fileElement.value == "") {
            // if (debug) { console.log("Restore( #" + fileElement.id + " ) : " + clone[fileElement.id].val().split("\\").pop()) }
            clone[fileElement.id].insertBefore(fileElement); //'Restoring Clone'
            $(fileElement).remove(); //'Removing Original'
            if (evenMoreListeners) { addEventListenersTo(clone[fileElement.id]) }//If Needed Re-attach additional Event Listeners
        }
        //What ever else you want to do when File Chooser Changed
    }



        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })
    </script>

@endpush