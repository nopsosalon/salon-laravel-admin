
@extends('service-mgmt.base')
@section('action-content')

<section class="content">



        <form action="{{ url('updateServiceSubCat')}}" method="POST" role="form" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="box-header">
                <div class="row">
                    <div class="col-sm-8">
                        <h3 class="box-title">Services</h3>
                    </div>
                    <div class="col-sm-4">


                        <button name="submit" type="submit" class="btn btn-info">Update</button>
  <a style="margin-left: 10px;" href="{{ url('manage_subcategores', ['id' => $sc_id])}}" class="btn btn-primary" >Cancel</a>
                   
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <?php
                foreach ($allServices as $value) {

                    if (array_key_exists("checked", $value)) {

                        $isChecked = $value->checked;
                    } else {
                        $isChecked = '';
                        // 
                    }
                    ?>
                    <div class="col-md-3">
                        <div class="checkbox">
                            
                            <label hidden>
                                <input type="text" 
                                       value="<?php echo $ssc_id ?>"
                                       name="ssc_id">   
                            </label>
                            <label hidden>
                                <input type="text" 
                                       value="<?php echo $sc_id ?>"
                                       name="sc_id">   
                            </label>
                            
                            
                            <label>
                                <input name="sal_type_status[]" value=" 
                                       <?php echo $value->ser_id; ?>"
                                       <?php echo $isChecked; ?>
                                       type="checkbox">
                                       <?php echo $value->ser_name; ?>
                            </label>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </form>
        <!-- /.box-body -->
    </div>
</section>
@endsection