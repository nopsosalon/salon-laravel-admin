@extends('service-mgmt.base')
@section('action-content')


<section class="content">
  <div class="row">
        <div class="col-sm-12">
            <h3>Service Categories</h3><br>
        </div>
    </div>
    <div style="margin-bottom: 30px;">
        <form role="form" method="get" action="{{ url('servicecategories') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Service Category
                          </label>
                          <input placeholder="Service category"  type="text" class="form-control" value="@isset($service_cat) {{$service_cat}}  @endisset" name="service_cat" maxlength="128">
                        </div>
                      </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>
                            Is Active  
                        </label>
                        <select class="form-control" name="is_active" id="is_active">
                          <option value='all_active' @if($is_active == "all_active") selected="checked" @endif >All</option>
                          <option value='1' @if($is_active == 1) selected="checked" @endif>Yes</option>
                          <option value='2' @if($is_active == 2) selected="checked" @endif>No</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>

        @if(!empty($service_cat) OR !empty($is_active))
                    @php

                      if(is_null($service_cat))
                        {
                          $service_cat = "";
                        } 
                        if(is_null($is_active))
                        {
                          $is_active = "";
                        }
                        $urls = ('servicecategories?'.'service_cat'.'='.$service_cat.'&'.'is_active'.'='.$is_active.'&'.'page='.$categories->currentPage());

                    @endphp
                  @else
                    @php
                       $urls = ('servicecategories?'.'page='.$categories->currentPage());
                    @endphp
                @endif


    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Categories</h3>
                </div>
                <div class="col-sm-4">
                    <a class="btn btn-info" href="{{ url('addcategory') }}">Add new Category</a>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody><tr>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Gender</th>
                        <th>Is Active</th>
                        <th>Action</th>
                    </tr>
                    <?php foreach ($categories as $category) { ?>
                        <tr>
                            <td><?php echo $category->sc_name ?></td>

                            <?php // $backslash = "/"; ?>
                            <td><img src="{{$cate_images}}{{$category->sc_image ? $category->sc_image : 'placeholder.png'}}" width="100" height="100"></td>
                            <td>
                              @if($category->sc_gender == 2)
                                Female
                              @elseif($category->sc_gender == 1)
                                Male
                              @endif
                            </td>
                            <td><?php echo $category->sc_status == 1 ? 'Yes' : 'No'; ?></td>
                            <td> <a href="{{ url('edit_category', ['id' => $category->sc_id,'urls'=>$urls])}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i></a> 
                                <a style="margin-left: 10px;" href="{{ url('manage_subcategores', ['id' => $category->sc_id,'urls'=>$urls])}}" class="btn btn-info">Manage Sub Categories</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

            {!! $categories->appends(['service_cat'=>isset($service_cat)?$service_cat:'','is_active'=>isset($is_active)?$is_active:'','page'=>$categories->currentPage()])->links() !!}
            
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection