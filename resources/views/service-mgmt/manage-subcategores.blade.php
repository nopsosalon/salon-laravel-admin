@extends('service-mgmt.base')

@section('action-content')

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Manage Service Sub-Category</h3><br>
        </div>
    </div>

    <div class="box box-primary">
        <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="{{$cate_images}}{{$category->sc_image ? $category->sc_image : 'placeholder.png' }}"  alt="User profile picture">
            <h3 class="profile-username text-center">{{ $category->sc_name }}</h3>
            <div class="col-sm-2 pull-right">
                <h1 style="margin-top: -100px;">
                    <a href="{{url($returnback)}}">Back</a>
                </h1>
            </div>
        </div>
    </div>

    <div style="margin-bottom: 30px;">
        <form role="form" method="get" action="{{ url('manage_subcategores'.'/'.$sc_id.'/'.$returnback) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Sub Category
                          </label>
                          <input placeholder="Sub category"  type="text" class="form-control" value="@isset($sub_cacte) {{$sub_cacte}}  @endisset" name="sub_cacte" maxlength="128">
                        </div>
                      </div>


                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>
                            Is Active  
                        </label>
                        <select class="form-control" name="is_active" id="is_active">
                          <option value='all_active' @if($is_active == "all_active") selected="checked" @endif>All</option>
                          <option value='1' @if($is_active == "1") selected="checked" @endif>Yes</option>
                          <option value='2' @if($is_active == "2") selected="checked" @endif>No</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Sub Categories</h3>
                </div>
                <div class="col-sm-4">
                    <a class="btn btn-info" href="{{ url('add_subcategory', ['sc_id' => $sc_id]) }}">Add new Sub Category</a>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody>
                @isset($sub_categories)
                    @if(!empty($sub_categories))
                    <tr>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Gender</th>
                        <th>Is Active</th>
                        <th>Action</th>
                    </tr>
                          @foreach($sub_categories as $sub_category)
                    <tr>
                        <td>{{$sub_category->ssc_name}}</td>
                        <td>
                            
                            <img src="{{$cate_images}}{{$sub_category->ssc_image ? $sub_category->ssc_image : 'placeholder.png'}}" width="100" height="100">
                        </td>
                        <td>
                            @if($sub_category->ssc_gender==1)
                                <p>Male</p>
                            @elseif($sub_category->ssc_gender==2)
                                <p>Female</p>
                            @elseif($sub_category->ssc_gender==3)
                                Both
                            @endif
                        </td>
                        <td>
                            @if($sub_category->ssc_status==1)
                                yes
                            @else
                                No
                            @endif
                        </td>
                        <td>
                            <a   href="{{ url('edit_sub_category' , [ 'ssc_id' =>  $sub_category->ssc_id, 'sc_id' => $sc_id] )}}" class="btn btn-info"> <i class="glyphicon glyphicon-edit"></i>  </a> 
                            <a style="margin-left: 10px;" href="{{ url('category_association' , ['sc_id' => $sub_category->sc_id, 'ssc_id' =>  $sub_category->ssc_id, 'sc_name' => $category->sc_name ] )}}" class="btn btn-info">Category for association</a>
                            {{-- <a style="margin-left: 10px;" href="{{ url('manageservices' , ['sc_id' => $sub_category->sc_id, 'ssc_id' =>  $sub_category->ssc_id] )}}" class="btn btn-info">manageservices</a> --}}
                        </td>
                    </tr>
                    @endforeach
                    @else
                        <h2></h2>Data not found!
                    @endif
                    @endisset
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection