@extends('salon-magmt.base') 
@section('action-content')

@push("css")
    <style type="text/css">

            input[type="file"]{
                    /*color: transparent;*/
                }

    </style>
@endpush
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Salon Data Upload</h3><br>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <div class="box">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" id="myTab">

                        <li id="one" class="@if(Session::get('activemenu') == 'fileupload' || Session::get('activemenu') == 'save_file')  active @endif">
                            <a href="#tab_1" data-toggle="tab" aria-expanded="true">
                                Salon
                            </a>
                        </li>
                        <li class="@if(Session::get('activemenu')=='save_file_services') active @endif">
                            <a href="#tab_2" data-toggle="tab" aria-expanded="false">
                                Salon Services
                            </a>
                        </li>
                        <li class="@if(Session::get('activemenu')=='temp_save_file') active @endif">
                            <a href="#tab_3" data-toggle="tab" aria-expanded="true">
                                New Salon
                            </a>
                        </li>
                        <li class="@if(Session::get('activemenu')=='temp_save_file_services') active @endif">
                            <a href="#tab_4" data-toggle="tab" aria-expanded="false">
                                New Salon Services
                            </a>
                        </li>

                        <!--                        <li id="three" class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Salon Services</a></li>-->
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane @if(Session::get('activemenu') == 'fileupload'|| Session::get('activemenu') == 'save_file') active @endif" id="tab_1">
                            <div class="box-header">
                                <h3 class="box-title"> 
                                
                                    Import Salon data  {{Session::get("activemenu")}}
                               
                                </h3> 
                                <!--<p>All fields are required* </p>-->
                            </div>
                            <div class="box-body">
                                <div>
                                    <div class="">
                                            @if(Session::get('change_db_connection')=="2")
                                                <form action="{{ url('save_file_pk') }}" method="POST" role="form" enctype="multipart/form-data">
                                            @else
                                                <form action="{{ url('save_file') }}" method="POST" role="form" enctype="multipart/form-data">
                                            @endif
                                        
                                            {{ csrf_field() }}
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">File Name</label>
                                                    <input required="" type="text" name="cat_name" class="form-control" id="exampleInputEmail1" placeholder="Enter file Name">
                                                
                                                </div>
                                                <div class="form-group">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputFile">Add File</label>
                                                    <input required="" name="cat_image" type="file" id="exampleInputFile" class="form-control" onclick="fileClicked(event)" onchange="fileChanged(event)">


                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                            @if(Session::has('salon_flash_message'))
                                            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('salon_flash_message') !!}</em></div>
                                            <br>
                                            <br>
                                            @endif

                                            @if(Session::has('error_message'))
                                                <div class="alert alert-danger">
                                                    <span class="glyphicon glyphicon-crox"></span>
                                                    <em> {!! session('error_message') !!}</em>
                                                </div>
                                                <br>
                                                <br>
                                            @endif


                                            {{-- display error messages  start code --}}
                                    <div class="row">
                                       @isset($salon_data)
                                               @php
                                                 $sal_data = trim($salon_data,",");
                                                 $salonid = explode(",", $sal_data); 
                                               @endphp
                                        @if(count($salonid) > 1)
                                            @if(Session::has('salon_error_message'))
                                               <div class="col-md-12"> 
                                                    <div class="alert alert-danger">
                                                        <span class="glyphicon glyphicon-crox"></span>
                                                        <em> {!! session('salon_error_message') !!}</em>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                            <div class="col-md-12 pull-left">
                                                <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <th style="width: 10px">#</th>
                                                                <th>Salon Id</th>
                                                                <th>Salon Name</th>
                                                                <th style="width: 40px">Problem In Sal_Hour</th>
                                                            </tr> 
                                                            @foreach($salonid as $key => $salid)
                                                                @php 
                                                                    $key += 1;
                                                                    $salonss = DB::table('salon')->where('sal_id',$salid)->first();
                                                                @endphp
                                                                @if(!empty($salonss))
                                                                    <tr>
                                                                        <td>{{$key}}</td>
                                                                        <td>{{$salonss->sal_id}}</td>
                                                                        <td>{{$salonss->sal_name}}</td>
                                                                        <td>{{$salonss->sal_hours}}</td>
                                                                    </tr> 
                                                                @endif
                                                            @endforeach 
                                                      </tbody>
                                                </table>
                                            </div>
                                        @endif
                                    @endisset                                         
                                    </div>
                                {{-- end code display error messages  --}}

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <a hidden="" class="btn btn-primary" style="margin-left: 10px;" href="{{ url('') }}" >Cancel</a>
                                            </div>
                                        </form>

                                    </div>
                                    <?php
//                                $attributes = array("name" => "services", "id" => "service_form");
//                                echo form_open("admin/update_services/", $attributes);
                                    ?>

                                    <form role="form" method="POST" action="{{ url('type_services_update') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->

                        <div class="tab-pane @if(Session::get('activemenu')=='save_file_services') active @endif" id="tab_2">
                             <div class="box-header">
                                <h3 class="box-title"> 
                                    Import Salon Services data 
                                </h3>
                                <!--<p>All fields are required* </p>-->

                            </div>
                            @if(Session::get('change_db_connection')=="2")
                                <form action="{{ url('save_file_services_pk') }}" method="POST" role="form" enctype="multipart/form-data">
                            @else
                             <form action="{{ url('save_file_services') }}" method="POST" role="form" enctype="multipart/form-data">
                            @endif
                                {{ csrf_field() }} 
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">File Name</label>
                                        <input required="" type="text" name="cat_name" class="form-control" id="exampleInputEmail1" placeholder="Enter file Name">
                                    </div>
                                    <div class="form-group">

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Add File</label>
                                        <input required="" name="cat_image" type="file" id="exampleInputFile" class="form-control" onclick="fileClicked(event)" onchange="fileChanged(event)">
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                @if(Session::has('service_flash_message'))
                                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('service_flash_message') !!}</em></div>
                                <br>
                                <br>
                                @endif
                                
                                {{-- display error messages  start code --}}
                                    <div class="row">
                                        @if(Session::has('service_error_message'))
                                           <div class="col-md-12"> 
                                                <div class="alert alert-danger">
                                                    <span class="glyphicon glyphicon-crox"></span>
                                                    <em> {!! session('service_error_message') !!}</em>
                                                </div>
                                            </div>
                                        @endif
                                       
                                        
                                        <div class="col-md-12 pull-left">
                                            @isset($saldata)
                                               @php
                                                 $sal_data = trim($saldata,",");
                                                 $salonid = explode(",", $sal_data); 
                                                 $tmpid = 0;
                                               @endphp
                                                @if(count($salonid) > 1)
                                                    <table class="table table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <th style="width: 10px">#</th>
                                                                    <th>Salon Id</th>
                                                                    <th>Salon Name</th>
                                                                    <th style="width: 40px">Problem In Sal_Hour</th>
                                                                </tr> 
                                                                @foreach($salonid as $key => $salid)
                                                                    @php 
                                                                        $key += 1;
                                                                        $salonss = DB::table('salon')->where('sal_id',$salid)->first();
                                                                    @endphp
                                                                    @if(!empty($salonss))
                                                                        @if($salonss->sal_id !== $tmpid)
                                                                           @php $tmpid = $salonss->sal_id; @endphp
                                                                            <tr>
                                                                                <td>{{$key}}</td>
                                                                                <td>{{$salonss->sal_id}}</td>
                                                                                <td>{{$salonss->sal_name}}</td>
                                                                                <td>{{$salonss->sal_hours}}</td>
                                                                            </tr> 
                                                                        @endif
                                                                    @endif
                                                                @endforeach 
                                                          </tbody>
                                                    </table>
                                                @endif
                                            @endisset
                                        </div>
                                    </div>
                                {{-- end code display error messages  --}}

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a hidden="" class="btn btn-primary" style="margin-left: 10px;" href="{{ url('') }}" >Cancel</a>
                                </div>
                            </form>
                        </div>
                        <br>
                        <div class="clearfix"></div>



                        <div class="tab-pane @if(Session::get('activemenu') == 'temp_save_file') active @endif" id="tab_3">
                            <div class="box-header">
                                <h3 class="box-title"> 
                                    Import New Salon data 
                                </h3>
                                <!--<p>All fields are required* </p>-->
                            </div>
                            <div class="box-body">
                                <div>
                                    <div class="">
                                    @if(Session::get('change_db_connection')=="2")
                                        <form action="{{ url('temp_save_file_salon_pk') }}" method="POST" role="form" enctype="multipart/form-data">
                                    @else
                                        <form action="{{ url('temp_save_file_salon') }}" method="POST" role="form" enctype="multipart/form-data">
                                    @endif
                                            {{ csrf_field() }}
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">File Name</label>
                                                    <input required="" type="text" name="cat_name" class="form-control" id="exampleInputEmail1" placeholder="Enter file Name">
                                                    
                                                </div>
                                                <div class="form-group">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputFile">Add File</label>
                                                    <input required="" name="cat_image" type="file" id="exampleInputFile" class="form-control" onclick="fileClicked(event)" onchange="fileChanged(event)">
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                            @if(Session::has('new_salon_flash_message'))
                                            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('new_salon_flash_message') !!}</em></div>
                                            <br>
                                            <br>
                                            @endif

                                            @if(Session::has('new_error_message'))
                                                <div class="alert alert-danger">
                                                    <span class="glyphicon glyphicon-crox"></span>
                                                    <em> {!! session('new_error_message') !!}</em>
                                                </div>
                                                <br>
                                                <br>
                                            @endif


                                            {{-- display error messages  start code --}}
                                    <div class="row">
                                       @isset($salon_data)
                                               @php
                                                 $sal_data = trim($salon_data,",");
                                                 $salonid = explode(",", $sal_data); 
                                               @endphp
                                        @if(count($salonid) > 1)
                                            @if(Session::has('new_salon_error_message'))
                                               <div class="col-md-12"> 
                                                    <div class="alert alert-danger">
                                                        <span class="glyphicon glyphicon-crox"></span>
                                                        <em> {!! session('new_salon_error_message') !!}</em>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                            <div class="col-md-12 pull-left">
                                                <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <th style="width: 10px">#</th>
                                                                <th>Salon Id</th>
                                                                <th>Salon Name</th>
                                                                <th style="width: 40px">Problem In Sal_Hour</th>
                                                            </tr> 
                                                            @foreach($salonid as $key => $salid)
                                                                @php 
                                                                    $key += 1;
                                                                    $salonss = DB::table('salon')->where('sal_id',$salid)->first();
                                                                @endphp
                                                                @if(!empty($salonss))
                                                                    <tr>
                                                                        <td>{{$key}}</td>
                                                                        <td>{{$salonss->sal_id}}</td>
                                                                        <td>{{$salonss->sal_name}}</td>
                                                                        <td>{{$salonss->sal_hours}}</td>
                                                                    </tr> 
                                                                @endif
                                                            @endforeach 
                                                      </tbody>
                                                </table>
                                            </div>
                                        @endif
                                    @endisset                                         
                                    </div>
                                {{-- end code display error messages  --}}

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <a hidden="" class="btn btn-primary" style="margin-left: 10px;" href="{{ url('') }}" >Cancel</a>
                                            </div>
                                        </form>

                                    </div>
                                   


                                </div>
                            </div>
                        </div>


                        <div class="clearfix"></div>
                        
                        <div class="tab-pane @if(Session::get('activemenu') == 'temp_save_file_services') active @endif" id="tab_4">
                            <div class="box-header">
                                <h3 class="box-title"> 
                                    Import New Salon Services data 
                                </h3>
                                <!--<p>All fields are required* </p>-->
                            </div>
                            <div class="box-body">
                                <div>
                                    <div class="">
                                    @if(Session::get('change_db_connection')=="2")
                                        <form action="{{ url('temp_save_file_services_pk') }}" method="POST" role="form" enctype="multipart/form-data">
                                    @else
                                        <form action="{{ url('temp_save_file_services') }}" method="POST" role="form" enctype="multipart/form-data">
                                    @endif
                                            {{ csrf_field() }}
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">File Name</label>
                                                    <input required="" type="text" name="cat_name" class="form-control" id="exampleInputEmail1" placeholder="Enter file Name">
                                                </div>
                                                <div class="form-group">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputFile">Add File</label>
                                                    <span class="control-fileupload">
                                                     
                                                        <input required="" name="cat_image" type="file" id="exampleInputFile" class="form-control vendor_logo_hide" placeholder="Please Selefile" onclick="fileClicked(event)" onchange="fileChanged(event)">
                                                 
                                                    
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                            @if(Session::has('new_service_flash_message'))
                                            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('new_service_flash_message') !!}</em></div>
                                            <br>
                                            <br>
                                            @endif

                                            {{-- @if(Session::has('error_message'))
                                                <div class="alert alert-danger">
                                                    <span class="glyphicon glyphicon-crox"></span>
                                                    <em> {!! session('error_message') !!}</em>
                                                </div>
                                                <br>
                                                <br>
                                            @endif --}}


                                            {{-- display error messages  start code --}}
                                    <div class="row">
                                       @isset($salon_data)
                                               @php
                                                 $sal_data = trim($salon_data,",");
                                                 $salonid = explode(",", $sal_data); 
                                               @endphp
                                        @if(count($salonid) > 1)
                                            @if(Session::has('new_service_error_message'))
                                               <div class="col-md-12"> 
                                                    <div class="alert alert-danger">
                                                        <span class="glyphicon glyphicon-crox"></span>
                                                        <em> {!! session('new_service_error_message') !!}</em>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                            <div class="col-md-12 pull-left">
                                                <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <th style="width: 10px">#</th>
                                                                <th>Salon Id</th>
                                                                <th>Salon Name</th>
                                                                <th style="width: 40px">Problem In Sal_Hour</th>
                                                            </tr> 
                                                            @foreach($salonid as $key => $salid)
                                                                @php 
                                                                    $key += 1;
                                                                    $salonss = DB::table('salon')->where('sal_id',$salid)->first();
                                                                @endphp
                                                                @if(!empty($salonss))
                                                                    <tr>
                                                                        <td>{{$key}}</td>
                                                                        <td>{{$salonss->sal_id}}</td>
                                                                        <td>{{$salonss->sal_name}}</td>
                                                                        <td>{{$salonss->sal_hours}}</td>
                                                                    </tr> 
                                                                @endif
                                                            @endforeach 
                                                      </tbody>
                                                </table>
                                            </div>
                                        @endif
                                    @endisset                                         
                                    </div>
                                {{-- end code display error messages  --}}

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <a hidden="" class="btn btn-primary" style="margin-left: 10px;" href="{{ url('') }}" >Cancel</a>
                                            </div>
                                        </form>

                                    </div>
  


                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    </section>
@endsection

@push("script")
    <script type="text/javascript">
        // $(function() {
        //     $('a[data-toggle="tab"]').on('click', function(e) {
        //         window.localStorage.setItem('activeTab', $(e.target).attr('href'));
        //     });
        //     var activeTab = window.localStorage.getItem('activeTab');
        //     if (activeTab) {
        //         $('#myTab a[href="' + activeTab + '"]').tab('show');
        //         window.localStorage.removeItem("activeTab");
        //     }
        // });

            

    </script>


                                  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
<script>
    //This is All Just For Logging:
    var debug = true;//true: add debug logs when cloning
    var evenMoreListeners = true;//demonstrat re-attaching javascript Event Listeners (Inline Event Listeners don't need to be re-attached)
    if (evenMoreListeners) {
        var allFleChoosers = $("input[type='file']");
        addEventListenersTo(allFleChoosers);
        function addEventListenersTo(fileChooser) {
            fileChooser.change(function (event) { console.log("file( #" + event.target.id + " ) : " + event.target.value.split("\\").pop()) });
            fileChooser.click(function (event) { console.log("open( #" + event.target.id + " )") });
        }
    }
    (function () {
        var old = console.log;
        var logger = document.getElementById('log');
        console.log = function () {
            for (var i = 0; i < arguments.length; i++) {
                if (typeof arguments[i] == 'object') {
                    logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
                } else {
                    logger.innerHTML += arguments[i] + '<br />';
                }
            }
            old.apply(console, arguments);
        }
    })();

    var clone = {};

    // FileClicked()
    function fileClicked(event) {
        var fileElement = event.target;
        if (fileElement.value != "") {
            // if (debug) { console.log("Clone( #" + fileElement.id + " ) : " + fileElement.value.split("\\").pop()) }
            clone[fileElement.id] = $(fileElement).clone(); //'Saving Clone'
        }
        //What ever else you want to do when File Chooser Clicked
    }

    // FileChanged()
    function fileChanged(event) {
        var fileElement = event.target;
        if (fileElement.value == "") {
            // if (debug) { console.log("Restore( #" + fileElement.id + " ) : " + clone[fileElement.id].val().split("\\").pop()) }
            clone[fileElement.id].insertBefore(fileElement); //'Restoring Clone'
            $(fileElement).remove(); //'Removing Original'
            if (evenMoreListeners) { addEventListenersTo(clone[fileElement.id]) }//If Needed Re-attach additional Event Listeners
        }
        //What ever else you want to do when File Chooser Changed
    }



        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })
    </script>

    <script type="text/javascript">
        $(function() {
          $('input[type=file]').change(function(){
            var t = $(this).val();
            var labelText = 'File : ' + t.substr(12, t.length);
            $(this).prev('label').text(labelText);
          })
        });


// active tabs keep selected tabs bootstraps
        $('a[data-toggle="tab"]').click(function (e) {
          e.preventDefault()
          $(this).tab('show')
        });
        $(function () {
          // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
          $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab', $(this).attr('href'));
          });
          // go to the latest tab, if it exists:
          var lastTab = localStorage.getItem('lastTab');
          if (lastTab) {
            $('#myTab li.active,.tab-pane.active').removeClass('active');
            $('#myTab a[href="' + lastTab + '"]').closest('li').addClass('active');
            $(lastTab).addClass('active');
          }
        });

    </script>



@endpush

