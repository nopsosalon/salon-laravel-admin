@extends('salon-magmt.base')
@section('action-content')

<section class="content">
    <div class="row">
    <div class="col-sm-12">
      <h3>Update Salon Services</h3>
    </div>
  </div>
    <div class="box">

        <form role="form" method="POST" action="{{ url('updateSalonServices') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <table class="table table-hover">
                <?php
                

                    if ($salon_services_salon->sser_enabled == '1') {
                        $status = 'checked';
                    } else {
                        $status = '';
                    }
                    ?>
                    <input type="hidden" class="form-control" name="returnback" value="{{$returnback}}">
                    <tbody>
                        <!-- style= "background-color: #84ed86; color: #761a9b; margin-top: 200px;" -->
                        <tr style="border:1pt;">
                            <th style="border:1pt;">Name</th>
                            <th style="border:1pt;">Status</th>
                        </tr>
                    <td style="border:1pt;">
                        <input type="text" value="<?php echo $salon_services_salon->sser_name; ?>" name="sser_name" style="width: 60%; padding:8px;">
                    </td>

                    <td style="border:1pt;">
                        @if($salon_services_salon->sser_enabled == 1)
                        <input style=" width: 30px; padding:0px;" name="sal_type_status" value="1" type="checkbox" checked="">
                        @else
                        <input style=" width: 30px; padding:0px;" name="sal_type_status" value="0" type="checkbox">
                        @endif

                               {{ 'Active' }} 
                    </td>
                    <tr style="border:1pt;">
                        <th style="border:1pt;">Rate</th>
                        <th style="border:1pt;">Featured</th>
                    </tr>

                    <td style="border:1pt;">
                        <input type="number"  placeholder="0" value="<?php echo $salon_services_salon->sser_rate; ?>" name="sser_rate" style="width: 60%; padding:8px;">

                    </td>
                    <td style="border:1pt;">
                        @if($salon_services_salon->sser_featured == 1)
                            <input style=" width: 30px; padding:0px;" name="sal_type_feature" value="1" type="checkbox" checked="" > 
                        @else
                            <input style=" width: 30px; padding:0px;" name="sal_type_feature" value="0" type="checkbox" >
                        @endif        
                               {{ 'Active' }}  
                            
                    </td>



                    <tr style="border:1pt;">
                        <th style="border:1pt;">Time</th>

                    </tr>
                    <td style="border:1pt;">
                        <input type="number" placeholder="0" value="<?php echo $salon_services_salon->sser_time; ?>" name="sser_time" style="width: 60%; padding:8px;">
                    </td>


                    <tr style="border:1pt;">
                        <th style="border:1pt;">Order</th>

                    </tr>
                    <td style="border:1pt;">
                        <input type="number"  placeholder="0" value="<?php echo $salon_services_salon->sser_order; ?>" name="sser_order" style="width: 60%; padding:8px;">
                    </td>

                    <tr style="border:1pt;">
                        <th style="border:1pt;">Salon Service {{-- {{$salon_services_salon->ssc_id}} --}}</th>

                    </tr>
                    <td style="border:1pt;">
                        <select name="ssc_id" style="width: 60%; padding:8px;" id="sscids" required>
                            @isset($salon_ser_vices)
                            @foreach($salon_ser_vices as $key => $value)
                                <option value="{{$value->ssc_id}}" >{{$value->ssc_name}}</option>
                            @endforeach
                            @endisset
                        </select>

                    </td>





                    <td style="border:1pt;">
                        <input type="hidden" value="<?php echo $salon_services_salon->sser_id; ?>" name="sser_id" style="width: 60%;height:80px;">
                        <input type="hidden" value="<?php echo $salId; ?>" name="salId" style="width: 60%;height:80px;">
                    </td>
                    </tbody>
                
            </table>

            <button style=" margin: 10px;" type="submit" name="SubmitFashion" class="btn btn-primary">Update</button>
         <button style=" margin: 10px;" type="reset" name="SubmitFashion" class="btn btn-primary">Reset</button>
           <a style=" margin: 10px;" href="{{$returnback}}" class="btn btn-primary">Cancel </a> 

        </form>

    </div>
</section>

@endsection


@push('script')
    
 <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

<script>
  @if(Session::has('salnserviceupdate'))
  
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('salnserviceupdate') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('salnserviceupdate') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('salnserviceupdate') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('salnserviceupdate') }}");
            break;
    }
  @endif

</script>

    <script type="text/javascript">
        $("#sscids").val("{{$salon_services_salon->ssc_id}}");
    </script>
                            
@endpush