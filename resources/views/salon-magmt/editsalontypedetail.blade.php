@extends('salon-magmt.base')

@section('action-content')
<section class="content">

    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <div class="box">

                <form action="{{ url('updatesalontypeservices')}}" method="POST" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="nav-tabs-custom">

                        <!--                    <ul class="nav nav-tabs">
                                                
                                                <li id="one" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Services</a></li>
                                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Salon Info</a></li>
                                                <li id="three" class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Add Service</a></li>
                                                
                                            </ul>-->

                        <div class="tab-content">

                            <div class="tab-pane active" id="tab_1">

                                <!--                            <div class="box-header">
                                                                <h3 class="box-title">Edit Salon</h3>
                                                                <p>All fields are required* </p>
                                                            </div>-->

                                <div class="box-body">
                                    <div class="row">

                                        <div class="" style="margin-left: 10px;" >
                                            <div class="form-group">

                                                <label style="font-size: 16px">  <?php echo $salon_name    ?> Services</label>

                                                <div class="row">

                                                    <?php
                                                    foreach ($allServices as $value) {
                                                        
                                                        if (array_key_exists("checked",$value)){
                                                          
                                                            $isChecked = $value->checked;   
                                                        }else{
                                                              $isChecked = '';
                                                        // 
                                                        }
                                                        //$isChecked = $value->checked;
//                                                        if (emptyString($value->checked)) {
//                                                            
//                                                        }  
//                                                       if (empty($isChecked)) {
//                                                           $isChecked='';
//                                                       }
                                                        ?>
                                                        <div class="col-md-3">
                                                            <div class="checkbox">
                                                                <label>
                                                                   
                                                                    <label hidden>
                                                                        <input type="text" 
                                                                               value="<?php echo $sty_id ?>"
                                                                               name="sty_id">   
                                                                      
                                                                    </label>
                                                                    
                                                                    <input name="sal_type_status[]" value=" 
                                                                           <?php echo $value->ser_id; ?>"
                                                                           <?php echo $isChecked; ?>
                                                                           type="checkbox">
                                                                           <?php echo $value->ser_name; ?>
                                                                    
                                                                </label>
                                                            </div>
                                                        </div>
                                                    
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button name="submit" type="submit" class="btn btn-primary">Update</button>
                                             <a style=" margin-left: 15px; margin-right: 8px;" class="btn btn-primary" href="{{ url('salon_types') }}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@endsection
