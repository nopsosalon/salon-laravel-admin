@extends('salon-magmt.base')

    @push("css")
        <style type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></style>
        <style type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"></style>
      
      <style type="text/css">
        .pagination{
            margin: 2px 0;
            white-space: nowrap;
        }
        .pagination>li{
              display: inline;
        }
      </style>

    @endpush
@section('action-content')
<section class="content">
  <div class="row">
        <div class="col-sm-12">
            <h3>Services</h3><br>
        </div>
    </div>
    {{-- <script src="https://code.jquery.com/jquery-1.10.2.js"></script> --}}

    <script>
        function myFunction() {
            var r = confirm("Press a button!");
            var txt;
            if (r == true) {
            txt = "You pressed OK!";
                    //   confirm("You Press a button!");
                    $deleteService(1);
                    txt = "You pressed OK!";
                    return true;
            } else {
            txt = "You pressed Cancel!";
                    return false;
            }
                < ?
                       $session(['sser_id' => '191']);
                 ? >
                        // document.getElementById("ser_id").innerHTML = txt;
            }
    </script>    
     @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif
    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <div  style="margin:0 auto;
                  width:100%;
                  height:50px;">
                    <div style="margin:0 auto;
                         height:50px;
                         float: right;
                         "class="div1">
                        <a href="{{url('addNewService')}}" style="padding:9px; margin-top: -4px; margin-left: 5px; margin-right: 5px;" name="submit"  class="btn btn-info" >Add New Service</a>    
                    </div>
            </div>
            <div class="row">
                <form method="get" action="{{ url('allServices') }}">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Service Name</label>
                            <input type="text" class="form-control" name="ser_name" value="@if(!empty($ser_name)) {{$ser_name}} @endif">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="submit" value="Search" class="btn btn-primary btn-lg" style="margin-top: 25px; padding: 3px 19px;">
                        </div>
                    </div>
                </form>
            </div> 
              {{-- @if(!empty($ser_name))
                @php  
                  if(!empty($ser_name))
                    {
                      $ser_name = $ser_name;
                    }
                    else
                    {
                        $ser_name = "";
                    }
                    $urls = ('allServices?'.'ser_name'.'='.$ser_name.'&'.'page='.$allServices->currentPage());
                @endphp
              @else
                @php
                   $urls = ('allServices?'.'page='.$allServices->currentPage());
                @endphp
            @endif  --}} 
                {{-- data table start code here  --}}
                          {{-- <div class="row">
                            <div class="col-md-12" style="margin-top: 50px;">
                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                  <thead>
                                      <tr>
                                          <th>#</th>
                                          <th>Services</th>
                                          <th>Action</th>
                                          
                                      </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($allServices as $key => $value)
                                      @php $key += 1; @endphp
                                    <tr>
                                        <td>
                                          {{$key}}
                                        </td>
                                        <td>
                                          {{$value->ser_name}}
                                        </td>
                                        <td>
                                          <a href="{{ url('editserviceinfo' , ['id' => $value->ser_id,'urls'=>$urls]) }}" class="btn btn-info btn-lg" style="margin-left: 10px;">
                                            <i class="glyphicon glyphicon-edit"></i> 
                                          </a>
                                          <a  href="{{ url('deleteService', ['id' => $value->ser_id]) }}" class="btn btn-danger btn-lg" style="margin-left: 10px;" >
                                              <i class="glyphicon glyphicon-trash"></i> 
                                          </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                  </tbody>
                                  <tfoot>
                                      <tr>
                                          <th>#</th>
                                          <th>Services</th>
                                          <th>Action</th>
                                      </tr>
                                  </tfoot>
                              </table>
                            </div>
                          </div> --}}
                          {{-- end data table code here  --}}
            <div class="box">
                <form action="{{ url('deleteService')}}"   method="POST" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="nav-tabs-custom">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                    @if(!empty($ser_name))
                                        @php  
                                          if(!empty($ser_name))
                                            {
                                              $ser_name = $ser_name;
                                            }
                                            else
                                            {
                                                $ser_name = "";
                                            }
                                            $urls = ('allServices?'.'ser_name'.'='.$ser_name.'&'.'page='.$allServices->currentPage());
                                        @endphp
                                      @else
                                        @php
                                           $urls = ('allServices?'.'page='.$allServices->currentPage());
                                        @endphp
                                    @endif


                                <div class="box-body">
                                    <div class="row">
                                        <div class="">
                                            <div class="form-group">
                                                <label style="margin-left: 10px;  ">Services</label>
                                                <label style="float: right; margin-right: 110px; font-size: 15px;">Actions</label>
                                                <div class="row">
                                                    <?php
                                                    foreach ($allServices as $key => $value) {
                                                        if (array_key_exists("checked", $value)) {
                                                            $isChecked = $value->checked;
                                                        } else {
                                                            $isChecked = '';
                                                        }
                                                        ?>
                                                        <div >
                                                            <div  style="margin-left: 10pt;"  >
                                                                <input hidden name="sal_type_status[]" value=" 
                                                                       <?php echo $value->ser_id; ?>"
                                                                       <?php echo $isChecked; ?>
                                                                       value="tes"
                                                                       type="text">
                                                                <table style="border:0pt;" class="table table-hover">
                                                                    <tbody>  
                                                                    <td>
                                                                        <input readonly type="text"  value="<?php echo $value->ser_name; ?>" name="sal_name" style=" width: 80%; border:0pt;">
                                                                        <input
                                                                            hidden="true"
                                                                            value="<?php echo $value->ser_id; ?>" name="ser_id" id="ser_id">
                                                                        </input>
                                                                    </td>
                                                                    <td>
                        <div class="btn-group" style="float: right; margin-right: 20px;" >
                    <a href="{{ url('editserviceinfo' , ['id' => $value->ser_id,'urls'=>$urls]) }}" class="btn btn-info btn-lg" style="margin-left: 10px;">
                         <i class="glyphicon glyphicon-edit"></i> 
                    </a>
              
                    <a  href="{{ url('deleteService', ['id' => $value->ser_id]) }}" class="btn btn-danger btn-lg" style="margin-left: 10px;" >
                        <i class="glyphicon glyphicon-trash"></i> 
                    </a>
                </div>
            </td>
         </tbody>
        </table>
    </div>
    </div>
        <?php
        }
        ?>
    </div>
                                            
            {!! $allServices->appends(['ser_name'=>isset($ser_name)? $ser_name:'','page'=>$allServices->currentPage()])->links() !!}
</div>
                                </div>
                                        <div class="form-group">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@endsection

@push("script")
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endpush
