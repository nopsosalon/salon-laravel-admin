@extends('salon-magmt.base')
@section('action-content')
<div    class="col-sm-4" style="float: right">
    <a  class="btn btn-info" href="{{ url('AddnewTypeService') }}">Add type service</a>
</div>
<div  >

</div>
<div>
    <table  id="salon_tabel"  class="table table-hover">
        <tr>

            <th>Salon Type Name</th>
            <th>Number of services</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        <?php
        foreach ($salons as $salon) {
            if ($salon->sty_status == '1') {
                $status = 'active';
            } else {
                $status = 'inactive';
            }
            ?>
            <tr>
                <td ><?php echo $salon->sty_name ?></td>
                <td ><?php echo $salon->tota_services_type ?></td>
                <td><?php echo $status ?></td>


    <!--            <td><div class="btn-group">
             <a href="{{ url('editsalon', ['id' => $salon->sty_id]) }}" class="btn btn-info">Edit</a>
         </div></td>-->

                <td> 



                    <a style="" href="{{ url('editsalontype', ['id' => $salon->sty_id]) }}" class="btn btn-info"> 
                        Edit 
                    </a>
                    
                    <a style="margin: 10px;" href="{{ url('editsalontypedetail', ['id' => $salon->sty_id, 'salon_name' => $salon->sty_name]) }}" class="btn btn-info"> 
                        Salon Type Services
                    </a>


                </td>


            </tr>
        <?php } ?>
    </table>
</div>
@endsection