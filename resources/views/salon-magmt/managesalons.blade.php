@extends('salon-magmt.base')
@section('action-content')

@push("css")
    <style type="text/css">
        .incons
        {
            border: none; 
            color: white; 
            padding: 5px 10px;
            font-size: 20px;
            margin-left:10px;
            cursor: pointer;"
        }
    </style>
@endpush
{{-- @php 
  if(Session::has("success")) 
    dd(Session::get("success")); 
@endphp --}}

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <h3>Salon Managements </h3><br>
    </div>
  </div>

    @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif

    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
    <div>
        <form role="form" method="get" action="{{ url('sarch_salons') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Search Salon
                          </label>
                          <input placeholder="Search salon"  type="text" class="form-control" @isset($sal_search) value="{{$sal_search}}" @endisset name="sal_search" maxlength="128">
                        </div>
                      </div>

                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>Cities</label>
                        <select class="form-control" name="sal_city" id="sal_city">
                          <option value="all">All</option>
                          @isset($cities)
                            @foreach($cities as $city)
                              <option value="{{$city->sal_city}}">{{$city->sal_city}}</option>
                            @endforeach
                          @endisset
                        </select>
                      </div>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>Temp Enabled</label>
                        <select class="form-control" name="sal_temp_enabled" id="sal_temp_enabled">
                          <option value="all">All</option>
                          <option value="1">yes</option>
                          <option value="2">No</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="is_active" id="is_active">
                          <option value="all_status">All</option>
                          <option value="1">Active</option>
                          <option value="2">In-Active</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>Reviewed</label>
                        <select class="form-control" name="sal_is_reviewed" id="sal_is_reviewed">
                          <option value="all_isreviewed">All</option>
                          <option value="1">Yes</option>
                          <option value="2">No</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>


            @if(!empty($sal_search) OR !empty($is_reviewed) OR !empty($is_active))
                    @php

                      if(is_null($sal_city))
                        {
                          $sal_city = "";
                        } 
                        if(is_null($sal_temp_enabled))
                        {
                          $sal_temp_enabled = "";
                        }

                      if(is_null($is_reviewed))
                        {
                          $is_reviewed = "";
                        }

                      if(empty($sal_search))
                        {
                          $sal_search = "";
                        }
                      if(is_null($is_active))
                        {
                          $is_active = "";
                        }

                        $urls = ('sarch_salons?'.'sal_search'.'='.$sal_search.'&'.'sal_city'.'='.$sal_city.'&'.'sal_temp_enabled'.'='.$sal_temp_enabled.'&'.'is_active'.'='.$is_active.'&'.'sal_is_reviewed'.'='.$is_reviewed.'&'.'page='.$salons->currentPage());

                    @endphp
                  @else
                    @php
                       $urls = ('managesalons?'.'page='.$salons->currentPage());
                    @endphp
                @endif


  
    <div class="box" style=" margin-top: 40px; margin-left: 0px; padding-left: 5px;">
        <form role="form" method="POST" action="{{ url('typeservices') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div   class="form-group">
                <table id="salon_tabel" class="table table-hover">
                    <tr>
                        <th>
                         Name   
                        </th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Appointments</th>
                        <th>
                          <th style="text-align: center;">Status</th>
                          <th style="text-align: center;">Temp Enabled</th>
                          <th style="text-align: center;">Is-Reviewed</th>
                          <th style="text-align: center;">Edit</th>
                        </th>
                    </tr>
                    <?php
                    foreach ($salons as $salon) {
                        ?>


                        <tr>
                            <td>
                              {{ $salon->sal_name }}
                            </td>
                            <td>
                              {{ $salon->sal_email }}
                            </td>
                            
                            <td>
                              {{ $salon->sal_address }} 
                            </td>
                            <td>
                                {{ $salon->sal_phone }}
                            </td>
                            <td style="text-align:center;">
                                {{ $salon->count }}
                            </td>
                              
                                  <td style="margin: 0px auto; padding: 0px;">
                                          <input type="hidden" value="{{ '-1' }}" name="salon_type">
                                          <input type="hidden" value="{{ $salon->sal_id }}" name="salon_type">
                                        <td style="text-align: center;">
                                          @if( $salon->sal_status == 0)
                                              <a href="{{ url('sal_status_enable', ['id' => $salon->sal_id]) }}"
                                                 class="btn btn-danger incons" title="In-Active">   
                                                  <i class="glyphicon glyphicon-remove"></i> 
                                              </a> 
                                          @else
                                              <a href="{{ url('sal_status_disable', ['id' => $salon->sal_id]) }}"
                                                 class="btn incons" style="background: #52C1F0;" title="Active">   
                                                  <i class="glyphicon glyphicon-ok"></i> 
                                              </a> 
                                          @endif
                                        </td>
                                        <td style="text-align: center;">
                                          @if($salon->sal_temp_enabled==0)
                                            <a href="{{ url('setUpSalon', ['id' => $salon->sal_id ]) }}" class="btn btn-danger incons" title="No">   
                                                  <i class="glyphicon glyphicon-remove"></i> 
                                              </a>
                                          @else
                                              <a href="{{url('tem_enabled'.'/'.$salon->sal_id)}}"
                                                 class="btn incons" style="background: #52C1F0;" title="Yes">   
                                                  <i class="glyphicon glyphicon-ok"></i> 
                                              </a>
                                          @endif
                                        </td>
                                        <td>
                                          @if( $salon->sal_is_reviewed ==0)
                                              <a href="{{ url('setSalonInfo', ['id' => $salon->sal_id]) }}"
                                                 class="btn btn-danger incons" title="No">   
                                                  <i class="glyphicon glyphicon-remove"></i> 
                                              </a> 
                                          @else
                                              <a href="{{ url('setSalonInfo', ['id' => $salon->sal_id]) }}"
                                                 class="btn incons" style="background: #52C1F0;" title="Yes">   
                                                  <i class="glyphicon glyphicon-ok"></i> 
                                              </a> 
                                          @endif
                                        </td>
                                        <td>
                                          <a style=" border: none;
                                             color: white;
                                             background: #52C1F0;
                                             cursor: pointer;"
                                             href="{{ url('editsalon', ['id' => $salon->sal_id , 'type_id' => '-1','urls'=>$urls]) }}"
                                             class="btn">   <i class="glyphicon glyphicon-edit"></i> </a>
                                          </td>
                                  </td>
                        </tr>
                    <?php } ?>
                </table>
                
                {!! $salons->appends(['sal_search'=>isset($sal_search)?$sal_search:'','sal_city'=>isset($sal_city)?$sal_city:'','sal_temp_enabled'=>isset($sal_temp_enabled)?$sal_temp_enabled:'','is_active'=>isset($is_active)? $is_active:'','sal_is_reviewed'=>isset($is_reviewed)?$is_reviewed:'','page'=>$salons->currentPage()])->links() !!}
                
            </div>
        </form>
    </div>
</section>

@endsection

@push("script")
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

<script>
    
  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>

      <script type="text/javascript">
          jQuery("#is_active").on("change",function(){
          var id = jQuery(this).val();
          // alert(id);
        });
      </script>
    @isset($is_reviewed)
      <script type="text/javascript">
        $("#sal_is_reviewed").val("{{$is_reviewed}}");
      </script>
    @endisset

    @isset($is_active)
      <script type="text/javascript">
        $("#is_active").val("{{$is_active}}");
      </script>
    @endisset

    @isset($sal_city)
      <script type="text/javascript">
        $("#sal_city").val("{{$sal_city}}");
      </script>
    @endisset

    @isset($sal_temp_enabled)
      <script type="text/javascript">
        $("#sal_temp_enabled").val("{{$sal_temp_enabled}}");
      </script>
    @endisset

@endpush