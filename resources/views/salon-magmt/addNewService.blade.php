@extends('salon-magmt.base')

@section('action-content')
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Add Service </h3><br>
        </div>
    </div>
    {{-- @php dd(Session::get('success')); @endphp --}}
   <div class="box">
        <form role="form" method="post" action="{{ url('InsertNewService') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <table class="table table-hover">
                    <tbody>
                        <!-- style= "background-color: #84ed86; color: #761a9b; margin-top: 200px;" -->
                        <tr style="border:1pt;">
                            <th style="border:1pt;">Add a new service </th>  
                        </tr>
                    <td style="border:1pt;">
                        <input type="text"  required="true"  placeholder="Enter service name" value="<?php echo "" ?>" name="sser_name" style="width: 50%; padding:8px;">
                    </td>
                    </tbody>
            </table>

            <button  style=" margin-left: 10px; font-size: 15px; margin-bottom: 10px;" type="submit" name="SubmitFashion" class="btn btn-primary">Add Service</button>
            <a href="allServices" style=" margin-left: 10px; font-size: 15px; margin-bottom: 10px; "  class="btn btn-primary" >
            Cancel
            </a>

         
        </form>

    </div>
</section>
@endsection

@push("script")
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


<script>
    toastr.options = {
            "closeButton": true,
            "debug": false,
            // "positionClass": "toast-bottom-full-width",
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "0",
            "timeOut": "0",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
// toastr.error(message, title);


  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>
@endpush



