<?php
use Illuminate\Http\Request;
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

  Route::get('/changePassword','Auth\LoginController@changePassword')->name('changePassword');
  Route::post('/postChangePassword','Auth\LoginController@postChangePassword')->name('postChangePassword');

  // Route::get("getimages",function(){
  // 	$string = "This is test string.ksdgjmnbasdfhgav msdhfbh....1.png";
  // 	$from = preg_replace('/\.[^.]+$/','',$string);
  	
  // 	 $from=substr($string, 0, (strlen ($string)) - (strlen (strrchr($string,'.'))));
  	 
  // });
  Route::get('fbd/{fb_id}/{fb_gender?}/{start?}/{end?}/{update_date?}','TestController@getFashionBrandDetailsPag');
  
Route::get('/', 'DashboardController@index')->middleware('auth');
Auth::routes();
Route::get('/dashboard',['as' => '/dashboard', 'uses' => 'DashboardController@index']);
// Salon routes
Route::get('/managesalons', 'AdminController@manage_salons');

Route::get('/sarch_salons', 'AdminController@sarch_salons');

Route::get('/editsalon/{id}/{type_id}/{urls}', 'AdminController@edit_salon');

Route::get('/setUpSalon/{id}', 'AdminController@setUpSalon');
Route::get('/setSalonInfo/{id}', 'AdminController@setSalonInfo');

Route::get('/editsalonservices/{id}/{salId}', 'AdminController@edit_salon_services');

//Route::get('/editsalon/{id}', 'AdminController@edit_salon');

//me 
Route::get('/salon_types', 'AdminController@salon_types');



Route::get('/editsalontype/{id}', 'AdminController@editsalontype');

Route::get('/updatesalontype/{id}/{id2}', 'AdminController@updatesalontype');

//Route::get('/updatesalontype/{id}/{id2}', 'AdminController@updatesalontype');

Route::post('/updatesalontype', 'AdminController@updatesalontype');

Route::post('/updatesalontypeservices', 'AdminController@updatesalontypeservices');

Route::post('/updateServiceSubCat', 'AdminController@updateServiceSubCat');


Route::get('/editsalontypedetail/{id}/{salon_name}', 'AdminController@editsalontypedetail');
//end me
Route::post('/typeservices', 'AdminController@type_services');
Route::post('/type_services_update', 'AdminController@type_services_update');



// Services Categories routes
Route::get('/servicecategories', 'AdminController@service_categories');


Route::get('/manage_subcategores/{id}/{urls}', 'AdminController@manage_subcategores');

Route::get('/manageservices/{sc_id}/{ssc_id}', 'AdminController@manage_services');

Route::get('/category_association/{sc_id}/{ssc_id}/{sc_name}', 'AdminController@category_association');



Route::get('/addcategory', 'AdminController@add_category');

Route::get('/fileupload', 'AdminController@fileupload');

Route::get('/addtypeservice', 'AdminController@add_category');
Route::get('/edit_category/{id}/{urls}', 'AdminController@edit_category');

Route::get('/edit_sub_category/{id}/{sc_id}', 'AdminController@edit_sub_category');



Route::get('/EditServiceSubCat/{id}', 'ServiceSubCatController@EditServiceSubCat');


Route::get('/EditServiceSubCatagory/{id}', 'ServiceSubCatogryController@EditServiceSubCatagory');



Route::get('/add_subcategory/{sc_id}', 'AdminController@add_subcategory');
Route::post('/save_subcategory', 'AdminController@save_subcategory');

Route::post('/save_category', 'AdminController@save_category');

Route::post('/save_file', 'AdminController@save_file');
Route::post('/save_file_services', 'AdminController@save_file_services');

Route::post('/update_category', 'AdminController@update_category');

Route::post('/update_sub_category', 'AdminController@update_sub_category');

Route::get('/profile', 'ProfileController@index');
Route::post('user-management/search', 'UserManagementController@search')->name('user-management.search');
Route::resource('user-management', 'UserManagementController');
Route::resource('employee-management', 'EmployeeManagementController');
Route::post('employee-management/search', 'EmployeeManagementController@search')->name('employee-management.search');
Route::get('name}', 'EmployeeManagementController@load');

// Route::get('/testFun'  ,  'AdminController@testFun');

Route::get('/allServices', 'ServiceTagsController@allServices');

Route::get('/deleteService/{id}', 'ServiceTagsController@deleteService');

Route::get('/addNewService', 'ServiceTagsController@addNewService');

Route::get('/editserviceinfo/{id}/{urls}', 'ServiceTagsController@editserviceinfo');

Route::get('/managetags/{id}/{ser_name}', 'ServiceTagsController@managetags');

Route::post('/updatetagservices', 'ServiceTagsController@updatetagservices');

Route::post('/updateservicename', 'ServiceTagsController@updateservicename');

Route::post('/InsertNewService', 'ServiceTagsController@InsertNewService');

//Route::get('/salon_types', 'AdminController@salon_types');
Route::get('/AddnewTypeService', 'ServicesManagementController@AddnewTypeService');
Route::post('/saveNewTypeService', 'ServicesManagementController@saveNewTypeService');
Route::post('/updateSalon', 'SalonManagementController@updateSalon');
Route::post('/updateSalonServices', 'AdminController@updateSalonServices');

Route::get('/post-category','AdminController@pinterest_board');
Route::get("admin/ajax_activate/{pb_id}","AdminController@ajax_activate");
Route::get("admin/ajax_deactivate/{pb_id}","AdminController@ajax_deactivate");
Route::post("admin/changessc","AdminController@changessc");
Route::get("admin/pc-filter","AdminController@pbfilter");
Route::post("admin/updateServiceCat","AdminController@updateServiceCat");
Route::get("admin/search-services","ServiceTagsController@services_search");

Route::post("temp_save_file_salon","AdminController@temp_salon_file");
Route::post("temp_save_file_services","AdminController@tem_salon_service_file");

Route::get("admin/advancesearch","AdminController@advancesearch");
Route::get("admin/post","AdminController@pins_search");
Route::get("admin/postmanagement","AdminController@postmanagement");
Route::post("admin/approved","AdminController@approved");
Route::get("admin/orderby/{ssc_id}/{p_status}/{val}","AdminController@orderby");
Route::post("admin/changeservice_category","AdminController@changeservice_category");
Route::get("admin/sser_enable/{id}","AdminController@sser_enable");
Route::get("admin/sser_disable/{id}","AdminController@sser_disable");
Route::get("sal_status_enable/{id}","AdminController@sal_status_enable");
Route::get("sal_status_disable/{id}","AdminController@sal_status_disable");

Route::get('newsletter','NewsletterController@newsletter');
Route::post('newsletter','NewsletterController@subscribe');

// mailchimp testing controllers 
Route::get('manageMailChimp', 'MailChimpController@manageMailChimp');
Route::post('subscribe',['as'=>'subscribe','uses'=>'MailChimpController@subscribe']);
Route::post('sendCompaign',['as'=>'sendCompaign','uses'=>'MailChimpController@sendCompaign']);
Route::get("admin/appointments","AdminController@appointment");
Route::post("admin/search-appointments","AdminController@search_app_req");
Route::get('tem_enabled/{id}','AdminController@tem_enabled');
Route::get('featured_enable/{id}','AdminController@featured_enable');
Route::get('featured_disable/{id}','AdminController@featured_disable');
Route::get("appointment-detail/{id}","AdminController@appoint_request_detail");
Route::post("store_conversation/{id}","AdminController@store_conversation");
Route::get("setas_category_image/{id}","AdminController@setas_category_image");
Route::get("setas_sub_category_image/{id}","AdminController@setas_sub_category_image");
Route::get("set_as_editor_img/{id}","AdminController@set_as_editor_img");
Route::get("add-new-post/{ssc_id}","AdminController@add_new_post");
Route::post("addnew_post","AdminController@addnew_post");
Route::post("upload_salon","AdminController@upload_salon");

Route::get("mail-status","MailChimpController@getList_status");

// image resize 
Route::get("resize","AdminController@resize");
Route::post("image_re","AdminController@image_re");

Route::get("admin/service-subcategory-posts","AdminController@posts");
Route::get("admin/service-subcategory-post-detail/{id}","AdminController@post_detail");
Route::get("admin/update-app-data","AdminController@update_app_data");
Route::post("admin/updateSalonSerSubCate","AdminController@updateSalonSerSubCate");

// for pk save salon file and salon service routes
Route::post('/save_file_pk', 'pk\uploadFilesControllerPK@save_file');
Route::post('/save_file_services_pk', 'pk\uploadFilesControllerPK@save_file_services');

Route::post("temp_save_file_salon_pk","pk\uploadFilesControllerPK@temp_salon_file");
Route::post("temp_save_file_services_pk","pk\uploadFilesControllerPK@tem_salon_service_file");

// customer controller 
Route::get("admin/customers","CustomerController@customers");
Route::get("admin/enable_cust/{id}","CustomerController@enable_cust");
Route::get("admin/disable_cust/{id}","CustomerController@disable_cust");

//technitian controller 
Route::get('admin/technicians','TechController@technicians');
//  beauty tips controller 
Route::get('admin/beauti_tips/{id}/{urls}','BeautytipController@beautytip_listing');

Route::get('admin/add_beautytips','BeautytipController@add_beautytips');

Route::post('add_beauty_tips','BeautytipController@add_beauty_tips');
Route::get('beautyTips_category','BeautytipController@beautyTips_category');
Route::get('admin/add_beautyTips_category/{urls}','BeautytipController@add_beautyTips_category');
Route::post('store_beautytips_category','BeautytipController@store_beautytips_category');
Route::get('admin/edit_beautytip_cat/{id}/{urls}','BeautytipController@edit_beautytip_cat');
Route::post('update_beautyTips_cat/{id}','BeautytipController@update_beautyTips_cat');
Route::get('edit_beautyTips/{id}/{btc_id}','BeautytipController@edit_beautyTips');

Route::get('admin/beauti_tips/search','BeautytipController@beautytip_search');

Route::post('active_inactive','BeautytipController@active_inactive');

//  ecommerces routes 
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	Route::get('product_categories',[
			'as'	=>	'product_categories',
			'uses'	=>	'ecommerce\PCController@product_category',
		]);
	Route::get('add_product_cat',[
			'as' 	=> 'add_product_cat',
			'uses' 	=> 'ecommerce\PCController@add_product_cat',
		]);
	Route::post('store_pcate',[
			'as' 	=> 'store_pcate',
			'uses'	=> 'ecommerce\PCController@store_pcate',
		]);
	Route::get('pc_enable/{id}',[
			'as'	=>	'pc_enable',
			'uses'	=>	'ecommerce\PCController@pc_enable',
		]);
	Route::get('pc_diable/{id}',[
			'as'	=>	'pc_diable',
			'uses'	=>	'ecommerce\PCController@pc_diable',
		]);
	Route::get('pc_delete/{id}',[
			'as'	=>	'pc_delete',
			'uses'	=>	'ecommerce\PCController@pc_delete',
		]);
	Route::get('pc_edit/{id}',[
			'as' 	=> 'pc_edit',
			'uses'	=>	'ecommerce\PCController@pc_edit',
		]);
	Route::post('pc_update/{id}',[
			'as'	=>	'pc_update',
			'uses'	=>	'ecommerce\PCController@pc_update',
		]);
	Route::get('pc_search',[
			'as' 	=> 'pc_search',
			'uses'	=> 'ecommerce\PCController@pc_search',
		]);

// product controller routes 
	Route::get('products/{id}',[
			'as'	=>	'products',
			'uses'	=>	'ecommerce\ProductController@products',
		]);
	Route::get('add-product/{id}',[
			'as'	=>	'add-product',
			'uses'	=>	'ecommerce\ProductController@add_product',
		]);
	Route::post('store_pproduct',[
			'as'	=>	'store_pproduct',
			'uses'	=>	'ecommerce\ProductController@store_pproduct',
		]);
	Route::get('edit_product/{id}/{pc_id}',[
			'as'	=>	'edit_product',
			'uses'	=>	'ecommerce\ProductController@edit_product',
		]);
	Route::post('update_product/{id}',[
			'as'	=>	'update_product',
			'uses'	=>	'ecommerce\ProductController@update_product',
		]);
	Route::get('delete_product/{id}',[
			'as'	=>	'delete_product',
			'uses'	=>	'ecommerce\ProductController@delete_product',
		]);
	Route::post('save_product_images',[
			'as'	=>	'save_product_images',
			'uses'	=>	'ecommerce\ProductController@save_product_images'
		]);
	Route::get('p_search/{id}',[
			'as'	=>	'p_search',
			'uses'	=>	'ecommerce\ProductController@p_search',
		]);

	Route::get('product_data_import',[
			'as'	=>	'product_data_import',
			'uses'	=>	'ecommerce\ProductController@product_data_import',
		]);
	Route::post('store_products_import_data',[
			'as'	=>	'store_products_import_data',
			'uses'	=>	'ecommerce\ProductController@store_products_import_data',
		]);

	// product vendors routes
	Route::get('product-vendors',[
			'as'	=>	'product-vendors',
			'uses'	=>	'ecommerce\VendorController@product_vendors',
		]);
	Route::get('add_vendor',[
			'as'	=>	'add_vendor',
			'uses'	=>	'ecommerce\VendorController@add_vendor',
		]);
	Route::post('store_vendor',[
			'as'	=>	'store_vendor',
			'uses'	=>	'ecommerce\VendorController@store_vendor',
		]);
	Route::get('edit_vendor/{id}',[
			'as'	=>	'edit_vendor',
			'uses'	=>	'ecommerce\VendorController@edit_vendor',
		]);
	Route::post('update_vendor/{id}',[
			'as'	=>	'update_vendor',
			'uses'	=>	'ecommerce\VendorController@update_vendor',
		]);
	Route::get('delete_vendor/{id}',[
			'as'	=>	'delete_vendor',
			'uses'	=>	'ecommerce\VendorController@delete_vendor',
		]);
	Route::get('pv_enable/{id}',[
			'as'	=>	'pv_enable',
			'uses'	=>	'ecommerce\VendorController@pv_enable',
		]);
	Route::get('pv_diable/{id}',[
			'as'	=>	'pv_diable',
			'uses'	=>	'ecommerce\VendorController@pv_diable',
		]);
	Route::get('pv_search',[
			'as'	=>	'pv_search',
			'uses'	=>	'ecommerce\VendorController@pv_search',
		]);
	// product brands
	Route::get('manage_brands/{id}',[
			'as'	=>	'manage_brands',
			'uses'	=>	'ecommerce\PBrandController@manage_brands',
		]);
	Route::get('add_pbrand/{id}',[
			'as'	=>	'add_pbrand',
			'uses'	=>	'ecommerce\PBrandController@add_pbrand',
		]);
	Route::post('store_pbrand',[
			'as'	=>	'store_pbrand',
			'uses'	=>	'ecommerce\PBrandController@store_pbrand',
		]);
	Route::get('edit_pbrand/{id}/{pv_id}',[
			'as'	=>	'edit_pbrand',
			'uses'	=>	'ecommerce\PBrandController@edit_pbrand',
		]);
	Route::post('update_pbrand/{id}',[
			'as'	=>	'update_pbrand',
			'uses'	=>	'ecommerce\PBrandController@update_pbrand',
		]);
	Route::get('delete_pbrand/{id}',[
			'as'	=>	'delete_pbrand',
			'uses'	=>	'ecommerce\PBrandController@delete_pbrand',
		]);
	Route::get('pb_enable/{id}',[
			'as'	=>	'pv_enable',
			'uses'	=>	'ecommerce\PBrandController@pb_enable',
		]);
	Route::get('pb_diable/{id}',[
			'as'	=>	'pb_diable',
			'uses'	=>	'ecommerce\PBrandController@pb_diable',
		]);
	Route::get('pb_search/{id}',[
			'as'	=>	'pb_search',
			'uses'	=>	'ecommerce\PBrandController@pb_search',
		]);

//  product colros routes 
	Route::get('add_product_colors',[
			'as'	=>	'add_product_colors',
			'uses'	=>	'ecommerce\PColorCOntroller@add_product_colors',
		]);
	Route::post('store_pcl/{id}',[
			'as'	=>	'store_pcl',
			'uses'	=>	'ecommerce\ProductController@store_pcl',
		]);
	Route::get('delete_images/{id}',[
			'as'	=>	'delete_images',
			'uses'	=>	'ecommerce\ProductController@delete_images',
		]);


//  orders routes controller 
	Route::get('orders',[
			'as'	=>	'orders',
			'uses'	=>	'ecommerce\OrderController@orders',
		]);
	Route::get('order_detail/{id}',[
			'as'	=>	'order_detail',
			'uses'	=>	'ecommerce\OrderController@order_detail',
		]);
	Route::post('get_product_id_for_colors',[
			'as'	=>	'get_product_id_for_colors',
			'uses'	=>	'ecommerce\PColorController@get_product_id_for_colors',
		]);
	Route::post('save_product_colors_images',[
			'as'	=>	'save_product_colors_images',
			'uses'	=>	'ecommerce\PColorController@save_product_colors_images',
		]);
	Route::get('delete_color_images/{pcl_id}',[
			'as'	=>	'delete_color_images',
			'uses'	=>	'ecommerce\PColorController@delete_color_images',
		]);

	// total active beauty tips dashboard
	Route::get('beauty_tips',[
			'as'	=>	'active_beauty_tips',
			'uses'	=>	'BeautytipController@active_beauty_tips',
		]);

	Route::get('beautytip_detail/{id}',[
		'as'	=>	'beautytips_detail',
		'uses'	=>	'BeautytipController@beautytips_detail',
		]);
	Route::get('setas_featured_image/{id}',[
		'as'	=>	'setas_featured_image',
		'uses'	=>	'BeautytipController@setas_featured_image',
		]);

	// fashions controllers 

	Route::get('addFashCollection/',[
		'as'	=>	'addFashCollection',
		'uses'	=>	'fashion\FashCollController@addFashCollection',
		]);
	Route::post('store_factionColletion/',[
		'as'	=>	'store_factionColletion',
		'uses'	=>	'fashion\FashCollController@store_factionColletion',
		]);
	Route::get('update_collection/{id}',[
		'as'	=>	'update_collection',
		'uses'	=>	'fashion\FashCollController@update_collection',
		]);
	Route::post('updatedCollection/{id}',[
		'as'	=>	'updatedCollection',
		'uses'	=>	'fashion\FashCollController@updatedCollection',
		]);

	Route::get('fashionCollection/',[
		'as'	=>	'fac_collections',
		'uses'	=>	'fashion\FashCollController@fac_collections',
		]);
	Route::get('deleteCollection/{id}',[
		'as'	=>	'deleteCollection',
		'uses'	=>	'fashion\FashCollController@deleteCollection',
		]);
	// brands controllers 
	Route::get('addFashBrand/',[
		'as'	=>	'addFashBrand',
		'uses'	=>	'fashion\FashBrandController@addFashBrand',
		]);
	Route::post('store_factionBrand/',[
		'as'	=>	'store_factionBrand',
		'uses'	=>	'fashion\FashBrandController@store_factionBrand',
		]);
	Route::get('edit_brand/{id}',[
		'as'	=>	'edit_brand',
		'uses'	=>	'fashion\FashBrandController@edit_brand',
		]);
	Route::post('updatedBrand/{id}',[
		'as'	=>	'updatedBrand',
		'uses'	=>	'fashion\FashBrandController@updatedBrand',
		]);

	Route::get('fashionBrand/',[
		'as'	=>	'fashionBrand',
		'uses'	=>	'fashion\FashBrandController@fashionBrand',
		]);
	// Route::get('deleteCollection/{id}',[
	// 	'as'	=>	'deleteCollection',
	// 	'uses'	=>	'fashion\FashBrandController@deleteCollection',
	// 	]);
	//
	Route::get('addFashBrandSubcate/{id}',[
			'as' 	=> 'addFashBrandSubcate',
			'uses' 	=> 'fashion\FashBrandSubCController@addFashBrandSubcate',
		]);
	Route::post('store_fashbrandcollection/{id}',[
			'as'	=>	'store_fashbrandcollection',
			'uses'	=>	'fashion\FashBrandSubCController@store_fashbrandcollection',
		]);
	Route::post('add_fbc_images/{id}',[
			'as'	=>	'add_fbc_images',
			'uses'	=>	'fashion\FashBrandSubCController@add_fbc_images',
		]);
	Route::get('fashionBrandCollection/{id}',[
			'as'	=>	'fashionBrandCollection',
			'uses'	=>	'fashion\FashBrandSubCController@fashionBrandCollection',
		]);
	Route::get('edit_brand_collection/{id}/{fb_id}',[
			'as'	=>	'edit_brand_collection',
			'uses'	=>	'fashion\FashBrandSubCController@edit_brand_collection',
		]);
	Route::get('catalog',[
			'as'	=>	'catalog',
			'uses'	=>	'fashion\CatalogController@catalog',
		]);

	Route::post('delete_images_fbci',[
			'as'	=>	'delete_images_fbci',
			'uses'	=>	'fashion\CatalogController@delete_images_fbci',
		]);

	Route::get('get_catalogs/{id}',[
			'as'	=>	'get_catalogs',
			'uses'	=>	'fashion\CatalogController@get_catalogs',
		]);
	Route::get('delete_catalog_image/{id}',[
			'as'	=>	'delete_catalog_image',
			'uses'	=>	'fashion\CatalogController@delete_catalog_image',
		]);
	Route::get('update_fashbrandc/{id}/{fb_id}',[
			'as'	=>	'update_fashbrandc',
			'uses'	=>	'fashion\FashBrandSubCController@edit_brand_collection',
		]); 
	Route::post('updatedBrand_collection/{id}/{fb_id}',[
			'as'	=>	'updatedBrand_collection',
			'uses'	=>	'fashion\FashBrandSubCController@updatedBrand_collection',
		]);
	Route::post('fashbrandcassoc/{id}/{fb_id}',[
			'as'	=>	'fashbrandcassoc',
			'uses'	=>	'fashion\FashBrandSubCController@fashbrandcassoc',
		]);

	Route::get('featured-products',[
			'as'	=>	'featuredProducts',
			'uses'	=>	'ecommerce\FeaturedController@featuredProducts',
		]);

	Route::post('isfeatured',[
			'as'	=>	'isfeatured',
			'uses'	=>	'ecommerce\FeaturedController@isfeatured',
		]);
	Route::post('get_product',[
			'as'	=>	'get_product',
			'uses'	=>	'ecommerce\FeaturedController@get_product',
		]);
	Route::get('is_featured_enable/{id}',[
			'as'	=>	'is_featured_enable',
			'uses'	=>	'ecommerce\FeaturedController@is_featured_enable',
		]);
	Route::get('is_featured_disable/{id}',[
			'as'	=>	'is_featured_disable',
			'uses'	=>	'ecommerce\FeaturedController@is_featured_disable',
		]);

	/////    Credit Admins Routes 
	Route::get('credit_admin',[
			'as'	=>	'credit_admin',
			'uses'	=>	'ecommerce\CreditAdminController@store_credit',
		]);
	Route::get('storecredits/{id}',[
			'as'	=>	'storecredits',
			'uses'	=>	'ecommerce\CreditAdminController@storecredits',
		]);
	Route::post('store_cust_credits',[
		'as'	=>	'store_cust_credits',
		'uses'	=>	'ecommerce\CreditAdminController@store_cust_credits',
		]);
	Route::get('coupons',[
		'as'	=>	'coupons',
		'uses'	=>	'ecommerce\CreditAdminController@coupons',
		]);
	Route::get('add_coupons',[
		'as'	=>	'add_coupons',
		'uses'	=>	'ecommerce\CreditAdminController@add_coupons',
		]);
	Route::post('store_coupons',[
		'as'	=>	'store_coupons',
		'uses'	=>	'ecommerce\CreditAdminController@store_coupons',
		]);
	Route::get('send_coupon/{id}',[
		'as'	=>	'send_coupon',
		'uses'	=>	'ecommerce\CreditAdminController@send_coupon',
		]);
	Route::post('send_coupons/{id}',[
		'as'	=>	'send_coupons',
		'uses'	=>	'ecommerce\CreditAdminController@send_coupons',
		]);
	Route::get('seo_data',[
		'as'	=>	'add_seodata',
		'uses'	=>	'SEOController@add_seodata',
		]);
	Route::get('seo_data',[
		'as'	=>	'add_seodata',
		'uses'	=>	'SEOController@add_seodata',
		]);

	Route::get('add-seo',[
		'as'	=>	'add_seo',
		'uses'	=>	'SEOController@add_seo',
		]);
	Route::post('store_seo',[
		'as'	=>	'store_seo',
		'uses'	=>	'SEOController@store_seo',
		]);
	Route::get('edit_seo/{id}',[
		'as'	=>	'edit_seo',
		'uses'	=>	'SEOController@edit_seo',
		]);
	Route::post('update_seo/{id}',[
		'as'	=>	'update_seo',
		'uses'	=>	'SEOController@update_seo',
		]);
	Route::get('delete_seo/{id}',[
		'as'	=>	'delete_seo',
		'uses'	=>	'SEOController@delete_seo',
		]);

	//   userapp actitity
	Route::get('userapp-activity',[
		'as'	=>	'userapp_activity',
		'uses'	=>	'AdminController@userapp_activity',
		]);
});

// Route::get('names',function(){
//   	dd(request()->getSchemeAndHttpHost());
//   });

Route::get('ajaxMetho','ecommerce\ProductController@ajaxMethod');

Route::get('get_data','fashion\CatalogController@get_data');

Route::get('products_images','ecommerce\ProductController@product_images');
Route::get('product_image/{id}','ecommerce\ProductController@get_product_images');
	// get product id for dropzone 
Route::post('get_product_id','ecommerce\ProductController@get_product_id');


// Route::get('save_image',function(){
// 	return view('save_image');
// });
// Route::post('save-images',function(Request $request){
// 	$file = $request->cat_image;
//         if (\File::exists($file)) {
//         	// $nameonly=preg_replace('/\..+$/', '', $file->getClientOriginalName());
// 			// $fullname=$nameonly.'.'.$file->getClientOriginalExtension();

//             $image_name = preg_replace('/\..+$/', '',$file->getClientOriginalName());
//             // $product['fbci_name'] = $image_name.'.'. $file->getClientOriginalExtension();
//               dd($image_name);
//        	}
// });






/////////////////////////////////////////////////////////////////////////////////////// 
Route::get('province','ProvinceController@province');
Route::get('add-province','ProvinceController@add_province');
Route::post('store_province','ProvinceController@store_province');
Route::get('edit_province/{id}','ProvinceController@edit_province');
Route::post('update_province/{id}','ProvinceController@update_province');
Route::get('delete_province/{id}','ProvinceController@delete_province');

/////////////////////////////////////////////////////////////////////////////////////// 
Route::get('cities/{state_id}','ProvinceController@cities');
Route::get('add-cities/{state_id}','ProvinceController@add_cities');
Route::post('store_cities/{state_id}','ProvinceController@store_cities');
Route::get('edit_cities/{state_id}/{id}','ProvinceController@edit_cities');
Route::post('update_cities/{state_id}/{id}','ProvinceController@update_cities');
Route::get('delete_cities/{id}','ProvinceController@delete_cities');

/////////////////////////////////////////////////////////////////////////////////////// 
Route::get('salon-areas','ProvinceController@salon_areas');
Route::get('add-area','ProvinceController@add_area');
Route::post('store_area','ProvinceController@store_area');
Route::get('edit_area/{id}','ProvinceController@edit_area');
Route::post('update_area/{id}','ProvinceController@update_area');
Route::get('delete_area/{id}','ProvinceController@delete_area');















