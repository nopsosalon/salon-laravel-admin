<?php 

namespace App\Helpers;
use Session;
use DB;
use Config;
use Auth;
use Illuminate\Support\Facades\Storage;

class Helper 
{
    
    public function setDBConnection(){
        if(Session::get('change_db_connection')=="1"){
             DB::purge('mysql');
                Config::set('database.connections.mysql.app_url', 'http://nopso.qwpcorp.com/');
                Config::set('database.connections.mysql.host', 'http://nopso.qwpcorp.com/');
                Config::set('database.connections.mysql.username', 'nopso');
                Config::set('database.connections.mysql.password', 'EjdS?Adi');
                Config::set('database.connections.mysql.database', 'nopso08');
             // $result = Config::set('database.connections.mysql.database', 'nopso08','nopso','EjdS?Adi');
            }
            elseif(Session::get('change_db_connection')=="2"){   
                // DB::purge('mysql');
                // Config::set('database.connections.mysql.app_url', 'http://nopso.qwpcorp.com/');
                // Config::set('database.connections.mysql.host', 'http://nopso.qwpcorp.com/');
                // Config::set('database.connections.mysql.username', 'nopso');
                // Config::set('database.connections.mysql.password', 'EjdS?Adi');
                // Config::set('database.connections.mysql.database', 'nopso10');
                $result = Config::set('database.connections.mysql.database', 'nopso10','root','');   
            }elseif(Session::get('change_db_connection')=="3"){
                DB::purge('mysql');
                $result = Config::set('database.connections.mysql2.app_url', 'http://nopso.qwpcorp.com/');
                $result = Config::set('database.connections.mysql2.host', 'mysql.beautyapp.pk');
                $result = Config::set('database.connections.mysql2.username', 'beautyapp');
                $result = Config::set('database.connections.mysql2.password', 'EjdS?Adi');
                $result = Config::set('database.connections.mysql2.database', 'beautyapp02');
                // dd($result);
            }elseif(Session::get('change_db_connection')=="4"){
                DB::purge('mysql');
                $result = Config::set('database.connections.mysql2.app_url', 'http://nopso.qwpcorp.com/');
                Config::set('database.connections.mysql.app_url', 'http://nopso.qwpcorp.com/');
                Config::set('database.connections.mysql.host', 'mysql.beautyapp.qwpcorp.com');
                Config::set('database.connections.mysql.username', 'beautyappdev');
                Config::set('database.connections.mysql.password', 'w8Avdu7i');
                Config::set('database.connections.mysql.database', 'beautappdev02');
                // dd($result);
            }else{
                Auth::logout();
                return redirect('/login');
            }
    }

    public function cate_images(){
        if(Session::get('change_db_connection')=="1"){
            return "/home/nopso/nopso.qwpcorp.com/salon/cat_images/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "/home/nopso/nopso.qwpcorp.com/apis_pk/cat_images/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "/home/beautyapp/beautyapp.pk/apis_pk/cat_images/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "/home/beautyappdev/beautyapp.qwpcorp.com/apis_pk/cat_images/";
        }   
    }
    public function salonImages(){
        if(Session::get('change_db_connection')=="1"){
            return "/home/nopso/nopso.qwpcorp.com/salon/salonimages/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "/home/nopso/nopso.qwpcorp.com/apis_pk/salonimages/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "/home/beautyapp/beautyapp.pk/apis_pk/salonimages/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "/home/beautyappdev/beautyapp.qwpcorp.com/apis_pk/salonimages/";
        }     
    }

    public function display_cate_images(){
        if(Session::get('change_db_connection')=="1"){
            return "https://nopso.qwpcorp.com/salon/cat_images/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "https://nopso.qwpcorp.com/apis_pk/cat_images/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "https://www.beautyapp.pk/apis_pk/cat_images/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "https://www.beautyapp.qwpcorp.com/apis_pk/cat_images/";
        }   
    }
    public function display_salonImages(){
        if(Session::get('change_db_connection')=="1"){
            return "https://nopso.qwpcorp.com/salon/salonimages/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "https://nopso.qwpcorp.com/apis_pk/salonimages/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "https://www.beautyapp.pk/apis_pk/salonimages/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "https://www.beautyapp.qwpcorp.com/apis_pk/salonimages/";
        }   
    }
    public function display_cust_img(){
        if(Session::get('change_db_connection')=="1"){
            return "https://nopso.qwpcorp.com/salon/customers/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "https://nopso.qwpcorp.com/apis_pk/customers/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "https://www.beautyapp.pk/apis_pk/customers/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "https://www.beautyapp.qwpcorp.com/apis_pk/customers/";
        }
    }

    public function display_tech_img(){
        if(Session::get('change_db_connection')=="1"){
            return "https://nopso.qwpcorp.com/salon/technicians/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "https://nopso.qwpcorp.com/apis_pk/technicians/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "https://www.beautyapp.pk/apis_pk/technicians/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "https://www.beautyapp.qwpcorp.com/apis_pk/technicians/";
        }
    }
    public function store_beautytips_images(){
        if(Session::get('change_db_connection')=="1"){
            return "/home/nopso/nopso.qwpcorp.com/salon/beautytips_images/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "/home/nopso/nopso.qwpcorp.com/apis_pk/beautytips_images/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "/home/beautyapp/beautyapp.pk/apis_pk/beautytips_images/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "/home/beautyappdev/beautyapp.qwpcorp.com/apis_pk/beautytips_images/";
        }   
    }
    public function display_beautytips_images(){
        if(Session::get('change_db_connection')=="1"){
            return "https://nopso.qwpcorp.com/salon/beautytips_images/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "https://nopso.qwpcorp.com/apis_pk/beautytips_images/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "https://www.beautyapp.pk/apis_pk/beautytips_images/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "https://www.beautyapp.qwpcorp.com/apis_pk/beautytips_images/";
        }
    }
    public function pc_image_store(){
        if(Session::get('change_db_connection')=="1"){
            return "/home/nopso/nopso.qwpcorp.com/salon/products_images/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "/home/nopso/nopso.qwpcorp.com/apis_pk/products_images/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "/home/beautyapp/beautyapp.pk/apis_pk/products_images/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "/home/beautyappdev/beautyapp.qwpcorp.com/apis_pk/products_images/";
        }
    }
    public function pc_image_display(){
        if(Session::get('change_db_connection')=="1"){
            return "https://nopso.qwpcorp.com/salon/products_images/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "https://nopso.qwpcorp.com/apis_pk/products_images/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "https://www.beautyapp.pk/apis_pk/products_images/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "https://www.beautyapp.qwpcorp.com/apis_pk/products_images/";
        }

    }

    public function fashion_image_store(){
        if(Session::get('change_db_connection')=="1"){
            // return public_path(). '/fashion_images/';
            return "/home/nopso/nopso.qwpcorp.com/salon/fashion_images/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "/home/nopso/nopso.qwpcorp.com/apis_pk/fashion_images/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "/home/beautyapp/beautyapp.pk/apis_pk/fashion_images/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "/home/beautyappdev/beautyapp.qwpcorp.com/apis_pk/fashion_images/";
        }

    }
    public function fashion_image_display(){
        if(Session::get('change_db_connection')=="1"){
            // return public_path(). '/fashion_images/';
            return "https://nopso.qwpcorp.com/salon/fashion_images/";
        }else if(Session::get('change_db_connection')=="2"){ 
            return "https://nopso.qwpcorp.com/apis_pk/fashion_images/";
        }else if(Session::get('change_db_connection')=="3"){ 
            return "https://www.beautyapp.pk/apis_pk/fashion_images/";
        }else if(Session::get('change_db_connection')=="4"){ 
            return "https://www.beautyapp.qwpcorp.com/apis_pk/fashion_images/";
        }
    }

    public function uploadimageFTP($filenametostore,$requestfile){
        //Upload File to external server
        \Storage::disk('ftp')->put($filenametostore, fopen($requestfile, 'r+'));
        //Store $filenametostore in the database
    }

    function send_sms($phone_number, $message){
        // create a new cURL resource
        //$url = "https://www.textmarketer.biz/gateway/?username=z3CBDZ&password=ygZfK3&orig=test&message=$message&number=$phone_number";
        //$URL = 'https://api.textmarketer.co.uk/gateway/'."?username=z3CBDZ&password=ygZfK3&option=xml";
        //$URL .= "&to=$phone_number&message=".urlencode($message).'&orig=SalonApp';
        $URL = "http://sendpk.com/api/sms.php?username=923008413519&password=8672&sender=BeautyApp.pk&mobile=$phone_number&message=".urlencode($message)."%20from%20BeautyApp.pk";
        // logmsg("send_sms: " . $URL);
        $fp = fopen($URL, 'r');
        return fread($fp, 1024);
        // return true;
    }
   
}