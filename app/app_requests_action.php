<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class app_requests_action extends Model
{
    //
    protected $table = 'app_requests_action';
    protected $primaryKey = 'ara_id';

    protected $guarded = [];
    public $timestamps = false;
}
