<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use File;
use App\Http\Requests;
use Config;
use Auth;
use App;
use App\Helpers\Helper;


class ServiceTagsController extends Controller {

    public function __construct()
    {

            $this->middleware('auth');
            $dbc = new Helper();
            $dbc->setDBConnection();
      
    }
    //
    public function allServices(Request $request) 
    {
        if(!empty($request->ser_name))
        {
            $page_title = "All Services";
            $ser_name = '';
            $ser_name = $request->ser_name;
            $data = DB::table('services');
            if(!empty($ser_name))
            {
                $data = $data->where('ser_name','<>','')->where("ser_name","like","%".$ser_name."%");
            }
            $allServices = $data->orderBy('ser_name','ASC')->paginate(10);
            Session::put("activemenu","allServices");
            return view('salon-magmt/all-services', compact('page_title','ser_name'),['allServices' => $allServices]);
        } 

        $page_title = "All Services";
        $allServices = DB::table('services')->where('ser_name','<>','')->paginate(10);
        Session::put("activemenu","allServices");
        return view('salon-magmt/all-services', compact('page_title','ser_name'),['allServices' => $allServices]);
    }

    public function deleteService($ser_id) {
         
        //  dd('You are in ServiceTagsController');



        $allServices = DB::table('services')->get();


        $result = DB::select('select * from salon_services where ser_id= ' . $ser_id);
       // dd($result);
        
        if (count($result) > 0) {
            $sal_id=$result[0]->sal_id;
            //dd($sal_id);
           
        //  dd($ser_id);
            $salons = DB::select('select sal_name, sal_email, sal_city from salon where sal_id='.$sal_id);
//        $salons = DB::table('salon')->get();
//        dd($salons);
            $message='This service can not be delete, it is using by '.$salons[0]->sal_name.' salon';
          //  dd($message);
           // dd($ser_id);
             $allServices = DB::table('services')->paginate(10);
             return back();
        return view('salon-magmt/all-services', ['allServices' => $allServices, 'message'=> 0, 'messageInfo'=>$message]);

        
        } 
        else 
        {
           
             $result = DB::table('salon_services')->where('ser_id', $ser_id)->delete();
            $result = DB::table('services')->where('ser_id', $ser_id)->delete();
            $message='Service is deleted';
            $allServices = DB::table('services')->paginate(10);
            return back();
            return view('salon-magmt/all-services', ['allServices' => $allServices, 'message'=> 1, 'messageInfo'=>$message]);
        }

       return back();
        return view('salon-magmt/all-services', ['allServices' => $allServices]);
    }

    public function editserviceinfo(Request $request,$id,$urls)
    {
        
        $page_title = "Service Update info";
        $returnback = url('/').'/'.$urls;
        $services = DB::table('services')->where('ser_id', $id)->get()[0];
        
        return view('salon-magmt/editserviceinfo',compact('returnback','page_title') ,['services' => $services]);
    }

    public function addNewService(Request $request) 
    {
        $page_title = "Add New Service";
        // Session::flash('flash_message', 'A new service added successfully.');

        return view('salon-magmt/addNewService',compact('page_title'));
    }
    public function updateservicename(Request $request) {
        $ser_id = $request->input('ser_id');
        $ser_name = $request->input('ser_name');
        if(!empty($ser_name) OR !is_null($ser_name))
        {
            $data = DB::table('services')->where('ser_id','<>',$ser_id)->where('ser_name',$ser_name)->first();
            if($data)
            {
                $notification = array(
                        'error'=>'service name is already exist, please enter different name!', 
                        'status' => 'error'
                    );
                    
                    return back()->with($notification);
            }
            else
            {
                DB::table('services')
                    ->where('ser_id', $ser_id)
                    ->update(['ser_name' => $ser_name]);

                    $notification = array(
                        'success' => 'Service updated successfully!', 
                        'status' => 'success'
                    );

                    return back()->with($notification);
            }
        }
        else
        {
            $notification = array(
                        'error' => 'Please enter the service name, service name cannot empty!', 
                        'status' => 'error'
                    );

                    return back()->with($notification);
        }
        // $allServices = DB::table('services')->paginate(10);
        // return view('salon-magmt/all-services', ['allServices' => $allServices]);
    }

    public function InsertNewService(Request $request) 
    {
        // If User has no selected any service then just call back view before 
        $sser_name = $request->input('sser_name');
        $result = DB::statement("INSERT INTO services (ser_name) VALUES ('" . $sser_name . "')
                    ON DUPLICATE KEY UPDATE ser_name=ser_name ");

        $result = DB::table('services')
                ->where('ser_name', '=', $sser_name)
                ->get();

        foreach ($result as $ser_id) {
            $ser_id = $ser_id->ser_id;
        }
        $ser_name = str_replace(" ", "%", $sser_name);
        $result1 = DB::statement("INSERT INTO service_tags (ser_id, tag_id) 
              select " . $ser_id . ", tag_id FROM tags where tag_name   like '%" . $ser_name . "%'  ON DUPLICATE KEY UPDATE service_tags.ser_id=service_tags.ser_id, service_tags.tag_id=service_tags.tag_id  ");
          // dd('Final result', $result1);

        $page_title = "Add New Service";

        $notification = array(
                    'success' => 'A new service added successfully.', 
                    'type' => 'success'
                );
        return back()->with($notification);
        return view('salon-magmt/addNewService',compact('page_title'))->with($notification);

        // ->with($notification);
        // return $this->edit_salon($salId, '-1');
        // dd($sser_name,$sser_rate, $sser_time, $sser_rate, $sser_enabled,$sser_featured, $sser_id );
    }

    public function managetags($id, $ser_name) {
        $page_title = "Manage Tags ";
        $type_id = session('type_id') ? session('type_id') : 1;
        // $salon = DB::table('salon')->where('sal_id', $id)->get();
        //dd($salon);
        $ser_id = $id;

        $allTags = DB::table('tags')
                ->where('is_main_tag', '=', '1')
                ->get();
        //$salon_types = DB::table('salon_types')->get();
        //service_tags 
        //services = tags
        //sscategories_services = service_tags 
        //service_sub_categories= services  
        $salon_types = DB::table('service_tags')
                ->join('services', 'service_tags.ser_id', '=', 'services.ser_id')
                ->where('services.ser_id', '=', $id)
                ->select('service_tags.*')
                ->get();

        //dd($salon_types);
        for ($index = 0; $index < count($allTags); $index++) {
            for ($index2 = 0; $index2 < count($salon_types); $index2++) {
                $objAllService = $allTags[$index];
                $objSalonType = $salon_types[$index2];

                if ($objAllService->tag_id == $objSalonType->tag_id) {
                    $objAllService->checked = 'checked';
                    $allTags[$index] = $objAllService;
                    break;
                } else {
                    $objAllService->checked = 'UnChecked';
                    $allTags[$index] = $objAllService;
                }
            }
        }

        // dd($allTags);
        // echo 'here';
        //  dd('It here', $ser_id);
        // dd($id,'Here');
        return view('salon-magmt/managetags',compact('page_title'), ['salon_types' => $salon_types, 'allServices' => $allTags, 'ser_id' => $ser_id, 'ser_name' => $ser_name]);
    }

    public function updatetagservices(Request $request) {


        // If User has no selected any service then just call back view before 
       
        $sty_id = $request->input('ser_id');
        // dd($sty_id);

        if (empty($request['sal_type_status'])) {
            //  dd('Its empty');

            $result = DB::table('service_tags')
                    ->Where('ser_id', $sty_id)
                    ->delete();


            //  $salons = DB::select('select sty_name, sty_status, sty_id,  (SELECT count(*) from salon_types_services a where a.sty_id = s.sty_id) as tota_services_type from salon_types s LIMIT 20');
            //   return view('salon-magmt/salon_types', ['salons' => $salons]);
            $allServices = DB::table('services')->paginate(10);
            return view('salon-magmt/all-services', ['allServices' => $allServices]);
        }

        //to delete service in case user uncheck some service.
        // we will delete all expect exist in selected array against selected salon type.

        $result = DB::table('service_tags')
                ->whereNotIn('ser_id', $request['sal_type_status'])
                ->Where('ser_id', $sty_id)
                ->delete();

        //dd($sty_id);

        if (!empty($request['sal_type_status'])) {
            foreach ($request['sal_type_status'] as $tag_id) {

                $result = DB::statement('INSERT INTO service_tags (tag_id, ser_id) VALUES (' . $tag_id . ', ' . $sty_id . ') ON DUPLICATE KEY UPDATE tag_id = 
    	              tag_id, ser_id=ser_id');
                
            }
        }
                
        //$salons = DB::select('select sty_name, sty_status, sty_id,  (SELECT count(*) from salon_types_services a where a.sty_id = s.sty_id) as tota_services_type from salon_types s LIMIT 20');
        // return view('salon-magmt/salon_types', ['salons' => $salons]);
        // $service = $request->input('services[]');
        $allServices = DB::table('services')->paginate(10);;
        return view('salon-magmt/all-services', ['allServices' => $allServices]);
    }

    function array_sort($array, $on, $order = SORT_ASC) {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    public function services_search(Request $request)
    {
        $ser_name = '';
        $page_title = "All Services";
        $ser_name = $request->ser_name;
        $data = DB::table('services');
        if(!empty($ser_name))
        {
            $data = $data->where("ser_name","like","%".$ser_name."%");
        }
        $allServices = $data->paginate(10);
        Session::put("activemenu","allServices");
        return view('salon-magmt/all-services', compact('page_title','ser_name'),['allServices' => $allServices]);

    }

}
