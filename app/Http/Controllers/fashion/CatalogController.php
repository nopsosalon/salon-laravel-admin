<?php

namespace App\Http\Controllers\fashion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use App\Helpers\Helper;
use Carbon\Carbon;
use App\Model\FashBrand;
use DateTime;
use Config;
use File;
use Auth;
use App;
use DB;

class CatalogController extends Controller
{
    //
    public function __construct()
    {        
        ini_set('memory_limit','512M');
        $this->middleware('auth');
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }

    public function catalog(){
        $returnback = \URL::previous();
        $id = null;
    	$page_title = '';
        $fash_brand = [];
        $fash_brands = FashBrand::with('FashBrandCollection')
                ->where('fb_status',1)
                ->get()->toArray();
       $fash_brand = $fash_brands;
        
    	return view('fashions/catalog/catalog',compact('fash_brand','returnback','id','fash_brands','page_title'));
    }
    public function get_catalogs($id){

        $returnback = \URL::previous();
        $id = $id;
        $fb_id = DB::table('fash_brand_collections')->where('fbc_id',$id)->first()->fb_id;
        $fb_name = DB::table('fash_brands')->where('fb_id',$fb_id)->first()->fb_name;
        // dd($fb_name);
        $data = DB::table('fbc_images')->where('fbc_id',$id)->where('fbci_status',1)->get();
        $totalimages = count($data);
        $fash_brand = [];
        $fash_brands = FashBrand::with('FashBrandCollection')
                        ->where('fb_status',1)
                        ->get()->toArray();
        $page_title = '';
        $fash_brand = $fash_brands;

        return view('fashions/catalog/catalog',compact('fb_name','totalimages','fb_id','fash_brand','returnback','id','data','fbc','page_title'));
    }
    public function delete_catalog_image($id){
        $helper = new Helper();
        $files = DB::table('fbc_images')->where('fbci_id',$id)->first();
        
        if($files){
            $file = $files->fbci_name; 
            $filename = $helper->fashion_image_display()."/".$file;
                if (file_exists($filename)) 
                {   
                    \File::delete($filename);                         
                }
            DB::table('fbc_images')->where('fbci_id',$id)
                        ->update(['fbci_status' => 0]);
        }
        return back();
    }

    public function get_data(){
        $data = FashBrand::with('FashBrandCollection')->get()->toArray();
        dd($data);
        $decodedJSON = json_decode(($brandsData));
        dd($decodedJSON);
    }

    public function delete_images_fbci(Request $request){
         $helper = new Helper();
         $fbci_name = $request->fbci_name;
         $fbci_id = $request->fbci_id;
         $fbcids = '';
         foreach ($fbci_id as $key => $value) {
             $fbcids = $fbcids.','.$value;
         }
         $fbci_ids = trim($fbcids,",");
         DB::statement("UPDATE fbc_images SET fbci_status = 0 WHERE fbci_id IN($fbci_ids)");
         foreach ($fbci_name as $key => $value) {
             $filename = $helper->fashion_image_display()."/".$value;
                if (file_exists($filename)) 
                {   
                    \File::delete($filename);                         
                }
         }
        
        return back()->with("message","Record Deleted Successfully !");
    }
}
