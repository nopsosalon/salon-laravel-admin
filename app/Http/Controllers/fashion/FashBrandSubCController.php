<?php

namespace App\Http\Controllers\fashion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use App\Helpers\Helper;
use Carbon\Carbon;
use DateTime;
use Config;
use File;
use Auth;
use App;
use DB;
use App\Model\FashBrand;
// use App\Model\FashBrandCollection;


class FashBrandSubCController extends Controller
{
    
     public function __construct()
    {        
        ini_set('memory_limit','512M');
        $this->middleware('auth');
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }
     public function addFashBrandSubcate(Request $request,$id){
        // dd($id);
        $returnback = \URL::previous();
        $helper = new Helper();
        $cate_images = $helper->fashion_image_display();
        $page_title = "";
        $fb = DB::table('fash_brands')->orderBy('fb_name','ASC')->get();
        $fc = DB::table('fash_collections')->orderBy('fc_title','ASC')->get();
        // dd($fb);
        return view('fashions/fash_brand_subcate/add_fash_brand_subcategory',compact('returnback','id','fc','fb','page_title','cate_images'));

    }
    public function store_fashbrandcollection(Request $request,$id){
        $helper = new Helper();
        $urls = $request->returnback;
    	$data['fbc_name'] 	= $request->fbc_name;
        $data['fb_id']  = $id;
        $data['fc_id']  = $request->fc_id;
        $data['fbc_status']  = $request->fbc_status;
        $data['fbc_link'] 	= $request->fbc_link;
        $data['fbc_gender']  = $request->fbc_gender;
        $data['fbc_featured']  = $request->fbc_featured == null ?  '' : $request->fbc_featured;
        $data['fbc_modify_datetime'] = \Carbon\Carbon::now()->toDateTimeString();

        if (Input::hasFile('fbc_image')) {
            $data['fbc_image'] = "fbc".time() . '.' . $request->fbc_image->getClientOriginalExtension();
            $destinationPath    = $helper->fashion_image_store();  
            // if(Session::get('change_db_connection')=="3"){
            //     $helper->uploadimageFTP('/fashion_images/'.$data['fbc_image'],$request->fbc_image);
            // }     
            $request->fbc_image->move($destinationPath, $data['fbc_image']);    
        }
    	DB::table('fash_brand_collections')->insert($data);
    	$fbc_id = DB::getPdo()->lastInsertId();
        // dd("okja");
        return redirect()->to($urls);
    	return $fbc_id;
    }
     //  save dropzop images saves in database and folders 
    public function add_fbc_images(Request $request,$id){
            Session::put('active_tabs','product_color_images');
            $file = $request->file('file');
            if (\File::exists($file)) {
                $helper = new Helper();
                $image_name = $file->getClientOriginalName();
                $from=substr($image_name, 0, (strlen ($image_name)) - (strlen (strrchr($image_name,'.'))));

                $product['fbci_name'] = $from.'.'.$file->getClientOriginalExtension();

                $fbci_name = $image_name;
                $fbc_id = $id;
                $fbci_datetime = Carbon::now()->toDateTimeString();
                
                $file_name = $file->getClientOriginalName(); //Get file original name
                DB::statement("INSERT INTO fbc_images(fbci_name,fbc_id,fbci_datetime) VALUES('".$image_name."','".$fbc_id."','".$fbci_datetime."') 
                  ON DUPLICATE KEY UPDATE fbc_images.delete_images=fbc_images.fbci_name");

                $image_names = DB::table('fbc_images')->select('fbci_name')->where('delete_images','<>','')->get();
                if($image_names){
                    foreach ($image_names as $key => $value) {
                        $file = $value->fbci_name;
                        $filename = $helper->fashion_image_display()."/".$file;
                        if (file_exists($filename)) 
                        {   
                            \File::delete($filename);                         
                        }
                    }
                    DB::statement(" UPDATE fbc_images fbci_name set fbci_name = delete_images WHERE delete_images <> '' ");
                    DB::statement(" UPDATE fbc_images delete_images set delete_images = '' WHERE delete_images <> '' ");
                }
                $destinationPath = $helper->fashion_image_store(); 
                //   if(Session::get('change_db_connection')=="3"){
                //     $helper->uploadimageFTP('/fashion_images/'.$product['fbci_name'],$file);
                // }  
                $file->move($destinationPath,$product['fbci_name']);
                 // $data['fbc_modify_datetime'] = \Carbon\Carbon::now()->toDateTimeString();
                DB::table('fash_brand_collections')->where('fbc_id',$id)->update([
                            'fbc_modify_datetime' => \Carbon\Carbon::now()->toDateTimeString(),
                        ]);
           }
           
        echo "data saved successfully ";
        return response()->json([
            'message' => 'Image saved Successfully !'
        ], 200);
        
    }

    public function edit_brand_collection(Request $request, $id,$fb_id){
        $returnback = \URL::previous();
        $fc_id = DB::table('fash_brand_collections')->where('fbc_id',$id)->first()->fc_id;
    	$helper = new Helper();
        $cate_images = $helper->fashion_image_display();
        $page_title = "";
        $fc = DB::table('fash_collections')->orderBy('fc_title','DESC')->get();
    	$fbc = DB::table('fash_brand_collections')->where('fbc_id',$id)->first();

    	 return view('fashions/fash_brand_subcate/update_fashbrandc',compact('fc_id','returnback','fb_id','id','fc','fbc','page_title','cate_images'));   
    }
    public function updatedBrand_collection(Request $request,$id,$fb_id){
        $helper = new Helper();
        $urls = $request->returnback;
    	$data['fbc_name']  = $request->fbc_name;
        $data['fb_id']  = $fb_id;
        $data['fc_id']  = $request->fc_id;
        $data['fbc_status']  = $request->fbc_status;
        $data['fbc_link']   = $request->fbc_link;
        $data['fbc_gender']  = $request->fbc_gender;
        $data['fbc_featured']  = $request->fbc_featured == null ?  '' : $request->fbc_featured;
        $data['fbc_modify_datetime'] = \Carbon\Carbon::now()->toDateTimeString();
        if($request->hasFile('fbc_image'))
            {
                $files = DB::table('fash_brand_collections')->where('fbc_id',$id)->first();
                $file = $files->fbc_image; 
                $filename = $helper->fashion_image_display()."/".$file;
            if (file_exists($filename)) 
            {   
                \File::delete($filename);                         
            }
            $image              = Input::file('fbc_image');
            $data['fbc_image'] = 'fbc_'.str_random(5).'.'.$image->getClientOriginalName();
            $destinationPath    = $helper->fashion_image_store();  
            // if(Session::get('change_db_connection')=="3"){
            //     $helper->uploadimageFTP('/fashion_images/'.$data['fbc_image'],Input::file('fbc_image'));
            // }     
            $image->move($destinationPath, $data['fbc_image']);    
        }
    	DB::table('fash_brand_collections')->where('fbc_id',$id)->update($data);
         return redirect()->to($urls);
    	return redirect()->to('admin/fashionBrand');
    }
    public function fashionBrandCollection(Request $request,$id){
        $returnback = \URL::previous();
    	$helper = new Helper();
        $cate_images = $helper->fashion_image_display();
        $page_title = "";
        $fbc_name = $request->fbc_name;
        $fbc_status = $request->fbc_status;

        $fb = DB::table('fash_brands')->where('fb_id',$id)->first();
        $data = DB::table('fash_brand_collections')->where('fb_id',$id);
        if(!empty($fbc_name) && !empty($fbc_status)){
            if($fbc_status == "all_active")
                {
                    $data = $data->where('fbc_name','LIKE','%'.$fbc_name.'%');
                }else{
                    if($fbc_status == "1")
                    {
                        $data = $data->where('fbc_name','LIKE','%'.$fbc_name.'%')->where('fbc_status','1');  
                    }
                    elseif($fbc_status == "2")
                    {
                        $data = $data->where('fbc_name','LIKE','%'.$fbc_name.'%')->where('fbc_status','0'); 
                    }
                }
        }elseif(!empty($fbc_name) && empty($fbc_status)){
            $data = $data->where('fbc_name','LIKE','%'.$fbc_name.'%');
        }elseif(empty($fbc_name) && !empty($fbc_status)){
            if($fbc_status == "all_active")
                {
                    $data = $data->where('fbc_status',0)->orWhere('fbc_status',1);
                }else{
                    if($fbc_status == "1")
                    {
                        $data = $data->where('fbc_status','1');  
                    }
                    elseif($fbc_status == "2")
                    {
                        $data = $data->where('fbc_status','0'); 
                    }
                }
        }
        $fbc = $data->paginate(10);
        // dd($fbc);
        return view('fashions/fash_brand_subcate/fashbrand_subcategory',compact('fbc_name','fbc_status','returnback','id','fbc','page_title','fb','cate_images'));
    }
    public function deleteBrand($id){
    	DB::table('fash_collections')->where('fc_id',$id)->delete();
    	return back()->with('message','Recored Deleted Successfully ');
    }
}
