<?php

namespace App\Http\Controllers\fashion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use App\Helpers\Helper;
use Carbon\Carbon;
use DateTime;
use Config;
use File;
use Auth;
use App;
use DB;

class FashBrandController extends Controller
{
    //
    public function __construct()
    {        
        ini_set('memory_limit','512M');
        $this->middleware('auth');
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }
     public function addFashBrand(Request $request) {
        $returnback = \URL::previous();
        $helper = new Helper();
        $cate_images = $helper->fashion_image_display();
        $page_title = "";
        // dd("brands");
        return view('fashions/fash_brands/add_fash_brand',compact('returnback','page_title','cate_images'));

    }
    public function store_factionBrand(Request $request){
        $helper = new Helper();
    	$data['fb_name'] 	= $request->fb_name;

    	$data['fb_status'] 	= $request->fb_status;
        $data['fb_gender']  = $request->fb_gender;
        $data['fb_featured']  = $request->fb_featured == null ?  '' : $request->fb_featured;
        $data['fb_status']  = $request->fb_status;

        if (Input::hasFile('fb_image')) {
            $data['fb_image'] = "fb".time() . '.' . $request->fb_image->getClientOriginalExtension();
            $destinationPath    = $helper->fashion_image_store(); 
            // if(Session::get('change_db_connection')=="3"){
            //     $helper->uploadimageFTP('/fashion_images/'.$data['fb_image'],$request->fb_image);
            // }      
            $request->fb_image->move($destinationPath, $data['fb_image']);    
        }
        // dd($data);
    	DB::table('fash_brands')->insert($data);
    	// dd("data inserted now ");
    	return redirect()->to('admin/fashionBrand');
    }
    public function edit_brand(Request $request, $id){
    	$returnback = \URL::previous();
    	$helper = new Helper();
        $cate_images = $helper->fashion_image_display();
        $page_title = "";
    	$fb = DB::table('fash_brands')->where('fb_id',$id)->first();
        // dd($fb);
    	 return view('fashions/fash_brands/add_fash_brand',compact('returnback','fb','page_title','cate_images'));   
    }
    public function updatedBrand(Request $request,$id){
        $helper = new Helper();
    	$data['fb_name']   = $request->fb_name;
        $data['fb_status']  = $request->fb_status;
        $data['fb_gender']  = $request->fb_gender;
        $data['fb_featured']  = $request->fb_featured == null ?  '' : $request->fb_featured;
    	// dd($data);
        if (Input::hasFile('fb_image')) {
            $data['fb_image'] = "fb".time() . '.' . $request->fb_image->getClientOriginalExtension();
            $fb = DB::table('fash_brands')->where('fb_id',$id)->first();
            if($fb){
                $fb_image = $helper->fashion_image_display()."/".$fb->fb_image;   
                // dd($fb_image);     
                if (File::exists($fb_image) && $fb->fb_image != '') 
                { 
                    unlink($fb_image);
                }
            }
            $destinationPath    = $helper->fashion_image_store();     
            //  if(Session::get('change_db_connection')=="3"){
            //     $helper->uploadimageFTP('/fashion_images/'.$data['fb_image'],$request->fb_image);
            // }
            $request->fb_image->move($destinationPath, $data['fb_image']);    
        }
    	DB::table('fash_brands')->where('fb_id',$id)->update($data);
    	return redirect()->to('admin/fashionBrand');
    }
    public function fashionBrand(Request $request){
    	// dd($request->all());
    	$helper = new Helper();
        $cate_images = $helper->fashion_image_display();
        $page_title = "";
        $fb_status = $request->fb_status;
        $fb_name = $request->fb_name;
        $data = DB::table('fash_brands');

        if(!empty($fb_name) && !empty($fb_status)){
            if($fb_status == "all_active")
                {
                    $data = $data->where('fb_name','LIKE','%'.$fb_name.'%');
                }else{
                    if($fb_status == "1")
                    {
                        $data = $data->where('fb_name','LIKE','%'.$fb_name.'%')->where('fb_status','1');  
                    }
                    else if($fb_status == "2")
                    {
                        $data = $data->where('fb_name','LIKE','%'.$fb_name.'%')->where('fb_status','0'); 
                    }
                }
        }else if(!empty($fb_name) && empty($fb_status)){
            $data = $data->where('fb_name','LIKE','%'.$fb_name.'%');
        }else if(empty($fb_name) && !empty($fb_status)){
            if($fb_status == "all_active")
                {
                    $data = $data->where('fb_status',0)->orWhere('fb_status',1);
                }else{
                    if($fb_status == "1")
                    {
                        $data = $data->where('fb_status','1');  
                    }
                    elseif($fb_status == "2")
                    {
                        $data = $data->where('fb_status','0'); 
                    }
                }
        }
        
            // dd($fb_name);
        $fb = $data->orderBy('fb_id','DESC')->paginate(10);
        // dd($fb);
        return view('fashions/fash_brands/fash_brands',compact('fb_name','fb_status','page_title','fb','cate_images'));
    }
    public function deleteBrand($id){
    	// dd($id);
    	DB::table('fash_collections')->where('fc_id',$id)->delete();
    	return back()->with('message','Recored Deleted Successfully ');
    }
}
