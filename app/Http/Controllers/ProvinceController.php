<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use App\Model\States;
use DateTime;
use Config;
use Auth;
use App;
use App\Helpers\Helper;

class ProvinceController extends Controller
{

	public function __construct()
    {        
        ini_set('memory_limit','512M');
        $this->middleware('auth');
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }

    //
    public function province(){
    	$page_title = "Province";
        $pro = DB::table('states')->orderBy('p_order_by','ASC')->get();
        return view('province/province',compact('pro','page_title'));
    }
    public function add_province(){
        return view('province/add_province');
    }
    public function store_province(Request $request){
        $data['name'] = $request['name'];
        $data['country_id'] = '166';
        DB::table('states')->insert($data);
        return redirect()->to('province');
    }
    public function edit_province($id){
    	$page_title = 'Province';
        $pro = DB::table('states')->where('id',$id)->first();
        return view('province/add_province',compact('pro','page_title'));
    }
    public function update_province(Request $request,$id){
    	// dd($request['name']);
        $data['name'] = $request['name'];
        $data['country_id'] = '166';
        DB::table('states')->where('id',$id)->update($data);
        return redirect()->to('province');   
    }
    // public function delete_province($id){
    //     DB::table('contact_us')->where('cs_id',$id)->delete();
    //     return back();
    // }
    // ////////////////////////////////////////////////////////////////////////
    public function cities($state_id){
    	$page_title = '';
    	// dd($state_id);
        $city = DB::table('cities as c')->select('c.*','s.name as state_name')->join('states as s','s.id','=','c.state_id')->where('state_id',$state_id)->orderBy('c.name','ASC')->paginate(50);
        return view('province/cities',compact('city','page_title','state_id'));
    }
    public function add_cities($state_id){
    	$page_title = '';
        return view('province/add_cities',compact('state_id','page_title'));
    }
    public function store_cities(Request $request,$state_id){
    	// dd($state_id);
        $data['name'] = $request['name'];
        $data['city_description'] = $request['city_description'];
        $data['state_id'] = $state_id;
        DB::table('cities')->insert($data);
        return back()->with('flash_message','City Added Successfully ');
        // return redirect()->to('admin/contactus');
    }
    public function edit_cities($state_id,$id){
    	$page_title = '';
        $city = DB::table('cities')->where('state_id',$state_id)->where('id',$id)->first();
        return view('province/add_cities',compact('city','page_title','state_id'));
    }
    public function update_cities(Request $request,$state_id,$id){
        $data['name'] = $request['name'];
        $data['city_description'] = $request['city_description'];
        $data['state_id'] = $state_id;
        DB::table('cities')->where('id',$id)->update($data);
        return back()->with('flash_message','City Updated Successfully ');
        // return redirect()->to('admin/contactus');   
    }
    // public function delete_cities($id){
    //     DB::table('cities')->where('id',$id)->delete();
    //     return back();
    // }



    // ////////////////////////////////////////////////////////////////////////
    public function salon_areas(){
        $page_title = '';

        $salon_areas = DB::table('salon_areas')->orderBy('sal_a_id','DESC')->paginate(50);
        return view('salon_area_des/salon_area_des',compact('salon_areas','page_title'));
    }
    public function add_area(){
        $city = States::with('getCity')->orderBy('p_order_by','ASC')
                ->get()->toArray();
            // dd($city);
        $page_title = '';

        return view('salon_area_des/add_salon_area_des',compact('city','page_title'));
    }
    public function store_area(Request $request){
        $data['sal_a_name'] = $request['sal_a_name'];
        $data['sal_a_description'] = $request['sal_a_description'];
        $data['sal_a_city'] = $request['sal_a_city'];
        $data['sal_a_status'] = $request['sal_a_status'];
        $data['sal_a_created_at'] = Carbon::now()->toDateTimeString();
        DB::table('salon_areas')->insert($data);

        return back()->with('flash_message','City Added Successfully ');
    }
    public function edit_area($id){
        $page_title = '';
        $city = States::with('getCity')->orderBy('p_order_by','ASC')
                ->get()->toArray();
        $salon_area = DB::table('salon_areas')->where('sal_a_id',$id)->first();
        return view('salon_area_des/add_salon_area_des',compact('salon_area','city','page_title'));
    }
    public function update_area(Request $request,$id){
        $data['sal_a_name'] = $request['sal_a_name'];
        $data['sal_a_description'] = $request['sal_a_description'];
        $data['sal_a_city'] = $request['sal_a_city'];
        $data['sal_a_status'] = $request['sal_a_status'];
        $data['sal_a_created_at'] = Carbon::now()->toDateTimeString();

        DB::table('salon_areas')->where('sal_a_id',$id)->update($data);
        return back()->with('flash_message','City Updated Successfully ');  
    }
    public function delete_area($id){
        DB::table('salon_areas')->where('sal_a_id',$id)->delete();
        return back();
    }
}
