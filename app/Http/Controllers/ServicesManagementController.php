<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Config;
use Auth;
use App;
use App\Helpers\Helper;


class ServicesManagementController extends Controller
{

    public function __construct()
    {

            $this->middleware('auth');
            $dbc = new Helper();
            $dbc->setDBConnection();
     
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function AddnewTypeService()
    {
        
        return view('service-mgmt/NewTypeService');
    }
    public function saveNewTypeService(Request $request)
    {
        
        $salonType = array();

        $salonType['sty_name'] = $request->input('sty_name');
        $salonType['sty_status'] = $request->input('sty_status');
        
//         if (Input::hasFile('cat_image')) {
//             $cat_data['ssc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
//             $request->cat_image->move(public_path('category_images'), $cat_data['ssc_image']);
// //         dd($cat_data);
//         }
        DB::table('salon_types')->insert($salonType);
//         $request->session()->flash('alert-success', 'Category was successfuly added!');
        Session::flash('flash_message', 'Salon Type successfully added.');

        return redirect()->back();

    }

}
