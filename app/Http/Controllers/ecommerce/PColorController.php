<?php

namespace App\Http\Controllers\ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use Carbon\Carbon;
use Response;
use Session;
use Input;
use URL;
use DB;
use File;

class PColorController extends Controller
{
 
 	public function __construct()
    {        
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }   

    public function add_product_colors(){
    	$page_title = "Add Product Colors";
    	$return_back = URL::previous();
    	$products = DB::table('products')->get();

    	return view('ecommerce/product_colors/add_product_colors',compact('page_title','return_back','products'));
    }

    public function get_product_id_for_colors(Request $request){
        $p_id = $request->product_id;
        return "product id =  ".$p_id;
    }
    public function save_product_colors_images(Request $request){
            Session::put('active_tabs','product_color_images');
            $file = $request->file('file');
        // dd("oka");
            if (\File::exists($file)) {
                $product['p_id'] = $_POST['p_id'];
                $helper = new Helper();
                // $product['p_id'] = $p_id;
                $image_name = $file->getClientOriginalName();
                $product['pcl_code'] = pathinfo($image_name, PATHINFO_FILENAME);
                $product['pcl_created_at'] = Carbon::now()->toDateTimeString();
                $product['pcl_modified_date'] = Carbon::now()->toDateTimeString();
                $product['pcl_image'] = 'pcl_'.str_random(5).'.'. $file->getClientOriginalExtension();
                $destinationPath = $helper->pc_image_store(); 
                // if(Session::get('change_db_connection')=="3"){
                //     $helper->uploadimageFTP('/products_images/'.$product['pcl_image'],$file);
                // }   
                $file->move($destinationPath, $product['pcl_image']); 
                DB::table('product_colors')->insert($product);   
           }
        echo "data saved successfully ";
        return \Response::json([
            'message' => 'Image saved Successfully'
        ], 200);
        
    }
    public function delete_color_images($pcl_id){
        $helper = new Helper();
        $files = DB::table('product_colors')->where('pcl_id',$pcl_id)->first();
        $file = $files->pcl_image; 
            $filename = $helper->pc_image_display()."/".$file;
        if (file_exists($filename)) 
        {   
            \File::delete($filename);                         
        }
        DB::table('product_colors')->where('pcl_id',$pcl_id)->delete();
        return back()->with('message','Image Deleted Successfuly ');
    }
}
