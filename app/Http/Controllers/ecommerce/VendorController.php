<?php

namespace App\Http\Controllers\ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Helpers\Helper;
use Session;
use DB;
use View;
use URL;

class VendorController extends Controller
{
    //
    
    public function __construct()
    {        
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }

    public function product_vendors(){
    	$page_title = "Product Vendors";
    	$pv = DB::table('product_vendors')->orderBy('pv_id','DESC')->get();
    	// dd('oka');
    	return view('ecommerce/vendors/product_vendors',compact('page_title','pv'));
    }
    public function add_vendor(){
    	$page_title = 'Add Product Vendor';
    	$return_back = URL::previous();
    	return view('ecommerce/vendors/add_vendor',compact('page_title','return_back'));
    }
    public function store_vendor(Request $request){
    	$data['pv_name'] 		= $request->pv_name;
    	$data['pv_address'] 	= $request->pv_address;
    	$data['pv_email']		= $request->pv_email;
    	$data['pv_contact']		= $request->pv_contact;
    	$data['pv_status']		= $request->pv_status;
        $data['pv_password']      = $request->pv_password;

    	$data['pv_created_at']	= Carbon::now()->toDateTimeString();
    	$data['pv_modified_date']	= Carbon::now()->toDateTimeString();
    	DB::table('product_vendors')->insert($data);
    	return redirect()->to('admin/product-vendors');
    }
    public function edit_vendor($id){
    	$page_title = 'Update Product vendor Detail';
    	$return_back = URL::previous();
    	$pv = DB::table('product_vendors')->where('pv_id',$id)->first();
    	return view('ecommerce/vendors/add_vendor',compact('pv','page_title','return_back'));
    }
    public function update_vendor(Request $request, $id){
    	$data['pv_name'] 		= $request->pv_name;
    	$data['pv_address'] 	= $request->pv_address;
    	$data['pv_email']		= $request->pv_email;
    	$data['pv_contact']		= $request->pv_contact;
    	$data['pv_status']		= $request->pv_status;
        $data['pv_password']      = $request->pv_password;
    	$data['pv_modified_date']	= Carbon::now()->toDateTimeString();
    	DB::table('product_vendors')->where('pv_id',$id)->update($data);
    	return redirect()->to('admin/product-vendors');
    }
    public function delete_vendor($id){
    	$pv = DB::table('product_vendors')->where('pv_id',$id)->delete();
    	return back()->with('message','Record Deleted Successfully');
    }
    public function pv_enable($id){
    	$pv = DB::table('product_vendors')->where('pv_id',$id)->update(array('pv_status'=>'1'));
    	return back()->with('message','Record Enable Successfully');	
    }
    public function pv_diable($id){
    	$pv = DB::table('product_vendors')->where('pv_id',$id)->update(array('pv_status'=>'0'));
    	return back()->with('message','Record Enable Successfully');	
    }
    public function pv_search(Request $request){
        Session::put("activemenu","product_categories");
        $page_title = "Search Product Vendors";
        $pv_name    = "";
        $pv_status  = "";
        try {
            $data = DB::table('product_vendors');
            if($request->pv_name){
                $pv_name = $request->pv_name;
                $data = $data->where('pv_name','LIKE','%'.$pv_name.'%');
            }
            if($request->pv_status){
                $pv_status = $request->pv_status;
                if($pv_status=='all'){
                    $data = $data->where('pv_status',1)->orWhere('pv_status',0);
                }elseif($pv_status == '1' ){
                    $data = $data->where('pv_status',1);
                }elseif($pv_status== '2' ){
                    $data = $data->where('pv_status',0);
                }
            
            }
            $pv = $data->paginate(10);
        } catch (ModelNotFoundException $e) {
            return back()->withError($exception->getMessage())->withInput();
        }
        return view('ecommerce/vendors/product_vendors',compact('page_title','pv','pv_name','pv_status'));
    }
}
