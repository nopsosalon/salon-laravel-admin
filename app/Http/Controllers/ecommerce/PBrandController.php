<?php

namespace App\Http\Controllers\ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Helpers\Helper;
use Session;
use DB;
use URL;
use File;
use Response;
use Input;

class PBrandController extends Controller
{
    public function __construct()
    {        
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }

    public function manage_brands($id){
    	$page_title = 'Product Brands';
    	$return_back_vendor = URL::previous();
    	$pv_id = $id;
    	$pb = DB::table('product_brands')
    				->join('product_vendors','product_vendors.pv_id','product_brands.pv_id')
                    ->where('product_brands.pv_id',$id)
                    ->orderBy('pb_id','DESC')->get();
    	return view('ecommerce/vendors/product_brand/product_brand',compact('pv_id','pb','page_title','return_back_vendor'));
    }

    public function add_pbrand($id){
    	$pv_id = $id;
    	$page_title = 'Add Product Brand';
    	$return_back = URL::previous();
    	$pv = DB::table('product_vendors')->get();
    	return view('ecommerce/vendors/product_brand/add_product_brand',compact('pv','pv_id','page_title','return_back'));
    }
    public function store_pbrand(Request $request){
    	$return_back = $request->return_back;
    	$data['pb_name'] 		= $request->pb_name;
    	$data['pb_manufacturer'] 	= $request->pb_manufacturer;
    	$data['pb_status']		= $request->pb_status;
    	$data['pv_id']		= $request->pv_id;
    	$data['pb_created_at']	= Carbon::now()->toDateTimeString();
    	$data['pb_modified_date']	= Carbon::now()->toDateTimeString();

        $helper = new Helper();

        if($request->hasFile('pb_image'))
            {
                $image              = Input::file('pb_image');
                $destinationPath    = $helper->pc_image_store();
                $filename           = strtotime("now").$image->getClientOriginalName();
                // if(Session::get('change_db_connection')=="3"){
                //     $helper->uploadimageFTP('/products_images/'.$filename,Input::file('pb_image'));
                // }  
                $image->move($destinationPath, $filename);          
                $data['pb_image']    = $filename;
            }

    	DB::table('product_brands')->insert($data);
    	return redirect()->to($return_back);
    }
    public function edit_pbrand($id,$pv_id){
    	$page_title = 'Update Product Brand Detail';
    	$return_back = URL::previous();
    	$pv = DB::table('product_vendors')->get();
    	$pb = DB::table('product_brands')->where('pb_id',$id)->first();
    	return view('ecommerce/vendors/product_brand/add_product_brand',compact('pv_id','pv','pb','page_title','return_back'));
    }
    public function update_pbrand(Request $request, $id){
    	$return_back = $request->return_back;
    	$data['pb_name'] 		= $request->pb_name;
    	$data['pb_manufacturer'] 	= $request->pb_manufacturer;
    	$data['pb_status']		= $request->pb_status;
    	$data['pv_id']		= $request->pv_id;
    	$data['pb_modified_date']	= Carbon::now()->toDateTimeString();
        $helper = new Helper();
        if($request->hasFile('pb_image'))
            {
                $files = DB::table('product_brands')->where('pb_id' , $id)->first();
                if(!empty($files)){
                    $file = $files->pb_image; 
                    $filename = $helper->salonImages()."/".$file;
                    if (file_exists($filename)) 
                    {   
                        \File::delete($filename);                         
                    }
                }
                $image              = Input::file('pb_image');
                $destinationPath    = $helper->pc_image_store();
                $filename           = strtotime("now").$image->getClientOriginalName();
                // if(Session::get('change_db_connection')=="3"){
                //     $helper->uploadimageFTP('/products_images/'.$filename,Input::file('pb_image'));
                // } 
                $image->move($destinationPath, $filename);            
                $data['pb_image']    = $filename;
            }
    	DB::table('product_brands')->where('pb_id',$id)->update($data);
    	return redirect()->to($return_back);
    }
    public function delete_pbrand($id){
    		DB::table('product_brands')->where('pb_id',$id)->delete();
    	return back()->with('message','Record Deleted Successfully');
    }
    public function pb_enable($id){
    	DB::table('product_brands')->where('pb_id',$id)->update(array('pb_status'=>'1'));
    	return back()->with('message','Record Enable Successfully');	
    }
    public function pb_diable($id){
    	DB::table('product_brands')->where('pb_id',$id)->update(array('pb_status'=>'0'));
    	return back()->with('message','Record Enable Successfully');	
    }

    public function pb_search(Request $request,$id){
        $page_title = 'Search Product Brands';
        $return_back_vendor = URL::previous();
        $pv_id = $id;
        $pb_name    = "";
        $pb_status  = "";
        try {
            $data = DB::table('product_brands')
                    ->join('product_vendors','product_vendors.pv_id','product_brands.pv_id');
            if($request->pb_name){
                $pb_name = $request->pb_name;
                $data = $data->where('pb_name','LIKE','%'.$pb_name.'%');
            }
            if($request->pb_status){
                $pb_status = $request->pb_status;
                if($pb_status=='all'){
                    $data = $data->where('pb_status',1)->orWhere('pb_status',0);
                }elseif($pb_status == '1' ){
                    $data = $data->where('pb_status',1);
                }elseif($pb_status== '2' ){
                    $data = $data->where('pb_status',0);
                }
            
            }
            $pb = $data->orderBy('pb_id','DESC')->paginate(10);
        } catch (ModelNotFoundException $e) {
            return back()->withError($exception->getMessage())->withInput();
        }

        // dd($pb);
        return view('ecommerce/vendors/product_brand/product_brand',compact('pb_name','pb_status','pv_id','pb','page_title','return_back_vendor'));
    }
}
