<?php

namespace App\Http\Controllers\ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use Carbon\Carbon;
use Session;
use Input;
use DB;
use URL;

class FeaturedController extends Controller
{
    //
    public function __construct()
    {        
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }
    public function featuredProducts(Request $request){
    	$page_title = "Featured Products";
        $featuredp = DB::table('products')->where('is_featured',1)->orderBy('p_modified_date','DESC')->get();
      
        return view('ecommerce/featured_products/featured_product',compact('featuredp','page_title'));
    }
    public function isfeatured(Request $request){
    	$helper = new Helper();
    	$pc_image_path = $helper->pc_image_display();
    	// echo "featured";
    	$p_name = $request->p_name;
    	$product = DB::table('products')->where('p_name','LIKE',$p_name.'%')->get();
    	 
		if(count($product) > 0){
			$output = '<ul class="dropdown-menu" style="display:block; position:relative">';
		      foreach($product as $row)
		      {
		       $output .= '
		       <li><a href="#">'.$row->p_name.'</a></li>
		       ';
		      }
		      $output .= '</ul>';
		      echo $output;

		}

    }
    public function get_product(Request $request){
    	$helper = new Helper();
    	$pc_image_path = $helper->pc_image_display();
    	$p_name = $request->p_name;
    	$product = DB::table('products')->where('p_name','=',$p_name)->first();
    	 $status = '';
            if($product->p_status==1){
              $status = 'Active';
            }else if($product->p_status==0){
              $status = 'Inactive';
            }
            $featured = '';
            $is_featured = '';
            if($product->is_featured == 1){
            	$url = url('admin/is_featured_disable', ['id' => $product->p_id]);
            	$is_featured = '<a href="'.$url.'" class="btn btn-info" title="Yes">
            						<i class="glyphicon glyphicon-ok"></i>
            					</a>';
            	$featured = 'Yes';
            }elseif(($product->is_featured == 0)){
            	$url = url('admin/is_featured_enable', ['id' => $product->p_id]);
            	$is_featured = '<a href="'.$url.'" class="btn btn-danger" title="No">
            						<i class="glyphicon glyphicon-remove"></i>
            					</a>';
            	$featured = 'No';
            }
            $data = '';
            // die();

    	$data = '<table id="salon_tabel"  class="table table-hover"><tr><th>Name</th><th>Image</th><th>Weight</th><th>Product Cost</th><th>Retail Price</th><th>Product Price</th><th>Product Dimention</th><td>Status </td><th><span>Is Featured</span></th><th><span>Action</span></th></tr><td>'.$product->p_name.'</td><td><img src="'.$pc_image_path.'/'.$product->p_image.'" width="100px" height="100px"></td><td>'.$product->p_weight.'</td><td>'.$product->p_cost.'</td><td>'.$product->p_rrp.'</td><td>'.$product->p_price.'</td><td>'.$product->p_dimention.'</td><td>'.$status.'</td><td>'.$featured.'<td>'.$is_featured.'</td></tr></table>';

    	echo $data;
    	// die();
    }
    public function is_featured_enable($id){
    	DB::table('products')->where('p_id',$id)->update([
                'is_featured' => 1,
                'p_modified_date' => Carbon::now()->toDateTimeString(),
            ]);
    	return back();
    }
    public function is_featured_disable($id){
    	DB::table('products')->where('p_id',$id)->update([
                'is_featured' => 0,
                'p_modified_date' => Carbon::now()->toDateTimeString(),
            ]);
    	return back();
    }


}
