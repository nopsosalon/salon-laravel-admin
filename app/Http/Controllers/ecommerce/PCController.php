<?php

namespace App\Http\Controllers\ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use Carbon\Carbon;
use Session;
use Input;
use DB;
use URL;


class PCController extends Controller
{
    public function __construct()
    {        
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }
    public function product_category(){
        Session::put("activemenu","product_categories");
        $page_title = "Product Categories";
        try {
            $pCategories = DB::table('product_categories')->orderBy('pc_id','DESC')->paginate(10);
        } catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        return view('ecommerce/product_categories/product_categories',compact('pCategories','page_title'));
    }
    public function add_product_cat(){
        $page_title = "Add Product Category";
        $return_back = URL::previous();
        return view('ecommerce/product_categories/add_product_cat',compact('page_title','return_back'));
    }
    public function store_pcate(Request $request){
        try {
              $data['pc_name']        = $request->pc_name;
              $data['pc_status']      = $request->pc_status;
              $data['pc_description'] = $request->pc_description;
              $data['pc_created_at']  = Carbon::now()->toDateTimeString();
              $data['pc_modified_date'] = Carbon::now()->toDateTimeString();
            if ($request->hasFile('pc_image')) {
                $helper = new Helper();
                $filename = $request->pc_image;
                $filename = 'pc_'.time().'.'.$filename->getClientOriginalExtension();
                $destinationPath = $helper->pc_image_store();
                // if(Session::get('change_db_connection')=="3"){
                //     $helper->uploadimageFTP('/products_images/'.$filename,Input::file('pc_image'));
                // } 
                $request->pc_image->move($destinationPath, $filename); 
                $data['pc_image'] = $filename;   
             }
            DB::table('product_categories')->insert($data);
            return redirect()->to('admin/product_categories');
            
        } catch (FatalThrowableError $e) {
            return back()->withError($e->getMessage())->withInput();
        }
    }
    public function pc_enable($id){
        try {
            DB::table('product_categories')->where('pc_id',$id)->update(array('pc_status' => 1));
        } catch (Exception $e) {
            return back()->withError($e->getMessage())->withInput();
        }
        return back()->with('message','Product Category Active Successfully!');
    }
    public function pc_diable($id){
        try {
            DB::table('product_categories')->where('pc_id',$id)->update(array('pc_status' => 0));
        } catch (Exception $e) {
            return back()->withError($e->getMessage())->withInput();
        }
        return back()->with('message','Product Category In-Active Successfully!');
    }
    public function pc_delete($id){
        try {
            DB::table('product_categories')->where('pc_id',$id)->delete();
        } catch (Exception $e) {
            return back()->withError($e->getMessage())->withInput();
        }
        return back()->with('message','Record Deleted Successfully!');
    }
    public function pc_edit($id){
        $page_title = 'Update Prdocut Category';
        $return_back = URL::previous();
        try {
            $pCategories = DB::table('product_categories')->where('pc_id',$id)->first();
        } catch (ModelNotFoundException $e) {
            return back()->withError($e->getMessage())->withInput();
        }
        return view('ecommerce/product_categories/add_product_cat',compact('pCategories','page_title','return_back'));
    }
    public function pc_update(Request $request,$id){
        try {
              $data['pc_name']        = $request->pc_name;
              $data['pc_status']      = $request->pc_status;
              $data['pc_description'] = $request->pc_description;
              $data['pc_modified_date'] = Carbon::now()->toDateTimeString();
            if ($request->hasFile('pc_image')) {
                $helper = new Helper();

                $files = DB::table('product_categories')->where('pc_id' , $id)->first();
                    $file = $files->pc_image; 
                    $filename = $helper->pc_image_display()."/".$file;
                    if (file_exists($filename)) 
                    {   
                        \File::delete($filename);                         
                    }
                $filename = $request->pc_image;
                $filename = 'pc_'.time().'.'.$filename->getClientOriginalExtension();
                $destinationPath = $helper->pc_image_store();
                // if(Session::get('change_db_connection')=="3"){
                //     $helper->uploadimageFTP('/products_images/'.$filename,Input::file('pc_image'));
                // } 
                $request->pc_image->move($destinationPath, $filename); 
                $data['pc_image'] = $filename;   
             }
            DB::table('product_categories')->where('pc_id',$id)->update($data);
            return redirect()->to('admin/product_categories');
            
        } catch (FatalThrowableError $e) {
            return back()->withError($e->getMessage())->withInput();
        }
    }
    public function pc_search(Request $request){
        Session::put("activemenu","product_categories");
        $page_title = "Product Categories";
        $pc_name    = "";
        $pc_status  = "";
        try {
            $data = DB::table('product_categories');
            if($request->pc_name){
                $pc_name = $request->pc_name;
                $data = $data->where('pc_name','LIKE','%'.$pc_name.'%');
            }
            if($request->pc_status){
                $pc_status = $request->pc_status;
                if($pc_status=='all'){
                    $data = $data->where('pc_status',1)->orWhere('pc_status',0);
                }elseif($pc_status == '1' ){
                    $data = $data->where('pc_status',1);
                }elseif($pc_status== '2' ){
                    $data = $data->where('pc_status',0);
                }
            
            }
            $pCategories = $data->paginate(10);
        } catch (ModelNotFoundException $e) {
            return back()->withError($exception->getMessage())->withInput();
        }
        return view('ecommerce/product_categories/product_categories',compact('pc_name','pc_status','pCategories','page_title'));
    }

}
