<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use DB;
use Config;
use Auth;
use Hash;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

     /**
     * Determine if the user has too many failed login attempts.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function hasTooManyLoginAttempts ($request) {
        $maxLoginAttempts = 2;
        $lockoutTime = 5; // 5 minutes
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), $maxLoginAttempts, $lockoutTime
        );
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest', ['except' => 'logout']);
    }
    
    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            
                if($request->change_db_con == 1){
                    Session::forget('change_db_connection');
                    Session::put('change_db_connection','1');
                    Session::save();
                }elseif($request->change_db_con == 2){
                    Session::forget('change_db_connection');
                    Session::put('change_db_connection','2');
                    Session::save();
                }elseif($request->change_db_con == 3){
                    Session::forget('change_db_connection');
                    Session::put('change_db_connection','3');
                    Session::save();
                }elseif($request->change_db_con == 4){
                    Session::forget('change_db_connection');
                    Session::put('change_db_connection','4');
                    Session::save();
                }else{
                    Auth::logout();
                    return redirect('/login');
                }
                // dd(Session::get('change_db_connection'));
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);
        // dd('logincontroller');

        return $this->sendFailedLoginResponse($request);
    }


    public function changePassword(){
        return view('auth/passwords/changepassword');
    }
    public function postChangePassword(Request $request){
        dd(Auth::user()->password);
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        dd($request->all());
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");

    }

}
