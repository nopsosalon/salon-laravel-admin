<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use File;
use App\Model\salon_services_sa_import;
use App\app_requests_action;
use Carbon\Carbon;
use DateTime;
use App\Model\serviceCategory;
use Config;
use Auth;
use App;
use App\Helpers\Helper;
use DB;



class SEOController extends Controller
{
    public function __construct()
    {        
        ini_set('memory_limit','512M');
        $this->middleware('auth');
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }

    public function add_seodata(){
        $seo = DB::table('seo_data')->orderBy('seo_id','DESC')->paginate(20);
    	$page_title = '';
    	return view('seo_data/seo_data',compact('page_title','seo'));
    }

    
    public function add_seo(){
        $page_title = '';
        return view('seo_data/add_seo_data',compact('page_title'));
    }
    public function store_seo(Request $request){
        // dd($request->all());
        $data['seo_main_title'] = 'null';
        $data['seo_title'] = $request['seo_title'];
        $data['seo_description'] = $request['seo_description'];
        $data['seo_keywords'] = $request['seo_keywords'];
        $data['seo_canonical'] = $request['seo_canonical'];
        $data['seo_page_type'] = $request['seo_page_type'];
        $data['seo_created_at'] = Carbon::now()->toDateTimeString();
        $data['seo_updated_at'] = Carbon::now()->toDateTimeString();
        DB::table('seo_data')->insert($data);
        return redirect()->to('admin/seo_data');
    }
    public function edit_seo($id){
        $page_title = '';
        $seo = DB::table('seo_data')->where('seo_id',$id)->first();
        return view('seo_data/add_seo_data',compact('seo','page_title'));
    }
    public function update_seo(Request $request,$id){
        $data['seo_main_title'] = 'null';
        $data['seo_title'] = $request['seo_title'];
        $data['seo_description'] = $request['seo_description'];
        $data['seo_keywords'] = $request['seo_keywords'];
        $data['seo_canonical'] = $request['seo_canonical'];
        $data['seo_page_type'] = $request['seo_page_type'];
        $data['seo_updated_at'] = Carbon::now()->toDateTimeString();

        DB::table('seo_data')->where('seo_id',$id)->update($data);
        return redirect()->to('admin/seo_data'); 
    }
    public function delete_seo($id){
        // dd($id);
        DB::table('seo_data')->where('seo_id',$id)->delete();
        return back();
    }

}
