<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use App\Model\Salon;
use Carbon\Carbon;
use Config;
use Auth;
use App;
use App\Helpers\Helper;


class SalonManagementController extends Controller
{

    public function __construct()
    {

           $this->middleware('auth');
            $dbc = new Helper();
            $dbc->setDBConnection();
       
    }
    

    public function updateSalon(Request $request){
        $helper = new Helper();
        
        if(is_null($request->sal_24clock))
        {
            $sal_24clock = 0;
        }
        else
        {
            $sal_24clock = 1;
        }
        
        // convert time in 24 hour 
        // $date = $request->fridayend;
        $sal_hour = date("H:i", strtotime($request->mondaystart)).'&'.date("H:i", strtotime($request->mondayend)).','.
                    date("H:i", strtotime($request->tuesdaystart)).'&'.date("H:i", strtotime($request->tuesdayend)).','.
                    date("H:i", strtotime($request->wednesdaystart)).'&'.date("H:i", strtotime($request->wednesdayend)).','.
                    date("H:i", strtotime($request->thursdaystart)).'&'.date("H:i", strtotime($request->thursdayend)).','.
                    date("H:i", strtotime($request->fridaystart)).'&'.date("H:i", strtotime($request->fridayend)).','.
                    date("H:i", strtotime($request->saturdaystart)).'&'.date("H:i", strtotime($request->saturdayend)).','.
                    date("H:i", strtotime($request->sundaystart)).'&'.date("H:i", strtotime($request->sundayend));
        // dd($sal_hour);

        $salon = array();

        $salon['sty_id']        = $request->sty_id;
        $salon['sal_hours']     = $sal_hour;
        $salon['is_active']     = $request->is_active;
        $salon['sal_name']      = $request->input('sal_name');
        $salon['sal_email']     = $request->input('sal_email')     == null ?  '' : $request->input('sal_email');
        $salon['sal_phone']     = $request->input('sal_phone')     == null ?  '' : $request->input('sal_phone');
        $salon['sal_city']      = $request->input('sal_city')      == null ?  '' : $request->input('sal_city');
        $salon['sal_address']   = $request->input('sal_address')   == null ?  '' : $request->input('sal_address');
        $salon['sal_zip']       = $request->input('sal_zip')       == null ?  '' : $request->input('sal_zip');
        $salon['sal_specialty'] = $request->input('sal_specialty') == null ?  '' : $request->input('sal_specialty');
        $salon['sal_facebook']  = $request->input('sal_facebook')  == null ?  '' : $request->input('sal_facebook') ;
        $salon['sal_instagram'] = $request->input('sal_instagram') == null ?  '' : $request->input('sal_instagram');
        $salon['sal_24clock']   = $sal_24clock;
        // $salon['sal_hours']     = $request->input('sal_hours')     == null ?  '' : $request->input('sal_hours');
        $salon['sal_website']   = $request->input('sal_website')   == null ?  '' : $request->input('sal_website');
        $salon['sal_biography'] = $request->input('sal_biography') == null ?  '' : $request->input('sal_biography');

        $salon['sal_description']     = $request->input('sal_description')     == null ?  '' : $request->input('sal_description');
        // add some more fields
        $salon['sal_contact_person']    = $request->input('sal_contact_person')     == null ? '' : $request->input('sal_contact_person');
        $salon['sal_password']          = $request->input('sal_password')           == null ? '' : $request->input('sal_password');
        $salon['sal_lat']               = $request->input('sal_lat')                == null ? '' : $request->input('sal_lat');
        $salon['sal_lng']               = $request->input('sal_lng')                == null ? '' : $request->input('sal_lng');
        $salon['sal_future_app_days']   = $request->input('sal_future_app_days')    == null ? '' : $request->input('sal_future_app_days');
        $salon['sal_twitter']           = $request->input('sal_twitter')            == null ? '' : $request->input('sal_twitter');
        // $salon['sal_timing']            = $request->input('sal_timing')             == null ? '' : $request->input('sal_timing');
        // $salon['sal_active_promotions'] = $request->input('sal_active_promotions')  == null ? '' : $request->input('sal_active_promotions');
        $salon['sal_weekly_offs']       = $request->input('sal_weekly_offs')        == null ? '' : $request->input('sal_weekly_offs');
        $salon['sal_auto_accept_app']   = $request->sal_auto_accept_app             == null ? '' : $request->sal_auto_accept_app;
        $salon['sal_area']  = $request->sal_area  == null ? '' : $request->sal_area;
        
        $salon['sal_modify_datetime'] = Carbon::now()->toDateTimeString();
        $sal_id = $request->input('sal_id');



            if($request->hasFile('sal_pic'))
                {
                    $files = DB::table('salon')->where('sal_id' , $sal_id)->first();
                    $file = $files->sal_pic; 
                    $filename = $helper->salonImages()."/".$file;
                    if (file_exists($filename)) 
                    {   
                        \File::delete($filename);                         
                    }
                    $image              = Input::file('sal_pic');
                    $destinationPath    = $helper->salonImages();
                    $filename           = strtotime("now").$image->getClientOriginalName();
                    // if(Session::get('change_db_connection')=="3"){
                    //     $helper->uploadimageFTP('/salonimages/'.$filename,Input::file('sal_pic'));
                    // }
                    $image->move($destinationPath, $filename);            
                    DB::table("salon")->where('sal_id',$sal_id)->update(array("sal_pic"=>$filename));      
                }
            if($request->hasFile('sal_profile_pic'))
                {
                    $files = DB::table('salon')->where('sal_id' , $sal_id)->first();
                    $file = $files->sal_profile_pic; 
                    
                        $filename = $helper->salonImages()."/".$file;
                        if (file_exists($filename)) 
                        {   
                            \File::delete($filename);                         
                        }
                    $image              = Input::file('sal_profile_pic');
                        $destinationPath    = $helper->salonImages();
                    
                    $filename           = strtotime("now").$image->getClientOriginalName();
                    // if(Session::get('change_db_connection')=="3"){
                    //     $helper->uploadimageFTP('/salonimages/'.$filename,Input::file('sal_profile_pic'));
                    // }
                    $image->move($destinationPath, $filename);            
                    DB::table("salon")->where('sal_id',$sal_id)->update(array("sal_profile_pic"=>$filename));   
                }
       
        try {
            $reponse = DB::table('salon')
            ->where('sal_id',$sal_id)
            ->update($salon);
            if(Session::get('change_db_connection')=="1"){
                $result = DB::statement("update salon s JOIN postcodelatlng pc on s.sal_zip = pc.postcode set s.sal_lat = pc.latitude, s.sal_lng = pc.longitude where s.sal_id = '$sal_id'");
                // dd($result);
            }
          }
          catch (\Exception $e) {
            $response["status"]  = "0";
            $response["message"] = ($e->getMessage());
            $response["Query"]   = ($e->getSql());
            $response["Binding"]   = ($e->getBindings());
            
            
            return json_encode($response);
        }
       
        Session::flash('flash_message', 'Salon  successfully updated.');

        Session::put("editsalon","salon-info");

        // $notification = array(
        //         'message' => 'Salon info updated successfully!', 
        //         'alert-type' => 'success'
        //     );
        $notification = array(
                'success' => 'Salon info updated successfully!', 
                'alert-type' => 'success'
            );
        
        return back()->with($notification);
    }
    

}
