<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use File;
use App\Model\salon_services_sa_import;
use App\app_requests_action;
use Carbon\Carbon;
use DateTime;
use App\Model\serviceCategory;
use Config;
use Auth;
use App;
use App\Helpers\Helper;

class AdminController extends Controller 
{
    public function __construct()
    {        
        ini_set('memory_limit','512M');
        $this->middleware('auth');
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }
    public function summary() 
    {
        $total_salons = DB::table('salon')->count();
        return view('salon-magmt/summary', ['total_salons' => $total_salons]);
    }
    public function testFun() 
    {

        dd('Hello its me');
    }
    public function manage_salons() 
    {
        $page_title = " Salon Mangement ";
//        $users = DB::table('users')
//                     ->select(DB::raw('count(*) as user_count, status'))
//                     ->where('status', '<>', 1)
//                     ->groupBy('status')
//                     ->get();
        // $categories = DB::table('salon')->paginate(10);
        //,return view('service-mgmt/managecategories', ['categories' => $categories]);
        // dd('',$categories);
        //SELECT count(*) from appointments a where a.sal_id = s.sal_id
        // salon
        $salons = DB::table('salon as s')
                ->where('s.sal_name','<>','')
                ->leftJoin('appointments as ap', 'ap.sal_id', '=', 's.sal_id')
                ->select(DB::raw('count(ap.sal_id) as count, s.sal_id, s.sal_name, s.sal_email, s.sal_address, s.sal_phone, s.sal_status, s.sal_temp_enabled, s.sal_is_reviewed, s.sal_review_datetime'))
                //  ->select( 'salon.sal_id', DB::raw("count(appointments.sal_id) as count"))
                ->groupBy('s.sal_id', 's.sal_name', 's.sal_email', 's.sal_address', 's.sal_phone', 's.sal_status', 's.sal_temp_enabled', 's.sal_is_reviewed', 's.sal_review_datetime')
                ->orderBy('s.sal_name','ASC')
                ->paginate(10);
               
               // ->orderBy('count','DESC')

        // dd($salons);
        //   dd('', $notices);
        // $salons = DB::select('select sal_name, sal_temp_enabled, sal_is_reviewed, sal_email, sal_city, sal_created_datetime, (SELECT count(*) from appointments a where a.sal_id = s.sal_id) as appointments, sal_status, sal_id from salon s LIMIT 40 ');
//        $salons = DB::table('salon')->get();
       $cities = DB::table('salon')->select('sal_city')->distinct('sal_city')->where('sal_city','<>','')->where('sal_city','<>','empty')->get();
       
            Session::put("activemenu","managesalons");
        return view('salon-magmt/managesalons',compact('page_title','cities'), ['salons' => $salons, 'message' => 3, 'messageInfo' => 'Here']);
    }

    public function sarch_salons(Request $request) 
    {
        $page_title         = "Search Salons";
        $sal_search         = $request->sal_search;
         // dd($request->all());
        $is_reviewed        = $request->sal_is_reviewed;
        $is_active          = $request->is_active;
        $sal_city           =  $request->sal_city;
        $sal_temp_enabled   = $request->sal_temp_enabled;
// dd($sal_temp_enabled);
        $salons;
        $data = DB::table('salon as s')
                ->leftJoin('appointments as ap', 'ap.sal_id', '=', 's.sal_id')->
                select(DB::raw('count(ap.sal_id) as count, s.sal_id, s.sal_name, s.sal_email, s.sal_address, s.sal_phone, s.sal_status, s.sal_temp_enabled, s.sal_is_reviewed, s.sal_review_datetime'));

            if(!empty($sal_city))
            {
                if($sal_city!=='all')
                {
                    $data = $data->where('sal_city','like','%'.$sal_city.'%');
                }
                else
                {

                }
            }

            if(!empty($sal_temp_enabled))
            {
                if($sal_temp_enabled =='1')
                {
                    $data = $data->where('sal_temp_enabled',1);
                }
                else if($sal_temp_enabled=='2')
                {
                    $data = $data->where('sal_temp_enabled',0);
                    
                }
                else if($sal_temp_enabled=='all')
                {

                }
            }

            if(!empty($is_reviewed))
            {
                if($is_reviewed == "1")
                {
                    $data = $data->where('s.sal_is_reviewed',$is_reviewed);
                    
                }
                elseif($is_reviewed == "2")
                {
                    $data = $data->where('s.sal_is_reviewed',0);
                    
                }
                elseif($is_reviewed == "all_isreviewed")
                {
                     
                }
            }
            if(!empty($is_active))
            {
                if($is_active == "1")
                {
                    $data = $data->where('s.sal_status',$is_active);
                }
                elseif($is_active == "2")
                {
                    $data = $data->where('s.sal_status',0);
                }
                elseif($is_active == "all_status")
                {
                    // return true;
                    // $data = $data->where('s.sal_status',1)->orWhere('s.sal_status',0);   
                }
            }
            if(!empty($sal_search))
            {
              $data =  $data->where('s.sal_name','like','%'.$sal_search.'%');
            }
            $data = $data->groupBy('s.sal_id', 's.sal_name', 's.sal_email', 's.sal_address', 's.sal_phone', 's.sal_status', 's.sal_temp_enabled', 's.sal_is_reviewed', 's.sal_review_datetime');
            $salons = $data->get();
            // dd($salons);
            $salons = $data->paginate(10);
    $cities = DB::table('salon')->select('sal_city')->distinct('sal_city')->where('sal_city','<>','')->where('sal_city','<>','empty')->get();
 
        return view('salon-magmt/managesalons', ['salons' => $salons],compact('page_title','sal_search','is_reviewed','is_active','sal_city','sal_temp_enabled','cities'));
    }
    public function salon_types() 
    {
        $page_title = "Salon Types";
        $salons = DB::select('select sty_name, sty_status, sty_id,  (SELECT count(*) from salon_types_services a where a.sty_id = s.sty_id) as tota_services_type from salon_types s LIMIT 20');
//        $salons = DB::table('salon')->get();
//        dd($salons);
        Session::put("activemenu","salon_types");

        return view('salon-magmt/salon_types',compact('page_title') ,['salons' => $salons]);
    }

    public function updatesalontypeservices(Request $request) {
        $page_title = 'Salon Type Details';
        // If User has no selected any service then just call back view before 
        $sty_id = $request->input('sty_id');
        if (empty($request['sal_type_status'])) {
            $result = DB::table('salon_types_services')
                    ->Where('sty_id', $sty_id)
                    ->delete();
            $salons = DB::select('select sty_name, sty_status, sty_id,  (SELECT count(*) from salon_types_services a where a.sty_id = s.sty_id) as tota_services_type from salon_types s LIMIT 20');
            return view('salon-magmt/salon_types',compact('page_title'), ['salons' => $salons]);
        }

        //to delete service in case user uncheck some service.
        // we will delete all expect exist in selected array against selected salon type.
        $result = DB::table('salon_types_services')
                ->whereNotIn('ser_id', $request['sal_type_status'])
                ->Where('sty_id', $sty_id)
                ->delete();
        if (!empty($request['sal_type_status'])) {
            foreach ($request['sal_type_status'] as $ser_id) {
                $result = DB::statement('INSERT INTO salon_types_services (sty_id, ser_id) VALUES (' . $sty_id . ', ' . $ser_id . ') ON DUPLICATE KEY UPDATE sty_id = 
                      sty_id, ser_id=ser_id');
            }
        }

        $salons = DB::select('select sty_name, sty_status, sty_id,  (SELECT count(*) from salon_types_services a where a.sty_id = s.sty_id) as tota_services_type from salon_types s LIMIT 20');
        return view('salon-magmt/salon_types', ['salons' => $salons]);

        // $service = $request->input('services[]');
    }

    public function updateSalonServices(Request $request) 
    {
      
        $returnback = $request->returnback;
        $ssc_id = $request->ssc_id;
        // If User has no selected any service then just call back view before 
        $sser_name = $request->input('sser_name');
        $SubmitFashion = $request->input('SubmitFashion');
        $salId = $request->input('salId');

        $sser_time = $request->input('sser_time');
        $sser_order = $request->input('sser_order');
        $sser_rate = $request->input('sser_rate');
        $sser_id = $request->input('sser_id');

        if(!is_null($request->sal_type_status)) 
        {
            $sser_enabled = '1';
            // dd($status);
        } 
        else 
        {
            $sser_enabled = '0';
                // dd($status);
        }
        if(!is_null($request->sal_type_feature)) 
        {
            $sser_featured = '1';
            //dd($sser_featured);
        } 
        else 
        {
            $sser_featured = '0';
            //  dd($sser_featured);
        }
        // dd("sser_feature",$sser_featured,"sser_enabled",$sser_enabled);
        $sal_id = DB::table('salon_services')->where('sser_id', $sser_id)->first()->sal_id;
        $result = DB::table('salon_services')
                ->where('sser_id', $sser_id)
                ->update([
                    'sser_name'     => $sser_name, 
                    'sser_time'     => $sser_time,
                    'sser_order'    => $sser_order, 
                    'sser_rate'     => $sser_rate,
                    'sser_enabled'  => $sser_enabled, 
                    'sser_featured' => $sser_featured,
                    'ssc_id'        => $ssc_id,
        ]);
        // dd($result);

            DB::table("salon")->where("sal_id",$sal_id)->update(array("sal_modify_datetime"=>Carbon::now()->toDateTimeString()));

            $page_title = "Salon Service Type Update Info";
    
        $salon_services_salon = DB::table('salon_services')
                ->where('sser_id',$sser_id)
                ->first();
            $returnback = $returnback;
            $salon_ser_vices = DB::table("salon_ser_categories")->where('sal_id',$sal_id)->get();
            // $notification = array(
            //     'message' => 'Service category updated successfully!', 
            //     'alert-type' => 'success'
            // );
            \Session::flash('salnserviceupdate','Service category updated successfully!');
            \Session::flash('alert-type', 'success');
        return view('salon-magmt/editsalonservice',compact('salon_ser_vices','page_title','returnback'), [ 'salon_services_salon' => $salon_services_salon, 'salId' => $salId]);

// return back();
        // return $this->updatesalonser($sser_id, '-1',$returnback);
        
        // dd($sser_name,$sser_rate, $sser_time, $sser_rate, $sser_enabled,$sser_featured, $sser_id );
    }
    public function updatesalonser($id, $salId,$returnback) {
        $page_title = "Salon Service Type Update Info";
    
        $salon_services_salon = DB::table('salon_services')
                ->where('sser_id',$id)
                ->first();
            $returnback = $returnback;

            // $notification = array(
            //     'message' => 'Service category updated successfully!', 
            //     'alert-type' => 'success'
            // );
            \Session::flash('salnserviceupdate','Service category updated successfully!');
            \Session::flash('alert-type', 'success');
        return view('salon-magmt/editsalonservice',compact('page_title','returnback'), [ 'salon_services_salon' => $salon_services_salon, 'salId' => $salId]);
    }

    public function editsalons($id, $type_id)
    {

        //Check Enable status in salon_services
        //if its id2=1 it means it first time call from out side 
       
        $salon = DB::table('salon')->where('sal_id', $id)->get()[0];
        $salon_services_salon = DB::table('salon_services')
                ->where('sal_id', '=', $id)
                ->get();

        // dd($salon_services_salon);
        //   dd($salon);
        if ($type_id == '-1') {
            $type_id = $salon->sty_id;

            if ($type_id == 0) {
                //   dd($id,$type_id);
                $type_id = 1;
                //set session here also
            }
        }
        else 
        {

            $type_id = $type_id;
            //otherwise ot's
        }

        session(['type_id' => $type_id]);
        //$type_id = session('type_id') ? session('type_id') : 1;
        $salon_services = DB::table('salon_services')->get();

        $salon_services_salon = DB::table('salon_services')
                ->where('sal_id', '=', $id)
                ->get();

              $sal_ser_sub_cat = DB::table("sal_ser_sub_categories")->get();
    // dd($type_id);

        $salon_types = DB::table('salon_types')->get();

        $servicecategory = DB::select("select service_sub_categories.*, sal_id, sssc_id  from `service_sub_categories` left join `sal_ser_sub_categories` on `sal_ser_sub_categories`.`ssc_id` = `service_sub_categories`.`ssc_id` and sal_ser_sub_categories.sal_id = $id order by `sal_ser_sub_categories`.`sal_id` desc");
        $notification = array(
                'success' => 'Service category updated successfully!', 
                'alert-type' => 'success'
            );
        return back()->with($notification);

        // return back();
    }

    public function updateServiceSubCat(Request $request) {

        // If User has no selected any service then just call back view before 
        $ssc_id = $request->input('ssc_id'); //service_sub_categories 

        $sc_id = $request->input('sc_id');


        if (empty($request['sal_type_status'])) {

            $result = DB::table('sscategories_services')
                    ->Where('ssc_id', $ssc_id)
                    ->delete();

            $category = DB::table('service_categories')->where('sc_id', $sc_id)->get()[0];
            $sub_categories = DB::table('service_sub_categories')->where('sc_id', $sc_id)->get();
            return view('service-mgmt/manage-subcategores', ['category' => $category, 'sub_categories' => $sub_categories]);

            //$salons = DB::select('select sty_name, sty_status, sty_id,  (SELECT count(*) from salon_types_services a where a.sty_id = s.sty_id) as tota_services_type from salon_types s LIMIT 20');
            // return view('salon-magmt/salon_types', ['salons' => $salons]);
        }


        // dd( $ssc_id);
        //to delete service in case user uncheck some service.
        // we will delete all expect exist in selected array against selected salon type.
        $result = DB::table('sscategories_services')
                ->whereNotIn('ser_id', $request['sal_type_status'])
                ->Where('ssc_id', $ssc_id)
                ->delete();

        if (!empty($request['sal_type_status'])) {
            foreach ($request['sal_type_status'] as $ser_id) {
                $result = DB::statement('INSERT INTO sscategories_services (ssc_id, ser_id) VALUES (' . $ssc_id . ', ' . $ser_id . ') ON DUPLICATE KEY UPDATE ssc_id = 
                      ssc_id, ser_id=ser_id');
            }
        }

        // $salons = DB::select('select sty_name, sty_status, sty_id,  (SELECT count(*) from salon_types_services a where a.sty_id = s.sty_id) as tota_services_type from salon_types s LIMIT 20');
        // return view('salon-magmt/salon_types', ['salons' => $salons]);


        $category = DB::table('service_categories')->where('sc_id', $sc_id)->get()[0];

        $sub_categories = DB::table('service_sub_categories')->where('sc_id', $sc_id)->get();

        return view('service-mgmt/manage-subcategores', ['category' => $category, 'sub_categories' => $sub_categories, 'sc_id' => $sc_id]);


        // $service = $request->input('services[]');
    }

    public function updatesalontype(Request $request) {
        $page_title = 'Update Salon Type';
        $salStatus = '0';
        //dd($request->input('sal_type_status'));
        if (!empty($request['sal_type_status'])) {
            $salStatus = '1';
        }
        // dd($salStatus);
        // dd($salStatus);
        $sty_id = $request->input('sty_id');
        $sty_name = $request->input('sty_name');

        DB::table('salon_types')
                ->where('sty_id', $sty_id)
                ->update(['sty_name' => $sty_name, 'sty_status' => $salStatus]);

        $salons = DB::select('select sty_name, sty_status, sty_id,  (SELECT count(*) from salon_types_services a where a.sty_id = s.sty_id) as tota_services_type from salon_types s LIMIT 20');
//        $salons = DB::table('salon')->get();
//        dd($salons);
        return view('salon-magmt/salon_types',compact('page_title'), ['salons' => $salons]);
    }

    public function edit_salon_services($id, $salId) {
        $page_title = "Salon Service Type Update Info";
            // dd($salId);
        $salon_services_salon = DB::table('salon_services')
                ->where('sser_id', '=', $id)
                ->first();
    $salon_ser_vices = DB::table("salon_ser_categories")->where('sal_id',$salId)->get();

    $returnback = \URL::previous();
    
        // dd($returnback);

        return view('salon-magmt/editsalonservice',compact('salon_ser_vices','page_title','returnback'), [ 'salon_services_salon' => $salon_services_salon, 'salId' => $salId]);
    }

    public function edit_salon($id, $type_id,$urls,Request $request) 
    {
        $returnback = url()->previous();
        // dd($returnback);
        $helper = new Helper();
        $cate_images = $helper->display_salonImages();
        $page_title = "Salon Service Type Update Info";
        $salontypes = DB::table('salon_types')->where('sty_status',1)->orderBy('sty_name','ASC')->get();
        // dd($salontypes);
        //$sal and type_id=-1
        $returnback = url('/').'/'.$urls;
        //Check Enable status in salon_services
        //if its id2=1 it means it first time call from out side 
       
        $salon = DB::table('salon')->where('sal_id', $id)->get()[0];
        $salon_services_salon = DB::table('salon_services')
                ->where('sal_id', '=', $id)
                ->get();

        // dd($salon_services_salon);
        //   dd($salon);
        if ($type_id == '-1') {
            $type_id = $salon->sty_id;

            if ($type_id == 0) {
                //   dd($id,$type_id);
                $type_id = 1;
                //set session here also
            }
        }
        else 
        {

            $type_id = $type_id;
            //otherwise ot's
        }

        session(['type_id' => $type_id]);
        //$type_id = session('type_id') ? session('type_id') : 1;
        $salon_services = DB::table('salon_services')->get();

        $salon_services_salon = DB::table('salon_services')
                ->leftJoin('salon_ser_categories','salon_ser_categories.ssc_id','=','salon_services.ssc_id')
                ->where('salon_services.sal_id', '=', $id)
                ->orderBy('salon_ser_categories.ssc_name','ASC')
                ->get();
// dd($salon_services_salon);
              $sal_ser_sub_cat = DB::table("sal_ser_sub_categories")->get();
    // dd($type_id);

        $salon_types = DB::table('salon_types')->get();

        $servicecategory = DB::select("select sc_gender,sc_name, service_sub_categories.*, sal_id, sssc_id from service_categories sc inner join `service_sub_categories` on (sc.sc_id = service_sub_categories.sc_id) left join `sal_ser_sub_categories` on `sal_ser_sub_categories`.`ssc_id` = `service_sub_categories`.`ssc_id` and sal_ser_sub_categories.sal_id = $id where ssc_status = 1 and sc_status = 1 order by sc_id asc,sc_name asc, ssc_name asc");
// dd($servicecategory);
        $service_category = DB::table("service_categories")->get();

         $serv = DB::table('service_sub_categories')
                     ->where('ssc_status',1)
                     ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                     ->where('service_categories.sc_status',1)
                     ->orderBy("ssc_name","ASC")->get(); 

        // $servicecategory = DB::table("service_sub_categories")
        // ->leftJoin("sal_ser_sub_categories","sal_ser_sub_categories.ssc_id","=","service_sub_categories.ssc_id")
        // ->orWhere("sal_ser_sub_categories.sal_id",8)
        // //->leftJoin("sal_ser_sub_categories","sal_ser_sub_categories.sal_id","=","8")
        // ->orderBy("sal_ser_sub_categories.sal_id","DESC")->get();
      
        // dd($servicecategory); 
      
        // $type_services = DB::select("SELECT * FROM services 
        // INNER JOIN salon_types_services SalonTypeServices ON SalonTypeServices.ser_id = services.ser_id 
        // INNER JOIN salon_types SalonTypes ON SalonTypes.sty_id = SalonTypeServices.sty_id
        // WHERE SalonTypes.sty_id = $type_id"); 
        // //sty_id

        // for ($index = 0; $index < count($type_services); $index++) {
        //     $objAllService = $type_services[$index];

        //     if ($objAllService->sty_id == $type_id) {

        //         $objAllService->typeSelected = $objAllService->sty_id;
        //         $type_services[$index] = $objAllService;
        //         break;
        //     } else {
        //         $objAllService->typeSelected = '';
        //         $type_services[$index] = $objAllService;
        //     }
        // }

        //  dd($type_services);

        // for ($index = 0; $index < count($type_services); $index++) {
        //     for ($index2 = 0; $index2 < count($salon_services); $index2++) {
        //         $objAllService = $type_services[$index];
        //         $objSalonType = $salon_services[$index2];

        //         if ($objAllService->ser_id == $objSalonType->ser_id) {
        //             //dd($objSalonType->sser_enabled);
        //             if ($objSalonType->sser_enabled == '1' && $objSalonType->sal_id == $salon->sal_id) {
        //                 //    echo 'sal_id'.$salon->sal_id.' ser_id'.$objAllService->ser_id ;;
        //                 // echo ','.$objSalonType->sser_enabled,' , ' .$objSalonType->sal_id,','.$objAllService->ser_id ;
        //                 //dd($objSalonType->sser_enabled);
        //                 $objAllService->checked = 'checked';
        //                 $type_services[$index] = $objAllService;
        //                 break;
        //             } else {
        //                 $objAllService->checked = 'UnChecked';
        //                 $type_services[$index] = $objAllService;
        //             }
        //         } else {
        //             $objAllService->checked = 'UnChecked';
        //             $type_services[$index] = $objAllService;
        //         }
        //     }
        // }
        
            $tab1 = "tab1";
           Session::put('editsalon','salon-info'); 
        return view('salon-magmt/editsalon',compact('returnback','cate_images','salontypes','tab1','servicecategory','returnback','page_title','serv'), ['salon' => $salon, 'salon_types' => $salon_types, 'salon_services_salon' => $salon_services_salon, 'tab' => '2']);
    }

    public function setUpSalon($id) 
    {
        //  $tempHours="13:00&14:00,13:00&14:00,13:00&14:00,13:00&14:00,13:00&14:00,13:00&14:00,13:00&14:00";
        //  $fixSlots=$this->weekly_time_ranges_to_slots($tempHours);
        // dd($id);
        $fixedSlots = "157&168,157&168,157&168,157&168,157&168,157&168,157&168";  //for tech_break_slots and tech_break_time 
        // dd($fixedSlots);
        // dd('SetUpSalo', $id);
        //get salon_services
        //select * from tech_services where tech_id in (select tech_id from technicians where sal_id = 1)
        //for that point 2) check sal_hours in salon table they must be valid weekly hours,
        $salon = DB::table('salon')
                        ->where('sal_id', '=', $id)
                        ->get()[0];
        // dd($salon);
        $sal_hours = $salon->sal_hours;
        $isValidHours = $this->is_valid_weekly_time($sal_hours);
        // dd($isValidHours);
        //for technician table set these values 
        $tech_slots = ""; //searilized array of slots
        $tech_break_slots = $fixedSlots;
// dd($tech_break_slots);
        $tech_work_time = $salon->sal_hours;
        $tech_break_time = $fixedSlots;
        $tech_slots_new = $fixedSlots;
        $tech_weekly_offs = $salon->sal_weekly_offs; //same as salon weekly off;
        $tech_specialty = $salon->sal_specialty;
        $tech_work_slots = "";
        //same as salon specialty;

        if ($isValidHours) {
            // dd('Valid salHours');
            $slots = $this->weekly_time_ranges_to_slots($sal_hours);
            $tech_work_slots = $slots;
            $rangeArray = $this->weekly_ranges_to_array($slots, $fixedSlots);
            $tech_slots = serialize($rangeArray);
            // dd($slots,$fixedSlots, $tech_slots);
            // dd($serializeValeu,'Here');
        } 
        else 
        {
            // dd('s');
            // $salons = DB::select('select sal_name, sal_temp_enabled, sal_is_reviewed, sal_email, sal_city, sal_created_datetime, (SELECT count(*) from appointments a where a.sal_id = s.sal_id) as appointments, sal_status, sal_id from salon s LIMIT 40 ');
            $notification = array(
                'success' => 'Salon opening hours not valid!', 
                'alert-type' => 'success'
            );
            return back()->with($notification);
            return view('salon-magmt/managesalons', ['salons' => $salons, 'message' => 0, 'messageInfo' => 'Invalid salon hours']);
            //go back to salon and show message there invalid salon hours 
        }
        // dd('$tech_slots', $tech_slots, '$tech_break_slots', $tech_break_slots, '$tech_work_time', $tech_work_time, '$tech_break_time', $tech_break_time, '$tech_slots_new', $tech_slots_new, '$tech_weekly_offs', $tech_weekly_offs, '$tech_specialty', $tech_specialty);
        //insert all above variable to technicans table and then continue 
        //check all the return statement in else block 
        $salon_services = DB::table('salon_services')
                ->where('sal_id', '=', $id)
                ->where('sser_rate', '>', 0)
                // ->where('sser_time', '>', 0)
                // ->where('sser_enabled', '=', 1)
                ->get();
        // dd($salon_services);
        $total_salon_services = count($salon_services);
        //sser_enabled
        //
        // dd($salon_services);
        //check number of service 
        // dd(count($salon_services));
        if (count($salon_services) < 1) 
        {
            // dd('There is no service exist with that salon');
            //return from here 
            $salons = DB::select('select sal_name, sal_temp_enabled, sal_is_reviewed, sal_email, sal_city, sal_created_datetime, (SELECT count(*) from appointments a where a.sal_id = s.sal_id) as appointments, sal_status, sal_id from salon s LIMIT 4 ');
            // dd($salons);
            return back();
            // return view('salon-magmt/managesalons', ['salons' => $salons, 'message' => 0, 'messageInfo' => 'There are no services exist for this salon']);
        }
        //check if sal_hours n salon table they must be valid weekly hours,
        //insert into technicians if there is no tech
        // dd(' tech', $technicians);
        $result = DB::select('select * from tech_services where tech_id in (select tech_id from technicians WHERE sal_id= ' . $id . ' )');
        $totalTechServies = count($result);
        $techId = '';
        $technicians = DB::table('technicians')
                ->where('sal_id', '=', $id)
                ->where('tech_is_active', '=', 1)
                ->get();


        $InsertedTechIdId = 0;
        if (count($technicians) < 1) 
        {

            // dd('There is no tech exist');
            //here insert into tech table 
            $InsertedTechIdId = DB::table('technicians')->insertGetId(
                    array('tech_name' => 'Manager', 'sal_id' => $id, 'tech_pic' => '', 'tech_pic' => 'default.png'
                        , 'tech_slots' => $tech_slots, 'tech_phone' => '',
                        'tech_bio' => '',
                        'tech_specialty' => $tech_specialty, 'tech_weekly_offs' => $tech_weekly_offs,
                        'tech_work_slots' => $tech_work_slots, 'tech_break_slots' => $tech_break_slots, 'tech_work_time' => $tech_work_time
                        , 'tech_break_time' => $tech_break_time
                        , 'tech_slots_new' => $tech_slots_new, 'tech_is_active' => 1
            ));
            // dd($InsertedTechIdId, 'Last id');   
            $techId = $InsertedTechIdId;
        }
        else 
        {
            $techId = $technicians[0]->tech_id;
            $InsertedTechIdId = $techId;
            // dd(' tech exist', $techId);
        }
        // dd($InsertedTechIdId);

        if ($totalTechServies < 1) 
        {
            $result1 = DB::statement("INSERT INTO tech_services (tech_id,tser_name, tser_order, sser_id) 
             SELECT " . $InsertedTechIdId . ", sser_name, sser_order, sser_id FROM salon_services where sal_id=  " . $id . ""
                            . " and sser_rate > 0 and sser_time > 0 and sser_enabled = 1 ");
        }
        //salon info here 
        $sal_temp_enabled = DB::table("salon")->where("sal_id",$id)->first()->sal_temp_enabled;
        // dd($sal_temp_enabled);
        if($sal_temp_enabled===0)
        {
            $result = DB::table('salon')
                    ->Where('sal_id', $id)
                    ->update([
                        'sal_status'                => 1, 'sal_temp_enabled' => 1,
                        'sal_appointment_interval'  => 30,
                        'sal_modify_datetime'       => Carbon::now()->toDateTimeString()
            ]);
            $notification = array(
                'success' => 'Salon temp-enabled successfully!', 
                'alert-type' => 'success'
            );
        }
        // $sal_appointment_interval = 30;
        // $result = DB::table('salon')
        //         ->Where('sal_id', $id)
        //         ->update(['sal_status' => 1, 'sal_temp_enabled' => 1]);
        $salons = DB::select('select sal_name, sal_temp_enabled,  sal_is_reviewed, sal_email, sal_city, sal_created_datetime, (SELECT count(*) from appointments a where a.sal_id = s.sal_id) as appointments, sal_status, sal_id from salon s LIMIT 40 ');
        
        $notification = array(
                'success' => 'Salon temp-enabled successfully!', 
                'alert-type' => 'success'
            );

        return back()->with($notification);
        return view('salon-magmt/managesalons',['salons' => $salons, 'message' => 1, 'messageInfo' => 'Salon updated successfully!'])->with($notification);
    }

    public function setSalonInfo($id) {
        //status 0 or 1 of sal_is_reviewd
        $t = time();
        
        $currentDateTime = date("Y-m-d h-m-s", $t);
       
        $salon = DB::table('salon')->where('sal_id', $id)->get()[0];
        $sal_is_reviewed = $salon->sal_is_reviewed;
        $sal_is_reviewed_status = 0;
        if ($sal_is_reviewed == '0') 
        {
            $sal_is_reviewed_status = 1;
            $notification = array(
                'success' => 'Salon is reviewed!', 
                'alert-type' => 'success'
            );
        } 
        else 
        {
            $sal_is_reviewed_status = 0;
            $notification = array(
                'success' => 'Salon is not reviewed!', 
                'alert-type' => 'success'
            );
        }

        $result = DB::table('salon')
                ->Where('sal_id', $id)
                ->update([
                        'sal_is_reviewed' => $sal_is_reviewed_status, 
                        'sal_review_datetime' => $currentDateTime,
                        'sal_modify_datetime' => Carbon::now()->toDateTimeString()
                    ]);
        
        // dd($notification);
            return back()->with($notification);
        // $salons = DB::select('select sal_name,sal_temp_enabled, sal_is_reviewed, sal_email, sal_city, sal_created_datetime, (SELECT count(*) from appointments a where a.sal_id = s.sal_id) as appointments, sal_status, sal_id from salon s LIMIT 40 ');
//        $salons = DB::table('salon')->get();
      // dd($salons);
        return view('salon-magmt/managesalons', ['salons' => $salons]);

        // return view('salon-magmt/editsalonservice', [ 'salon_services_salon' => $salon_services_salon, 'salId' => $salId]);
    }

    public function type_services(Request $request) 
    {

        $page_title = "Salon Service Type Update Info";
            $returnback = $request->returnback;

        $sal_id = $request->input('sal_id');
        $salon = DB::table('salon')->where('sal_id', $sal_id)->get()[0];
        $salon_services_salon = DB::table('salon_services')
                ->where('sal_id', '=', $sal_id)
                ->get();

        //  dd($sal_id);
        $salon = DB::table('salon')->where('sal_id', $sal_id)->get()[0];

        $salon_type = $request->input('salon_type');
        if ($salon_type == '-1') {

            $salon_type = $salon->sty_id;
            //  dd('Need to be set here ', $salon_type, $salon->sal_id);  
            if ($salon_type == 0) {
                $salon_type = 1;
            }
        }

        session(['type_id' => $salon_type]);
        $type_id = $salon_type;
        $salon_services = DB::table('salon_services')->get();
        $salon_types = DB::table('salon_types')->get();
        $type_services = DB::select("SELECT * FROM services 
        INNER JOIN salon_types_services SalonTypeServices ON SalonTypeServices.ser_id = services.ser_id 
        INNER JOIN salon_types SalonTypes ON SalonTypes.sty_id = SalonTypeServices.sty_id
        WHERE SalonTypes.sty_id = $type_id"); //sty_id

        for ($index = 0; $index < count($type_services); $index++) {
            $objAllService = $type_services[$index];

            if ($objAllService->sty_id == $type_id) {

                $objAllService->typeSelected = $objAllService->sty_id;
                $type_services[$index] = $objAllService;
                break;
            } else {
                $objAllService->typeSelected = '';
                $type_services[$index] = $objAllService;
            }
        }
        for ($index = 0; $index < count($type_services); $index++) {
            for ($index2 = 0; $index2 < count($salon_services); $index2++) {
                $objAllService = $type_services[$index];
                $objSalonType = $salon_services[$index2];
                if ($objAllService->ser_id == $objSalonType->ser_id) {
                    //dd($objSalonType->sser_enabled);
                    if ($objSalonType->sser_enabled == '1' && $objSalonType->sal_id == $salon->sal_id) {
                        //    echo 'sal_id'.$salon->sal_id.' ser_id'.$objAllService->ser_id ;;
                        // echo ','.$objSalonType->sser_enabled,' , ' .$objSalonType->sal_id,','.$objAllService->ser_id ;
                        //dd($objSalonType->sser_enabled);
                        $objAllService->checked = 'checked';
                        $type_services[$index] = $objAllService;
                        break;
                    } else {
                        $objAllService->checked = 'UnChecked';
                        $type_services[$index] = $objAllService;
                    }
                } else {
                    $objAllService->checked = 'UnChecked';
                    $type_services[$index] = $objAllService;
                }
            }
        }
        //dd('Here');
        // $type_services = $this->array_sort($type_services, 'checked', SORT_DESC);
        //return view('salon-magmt/editsalon', ['salon' => $salon, 'salon_types' => $salon_types, 'type_services' => $type_services]);
        return view('salon-magmt/editsalon',compact('returnback','page_title'), ['salon' => $salon, 'salon_types' => $salon_types, 'type_services' => $type_services, 'salon_services_salon' => $salon_services_salon]);

        //  return redirect()->back();
    }

    public function type_services_update(Request $request) {
        
            $returnback = $request->returnback;
        $sal_id = $request->input('sal_id');
        if (empty($request['sal_type_status'])) {

            $result1 = DB::statement('DELETE from   salon_tags where sal_id = ' . $sal_id);

            $result = DB::statement('UPDATE salon_services
             SET sser_enabled = 0
             WHERE sal_id= ' . $sal_id);
            $sty_id = session('type_id') ? session('type_id') : 1;
            $result = DB::table('salon')
                    ->Where('sal_id', $sal_id)
                    ->update(['sty_id' => $sty_id]);
            $this->reload_services_page($sal_id, $sty_id,$returnback);
            // return redirect()->back();
        } 
        else 
        {

            $result = DB::table('salon_services')
                    ->whereNotIn('ser_id', $request['sal_type_status'])
                    ->Where('sal_id', $sal_id)
                    ->update(['sser_enabled' => 0]);

            //delete from salon tags 
            $result1 = DB::statement("DELETE from salon_tags where sal_id = " . $sal_id . " and tag_id not in(SELECT  tag_id FROM service_tags "
                            . "where ser_id in (" . implode(",", $request['sal_type_status']) . "))");
            // dd($result1);
//             $result = DB::table('salon_tags')
//                ->whereNotIn('ser_id', $request['sal_type_status'])
//                ->Where('ssc_id', $ssc_id)
//                ->delete();
        }
        //dd('Here2');
        //insert into salon tags 
        if (!empty($request['sal_type_status'])) {
            $result1 = DB::statement("INSERT INTO salon_tags (sal_id, tag_id) 
             SELECT " . $sal_id . ", tag_id FROM service_tags where ser_id  in (" . implode(",", $request['sal_type_status']) . ""
                            . ") ON DUPLICATE KEY UPDATE sal_id = 
                      sal_id, salon_tags.tag_id=salon_tags.tag_id ");
        }
        // dd($result1);

        $ssc_id = 15;
        // $sser_name = 'sada';
        $sser_enabled = 1;
        if (!empty($request['sal_type_status'])) {
            for ($index = 0; $index < count($request['sal_type_status']); $index++) {
                $ser_id = $request['sal_type_status'][$index];
                $sser_name = $request['sal_type_name'][$index];

                $result = DB::statement("INSERT INTO salon_services (sser_name, ser_id, sal_id,  ssc_id, sser_enabled) VALUES ('" . $sser_name . "' , '" . $ser_id . "', '" . $sal_id . "', '" . $ssc_id . "', '" . $sser_enabled . "') ON DUPLICATE KEY UPDATE ser_id = 
                      ser_id, sal_id=sal_id, sser_enabled=1, sser_name=sser_name ");
            }

            foreach ($request['sal_type_status'] as $ser_id) {
//                $result = DB::statement("INSERT INTO salon_services (sser_name, ser_id, sal_id,  ssc_id, sser_enabled) VALUES ('" .$sser_name. "' , '".$ser_id . "', '" . $sal_id . "', '" . $ssc_id . "', '" . $sser_enabled . "') ON DUPLICATE KEY UPDATE ser_id = 
//                    ser_id, sal_id=sal_id, sser_enabled=1");
            }
        }
        //  dd('Im here');
        $sty_id = session('type_id') ? session('type_id') : 1;
        $result = DB::table('salon')
                ->Where('sal_id', $sal_id)
                ->update(['sty_id' => $sty_id]);

        return $this->reload_services_page($sal_id, $sty_id,$returnback);
        // return redirect()->back();
    }

    private function reload_services_page($sal_id, $type_id,$returnback) {
            
        $returnback = $returnback;

        $id = $sal_id;
        $salon_services_salon = DB::table('salon_services')
                ->where('sal_id', '=', $id)
                ->get();
        //  dd('sfsdsa');
        //Check Enable status in salon_services
        //if its id2=1 it means it first time call from out side 
        $salon = DB::table('salon')->where('sal_id', $id)->get()[0];
        $salon_services = DB::table('salon_services')->get();
        $salon_types = DB::table('salon_types')->get();
        $type_services = DB::select("SELECT * FROM services 
        INNER JOIN salon_types_services SalonTypeServices ON SalonTypeServices.ser_id = services.ser_id 
        INNER JOIN salon_types SalonTypes ON SalonTypes.sty_id = SalonTypeServices.sty_id
        WHERE SalonTypes.sty_id = $type_id"); //sty_id
        for ($index = 0; $index < count($type_services); $index++) {
            $objAllService = $type_services[$index];

            if ($objAllService->sty_id == $type_id) {

                $objAllService->typeSelected = $objAllService->sty_id;
                $type_services[$index] = $objAllService;
                break;
            } else {
                $objAllService->typeSelected = '';
                $type_services[$index] = $objAllService;
            }
        }
        for ($index = 0; $index < count($type_services); $index++) {
            for ($index2 = 0; $index2 < count($salon_services); $index2++) {
                $objAllService = $type_services[$index];
                $objSalonType = $salon_services[$index2];

                if ($objAllService->ser_id == $objSalonType->ser_id) {
                    if ($objSalonType->sser_enabled == '1' && $objSalonType->sal_id == $salon->sal_id) {
                        $objAllService->checked = 'checked';
                        $type_services[$index] = $objAllService;
                        break;
                    } else {
                        $objAllService->checked = 'UnChecked';
                        $type_services[$index] = $objAllService;
                    }
                } else {
                    $objAllService->checked = 'UnChecked';
                    $type_services[$index] = $objAllService;
                }
            }
        }
        $page_title = "Salon Service Type Update Info";
        //   $type_services = $this->array_sort($type_services, 'checked', SORT_DESC);
        // return view('salon-magmt/editsalon', ['salon' => $salon, 'salon_types' => $salon_types, 'type_services' => $type_services]);
        return view('salon-magmt/editsalon',compact('page_title'),['returnback' => $returnback,'salon' => $salon, 'salon_types' => $salon_types, 'type_services' => $type_services, 'salon_services_salon' => $salon_services_salon]);
    }

    public function editsalontype($id) {
        // dd('Here');
        $page_title = "Update Salon Type ";
        $type_id = session('type_id') ? session('type_id') : 1;
        // $salon = DB::table('salon')->where('sal_id', $id)->get();
        //dd($salon);
        $sty_id = $id;

        $allServices = DB::table('services')->get();

        $salon_types = DB::table('salon_types')->get();
        $type_services = DB::select("SELECT * FROM services 
        INNER JOIN salon_types_services SalonTypeServices ON SalonTypeServices.ser_id = services.ser_id 
        INNER JOIN salon_types SalonTypes ON SalonTypes.sty_id = SalonTypeServices.sty_id
        WHERE SalonTypes.sty_id =  $sty_id");
        //$type_services 
        $currentSalonType = DB::select("SELECT * FROM salon_types 
                      WHERE sty_id =  $sty_id");
        return view('salon-magmt/editsalontype', compact('page_title'),['salon_types' => $salon_types, 'type_services' => $type_services, 'currentSalonType' => $currentSalonType]);
    }

    public function editsalonWithCheck($id) {
        //get checked array first
        //get unchecked array then
        $type_services = DB::select("SELECT * FROM services 
        INNER JOIN salon_types_services SalonTypeServices ON SalonTypeServices.ser_id = services.ser_id 
        INNER JOIN salon_types SalonTypes ON SalonTypes.sty_id = SalonTypeServices.sty_id
        WHERE SalonTypes.sty_id =  $id");
        //$type_services 
        $allServices = DB::table('services')->get();
        $type_id = $this->session->flashdata('type_id');
        $data['type_services'] = $this->Admin_model->get_type_services($type_id);
        $data['salon_services'] = $this->Admin_model->get_salon_services();

        foreach ($data['allServices'] as $type_service_key => $type_service) {
            foreach ($data['type_services'] as $salon_service_key => $salon_service) {
                if ($type_services['ser_id'] == $allServices['ser_id']) {
                    $checked = 'checked';
                    $data['allServices'][$type_service_key]['checked'] = 'checked';
                }
            }
        }


        $data['type_id'] = $type_id;
        $data['salon'] = $this->Admin_model->get_salon($id);
        $data['salon_types'] = $this->Admin_model->get_salon_types();

        return view('salon-magmt/editsalontypedetail', ['salon_types' => $salon_types, 'type_services' => $type_services, 'allServices' => $allServices]);
    }

    public function editsalontypedetail($id, $salon_name) {

        $page_title = "Salon Type Details";
        $type_id = session('type_id') ? session('type_id') : 1;
        // $salon = DB::table('salon')->where('sal_id', $id)->get();
        //dd($salon);
        $sty_id = $id;
        $allServices = DB::table('services')->get();
        //$salon_types = DB::table('salon_types')->get();
        $salon_types = DB::select("SELECT * FROM services 
        INNER JOIN salon_types_services SalonTypeServices ON SalonTypeServices.ser_id = services.ser_id 
        INNER JOIN salon_types SalonTypes ON SalonTypes.sty_id = SalonTypeServices.sty_id
        WHERE SalonTypes.sty_id =  $sty_id");
        for ($index = 0; $index < count($allServices); $index++) {
            for ($index2 = 0; $index2 < count($salon_types); $index2++) {
                $objAllService = $allServices[$index];
                $objSalonType = $salon_types[$index2];
                if ($objAllService->ser_id == $objSalonType->ser_id) {
                    $objAllService->checked = 'checked';
                    $allServices[$index] = $objAllService;
                    break;
                } else {
                    $objAllService->checked = 'UnChecked';
                    $allServices[$index] = $objAllService;
                }
            }
        }
        // echo 'here';
        // dd('It here', $sty_id);
        return view('salon-magmt/editsalontypedetail',compact('page_title') ,['salon_types' => $salon_types, 'allServices' => $allServices, 'sty_id' => $sty_id, 'salon_name' => $salon_name]);
    }

    public function service_categories(Request $request) {
        $helper = new Helper();

        $cate_images = $helper->display_cate_images();
        $page_title = "";
        //  dd('here');
        $service_cat = $request->service_cat;
        $is_active   = $request->is_active;
       // dd($is_active);
       if(empty($service_cat) AND $is_active == "all_active")
       {
            $categories = DB::table('service_categories')->orderBy('sc_id','DESC')->paginate(10);
            Session::put("activemenu","servicecategories");
            return view('service-mgmt/managecategories',compact('cate_images','page_title','service_cat','is_active'), ['categories' => $categories]);
       }

        if(!empty($service_cat))
        {
            if(!empty($is_active))
            {
                if($is_active == "all_active")
                {
                    $categories = DB::table('service_categories')
                                    ->where('sc_name','LIKE','%'.$service_cat.'%')
                                    ->orderBy('sc_id','DESC')
                                    ->paginate(10);
                }
                else
                {
                    if($is_active == "1")
                    {
                        $categories = DB::table('service_categories')
                                    ->where('sc_name','LIKE','%'.$service_cat.'%')
                                    ->where('sc_status',1)
                                    ->orderBy('sc_id','DESC')
                                    ->paginate(10);  
                    }
                    elseif($is_active == "2")
                    {
                        $categories = DB::table('service_categories')
                                     ->where('sc_name','LIKE','%'.$service_cat.'%')
                                     ->where('sc_status',0)
                                     ->orderBy('sc_id','DESC')
                                      ->paginate(10); 
                    }
                   
                }
            }
            Session::put("activemenu","servicecategories");
            return view('service-mgmt/managecategories',compact('cate_images','page_title','service_cat','is_active'), ['categories' => $categories]);
        }
        elseif(!empty($is_active))
        {
            if(!empty($service_cat) AND $is_active != "all_active")
            {
                if($is_active == "1")
                {
                    $categories = DB::table('service_categories')
                                     ->where('sc_name','LIKE','%'.$service_cat.'%')
                                     ->where('sc_status',1)
                                     ->orderBy('sc_id','DESC')
                                      ->paginate(10); 
                }
                elseif($is_active == "2")
                {
                    $categories = DB::table('service_categories')
                                     ->where('sc_name','LIKE','%'.$service_cat.'%')
                                     ->where('sc_status',0)
                                     ->orderBy('sc_id','DESC')
                                      ->paginate(10); 
                }
                
            }
            elseif(!empty($service_cat) AND $is_active == "all_active")
            {
                $categories = DB::table('service_categories')
                                     ->where('sc_name','LIKE','%'.$service_cat.'%')
                                     ->orderBy('sc_id','DESC')
                                      ->paginate(10);
            }
            else
            {
                if($is_active == "1")
                {
                    $categories = DB::table('service_categories')
                                     ->where('sc_status',1)
                                     ->orderBy('sc_id','DESC')
                                      ->paginate(10); 
                }
                elseif($is_active == "2")
                {
                    $categories = DB::table('service_categories')
                                     ->where('sc_status',0)
                                     ->orderBy('sc_id','DESC')
                                      ->paginate(10); 
                }
            }
            Session::put("activemenu","servicecategories");
            return view('service-mgmt/managecategories',compact('cate_images','page_title','service_cat','is_active'), ['categories' => $categories]);
        }
        else
        {
            $categories = DB::table('service_categories')->orderBy('sc_id','DESC')->paginate(10);
        }

        Session::put("activemenu","servicecategories");
        return view('service-mgmt/managecategories',compact('cate_images','page_title','service_cat','is_active'), ['categories' => $categories]);
    }

    public function manage_subcategores(Request $request, $id,$urls) 
    {
        $helper = new Helper();
        $cate_images = $helper->display_cate_images();

        $page_title = "";

        $returnback = $urls;

        $category = DB::table('service_categories')->where('sc_id', $id)->get()[0];
        $sub_cacte = $request->sub_cacte;
        $is_active = $request->is_active;

        if(empty($sub_cacte) AND $is_active == "all_active")
       {
            $sub_categories = DB::table('service_sub_categories')->where('sc_id', $id)->get();
            return view('service-mgmt/manage-subcategores',compact('cate_images','page_title','returnback','sub_cacte','is_active'), ['category' => $category, 'sub_categories' => $sub_categories, 'sc_id' => $id]);
       }

        if(!empty($sub_cacte))
        {
            if(!empty($is_active))
            {
                if($is_active == "all_active")
                {
                    $sub_categories = DB::table('service_sub_categories')->where('sc_id', $id)
                                         ->where('ssc_name','LIKE','%'.$sub_cacte.'%') 
                                          ->get();
                }
                else
                {
                    if($is_active == "1")
                    {
                        $sub_categories = DB::table('service_sub_categories')
                                    ->where('sc_id', $id)
                                     ->where('ssc_name','LIKE','%'.$sub_cacte.'%')
                                     ->where('ssc_status',1)
                                      ->get();  
                    }
                    elseif($is_active == "2")
                    {
                        $sub_categories = DB::table('service_sub_categories')
                                    ->where('sc_id', $id)
                                     ->where('ssc_name','LIKE','%'.$sub_cacte.'%')
                                     ->where('ssc_status',0)
                                      ->get(); 
                    }
                   
                }
            }
            return view('service-mgmt/manage-subcategores',compact('cate_images','page_title','returnback','sub_cacte','is_active'), ['category' => $category, 'sub_categories' => $sub_categories, 'sc_id' => $id]);
        }
        elseif(!empty($is_active))
        {
            if(!empty($sub_cacte) AND $is_active != "all_active")
            {
                if($is_active == "1")
                {
                    $sub_categories = DB::table('service_sub_categories')
                                    ->where('sc_id', $id)
                                     ->where('ssc_name','LIKE','%'.$sub_cacte.'%')
                                     ->where('ssc_status',1)
                                      ->get(); 
                }
                elseif($is_active == "2")
                {
                    $sub_categories = DB::table('service_sub_categories')
                                    ->where('sc_id', $id)
                                     ->where('ssc_name','LIKE','%'.$sub_cacte.'%')
                                     ->where('ssc_status',0)
                                      ->get();  
                }
                
            }
            elseif(!empty($sub_cacte) AND $is_active == "all_active")
            {
                $sub_categories = DB::table('service_sub_categories')
                                    ->where('sc_id', $id)
                                     ->where('ssc_name','LIKE','%'.$sub_cacte.'%')
                                      ->get();
            }
            else
            {
                if($is_active == "1")
                {
                    $sub_categories = DB::table('service_sub_categories')
                                    ->where('sc_id', $id)
                                     ->where('ssc_status',1)
                                      ->get(); 
                }
                elseif($is_active == "2")
                {
                    $sub_categories = DB::table('service_sub_categories')
                                    ->where('sc_id', $id)
                                     ->where('ssc_status',0)
                                      ->get(); 
                }
            }
            return view('service-mgmt/manage-subcategores',compact('cate_images','page_title','returnback','sub_cacte','is_active'), ['category' => $category, 'sub_categories' => $sub_categories, 'sc_id' => $id]);
        }
        else
        {
            $sub_categories = DB::table('service_sub_categories')->where('sc_id', $id)->get();
             return view('service-mgmt/manage-subcategores',compact('cate_images','page_title','returnback','sub_cacte','is_active'), ['category' => $category, 'sub_categories' => $sub_categories, 'sc_id' => $id]);
        }
       
       
    }

    public function edit_category($id,$urls) {
        $page_title = "";
        $returnback = $urls;
        $category = DB::table('service_categories')->where('sc_id', $id)->get()[0];
        // dd($category);
        return view('service-mgmt/edit-category',compact('page_title','returnback'), ['category' => $category]);
    }

    public function edit_sub_category($id, $sc_id) {
        $page_title = "";
        $categories = DB::table("service_categories")->get();
        $category = DB::table('service_sub_categories')->where('ssc_id', $id)->get()[0];
        // dd($category);
        $scid = $sc_id;
        return view('service-mgmt/edit-sub-category',compact('page_title','categories','scid'), ['category' => $category, 'sc_id' => $sc_id]);
    }

    public function update_category(Request $request) {
        $helper = new Helper();

        $isActive = "";
        if (!empty($request['sal_type_status'])) {
            $isActive = "1";
        } else {
            $isActive = "0";
        }


        $cat_data = array();
        //   dd($request->input('sal_type_status'));
        $sc_id = $request->input('cat_id');
        $cat_data['sc_gender'] = $request->input('sc_gender');
        $cat_data['sc_name'] = $request->input('cat_name');

        $cat_data['sc_featured'] = $request->input('sc_featured') == null ?  '' : $request->input('sc_featured');
        $cat_data['sc_type'] = $request->input('sc_type');
        // $cat_data['sc_status'] = $request->input('cat_status');
        $cat_data['sc_status'] = $isActive;

        // dd($cat_data['sc_status']);
        if (Input::hasFile('cat_image')) {
            $cat_data['sc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
            $category = DB::table('service_categories')->where('sc_id', $request->input('cat_id'))->get()[0];
            $cat_Image = $helper->cate_images()."/".$category->sc_image;        
            // $cat_Image = public_path("category_images/{$category->sc_image}"); // get previous image from folder
            if (File::exists($cat_Image) && $category->sc_image != '') 
            { // unlink or remove previous image from folder
                unlink($cat_Image);
            }
                    $destinationPath    = $helper->cate_images();  
                    // if(Session::get('change_db_connection')=="3"){
                    //     $helper->uploadimageFTP('/cat_images/'.$cat_data['sc_image'],$request->cat_image);
                    // }        
                    $request->cat_image->move($destinationPath, $cat_data['sc_image']);    
            // $request->cat_image->move(public_path('category_images'), $cat_data['sc_image']);
        }
        DB::table('service_categories')->where('sc_id', $sc_id)->update($cat_data);
        Session::flash('flash_message', 'Category updated successfully.');
        return redirect()->back();
    }

    public function update_sub_category(Request $request) 
    {

        
        $Subcat_data = array();
        //   dd($request->input('sal_type_status'));
        $ssc_id = $request->input('ssc_id');

        $Subcat_data['ssc_keywords']        = $request->input('ssc_keywords');
        $Subcat_data['ssc_name']        = $request->input('ssc_name');
        $Subcat_data['ssc_link']   = $request->input('ssc_link');
        $Subcat_data['ssc_gender']      =   $request->input('ssc_gender');
        $Subcat_data['sc_id']           = $request->input('cat_name');
        //$Subcat_data['ssc_status'] = $request->input('ssc_status');

        $isActive = "";
        if (!empty($request['sal_type_status'])) {
            $isActive = "1";
        } else {
            $isActive = "0";
        }
        $Subcat_data['ssc_status'] = $isActive;

        // dd($Subcat_data['ssc_status']);
        if (Input::hasFile('ssc_image')) 
        {
            $helper = new Helper();
            $files = DB::table('service_sub_categories')->where('ssc_id', $ssc_id)->first();
                $file = $files->ssc_image; 
                        $filename = $helper->cate_images()."/".$file;
                    
                // $filename = public_path()."/category_images//".$file;
                if (file_exists($filename)) 
                {   
                    \File::delete($filename);                         
                }

            $Subcat_data['ssc_image'] = time() . '.' . $request->ssc_image->getClientOriginalExtension();
            $sub_Category = DB::table('service_sub_categories')->where('ssc_id', $request->input('ssc_id'))->get()[0];

            // $dir_Subcat_Image = public_path("category_images/{$sub_Category->ssc_image}"); // get previous image from folder
            // if (File::exists($dir_Subcat_Image)) 
            // { 
            //     // unlink or remove previous image from folder
            //     unlink($dir_Subcat_Image);
            // }
             
                        $destinationPath    = $helper->cate_images();
                    // if(Session::get('change_db_connection')=="3"){
                    //     $helper->uploadimageFTP('/cat_images/'.$Subcat_data['ssc_image'],$request->ssc_image);
                    // } 
                    $request->ssc_image->move($destinationPath, $Subcat_data['ssc_image']);  
            // $request->ssc_image->move(public_path('category_images'), $Subcat_data['ssc_image']);
        }
        //Session::flash('flash_message', 'Category updated successfully.');
        DB::table('service_sub_categories')->where('ssc_id', $ssc_id)->update($Subcat_data);
        //Session::flash('flash_message', 'Category updated successfully.');
         $notification = array(
                'success' => 'Service Sub-category updated successfully!', 
                'alert-type' => 'success'
            );
         Session::flash("sub-cat","Service Sub-category updated successfully!");

        return back()->with($notification);
    }

    public function add_category() {
        $page_title = "";
        return view('service-mgmt/add-category',compact('page_title'));
    }

    public function fileupload() {
        $page_title = "File Upload ";
        // dd('Here');
        Session::put("activemenu","fileupload");
        return view('service-mgmt/fileupload',compact('page_title'));
    }

    public function add_subcategory($sc_id){
        // dd($sc_id);
        $page_title = "";
        $categories = DB::table('service_categories')->get();
        return view('service-mgmt/add-subcategory', ['page_title' => $page_title,'categories' => $categories, 'sc_id' => $sc_id]);
    }

    public function save_subcategory(Request $request) 
    {
        $helper = new Helper();
        $cat_data = array();

        $cat_data['ssc_link']  = $request->input('ssc_link');
        $cat_data['ssc_keywords']       = $request->input('ssc_keywords');
        $cat_data['ssc_name']       = $request->input('sub_cat_name');
        $cat_data['sc_id']          = $request->input('cat_name');
        $cat_data['ssc_status']     = $request->input('cat_status');
        $cat_data['ssc_gender']     = $request->input('ssc_gender');

        if (Input::hasFile('cat_image')) {
                
                $destinationPath    = $helper->cate_images();
                    
            $cat_data['ssc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
            // if(Session::get('change_db_connection')=="3"){
            //         $helper->uploadimageFTP('/cat_images/'.$cat_data['ssc_image'],$request->cat_image);
            //     } 
              $request->cat_image->move($destinationPath, $cat_data['ssc_image']);    
            // $request->cat_image->move(public_path('category_images'), $cat_data['ssc_image']);
//         dd($cat_data);
        }
        DB::table('service_sub_categories')->insert($cat_data);
//         $request->session()->flash('alert-success', 'Category was successfuly added!');
        Session::flash('flash_message', 'Sub Category successfully added.');

        return redirect()->back();
    }

    public function save_category(Request $request) {
        //dd('till now');
        $helper = new Helper();
        $cat_data = array();
        $cat_data['sc_name']        = $request->input('cat_name');
        $cat_data['sc_status']      = $request->input('sc_status');
        $cat_data['sc_gender']      = $request->input('sc_gender');
        $cat_data['sc_featured']    = $request->input('sc_featured') == null ?  '' : $request->input('sc_featured');
        $cat_data['sc_type']        = $request->input('sc_type');


        if (Input::hasFile('cat_image')) {
            
            $destinationPath    = $helper->cate_images();
                   
            $cat_data['sc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
                // if(Session::get('change_db_connection')=="3"){
                //     $helper->uploadimageFTP('/cat_images/'.$cat_data['sc_image'],$request->cat_image);
                // }
            $request->cat_image->move($destinationPath, $cat_data['sc_image']); 
            // $request->cat_image->move(public_path('category_images'), $cat_data['sc_image']);
//         dd($cat_data);
        } else {
            $cat_data['sc_image'] = '';
            // dd('there was');  
        }

        DB::table('service_categories')->insert($cat_data);

//         $request->session()->flash('alert-success', 'Category was successfuly added!');
        Session::flash('flash_message', 'Category successfully added.');

        return redirect()->to('servicecategories');
    }

    //save file salon services
    public function save_file_services(Request $request) 
    {
        // header('Content-Type: text/html; charset=UTF-8');

        $saldata = '';
        $linenum = '';

        $page_title = " File Upload ";
        $cat_data = array();
        $cat_data['sc_name'] = $request->input('cat_name');
        //cat_image=salon_service_file
        if (Input::hasFile('cat_image')) 
        {
            $cat_data['sc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
            $request->cat_image->move(public_path('category_images'), $cat_data['sc_image']);
            // dd($cat_data['sc_image']);
        } 
        else 
        {
            $cat_data['sc_image'] = '';
        }
        // dd($cat_data['sc_image']);
        $file_name = $cat_data['sc_image'];
        define('CSV_PATH', 'category_images/');
        $csv_file2 = CSV_PATH . $file_name;

        // $file_data = file_get_contents($csv_file2);
        // //$utf8_file_data = utf8_encode($file_data);

        // $utf8_file_data = mb_convert_encoding($file_data, "UTF-8", "UTF-16LE");
        // //$utf8_file_data = iconv("UTF-16LE","UTF-8",$file_data);

        // $new_file_name = $csv_file2;
        // file_put_contents($new_file_name , $utf8_file_data );
        // // dd($csv_file2);
        
        // $csvContents=file_get_contents($csv_file2);
        // $csvConverted = mb_convert_encoding($csvContents, "UTF-8");
        // $fil = file_put_contents($csv_file2, $csvConverted);

        // dd($csv_file2);
        // dd("outside if condition",$csv_file2);
         //import services table here
        DB::statement("DELETE FROM salon_services_sa_import");

        // DB::statement("LOAD DATA INFILE '$csv_file2' INTO TABLE salon_services_sa_import");

        // $pdo = DB::connection()->getPdo();
    // dd($pdo);
        // $pdo = DB::connection()->getPdo();
        // DB::statement("LOAD DATA LOCAL INFILE '".storage_path($csv_file2)."' INTO TABLE salon_services_sa_import");

        $pdo = DB::connection()->getPdo();
        $exfile = $pdo->exec("LOAD DATA LOCAL INFILE '".$csv_file2."' INTO TABLE salon_services_sa_import FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n'");

        // COLUMNS TERMINATED BY '\t' --Use this only if your column / fields terminate or are separated with tab if they are terminate with comma use FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' as given below,only use either one of them at a time.

        // IGNORE 1 LINES (sal_id,sal_name,ssc_name,sser_name,sser_rate,sser_time) 
        
    // dd($exfile);
        DB::statement("UPDATE salon_services_sa_import set 
            sser_rate = REPLACE(sser_rate,'£',''), sser_name = substring(sser_name,1,245)");
        
        // REPLACE INTO salon_services_sa_importas VALUES (1, 'Old', '2014-08-20 18:47:00');
        // Query OK, 1 row affected (0.04 sec)
        // // dd('ok');
        // substring(sser_name,1,245)
        // if (($handle = fopen($csv_file2, "r")) !== FALSE) 
        // {
        //     // dd($handle);
        //     DB::statement("DELETE FROM salon_services_sa_import");

        //     $i=0;
        //     $what_to_insert = array();
        //     // dd($handle);
        //     fgetcsv($handle);
        //     if(($data = fgetcsv($handle, 1000,",")) !== FALSE)
        //     {
        //         dd("ok");

        //         while(($data = fgetcsv($handle, 1000,",")) !== FALSE) 
        //         {
        //             //format csv header row into array of strings
                     
        //             // dd("in whileloop", $data);
        //             $i += 1;

        //             if($i > 0)
        //             {
        //                 $price = str_replace("£",'',$data[4]) == null ? 0 : str_replace("£",'',$data[4]); 
        //                 $service_time   = $data[5] == null ? 0 : $data[5];

        //                 array_push($what_to_insert, "('".'0'."','".$data[0]."','".str_replace("'", '',$data[3])."','".$price."','".$service_time."','".'0'."','".str_replace("'", '',$data[1])."','".str_replace("'", '',$data[2])."','".'0'."')");
        //             }
        //             if (count($what_to_insert) > 0 && $i == 100)
        //             {
        //         // dd("if condition ");
        //             $import= DB::statement("INSERT into salon_services_sa_import(ser_id,sal_id,sser_name,sser_rate,sser_time,sser_id,sal_name, ssc_name,ssc_id) values " . implode(",", $what_to_insert));
        //         // dd("import data ",$import);
        //                 unset($what_to_insert);
        //                 $what_to_insert = array();
        //                 $i = 1; 
        //             }

                    
        //             // $result = DB::statement("INSERT INTO salon_services_sa_import (ser_id,sal_id,sser_name,sser_rate,sser_time,sser_id,sal_name, ssc_name,ssc_id) VALUES ('". $ser_id."','". $sal_id . "', '" . $sser_name . "', '". $sser_rate . "', '" . $sser_time . "', '" . $sser_id . "', '". $sal_name . "', '" . $ssc_name ."','". $ssc_id ."')");
        //         }
        //     }
        //     else
        //     {
        //         dd("file not read");
        //         return back()->with("file not read, please try again upload");
        //     }

        //    //  $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN services s on s.ser_name = sss.sser_name SET sss.ser_id = s.ser_id WHERE sss.ser_id = 0");
        //    //  // dd($result);
        //    //  //all salon_services_sa_import with zero id insert into service and again update salon_services_sa_import
        //    //  $result1 = DB::statement("INSERT INTO services (ser_name)
        //    //   SELECT DISTINCT substring(sser_name,1,245) FROM salon_services_sa_import as s_imp WHERE ser_id=0");
        //    // // dd($result1, 'till now as');
        //    //  $results = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN services s on s.ser_name = sss.sser_name SET sss.ser_id = s.ser_id WHERE sss.ser_id = 0");
        //    //  $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN salon_ser_categories s on s.ssc_name = sss.ssc_name SET sss.ssc_id = s.ssc_id WHERE sss.ssc_id = 0");
        //    //  // dd($result);
        //    //  //all salon_services_sa_import with zero id insert into service and again update salon_services_sa_import
        //    //  $result1 = DB::statement("INSERT INTO salon_ser_categories (ssc_name, sal_id)
        //    //   SELECT  ssc_name, sal_id FROM salon_services_sa_import as s_imp WHERE ssc_id=0 ");
        //    // // dd($result1, 'till now as');
        //    //  $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN salon_ser_categories s on s.ssc_name = sss.ssc_name 
        //    //    SET sss.ssc_id = s.ssc_id WHERE sss.ssc_id = 0");

        //    // $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN service_sub_categories s on s.ssc_name = sss.ssc_name 
        //    // SET sss.ssc_id = s.ssc_id WHERE sss.ssc_id = 0");           
           
        //    // // $result1 = DB::statement("INSERT INTO salon_ser_categories (ssc_name,sal_id)
        //    // //  SELECT  sser_name,ssc_id FROM salon_services_sa_import as s_imp WHERE ser_id=0");
        //     fclose($handle);

        //     if (count($what_to_insert)>0){
        //         // dd("if condition ");
        //             $import= DB::statement("INSERT into salon_services_sa_import(ser_id,sal_id,sser_name,sser_rate,sser_time,sser_id,sal_name, ssc_name,ssc_id) values " . implode(",", $what_to_insert));
        //         // dd("import data ",$import);
        //         }

        // }
        // dd(" out side if condition file close ok");

            $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN services s on s.ser_name = substring(sss.sser_name,1,245) SET sss.ser_id = s.ser_id WHERE sss.ser_id = 0");
            // dd($result);
            //all salon_services_sa_import with zero id insert into service and again update salon_services_sa_import
            $result1 = DB::statement("INSERT INTO services (ser_name)
             SELECT DISTINCT substring(sser_name,1,245) FROM salon_services_sa_import as s_imp WHERE ser_id=0");
           // dd($result1, 'till now as');
            $results = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN services s on s.ser_name = sss.sser_name SET sss.ser_id = s.ser_id WHERE sss.ser_id = 0");
            // dd("services table ");
            $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN salon_ser_categories s on s.ssc_name = sss.ssc_name and s.sal_id = sss.sal_id SET sss.ssc_id = s.ssc_id WHERE sss.ssc_id = 0");
            // dd("salon_service category");
            //all salon_services_sa_import with zero id insert into service and again update salon_services_sa_import
            $result1 = DB::statement("INSERT INTO salon_ser_categories (ssc_name, sal_id)
             SELECT  distinct ssc_name, sal_id FROM salon_services_sa_import as s_imp WHERE ssc_id=0 ");

            $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN salon_ser_categories s on s.ssc_name = sss.ssc_name and s.sal_id = sss.sal_id SET sss.ssc_id = s.ssc_id WHERE sss.ssc_id = 0");

           // dd("insert salon_ser_category ");
            // $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN salon_ser_categories s on s.ssc_name = sss.ssc_name 
            //   SET sss.ssc_id = s.ssc_id WHERE sss.ssc_id = 0");

           // $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN service_sub_categories s on s.ssc_name = sss.ssc_name 
           // SET sss.ssc_id = s.ssc_id WHERE sss.ssc_id = 0");           
           
           // $result1 = DB::statement("INSERT INTO salon_ser_categories (ssc_name,sal_id)
           //  SELECT  sser_name,ssc_id FROM salon_services_sa_import as s_imp WHERE ser_id=0");

        // dd("data inserted ");
       
        $result1 = DB::statement("INSERT INTO salon_services (ser_id,sser_name, sal_id, sser_rate, sser_time, ssc_id)
        SELECT ser_id, substring(sser_name,1,245), sal_id,sser_rate, sser_time, ssc_id FROM salon_services_sa_import  WHERE ser_id<>0
        ON DUPLICATE KEY UPDATE salon_services.ssc_id = salon_services.ssc_id");

        $salonasiport = DB::table("salon_services_sa_import")->DISTINCT('sal_id','sser_id')->get();

       $i = 0;
    $pres_id = 0;

    $salids  = '';
    $sserid = '';
        foreach($salonasiport as $salon_s)
        {   
            $salids  = $salids.','.$salon_s->sal_id;
            $sserid = $sserid.','.$salon_s->sser_id;
            if($salon_s->sal_id != $pres_id)
            {
                $pres_id = $salon_s->sal_id;
                $tech = $this->recal_setUpSalon($salon_s->sal_id);

                if($tech == 'false')
                {   
                    $i += 1; 
                    $saldata = $saldata.','.$salon_s->sal_id;
                    $linenum = $linenum.','.$i;
                    Session::flash('service_error_message', 'Some errors please check in salon opening hours ');
                }
            }

        }

        $user_ids = \Auth::User()->id;

        $results = DB::statement("INSERT INTO salon_data_upload (user_id,type,sal_id,sal_service_id,created_at) VALUES('".$user_ids."','"."salon-service"."','".$salids."','".$sserid."','".\Carbon\Carbon::now()->toDateTimeString()."') ");

        Session::flash('service_flash_message', 'Salon Service successfully added.');
        Session::put('activemenu','save_file_services');   
        return view('service-mgmt/fileupload',compact('page_title','saldata','linenum'));
        //end services import
        //edn save data
        //for saving file to server side
    }
    // read file function
    public function get_hours($hours)
    {
        //     $hours = "TUESDAY 9am to 2pm, WEDNESDAY 9am to 2pm, THURSDAY 9am to 6pm, FRIDAY 9am to 6pm, SATURDAY 9am to 6pm";
        //        $hours1 = "MON 10.00   19.00, TUE 10.00   19.00, WED 10.00   19.00, THU 12.00   19.00, FRI 09.00   20.00, SAT 09.00   20.00";
        //        $hours2 = "Monday 11.00am 8.00pm, Tuesday 11.00am 8.00pm, Wednesday 11.00am 8.00pm, Thursday 11.00am 8.00pm, Friday 11.00am 8.00pm, Saturday 11.00am 8.00pm, Sunday 11.00am 6.00pm";
        //        $hours3 = "Monday 10am 7:30pm, Tuesday 10am 7:30pm, Wednesday 10am 7:30pm, Thursday 10am 7:30pm, Friday 10am 8:30pm, Saturday 9am 7pm, Sunday 11am 6:30pm";
        //        $hours4 = " Monday 8:30AM 7PM, Sunday Closed, Monday 8:30AM 7PM, Wednesday 8:30AM 7PM, Thursday 8:30AM 7PM, Friday 8:30AM 7PM, Saturday 8:30AM 7PM, Sunday Closed, Monday 8:30AM 7PM, Tuesday 8:30AM 7PM";
        //       
                //$hours_new= explode(",", $hours);
        // dd('At save file', $hours_new);
         $hourss = explode(",", $hours);
        $string_hour = "";
        $hourss_cal="";
        foreach ($hourss as $key => $hour) 
        {
            $trms = preg_replace('!\s+!', ' ', $hour);
                // dd($trm);
            // here split each value
            $split_val_arr = explode(" ",trim(str_replace(" to "," ", $trms)));
            // dd($split_val_arr);
            if(count($split_val_arr)==3)
            {
                $withoutDay = array_splice($split_val_arr, 0, 1);
                //   dd($withoutDay);
                $string_hour = date("H:i", strtotime($split_val_arr[0])).'&'.date("H:i", strtotime($split_val_arr[1]));
                // echo $string_hour."<br>";
                $hourss_cal=$hourss_cal.','.$string_hour;
            }
            elseif(count($split_val_arr)==2)
            {
                if($split_val_arr[1] == "Closed" OR $split_val_arr[1] == "closed")
                {
                 $withoutDay = array_splice($split_val_arr, 0, 1);
                 // dd($withoutDay);
                 $string_hour = date("H:i", strtotime($split_val_arr[0])).'&'.date("H:i", strtotime($split_val_arr[0]));    
                 $hourss_cal=$hourss_cal.','.$string_hour;
                }
                else
                {
                  $withoutDay = array_splice($split_val_arr, 0, 1);
                  $string_hour = date("H:i", strtotime($split_val_arr[0])).'&'.date("H:i", strtotime($split_val_arr[0]));
                  $hourss_cal=$hourss_cal.','.$string_hour;
                }
            }
            else
            {
                $hourss_cal = '00:00&00:00';
            }
        }
     
        $str2 = substr($hourss_cal,1);
        return $str2;
    }
    // end  read file function
    public function save_file(Request $request) 
        {
            $salon_data = '';
            $page_title = "File Upload ";
            //temp
         $cat_data = array();
        $cat_data['sc_name'] = $request->input('cat_name');
        //cat_image=salon_service_file
        //temp
        if (Input::hasFile('cat_image')) 
        {
             // dd('here');
            $path = $request->file('cat_name');
            //dd($path);
            $cat_data['sc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
            $request->cat_image->move(public_path('category_images'), $cat_data['sc_image']);
            // dd($cat_data);
        } 
        else 
        {
            dd('Image not found');
            $cat_data['sc_image'] = '';
        }

   // dd("end");
    $i = 0;
    $salon_linenum = '';
        $file_name = $cat_data['sc_image'];
        define('CSV_PATH', 'category_images/');
        $csv_file = CSV_PATH . $file_name;
         
      //  $cat_data = array();
      //  $cat_data['sc_name'] = $request->input('cat_name');
      //  define('CSV_PATH', 'category_images/');
       // $csv_file = CSV_PATH . "sal_update.csv";        

        if(($handle = fopen($csv_file, "r")) !== FALSE)
        {
            $data = DB::table("salon_sa_import")->truncate();
            //fgetcsv($handle);
            if((fgetcsv($handle, 1000, ",")) !== FALSE)
            {
                // dd($data);
                while(($data = fgetcsv($handle, 1000, ",")) !== FALSE)
                {
                    //dd($data);
                    $num = count($data);
                    for($c = 0; $c < $num; $c++) 
                    {
                        $col[$c] = $data[$c];
                    }
                    // dd(count($col));
                    if(count($col) == 17 )
                    {
                        $sal_id         = $col[0];
                        $monday         = $col[1]; 
                        $tuesday        = $col[2];
                        $wednesday      = $col[3];
                        $thursday       = $col[4];
                        $friday         = $col[5];
                        $saturday       = $col[6];
                        $sunday         = $col[7];
                        $sal_phone      = $col[8];
                        $sal_pic        = $col[9];
                        $websie         = $col[10];
                        $sal_facebook   = $col[11];
                        $sal_twitter    = $col[12];
                        $sal_instagram  = $col[13];
                        $sty_id         = $col[14];
                        $sal_address    = $col[15];
                        $sal_email       = $col[16];

                        $sal_hours = str_replace("-", "&", trim($col[1]) . "," . trim($col[2]) . "," . trim($col[3]) . "," . trim($col[4]) . "," . trim($col[5]) . "," . trim($col[6]) . "," . trim($col[7]) );
                        $sal_hours = str_replace('–','&', $sal_hours);
                        if($sal_hours==',,,,,,')
                        {
                            $sal_hours = '';
                        }
                        else
                        {
                            // $getHours       = $this->get_hours($sal_hours);   
                            $isValid_hours = $this->is_valid_weekly_time($sal_hours);
                            if($isValid_hours == false)
                            {
                               $salon_data = $salon_data.','.$sal_id;
                                 $i += 1;
                                $salon_linenum = $salon_linenum.','.$i;
                                Session::flash('salon_error_message', 'Some errors please check in salon opening hours ');
                            }
                        }
                        try 
                        {
                            // echo '$sal_hours' . $sal_hours . '--------'."<br>";
                            $result = DB::statement("INSERT INTO salon_sa_import (sal_id, sal_hours, sal_phone,  sal_facebook,  sal_twitter, sal_instagram, sty_id,sal_pic,monday,tuesday,wednesday,thursday,friday,saturday,sunday,sal_address,sal_email) VALUES "
                                            . "('" . $sal_id . "','" . $sal_hours . "','" . $sal_phone . "','" . $sal_facebook . "','" . $sal_twitter . "','" . $sal_instagram . "','" . $sty_id ."','".$sal_pic."','".$col[1]."','".$col[2]."','".$col[3]."','".$col[4]."','".$col[5]."','".$col[6]."','".$col[7]."','".$sal_address ."','".$col[16] ."') ");
                            // echo 'If you see this, the number is 1 or below';
                        }
                        //catch exception
                        catch (Exception $e) {
                            dd('Exception');
                            //  echo 'Message: ' . $e->getMessage();
                        }

                    }
                    elseif(count($col) == 9)
                    {
                        $sal_id         = $col[0];
                        $sal_hours      = $col[1]; 
                        $sal_phone      = $col[2];
                        $sal_facebook   = $col[3];
                        $sal_twitter    = $col[4];
                        $sal_instagram  = $col[5];
                        $sty_id         = $col[6];
                        $sal_pic        = $col[7];
                        $sal_address    = $col[8];
                        $sal_email      = $col[9];

                        $getHours       = $this->get_hours($sal_hours);  
                        // dd($getHours); 
                        $isValid_hours = $this->is_valid_weekly_time($getHours);
                        if($isValid_hours == false)
                        {
                           $salon_data = $salon_data.','.$sal_id;
                             $i += 1;
                            $salon_linenum = $salon_linenum.','.$i;
                            Session::flash('salon_error_message', 'Some errors please check in salon opening hours ');
                        }
                        try 
                        {
                            // echo '$sal_hours' . $sal_hours . '--------'."<br>";
                            $result = DB::statement("INSERT INTO salon_sa_import (sal_id, sal_hours, sal_phone,  sal_facebook,  sal_twitter, sal_instagram, sty_id,sal_pic,sal_address,sal_email) VALUES "
                                            . "('" . $sal_id . "','" . $getHours . "','" . $sal_phone . "','" . $sal_facebook . "','" . $sal_twitter . "','" . $sal_instagram . "','" . $sty_id ."','".$sal_pic."','".$sal_address ."','".$sal_email."') ");
                            // echo 'If you see this, the number is 1 or below';
                        }
                        //catch exception
                        catch (Exception $e) {
                            dd('Exception');
                            //  echo 'Message: ' . $e->getMessage();
                        }
                    }
                    else
                    {
                        Session::flash("error_message","CSV file is not valid, please try again upload the correct CSV file ");
                        return back();
                    }
                }
            }
            else
            {
                Session::flash("error_message","CSV file is not valid, please try again upload the correct CSV file ");
                        return back();
            }
            
            fclose($handle);
        }
        else
        {
            Session::flash("error_message","CSV file is not valid, please try again upload the correct CSV file ");
                        return back();
        }
            $userid = \Auth::user()->id;
            // dd($userid);
                
                // DB::statement("UPDATE `salon_sa_import` SET sty_id = 0 where sty_id not in (SELECT sty_name from salon_types)");        
                // DB::statement("UPDATE `salon_sa_import` ssi INNER JOIN salon_types st on ssi.sty_id = st.sty_name  
                //     SET ssi.sty_id = st.sty_id");

                $get_salons = DB::select("SELECT sal_id FROM salon_sa_import WHERE salon_sa_import.sal_id NOT IN (SELECT s.sal_id FROM salon s inner join salon_sa_import s1 on s1.sal_id = s.sal_id )");

                $result = DB::statement("INSERT INTO salon
                Select * from salon_all sa WHERE sa.sal_id in (SELECT sal_id FROM salon_sa_import WHERE salon_sa_import.sal_id NOT IN (SELECT s.sal_id FROM salon s inner join salon_sa_import s1 on s1.sal_id = s.sal_id ) )");



                DB::statement("UPDATE `salon` s INNER JOIN salon_sa_import ssi on s.sal_id = ssi.sal_id SET s.sal_hours = if(ssi.sal_hours <> '',ssi.sal_hours,s.sal_hours), s.sal_zip = ssi.sty_id, s.sal_modify_datetime = now(), s.sal_appointment_interval = 30, s.sal_pic = ssi.sal_pic, s.sal_email = ssi.sal_email, s.sal_phone = if(ssi.sal_phone <> '',ssi.sal_phone,s.sal_phone) ");

                // DB::statement("UPDATE `salon` s INNER JOIN salon_sa_import ssi on s.sal_id = ssi.sal_id SET s.sal_hours = ssi.sal_hours, s.sal_facebook = ssi.sal_facebook, s.sal_instagram = ssi.sal_instagram, s.sal_zip = ssi.sty_id, s.sal_twitter = ssi.sal_twitter, s.sal_modify_datetime = now(), s.sal_appointment_interval = 30, s.sal_address = ssi.sal_address, s.sal_pic = ssi.sal_pic, s.sal_email = ssi.sal_email");
/////
                // DB::statement("UPDATE `salon` s INNER JOIN salon_sa_import ssi on s.sal_id = ssi.sal_id SET s.sal_phone = ssi.sal_phone WHERE ssi.sal_phone <> '' ");

///////
            //    UPDATE `salon_sa_import` SET sty_id = 0 where sty_id not in (SELECT sty_name from salon_types)

                // email,phone,website,potal_code,sal_pic
                
             
            // if(!empty($get_salons))
            // {
            //     $result = DB::statement("INSERT INTO salon
            //     Select * from salon_all sa WHERE sa.sal_id in (SELECT sal_id FROM salon_sa_import WHERE salon_sa_import.sal_id NOT IN (SELECT s.sal_id FROM salon s inner join salon_sa_import s1 on s1.sal_id = s.sal_id )");

            //     // foreach ($get_salons as $key => $value) {
            //     //         $data['sal_id'] = $value->sal_id;
            //     //         $data['sal_hours'] = $value->sal_hours;
            //     //         $data['sal_phone'] = $value->sal_phone;
            //     //         $data['sal_facebook'] = $value->sal_facebook;
            //     //         $data['sal_twitter'] = $value->sal_twitter;
            //     //         $data['sal_instagram'] = $value->sal_instagram;
            //     //         $data['sal_zip'] = $value->sty_id;
            //     //         $data['sal_pic'] = $value->sal_pic;
            //     //         $data['sal_address'] = $value->sal_address;
            //     //         $data['sal_modify_datetime'] = Carbon::now();
            //     //         $data['sal_appointment_interval'] = 30;
            //     //         $data['sal_name']    = $value->sal_name;
            //     //         $data['sal_city']    = '';
            //     //         $data['sal_contact_person']    = '';
            //     //         $data['sal_email'] = '';
            //     //         $data['sal_profile_pic'] = '';
            //     //         $data['sal_created_datetime'] = Carbon::now();
            //     //         $data['sal_status']    = '0';
            //     //         $data['admin_device_id']    = '';
            //     //         $data['admin_device_type']    = '';
            //     //         $data['sal_lat']    = '';
            //     //         $data['sal_lng']    = '';
            //     //         $data['sal_timing']    = '';
            //     //         $data['sal_nearby_zip']    = '';
            //     //         $data['sal_search_words']    = '';
            //     //         $data['sal_biography']    = '';
            //     //         $data['sal_weekly_offs']    = '';
            //     //         $data['sty_id']    = 0;
            //     //         $data['sal_services']    = '';
            //     //         $data['sal_review_datetime']    = Carbon::now();

            //     //         $result = DB::table("salon")->insert($data);
            //     // }
            // }
        
            $salonid = DB::table("salon_sa_import")->get();
            $salon_id = '';
            foreach ($salonid as $key => $value) {
                $salon_id = $salon_id.','.$value->sal_id;
            }
            $salonsid = trim($salon_id,",");
            // dd($salonsid);
            $results = DB::statement("INSERT INTO salon_data_upload (user_id,type,sal_id,created_at)
                                        values('".$userid."','"."salon"."','".$salonsid."','".\Carbon\Carbon::now()->toDateTimeString()."')");

            $update_zipcode = DB::statement("update `salon` s inner JOIN postcodelatlng pc on s.sal_zip = pc.postcode set s.sal_lat = pc.latitude, s.sal_lng = pc.longitude where s.sal_lat = '' and s.sal_lng = '' ");

                DB::statement("update salon s JOIN postcodelatlng pc on s.sal_zip = pc.postcode set s.sal_lat = pc.latitude, s.sal_lng = pc.longitude where s.sal_lat = '' or s.sal_lng = '' ");
            
            // $data = DB::table("salon_sa_import")->truncate();
            // DB::statement("DELETE FROM salon_sa_import");
             

           Session::flash('salon_flash_message', 'Salon details successfully added.');

        Session::put("activemenu","save_file");

        return view('service-mgmt/fileupload',compact('page_title','salon_data','salon_linenum'));
    }
    public function manage_services($sc_id, $ssc_id) {

        $salon_types = DB::table('services')
                ->join('sscategories_services', 'services.ser_id', '=', 'sscategories_services.ser_id')
                ->join('service_sub_categories', 'sscategories_services.ssc_id', '=', 'service_sub_categories.ssc_id')
                ->where('sscategories_services.ssc_id', '=', $ssc_id)
                ->select('services.*')
                ->get();

        $allServices = DB::table('services')->get();
        for ($index = 0; $index < count($allServices); $index++) {
            for ($index2 = 0; $index2 < count($salon_types); $index2++) {
                $objAllService = $allServices[$index];
                $objSalonType = $salon_types[$index2];
                if ($objAllService->ser_id == $objSalonType->ser_id) {
                    $objAllService->checked = 'checked';
                    $allServices[$index] = $objAllService;
                    break;
                } else {
                    $objAllService->checked = 'UnChecked';
                    $allServices[$index] = $objAllService;
                }
            }
        }
        return view('service-mgmt/manage-services', ['allServices' => $allServices, 'sc_id' => $sc_id, 'ssc_id' => $ssc_id]);
    }

    function is_valid_weekly_time($weekly_time) {
        $error = 0;
        $valid_chars = "0123456789:&,";
        if (strlen($weekly_time) != 83) {
            $error = 1;
        }

        for ($i = 0; $i < strlen($weekly_time) - 1; $i++) {
            if (strpos($valid_chars, substr($weekly_time, $i, 1)) === false) {
                $error = 2;
            }
        }

        $arr_all_days = explode(",", $weekly_time);
        if (sizeof($arr_all_days) != 7) {
            $error = 3;
        }

        for ($i = 0; $i < sizeof($arr_all_days); $i++) {
            if (!$this->is_valid_time_range($arr_all_days[$i])) {
                return false;
            }
        }

        if ($error > 0) {
            return false;
        } else {
            return true;
        }
    }

    function is_valid_time_range($time_range) {
        $error = 0;
        if (strlen($time_range) != 11) 
        {
            $error = 4;
        } 
        else 
        {
            $arr_day = explode("&", $time_range);
            if (sizeof($arr_day) != 2) 
            {
                $error = 5;
            } 
            else 
            {

                if (strlen($arr_day[0]) != 5) 
                {
                    $error = 6;
                } 
                else 
                {
                    if (strlen($arr_day[1]) != 5) 
                    {

                        $error = 7;
                    } 
                    else 
                    {
                        $arr_start_time = explode(":", $arr_day[0]);
                        if (strlen($arr_start_time[0]) != 2 || strlen($arr_start_time[1]) != 2) 
                        {
                            $error = 8;
                        } 
                        else 
                        {

                            $arr_end_time = explode(":", $arr_day[1]);
                            if (strlen($arr_end_time[0]) != 2 || strlen($arr_end_time[1]) != 2) 
                            {
                                $error = 9;
                            }
                        }
                    }
                }
            }
        }

        if ($error > 0) {

            return false;
        } else {

            return true;
        }
    }

    function weekly_time_ranges_to_slots($weekly_time_ranges) 
    {
        if (strpos($weekly_time_ranges, ",") !== false) {
            $weekly_time_ranges = explode(",", $weekly_time_ranges);
            for ($i = 0; $i < sizeof($weekly_time_ranges); $i++) {
                $single_day_time = explode("&", $weekly_time_ranges[$i]);
                $weekly_slot_ranges[$i] = $this->time_to_slot($single_day_time[0], 1) . "&" . $this->time_to_slot($single_day_time[1], 0);
            }
        }
        return implode(",", $weekly_slot_ranges);
    }

    function time_to_slot($time, $time_flg = 1, $slot_duration = 5) {
        if ($time == "00:00" || $time == "" || $time == "0") {
            return "0";
        }
        $time = explode(":", $time);
        $hr = $time[0];
        $min = $time[1];
        $slot = $hr * (60 / $slot_duration);
        $slot += $time_flg;
        $slot += (int) ($min / $slot_duration);
        return $slot;
    }

    function weekly_ranges_to_array($include_range, $exclude_range = "") 
    {
        $include_range = explode(",", $include_range);
        $exclude_range = explode(",", $exclude_range);
        for ($i = 1; $i <= 7; $i++) {
            $include_range_current = explode("&", $include_range[$i - 1]);
            $exclude_range_current = explode("&", $exclude_range[$i - 1]);
            $include_range_current = range($include_range_current[0], $include_range_current[1]);
            $exclude_range_current = range($exclude_range_current[0], $exclude_range_current[1]);
            $result[$i] = array_diff($include_range_current, $exclude_range_current);
        }
        return $result;
    }

    public function pinterest_board()
    {
        $page_title = "Post Category";
        $title = "Pinterest Borad";

        $pb = DB::table('pinterest_boards')->paginate(10);
        Session::put("activemenu","pinterest-board");
        return  view("pinterestboard/pinterestboard",compact('title','pb','sscs','page_title'));
    }

    public function ajax_activate($pb_id)
    {
        
        $feedback = DB::table('pinterest_boards')->where('pb_id' , $pb_id)->update(array('pb_status' => 1));
        $notification = array(
                'success' => 'Post status actived successfully!', 
                'status' => 'success'
            );

        return back()->with($notification);
        // return redirect()->to(\URL::Previous());
    }

    public function ajax_deactivate($pb_id)
    {
        // $pb_id = $request->pb_id;
        $feedback = DB::table('pinterest_boards')->where('pb_id' , $pb_id)->update(array('pb_status' => 0));
        $notification = array(
                'success' => 'Post status de-actived successfully!', 
                'status' => 'success'
            );

        return back()->with($notification);
        // return redirect()->to(\URL::Previous());
    }
    public function changessc(Request $request)
    {
        $ssc_id = $request->s_id;
        $pb_id = $request->pb_id;
        $urls = \URL::previous();
        $feedback = DB::table('pinterest_boards')->where('pb_id' , $pb_id)->update(array('ssc_id' => $ssc_id));
        if($feedback)
        {
            return '<div class="alert alert-success" id="me">
                             <p style="color: white;" class="text-success">
                                Service sub-category updated successfully
                             </p>
                    </div>';
        }
    }
    public function pbfilter(Request $request)
    {
        $page_title = "Post Category";
        
            $pb_name = $request->pb_name;
            
            $service_sub_cat = $request->service_sub_cat;

        $data = DB::table('pinterest_boards');

            if(!empty($service_sub_cat))
            {
                if($service_sub_cat == "all")
                {
                    // $data->orWhere('ssc_id',0)->orWhere('ssc_id',1);
                }
                else
                {
                    $data = $data->where('ssc_id',$service_sub_cat);
                }
                
            }
          
            if(!empty($pb_name))
            {
              $data =  $data->where('pb_name','like','%'.$pb_name.'%');
            }
            // $pb = $data->get();
            $pb = $data->paginate(10);
            $title = "Pinterest Borad";
        return view("pinterestboard/pinterestboard",compact('page_title','title','pb','pb_name','service_sub_cat'));
    }


    // recal setup salons import excel files
    public function recal_setUpSalon($id) 
    {
        global $i;
        $fixedSlots = "157&168,157&168,157&168,157&168,157&168,157&168,157&168";  //for tech_break_slots and tech_break_time 
        $salon = DB::table('salon')->where('sal_id', '=', $id)
                        ->first();
            
        $sal_hours = $salon->sal_hours;
        
        $isValidHours = $this->is_valid_weekly_time($sal_hours);

        $tech_slots = ""; //searilized array of slots
        $tech_break_slots = $fixedSlots;
        $tech_work_time = $salon->sal_hours;
        $tech_break_time = $fixedSlots;
        $tech_slots_new = $fixedSlots;
        $tech_weekly_offs = $salon->sal_weekly_offs; //same as salon weekly off;
        $tech_specialty = $salon->sal_specialty;
        if(empty($tech_specialty))
        {
            $tech_specialty = "";
        }
        $tech_work_slots = "";
        //same as salon specialty;
        if ($isValidHours) 
        {
            // echo "(if condition here )";
            $slots = $this->weekly_time_ranges_to_slots($sal_hours);
            $tech_work_slots = $slots;
            $rangeArray = $this->weekly_ranges_to_array($slots, $fixedSlots);
            $tech_slots = serialize($rangeArray);

            // dd($tech_work_slots);
            // dd("in if condition here");
        } 
        else 
        {
            
            return 'false';   
        }
      
        $salon_services = DB::table('salon_services')
                ->where('sal_id', '=', $id)
                ->where('sser_rate', '>', 0)
                // ->where('sser_time', '>', 0)
                // ->where('sser_enabled', '=', 1)
                ->get();
        $total_salon_services = count($salon_services);
        // dd($total_salon_services);
        if (count($salon_services) < 1) 
        {
            return true;
        }
        //check if sal_hours n salon table they must be valid weekly hours,
        //insert into technicians if there is no tech
        // dd(' tech', $technicians);
        $result = DB::select('select * from tech_services where tech_id in (select tech_id from technicians WHERE sal_id= ' . $id . ' )');
        $totalTechServies = count($result);
        $techId = '';
        $technicians = DB::table('technicians')
                ->where('tech_name','Manager')
                ->where('sal_id', '=', $id)
                ->where('tech_is_active', '=', 1)
                ->get();
        

        $InsertedTechIdId = 0;
        if(count($technicians) < 1) 
        {
            // dd('There is no tech exist');
            //here insert into tech table 
            $InsertedTechIdId = DB::table('technicians')->insertGetId(
                    array('tech_name' => 'Manager', 'sal_id' => $id, 'tech_pic' => '', 'tech_pic' => 'default.png'
                        , 'tech_slots' => $tech_slots, 'tech_phone' => '',
                        'tech_bio' => '',
                        'tech_specialty' => $tech_specialty, 'tech_weekly_offs' => $tech_weekly_offs,
                        'tech_work_slots' => $tech_work_slots, 'tech_break_slots' => $tech_break_slots, 'tech_work_time' => $tech_work_time
                        , 'tech_break_time' => $tech_break_time
                        , 'tech_slots_new' => $tech_slots_new, 'tech_is_active' => 1
            ));

            // dd('technicians ',$InsertedTechIdId);
            $techId = $InsertedTechIdId;
        }
        else 
        {
            $techId = $technicians[0]->tech_id;
            // dd($id);
            // dd("work slot",$tech_work_slots," break slot",$tech_break_slots);
            $updat = DB::table("technicians")->where("tech_name","Manager")
                                             ->where("sal_id",$id)
                                             ->update(array(
                                                'tech_work_slots'   => $tech_work_slots,
                                                'tech_break_slots'  => $tech_break_slots,

                                                'tech_slots'        => $tech_slots, 
                                                'tech_specialty'    => $tech_specialty, 
                                                'tech_weekly_offs'  => $tech_weekly_offs,
                                                'tech_work_time'    => $tech_work_time, 
                                                'tech_break_time'   => $tech_break_time,
                                                'tech_slots_new'    => $tech_slots_new, 
                                                'tech_is_active'    => 1
                                             ));
                        // dd($id,$updat);
            $InsertedTechIdId = $techId;
        }
        if ($totalTechServies < 1) 
        {
            $result1 = DB::statement("INSERT INTO tech_services (tech_id,tser_name, tser_order, sser_id) 
             SELECT " . $InsertedTechIdId . ", sser_name, sser_order, sser_id FROM salon_services where sal_id=  " . $id . ""
                            . " and sser_rate > 0 and sser_time > 0 and sser_enabled = 1 ON DUPLICATE KEY UPDATE tech_id = 
                      tech_id ");
        }

        //salon info here 
        $result = DB::table('salon')
                ->Where('sal_id', $id)
                ->update(['sal_status' => 0, 'sal_temp_enabled' => 1,
            'is_active' => 1,
            'sal_appointment_interval' => 30
        ]);

        // dd($result);        
        
    }

    public function updateServiceCat(Request $request)
    {
        $page_title = " Service Category ";
        $returnback = $request->returnback;
        $sal_id = $request->sal_id;

        DB::table("salon")->where("sal_id",$sal_id)->update(array("sal_modify_datetime"=>Carbon::now()->toDateTimeString()));
        $sscid = $request->ssc_id;
         
            $what_to_insert = array();
        if(!empty($sscid))
        {
            foreach ($sscid as $key => $value) 
            {
                array_push($what_to_insert, "('".$sal_id."','".$value."')");
            }
        }

        if (!empty($what_to_insert) AND count($what_to_insert)>0)
        {
            $import= DB::statement("INSERT into sal_ser_sub_categories(sal_id,ssc_id) values " . implode(",", $what_to_insert)." ON DUPLICATE KEY UPDATE sal_id = sal_id");
        }
        $sscids = 0;
        if(!empty($request->ssc_id)){
            $sscids = implode(",", $request->ssc_id);
        }
         DB::statement("DELETE FROM sal_ser_sub_categories WHERE sal_id=$sal_id AND ssc_id NOT in ($sscids)");
        Session::put("editsalon","service-category");
        Session::flash("message","Service category updated successfully ");
        $notification = array(
                'success' => 'Service category updated successfully!', 
                'alert-type' => 'success'
            );
        return back()->with($notification);

    }

    public function temp_salon_file(Request $request)
    {
        
        $salon_data = '';
            $page_title = "File Upload ";
            //temp
         $cat_data = array();
        $cat_data['sc_name'] = $request->input('cat_name');
        //cat_image=salon_service_file
        //temp
        if (Input::hasFile('cat_image')) 
        {
             // dd('here');
            $path = $request->file('cat_name');
            //dd($path);
            $cat_data['sc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
            $request->cat_image->move(public_path('category_images'), $cat_data['sc_image']);
            // dd($cat_data);
        } 
        else 
        {
            dd('Image not found');
            $cat_data['sc_image'] = '';
        }

   // dd("end");
    $i = 0;
    $salon_linenum = '';
        $file_name = $cat_data['sc_image'];
        define('CSV_PATH', 'category_images/');
        $csv_file = CSV_PATH . $file_name;

        if (($handle = fopen($csv_file, "r")) !== FALSE) {
               $data = DB::table("temp_salon_sa_import")->truncate();
            fgetcsv($handle);
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                // dd($data);
                $num = count($data);
                for ($c = 0; $c < $num; $c++) {
                    $col[$c] = $data[$c];
                }
                
                if(count($col) == 19 )
                {
                    $sal_id         = $col[0];
                    $monday         = $col[1]; 
                    $tuesday        = $col[2];
                    $wednesday      = $col[3];
                    $thursday       = $col[4];
                    $friday         = $col[5];
                    $saturday       = $col[6];
                    $sunday         = $col[7];
                    $sal_phone      = $col[8];
                    $sal_pic        = $col[9];
                    $sal_website    = $col[10];
                    $sal_facebook   = $col[11];
                    $sal_twitter    = $col[12];
                    $sal_instagram  = $col[13];
                    $sty_id         = $col[14];
                    $sal_address    = $col[15];
                    $sal_name       = $col[16];
                    $sal_city       = $col[17];
                    $sal_email      = $col[18];


                    $sal_hours = str_replace("-", "&", trim($col[1]) . "," . trim($col[2]) . "," . trim($col[3]) . "," . trim($col[4]) . "," . trim($col[5]) . "," . trim($col[6]) . "," . trim($col[7]) );
                    $sal_hours = str_replace('–','&', $sal_hours);
                    // dd($sal_hours);

                    // $getHours       = $this->get_hours($sal_hours);   
                    $isValid_hours = $this->is_valid_weekly_time($sal_hours);
                    if($isValid_hours == false)
                    {
                       $salon_data = $salon_data.','.$sal_id;
                         $i += 1;
                        $salon_linenum = $salon_linenum.','.$i;
                        Session::flash('new_error_message', 'Some errors please check in salon opening hours ');
                    }
                    try 
                    {


                        // echo '$sal_hours' . $sal_hours . '--------'."<br>";
                        $result = DB::statement("INSERT INTO temp_salon_sa_import (temp_sal_id, sal_hours, sal_phone,  sal_facebook,  sal_twitter, sal_instagram, sty_id,sal_pic,monday,tuesday,wednesday,thursday,friday,saturday,sunday,sal_address,sal_name,sal_city,sal_email,sal_website) VALUES "
                                        . "('" . $sal_id . "','" . $sal_hours . "','" . $sal_phone . "','" . $sal_facebook . "','" . $sal_twitter . "','" . $sal_instagram . "','" . $sty_id ."','".$sal_pic."','".$col[1]."','".$col[2]."','".$col[3]."','".$col[4]."','".$col[5]."','".$col[6]."','".$col[7]."','".$sal_address."','".$col[16]."','".$col[17]."','".$col[18]."','".$sal_website."')");
                        // echo 'If you see this, the number is 1 or below';
                       
                    }
                    //catch exception
                    catch (Exception $e) {
                        dd('Exception');
                        //  echo 'Message: ' . $e->getMessage();
                    }

                }
                elseif(count($col) == 9)
                {
                    $sal_id         = $col[0];
                    $sal_hours      = $col[1]; 
                    $sal_phone      = $col[2];
                    $sal_facebook   = $col[3];
                    $sal_twitter    = $col[4];
                    $sal_instagram  = $col[5];
                    $sty_id         = $col[6];
                    $sal_pic        = $col[7];
                    $sal_address    = $col[8];

                    $getHours       = $this->get_hours($sal_hours);  
                    // dd($getHours); 
                    $isValid_hours = $this->is_valid_weekly_time($getHours);
                    if($isValid_hours == false)
                    {
                       $salon_data = $salon_data.','.$sal_id;
                         $i += 1;
                        $salon_linenum = $salon_linenum.','.$i;
                        Session::flash('new_salon_error_message', 'Some errors please check in salon opening hours ');
                    }
                    try 
                    {
                        // echo '$sal_hours' . $sal_hours . '--------'."<br>";
                        $result = DB::statement("INSERT INTO temp_salon_sa_import (temp_sal_id, sal_hours, sal_phone,  sal_facebook,  sal_twitter, sal_instagram, sty_id,sal_pic,sal_address,sal_name,sal_city,sal_email) VALUES "
                                        . "('" . $sal_id . "','" . $getHours . "','" . $sal_phone . "','" . $sal_facebook . "','" . $sal_twitter . "','" . $sal_instagram . "','" . $sty_id ."','".$sal_pic."','".$sal_address ."','".$col[16]."','".$col[17]."','".$col[18]."') ");
                        // echo 'If you see this, the number is 1 or below';
                    }
                    //catch exception
                    catch (Exception $e) {
                        dd('Exception');
                        //  echo 'Message: ' . $e->getMessage();
                    }
                }
                else
                {
                    Session::flash("new_error_message","CSV file is not valid, please try again upload the correct CSV file ");
                    return back();
                }
            }
             
            fclose($handle);
        }
            $userid = \Auth::user()->id;
        
            // dd("ok");

       $temp =  DB::statement("INSERT INTO salon(sal_hours,temp_sal_id, sal_facebook, sal_instagram, sal_zip, sal_twitter, sal_address, sal_pic, sal_modify_datetime, sal_appointment_interval,sal_phone,sal_name,sal_city,sal_email,sal_profile_pic,sal_created_datetime,sal_status,admin_device_id,admin_device_type,sal_lat,sal_lng,sal_timing,sal_nearby_zip,sal_search_words,sal_biography,sal_weekly_offs,sty_id,sal_services,sal_review_datetime,sal_contact_person,sal_website,forgot_pass_identity) SELECT sal_hours,temp_sal_id, sal_facebook, sal_instagram, sty_id, sal_twitter, sal_address,sal_pic,'".\Carbon\Carbon::now()->toDateTimeString()."','30',sal_phone,sal_name,sal_city,sal_email,'','".\Carbon\Carbon::now()->toDateTimeString()."',0,'','','','','','','','','',0,'','".\Carbon\Carbon::now()->toDateTimeString()."','',sal_website,'' from temp_salon_sa_import  ON DUPLICATE KEY UPDATE salon.temp_sal_id = salon.temp_sal_id");

           
// dd("ok");
       
            $salonid = DB::table("salon_sa_import")->get();
            $salon_id = '';
            foreach ($salonid as $key => $value) {
                $salon_id = $salon_id.','.$value->sal_id;
            }
            $salonsid = trim($salon_id,",");
            // dd($salonsid);
            $results = DB::statement("INSERT INTO salon_data_upload (user_id,type,sal_id,created_at)
                                        values('".$userid."','"."salon"."','".$salonsid."','".\Carbon\Carbon::now()->toDateTimeString()."')");
            DB::statement("update salon s JOIN postcodelatlng pc on s.sal_zip = pc.postcode set s.sal_lat = pc.latitude, s.sal_lng = pc.longitude where s.sal_lat = '' or s.sal_lng = '' ");

           Session::flash('new_salon_flash_message', 'Salon details successfully added.');

        Session::put("activemenu","temp_save_file");
        // return back();

        return view('service-mgmt/fileupload',compact('page_title','salon_data','salon_linenum'));
    }
    public function tem_salon_service_file(Request $request)
    {
        // header('Content-Type: text/html; charset=UTF-8');
        // dd("services ");

        $saldata = '';
        $linenum = '';

        $page_title = " File Upload ";
        $cat_data = array();
        $cat_data['sc_name'] = $request->input('cat_name');
        //cat_image=salon_service_file
        if (Input::hasFile('cat_image')) 
        {
            $cat_data['sc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
            $request->cat_image->move(public_path('category_images'), $cat_data['sc_image']);
            // dd($cat_data['sc_image']);
        } 
        else 
        {
            $cat_data['sc_image'] = '';
        }
        // dd($cat_data['sc_image']);
        $file_name = $cat_data['sc_image'];
        define('CSV_PATH', 'category_images/');
        $csv_file2 = CSV_PATH . $file_name;

        DB::statement("DELETE FROM temp_salon_service_sa_import");

        $pdo = DB::connection()->getPdo();
        $exfile = $pdo->exec("LOAD DATA LOCAL INFILE '".$csv_file2."' INTO TABLE temp_salon_service_sa_import FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n'");
    
        DB::statement("UPDATE temp_salon_service_sa_import set 
            sser_rate = REPLACE(sser_rate,'£',''), sser_name = substring(sser_name,1,245)");
        

            $result = DB::statement("UPDATE `temp_salon_service_sa_import` sss INNER JOIN services s on s.ser_name = substring(sss.sser_name,1,245) SET sss.ser_id = s.ser_id WHERE sss.ser_id = 0");

            // dd($result);
            //all salon_services_sa_import with zero id insert into service and again update salon_services_sa_import
            $result1 = DB::statement("INSERT INTO services (ser_name)
             SELECT DISTINCT substring(sser_name,1,245) FROM temp_salon_service_sa_import ON DUPLICATE KEY UPDATE services.ser_id = services.ser_id");
           // dd($result1, 'till now as');
            $results = DB::statement("UPDATE `temp_salon_service_sa_import` sss INNER JOIN services s on s.ser_name = sss.sser_name SET sss.ser_id = s.ser_id WHERE sss.ser_id = 0");
            // dd("services table ");
            
            // dd("salon_service category");
            //all salon_services_sa_import with zero id insert into service and again update salon_services_sa_import

            $result = DB::statement("UPDATE `temp_salon_service_sa_import` sss INNER JOIN salon s on s.temp_sal_id = sss.temp_sal_id SET sss.sal_id = s.sal_id");
            // dd($result);

            $result1 = DB::statement("INSERT INTO salon_ser_categories (ssc_name, sal_id)
             SELECT  distinct ssc_name, sal_id FROM temp_salon_service_sa_import as s_imp WHERE ssc_id=0 ON DUPLICATE KEY UPDATE salon_ser_categories.ssc_name = salon_ser_categories.ssc_name ");
           
           $result = DB::statement("UPDATE `temp_salon_service_sa_import` sss INNER JOIN salon_ser_categories s on s.ssc_name = sss.ssc_name and s.sal_id = sss.sal_id SET sss.ssc_id = s.ssc_id WHERE sss.ssc_id = 0");
       // dd($result1);
        $result1 = DB::statement("INSERT INTO salon_services (ser_id,sser_name,sal_id, sser_rate, sser_time, ssc_id)
        SELECT ser_id, substring(sser_name,1,245),sal_id,sser_rate, sser_time, ssc_id FROM temp_salon_service_sa_import  WHERE ser_id<>0
                ON DUPLICATE KEY UPDATE salon_services.ssc_id = salon_services.ssc_id");

        $salonasiport = DB::table("temp_salon_service_sa_import")->DISTINCT('sal_id','sser_id')->get();

       $i = 0;
    $pres_id = 0;

    $salids  = '';
    $sserid = '';
        foreach($salonasiport as $salon_s)
        { 

            $salids  = $salids.','.$salon_s->sal_id;
            $sserid = $sserid.','.$salon_s->sser_id;

            if($salon_s->sal_id != $pres_id)
            {
                $pres_id = $salon_s->sal_id;
                $tech = $this->recal_setUpSalon($salon_s->sal_id);
                
                if($tech == 'false')
                {   
                    $i += 1; 
                    $saldata = $saldata.','.$salon_s->sal_id;
                    $linenum = $linenum.','.$i;
                    Session::flash('new_service_error_message', 'Some errors please check in salon opening hours ');
                }
            }

        }

        $user_ids = \Auth::User()->id;

        $results = DB::statement("INSERT INTO salon_data_upload (user_id,type,sal_id,sal_service_id,created_at) VALUES('".$user_ids."','"."salon-service"."','".$salids."','".$sserid."','".\Carbon\Carbon::now()->toDateTimeString()."') ");

        Session::flash('new_service_flash_message', 'Salon Service successfully added.');
        Session::put('activemenu','temp_save_file_services');   
        return view('service-mgmt/fileupload',compact('page_title','saldata','linenum'));
        //end services import
        //edn save data
        //for saving file to server side
    }

    public function advancesearch(Request $request)
    {
        $page_title = "Posts";
        // SELECT count(*), pp.ssc_id, ssc_name, date(p_fetch_date) FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '2018-05-26' and '2018-06-01' group by 2,3,4
        $startDate = $request->startDate;
        $endDate  = $request->endDate;
        $sscs = $request->ssc;
        $p_status = $request->p_status;

        $data = DB::table("pinterest_pins")->leftJoin("pinterest_boards","pinterest_boards.pb_id","=","pinterest_pins.pb_id");
        
        if(empty($startDate) && empty($endDate) && $sscs == 'all')
        {
            $d = date('Y-m-d',strtotime("-1 days"));
            $data = $data->where('p_fetch_date',$d);
        }
        if(empty($startDate) && empty($endDate) && is_null($sscs))
        {
            $d = date('Y-m-d',strtotime("-1 days"));
            $data = $data->where('p_fetch_date',$d);

        }
        $pins = $data->paginate(10); 
        
        $current = Carbon::now();
        $endDate  = date('Y-m-d',strtotime($current));
        $yesterday = Carbon::yesterday();
        $startDate = date('Y-m-d',strtotime($yesterday));

        $sscs = 'all';
        $p_status = 'all';
        
        $pin = DB::statement("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '2018-05-26' and '2018-06-01' group by 2,3,4 order by count DESC");

        $data = DB::table("pinterest_pins")->leftJoin("service_sub_categories","service_sub_categories.ssc_id","=","pinterest_pins.ssc_id");
        if(!empty($startDate))
        {
            $startDate = date('Y-m-d',strtotime($startDate));
            $data = $data->where('p_fetch_date','>',$startDate);
        }
        if(!empty($endDate))
        {
            $endDate = date('Y-m-d',strtotime($endDate));
            $data = $data->where('p_fetch_date','<',$endDate);
        }
        if(!empty($sscs))
        {
            if($sscs == 'all')
            {

            }
            else
            {
                $data = $data->where('pp.ssc_id',$sscs);
            }
        }
        if(empty($startDate) && empty($endDate) && $sscs == 'all')
        {
            $d = date('Y-m-d',strtotime("-1 days"));
            $data = $data->where('p_fetch_date',$d);
        }
        if(empty($startDate) && empty($endDate) && is_null($sscs))
        {
            $d = date('Y-m-d',strtotime("-1 days"));
            $data = $data->where('p_fetch_date',$d);
        }

        if($sscs=='all' OR is_null($sscs) AND $p_status=='all' OR is_null($p_status))
        {
            $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC");
        }
        if(!is_null($sscs))
        {
            if($sscs=='all')
            {
                $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC");
            }
            else
            {
                $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = '$sscs' and ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC");   
            }
        }
        if(!is_null($p_status) AND $p_status!='all' )
        {
            if($p_status=='1')
            {
                if($sscs=='all' OR is_null($sscs))
                {
                    $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where pp.p_status = '1' and ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC"); 
                }
                else
                {
                    $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = '$sscs' and pp.p_status = '1' and ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC"); 
                }
            }
            else
            {
                if($sscs=='all' OR is_null($sscs))
                {
                    $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where pp.p_status = '0' and ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC");    
                }
                else
                {
                    $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = '$sscs' and pp.p_status = '0' and ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC"); 
                }
            }
        }

        return view("pinterestboard/advns_pinterest",compact("page_title","sscs",'pins','startDate','endDate','p_status'));
    }

    public function pins_search(Request $request)
    {
        $page_title = "Post Category";
        
        $startDate = $request->startDate;
        $endDate  = $request->endDate;
        $sscs = $request->ssc;
        $p_status = $request->p_status;
        
        $pin = DB::statement("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '2018-05-26' and '2018-06-01' group by 2,3,4 order by count DESC");

        $data = DB::table("pinterest_pins")->leftJoin("service_sub_categories","service_sub_categories.ssc_id","=","pinterest_pins.ssc_id");
        if(!empty($startDate))
        {
            $startDate = date('Y-m-d',strtotime($startDate));
            $data = $data->where('p_fetch_date','>',$startDate);
        }
        if(!empty($endDate))
        {
            $endDate = date('Y-m-d',strtotime($endDate));
            $data = $data->where('p_fetch_date','<',$endDate);
        }
        if(!empty($sscs))
        {
            if($sscs == 'all')
            {

            }
            else
            {
                $data = $data->where('pp.ssc_id',$sscs);
            }
        }
        if(empty($startDate) && empty($endDate) && $sscs == 'all')
        {
            $d = date('Y-m-d',strtotime("-1 days"));
            $data = $data->where('p_fetch_date',$d);
        }
        if(empty($startDate) && empty($endDate) && is_null($sscs))
        {
            $d = date('Y-m-d',strtotime("-1 days"));
            $data = $data->where('p_fetch_date',$d);
        }

        if($sscs=='all' OR is_null($sscs) AND $p_status=='all' OR is_null($p_status))
        {
            $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC");
        }
        if(!is_null($sscs))
        {
            if($sscs=='all')
            {
                $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC");
            }
            else
            {
                $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = '$sscs' and ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC");   
            }
        }
        if(!is_null($p_status) AND $p_status!='all' )
        {
            if($p_status=='1')
            {
                if($sscs=='all' OR is_null($sscs))
                {
                    $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where pp.p_status = '1' and ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC"); 
                }
                else
                {
                    $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = '$sscs' and pp.p_status = '1' and ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC"); 
                }
            }
            else
            {
                if($sscs=='all' OR is_null($sscs))
                {
                    $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where pp.p_status = '0' and ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC");    
                }
                else
                {
                    $pins =  DB::select("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date,p_status  FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = '$sscs' and pp.p_status = '0' and ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '$startDate' and '$endDate' group by 2,3,4,5 order by count DESC"); 
                }
            }
        }

        return view("pinterestboard/advns_pinterest",compact("page_title","sscs",'pins','startDate','endDate','p_status'));
    }
    public function postmanagement(Request $request)
    {

        $approved_post      = 0;
        $un_approved_post   = 0;
        $delete_post        = 0;
        $page_title = "Post Management";
            $ssc_id = $request->ssc_id;
                if(is_null($ssc_id))
                {
                    $ssc_id = $request->ssc_id;
                }
                else
                {
                    $ssc_id = $request->ssc_id;
                    Session::put("ssc_id",$ssc_id);
                }

            if(Session::has('ssc_id'))
             {
                $ssc_id = Session::get('ssc_id');
             }
            $p_status = $request->p_status;
            $orderby = $request->orderby;
             // if($p_status == null)
             // {
             //    $p_status = 'all';
             // }
             if($orderby == null)
             {
                $orderby = 'DESC';
             }
      
        if(is_null($ssc_id))
        {
            $sscid = DB::table("service_sub_categories")
                        ->where('ssc_status',1)
                        ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                        ->where('service_categories.sc_status',1)
                        ->orderby("ssc_name","ASC")->get()[0];
            $ssc_id = $sscid->ssc_id;
        }
        Session::put('activemenu','postmanagement'); 

        $ssc = DB::table("service_sub_categories")
                   ->where('ssc_status',1)
                   ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                   ->where('service_categories.sc_status',1)
                   ->orderBy('ssc_gender','ASC')
                   ->orderBy("ssc_name","ASC")->get();
        // dd($ssc);
        if(is_null($ssc_id) OR $ssc_id=='all' AND is_null($p_status) AND is_null($orderby))
        {
            $pins = DB::table("pinterest_pins")->where('pinterest_pins.ssc_id',3)
                        ->where('p_status','<>',2)
                        ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                        ->where('service_sub_categories.ssc_status',1)
                        ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                        ->where('service_categories.sc_status',1)
                        ->orderBy('p_setas_editor_img','DESC')->orderBy('update_date','DESC')->paginate(52);


            $approved_post      = DB::table("pinterest_pins")
                                      ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                                      ->where('service_sub_categories.ssc_status',1)
                                      ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                      ->where('service_categories.sc_status',1)
                                      ->where("p_status",1)->count();
            $un_approved_post   = DB::table("pinterest_pins")
                                      ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                                      ->where('service_sub_categories.ssc_status',1)
                                      ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                      ->where('service_categories.sc_status',1)
                                      ->where("p_status",0)->count();
            return view("pinterestboard/approvedpins",compact('un_approved_post','approved_post','page_title','ssc','pins','ssc_id','p_status','orderby'));
            
        }

        if($ssc_id == 'all' OR is_null($ssc_id))
        {

            if(is_null($p_status) OR $p_status == 'all')
            {
                $approved_post  = DB::table("pinterest_pins")
                                      ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                                      ->where('service_sub_categories.ssc_status',1)
                                      ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                      ->where('service_categories.sc_status',1)
                                      ->where("p_status",1)->count();
                $un_approved_post = DB::table("pinterest_pins")
                                        ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                                        ->where('service_sub_categories.ssc_status',1)
                                        ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                        ->where('service_categories.sc_status',1)
                                        ->where("p_status",0)->count();
                
                $pins = DB::table("pinterest_pins")->where('p_status','<>',2)
                            ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                            ->where('service_sub_categories.ssc_status',1)
                            ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                            ->where('service_categories.sc_status',1)
                            ->orderBy('p_setas_editor_img','DESC')
                            ->orderBy('update_date',$orderby)->paginate(52); 
            }
            else
            {
                if($p_status=='1')
                {
                    $approved_post  = DB::table("pinterest_pins")
                                          ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                                          ->where('service_sub_categories.ssc_status',1)
                                          ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                          ->where('service_categories.sc_status',1)
                                          ->where("p_status",1)->count();

                    $pins = DB::table("pinterest_pins")->where('p_status',$p_status)
                                ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                                ->where('service_sub_categories.ssc_status',1)
                                ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                ->where('service_categories.sc_status',1)
                                ->orderBy('p_setas_editor_img','DESC')->orderBy('update_date',$orderby)->paginate(52);
                }
                elseif($p_status=='0')
                {
                    $pins = DB::table("pinterest_pins")->where('p_status',$p_status)
                                ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                                ->where('service_sub_categories.ssc_status',1)
                                ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                ->where('service_categories.sc_status',1)
                                ->orderBy('p_setas_editor_img','DESC')->orderBy('update_date',$orderby)->paginate(52);
                    $un_approved_post = DB::table("pinterest_pins")
                                            ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                                            ->where('service_sub_categories.ssc_status',1)
                                            ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                            ->where('service_categories.sc_status',1)
                                            ->where("p_status",0)->count();
                }
                elseif($p_status=='2')
                {
                    $pins = DB::table("deleted_posts")->where('p_status',$p_status)
                                ->join('service_sub_categories','service_sub_categories.ssc_id','=','deleted_posts.ssc_id')
                                ->where('service_sub_categories.ssc_status',1)
                                ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                ->where('service_categories.sc_status',1)
                                ->orderBy('p_setas_editor_img','DESC')->orderBy('update_date',$orderby)->paginate(52);
                }
            }

            return view("pinterestboard/approvedpins",compact('un_approved_post','approved_post','page_title','ssc','pins','ssc_id','p_status','orderby'));
            
        }
        if(!empty($ssc_id) OR $ssc_id != 'all')
        {
            
           
            if(is_null($p_status) OR $p_status == '')
            {
                $un_approved_post = DB::table("pinterest_pins")
                                        ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                                        ->where('service_sub_categories.ssc_status',1)
                                        ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                        ->where('service_categories.sc_status',1)
                                        ->where("pinterest_pins.ssc_id",$ssc_id)
                                        ->where("p_status",0)->count();

                $approved_post  = DB::table("pinterest_pins")
                                      ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                                        ->where('service_sub_categories.ssc_status',1)
                                        ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                        ->where('service_categories.sc_status',1)
                                      ->where("pinterest_pins.ssc_id",$ssc_id)
                                      ->where("p_status",1)->count();

                $pins = DB::table("pinterest_pins")->where("pinterest_pins.ssc_id",$ssc_id)->where('p_status','<>',2)
                            ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                                        ->where('service_sub_categories.ssc_status',1)
                                        ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                        ->where('service_categories.sc_status',1)
                            ->orderBy('p_setas_editor_img','DESC')->orderBy('update_date',$orderby)->paginate(52);
            }
            else
            {
                if($p_status=='1')
                {
                    $approved_post  = DB::table("pinterest_pins")
                                        ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                        ->where('service_sub_categories.ssc_status',1)
                        ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                        ->where('service_categories.sc_status',1)
                                          ->where("pinterest_pins.ssc_id",$ssc_id)->where("p_status",1)->count();

                    $pins = DB::table("pinterest_pins")->where("pinterest_pins.ssc_id",$ssc_id)->where('p_status',$p_status)
                                ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                        ->where('service_sub_categories.ssc_status',1)
                        ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                        ->where('service_categories.sc_status',1)
                                ->orderBy('p_setas_editor_img','DESC')->orderBy('update_date',$orderby)->paginate(52);
                }
                elseif($p_status=='0')
                {
                    $un_approved_post = DB::table("pinterest_pins")
                                            ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                        ->where('service_sub_categories.ssc_status',1)
                        ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                        ->where('service_categories.sc_status',1)
                                            ->where("pinterest_pins.ssc_id",$ssc_id)->where("p_status",0)->count();

                    $pins = DB::table("pinterest_pins")->where("pinterest_pins.ssc_id",$ssc_id)->where('p_status',$p_status)
                                ->join('service_sub_categories','service_sub_categories.ssc_id','=','pinterest_pins.ssc_id')
                        ->where('service_sub_categories.ssc_status',1)
                        ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                        ->where('service_categories.sc_status',1)
                                ->orderBy('p_setas_editor_img','DESC')->orderBy('update_date',$orderby)->paginate(52);   
                }
                elseif($p_status=='2')
                {
                    $pins = DB::table("deleted_posts")->where("deleted_posts.ssc_id",$ssc_id)->where('p_status',$p_status)
                                ->join('service_sub_categories','service_sub_categories.ssc_id','=','deleted_posts.ssc_id')
                        ->where('service_sub_categories.ssc_status',1)
                        ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                        ->where('service_categories.sc_status',1)
                                ->orderBy('p_setas_editor_img','DESC')->orderBy('update_date',$orderby)->paginate(52); 
                }
            }

            return view("pinterestboard/approvedpins",compact('un_approved_post','approved_post','page_title','ssc','pins','ssc_id','p_status','orderby'));
            
        }

        
        return view("pinterestboard/approvedpins",compact('un_approved_post','approved_post','page_title','ssc','pins','ssc_id','p_status','orderby'));
    }

    public function approved(Request $request)
    {
        if(!is_null($request->approvedpins))
        {
            if($request->approved)
            {
                $pins = $request->approvedpins;
                $admin_id = \Auth::User()->id;

                $values = array_map('_', $pins);
                $pin = implode(',', $values);

                // $date = \Carbon\Carbon::now();

                // $update_date = \Carbon\Carbon::now()->toDateTimeString();
              
                // foreach($pins as $key => $pin)
                // {

                //     DB::table("pinterest_pins")->where("pp_id",$pin)->update(array(
                //         "p_status"      => "1",
                //         "update_date"   => $update_date,
                //         "admin_id"    => $admin_id,
                //     ));
                // }

                $result = DB::statement("UPDATE pinterest_pins SET update_date = NOW(), admin_id = $admin_id, p_status = 1
                WHERE pp_id in( $pin ) AND p_status = 0 ");


                $notification = array(
                    'success' => 'Post Record  Updated successfully!', 
                    'alert-type' => 'success'
                );
                \Session::flash('postapproved','Posts Approved successfully!');
                \Session::flash('alert-type', 'approved');
            }
            if($request->delete)
            {
                $pins = $request->approvedpins;
                 $values = array_map('_', $pins);
                $pin = implode(',', $values);
    
                DB::statement("UPDATE pinterest_pins SET p_status = 2
                WHERE pp_id in( $pin )");

                $result = DB::statement("INSERT INTO deleted_posts (pp_id,pp_creator, pp_datetime, pb_id, p_url, p_link,p_imageUrl,p_pin_id,p_metadata,p_keywords,p_fetch_date,ssc_id,p_status,update_date,admin_id,p_setas_editor_img)
                    SELECT pp_id,pp_creator, pp_datetime, pb_id, p_url, p_link,p_imageUrl,p_pin_id,p_metadata,p_keywords,p_fetch_date,ssc_id,p_status,update_date,admin_id,p_setas_editor_img FROM pinterest_pins  WHERE p_status = 2 and pp_id in($pin)
                    ON DUPLICATE KEY UPDATE deleted_posts.p_imageUrl = deleted_posts.p_imageUrl");

                DB::statement("DELETE FROM pinterest_pins
                WHERE p_status = 2 and pp_id in( $pin )");

                // dd($result);
                \Session::flash('postapproved','Posts Approved successfully!');
                \Session::flash('alert-type', 'postdelete');

                $notification = array(
                    'success' => 'Post Record  Deleted successfully!', 
                    'alert-type' => 'success'
                );
            }

            if($request->un_delete_unapp)
            {
                $pins = $request->approvedpins;
                 $values = array_map('_', $pins);
                $pin = implode(',', $values);
    
                DB::statement("UPDATE deleted_posts SET p_status = 0
                WHERE pp_id in( $pin )");

                $result = DB::statement("INSERT INTO pinterest_pins (pp_id,pp_creator, pp_datetime, pb_id, p_url, p_link,p_imageUrl,p_pin_id,p_metadata,p_keywords,p_fetch_date,ssc_id,p_status,update_date,admin_id,p_setas_editor_img)
                    SELECT pp_id,pp_creator, pp_datetime, pb_id, p_url, p_link,p_imageUrl,p_pin_id,p_metadata,p_keywords,p_fetch_date,ssc_id,p_status,update_date,admin_id,p_setas_editor_img FROM deleted_posts  WHERE p_status = 0 and pp_id in($pin)
                    ON DUPLICATE KEY UPDATE pinterest_pins.p_imageUrl = pinterest_pins.p_imageUrl");

                DB::statement("DELETE FROM deleted_posts
                WHERE p_status = 0 and pp_id in( $pin )");

                // dd($result);
                \Session::flash('postapproved','Posts Approved successfully!');
                \Session::flash('alert-type', 'postdelete');

                $notification = array(
                    'success' => 'Post Record  Un-Deleted with un-approved successfully!', 
                    'alert-type' => 'success'
                );
            }

            if($request->un_delete_app)
            {
                $pins = $request->approvedpins;
                 $values = array_map('_', $pins);
                $pin = implode(',', $values);
    
                DB::statement("UPDATE deleted_posts SET p_status = 1
                WHERE pp_id in( $pin )");

                $result = DB::statement("INSERT INTO pinterest_pins (pp_id,pp_creator, pp_datetime, pb_id, p_url, p_link,p_imageUrl,p_pin_id,p_metadata,p_keywords,p_fetch_date,ssc_id,p_status,update_date,admin_id,p_setas_editor_img)
                    SELECT pp_id,pp_creator, pp_datetime, pb_id, p_url, p_link,p_imageUrl,p_pin_id,p_metadata,p_keywords,p_fetch_date,ssc_id,p_status,update_date,admin_id,p_setas_editor_img FROM deleted_posts  WHERE p_status = 1 and pp_id in($pin)
                    ON DUPLICATE KEY UPDATE pinterest_pins.p_imageUrl = pinterest_pins.p_imageUrl");

                DB::statement("DELETE FROM deleted_posts
                WHERE p_status = 1 and pp_id in( $pin )");

                // dd($result);
                \Session::flash('postapproved','Posts Approved successfully!');
                \Session::flash('alert-type', 'postdelete');

                $notification = array(
                    'success' => 'Post Record  Un-Deleted with approved successfully!', 
                    'alert-type' => 'success'
                );
            }

        }

        if(is_null($request->approvedpins))
        {
            $notification = array(
                'success' => 'Please Select the Post!', 
                'alert-type' => 'success'
            );
        }
        
        return back()->with($notification);  
    }

    public function changeservice_category(Request $request)
    {
        $ssubc_id = $request->ssubc_id;
        $sser_id = $request->sser_id;
        $urls = \URL::previous();
        $sal_id = DB::table('salon_services')->where('sser_id' , $sser_id)->first()->sal_id;
        
        $feedback = DB::table('salon_services')->where('sser_id' , $sser_id)->update(array('ssubc_id' => $ssubc_id));
        
                    DB::table('salon')->where('sal_id' , $sal_id)->update(array('sal_modify_datetime'=>Carbon::now()->toDateTimeString()));
        if($feedback)
        {
            return '<div class="alert alert-success" id="me">
                             <p style="color: white;" class="text-success">
                                Service category updated successfully
                             </p>
                    </div>';
        }
        else
        {
            return '<div class="alert alert-danger" id="me">
                             <p style="color: white;" class="text-success">
                                Some occure errors in service category 
                             </p>
                    </div>';
        }
    }

    public function sser_enable($id)
    {
        $sal_id = DB::table('salon_services')->where('sser_id' , $id)->first()->sal_id;

        DB::table('salon_services')->where('sser_id' , $id)->update(array('sser_enabled' => 1));
        DB::table('salon')->where('sal_id' , $sal_id)->update(array('sal_modify_datetime'=>Carbon::now()->toDateTimeString()));

        $notification = array(
                'success' => 'Salon service enable successfully!', 
                'alert-type' => 'success'
            );
        Session::put('editsalon','services_enable'); 
        return back()->with($notification);
    }
    public function sser_disable($id)
    {
        $sal_id = DB::table('salon_services')->where('sser_id' , $id)->first()->sal_id;

        DB::table('salon_services')->where('sser_id' , $id)->update(array('sser_enabled' => 0));
        DB::table('salon')->where('sal_id' , $sal_id)->update(array('sal_modify_datetime'=>Carbon::now()->toDateTimeString()));
        $notification = array(
                'success' => 'Salon services disable successfully!', 
                'alert-type' => 'success'
            );
        Session::put('editsalon','services_enable'); 
        return back()->with($notification);
    }

    public function sal_status_enable($id)
    {
        DB::table("salon")->where("sal_id",$id)->update(array('sal_status'=>1,'sal_modify_datetime'=>Carbon::now()->toDateTimeString()));
        
        $notification = array(
                'success' => 'Salon status successfully active!', 
                'alert-type' => 'success'
            );
        return back()->with($notification);

    }
    public function sal_status_disable($id)
    {
        DB::table("salon")->where("sal_id",$id)->update(array('sal_status'=>0,'sal_modify_datetime'=>Carbon::now()->toDateTimeString()));
        $notification = array(
                'success' => 'Salon status successfully in-active!', 
                'alert-type' => 'success'
            );
        return back()->with($notification);
    }

    public function appointment(Request $request)
    {
        $page_title = "Appointments ";
        // dd($request->startDate);
        $current = Carbon::now();
        $endDate  = date('Y-m-d',strtotime($current));
        $yesterday = Carbon::yesterday();
        $startDate = date('Y-m-d',strtotime($yesterday));
        $ara_follow_date = null;

        // $startDate  = '2018/06/01';
        // $endDate    = null;
        $sal_id = null;
        // $app_request =  DB::select("SELECT s.sal_name, s.sal_email,ar_datetime, s.sal_phone, IFNULL((select ara_datetime from app_requests_action ara where ara.ar_id = ar.ar_id  order by ara_id desc limit 0,1),ar_datetime) as ara_datetime, IFNULL((select ara_status from app_requests_action ara where ara.ar_id = ar.ar_id order by ara_id desc limit 0,1),ar_status) as ar_status, IFNULL((select ara_follow_date from app_requests_action ara where ara.ar_id = ar.ar_id order by ara_id desc limit 0,1),'') as ara_follow_date, ar_id FROM `app_requests` ar, salon s where s.sal_id = ar.sal_id and date(ar_datetime) BETWEEN '$startDate' and '$endDate' order by ar_datetime DESC ");
// INNER JOIN customers c ON c.cust_id = app.cust_id
        $app_request =  DB::select("SELECT c.cust_name, app_status, s.sal_name, s.sal_email,app_created, s.sal_phone, app_last_modified, app_id FROM `appointments` app inner join salon s on s.sal_id = app.sal_id INNER JOIN customers c ON c.cust_id = app.cust_id where s.sal_id = app.sal_id and date(app_created) BETWEEN '$startDate' and '$endDate' order by app.app_created DESC ");
        $salons = DB::table('salon')->select('sal_id','sal_name','sal_email')->orderBy('sal_name','ASC')->get();
        $app_status = null;
        // dd($salons);
// dd($app_request);
        Session::put('activemenu','appointment_request'); 
        return view("pinterestboard/appoint-requests",compact('app_status','sal_id','salons','ara_follow_date','app_request','startDate','endDate','page_title'));
    }

    public function search_app_req(Request $request)
    {
        $startDate      = $request->startDate;
        $endDate        = $request->endDate;
        $ara_follow_date = $request->ara_follow_date;
        $app_status = $request->app_status;
        // dd($app_status);
        $sal_id = $request->sal_id;
        if( $sal_id !== null AND $sal_id !== '' AND $app_status !== null AND $app_status !== ''){

            // dd('sal_empty');
            $app_request =  DB::select("SELECT c.cust_name,app_status, s.sal_name, s.sal_email,app_created, s.sal_phone, app_last_modified, app_id FROM `appointments` app inner join salon s on s.sal_id = app.sal_id INNER JOIN customers c ON c.cust_id = app.cust_id where s.sal_id = app.sal_id and date(app_created) BETWEEN '$startDate' and '$endDate' and app.sal_id = '$sal_id' and app.app_status = '$app_status' order by app.app_created DESC ");

        }elseif($sal_id !== null AND $sal_id !== '' AND $app_status == null AND $app_status == ''){
            // dd('sal_');
            $app_request =  DB::select("SELECT c.cust_name,app_status, s.sal_name, s.sal_email,app_created, s.sal_phone, app_last_modified, app_id FROM `appointments` app inner join salon s on s.sal_id = app.sal_id INNER JOIN customers c ON c.cust_id = app.cust_id where s.sal_id = app.sal_id and date(app_created) BETWEEN '$startDate' and '$endDate' and app.sal_id = '$sal_id' order by app.app_created DESC ");

        }elseif($sal_id == null AND $sal_id == '' AND $app_status !== null AND $app_status !== ''){
            // dd('sal_empty');
            $app_request =  DB::select("SELECT c.cust_name,app_status, s.sal_name, s.sal_email,app_created, s.sal_phone, app_last_modified, app_id FROM `appointments` app inner join salon s on s.sal_id = app.sal_id INNER JOIN customers c ON c.cust_id = app.cust_id where s.sal_id = app.sal_id and date(app_created) BETWEEN '$startDate' and '$endDate' and app.app_status = '$app_status' order by app.app_created DESC ");

        }else{
            // dd('sal_emelse ');
            $app_request =  DB::select("SELECT c.cust_name,app_status, s.sal_name, s.sal_email,app_created, s.sal_phone, app_last_modified, app_id FROM `appointments` app inner join salon s on s.sal_id = app.sal_id INNER JOIN customers c ON c.cust_id = app.cust_id where s.sal_id = app.sal_id and date(app_created) BETWEEN '$startDate' and '$endDate' order by app.app_created DESC ");
        }
        // dd($ara_follow_date);

        // $app_request =  DB::select("SELECT s.sal_name, ar_datetime, s.sal_phone, IFNULL((select ara_datetime from app_requests_action ara where ara.ar_id = ar.ar_id  order by ara_id desc limit 0,1),ar_datetime) as ara_datetime, IFNULL((select ara_status from app_requests_action ara where ara.ar_id = ar.ar_id order by ara_id desc limit 0,1),ar_status) as ar_status, IFNULL((select ara_follow_date from app_requests_action ara where ara.ar_id = ar.ar_id order by ara_id desc limit 0,1),'') as ara_follow_date, ar_id, (SELECT status from mailchimp_email_status as mes where mes.email_address = ar.ar_email )as mc_status, ar.ar_email FROM `app_requests` ar, salon s where s.sal_id = ar.sal_id and date(ar_datetime) BETWEEN '$startDate' and '$endDate' order by ar_datetime DESC ");

        // $app_request =  DB::select("SELECT s.sal_name, app_created, s.sal_phone, s.sal_email FROM `appointments` app, salon s where s.sal_id = app.sal_id and date(app_created) BETWEEN '$startDate' and '$endDate' order by app_created DESC ");

        
        // dd($app_request);
        $salons = DB::table('salon')->orderBy('sal_name','ASC')->get();
        // dd($salons);
        $page_title = "Appointments";
        return view("pinterestboard/appoint-requests",compact('app_status','sal_id','salons','ara_follow_date','app_request','startDate','endDate','page_title'));
        dd($app_request);
    }
    public function tem_enabled($id)
    {
        DB::table('salon')->where('sal_id',$id)->update(array('sal_temp_enabled'=>0,'sal_modify_datetime'=>Carbon::now()->toDateTimeString()));
        $notification = array(
                'success' => 'Salon temp-disabled successfully active!', 
                'alert-type' => 'success'
            );
        return back()->with($notification);
      
    }
    public function featured_enable($id)
    {
        $sal_id = DB::table('salon_services')->where('sser_id',$id)->first()->sal_id;

        DB::table('salon_services')->where('sser_id',$id)->update(array('sser_featured'=>1));
        DB::table("salon")->where("sal_id",$sal_id)->update(array('sal_modify_datetime'=>Carbon::now()->toDateTimeString()));
        $notification = array(
                'success' => 'Salon featured successfully active!', 
                'alert-type' => 'success'
            );
        return back()->with($notification);
    }
    public function featured_disable($id)
    {
        $sal_id = DB::table('salon_services')->where('sser_id',$id)->first()->sal_id;

        DB::table('salon_services')->where('sser_id',$id)->update(array('sser_featured'=>0));

        DB::table("salon")->where("sal_id",$sal_id)->update(array('sal_modify_datetime'=>Carbon::now()->toDateTimeString()));
        $notification = array(
                'success' => 'Salon featured successfully in-active!', 
                'alert-type' => 'success'
            );
        return back()->with($notification);
   
    }

    public function appoint_request_detail($id)
    {
        $page_title = "Appointment Details:";

        // $ar_details =  DB::select("SELECT s.sal_name, s.sal_email,ar_datetime, s.sal_phone, IFNULL((select ara_datetime from app_requests_action ara where ara.ar_id = ar.ar_id  order by ara_id desc limit 0,1),ar_datetime) as ara_datetime, IFNULL((select ara_status from app_requests_action ara where ara.ar_id = ar.ar_id order by ara_id desc limit 0,1),ar_status) as ar_status, ar_id, c.cust_name FROM `app_requests` ar, salon s, customers c where s.sal_id = ar.sal_id and c.cust_id = ar.cust_id and ar.ar_id = '$id' LIMIT 1 ");
        
        $ar_details =  DB::select("SELECT s.sal_name, s.sal_email,app_created, s.sal_phone, app_id, c.cust_name FROM `appointments` app, salon s, customers c where s.sal_id = app.sal_id and c.cust_id = app.cust_id and app.app_id = '$id' LIMIT 1 ");

        $conversations = DB::table("app_requests_action")->where("ar_id",$id)->get();
        // $conversations = DB::table("appointments")->where("app_id",$id)->get();
        $app_id = $id;
        // dd(count($conversations));
        return view("pinterestboard/appoint-request-detail",compact('app_id','conversations','ar_details','page_title'));
    }

    public function store_conversation(Request $request,$id)
    {
        $apar = new app_requests_action();

        $apar->admin_id         = \Auth::user()->id;
        $apar->ara_talkwith     = $request->ara_talkwith;
        $apar->ara_status       = $request->ara_status;
        if(!empty($request->ara_follow_date))
        {
            $apar->ara_follow_date  = date('Y-m-d',strtotime($request->ara_follow_date));
        }
        else
        {
            // $apar->ara_follow_date = 0;
        }

        $apar->ara_comments     = $request->ara_comments;
        $apar->ar_id            = $id;
        $apar->ara_datetime     = \Carbon\Carbon::now()->toDateTimeString();

        $apar->save();

        $notification = array(
                'success' => 'conversation added successfully!', 
                'alert-type' => 'success'
            );
        
        return back()->with($notification);
        // return back()->with();

    }

    public function setas_sub_category_image($id)
    {
        $helper = new Helper();
        if($id != "undefined")
        {
            $pp = DB::table("pinterest_pins")->where("pp_id",$id)->first();
            $ssc_image = DB::table("service_sub_categories")->where("ssc_id",$pp->ssc_id)->first()->ssc_image;
            if(!empty($ssc_image))
            {
               
                        $filename = $helper->cate_images()."/".$ssc_image;
                   

                 // $filename = public_path()."/category_images//".$ssc_image;
                
                        if (file_exists($filename)) 
                        {   
                            \File::delete($filename);                         
                        }
            }
            
            $url = $pp->p_imageUrl;       
            $contents = file_get_contents($url);
            $name = substr($url, strrpos($url, '/') + 1);
            \Storage::put($name, $contents);

            $image = storage_path().'/'.'app'.'/'.$name;

          
                        $destination    = $helper->cate_images();
                   
            // $destination= public_path("category_images/");

            $img_name=basename($image);

            if( rename( $image , $destination.'/'.$img_name )){
             
            } 

            DB::table("service_sub_categories")->where("ssc_id",$pp->ssc_id)->update(array("ssc_image"=>$name));
            $ssc = DB::table("service_sub_categories")->where("ssc_id",$pp->ssc_id)->first();
        }
        else
        {   
            $notification = array(
                    'warning' => 'Not set as category image!', 
                    'alert-type' => 'warning'
                );
               return back()->with($notification);  
        }
        $notification = array(
                'success' => 'Set as category image successfully!', 
                'alert-type' => 'success'
            );
        
        return back()->with($notification);
        
        // $pp->p_imageUrl;$pp->ssc_id;
    }

    public function setas_category_image($id)
    {
        $helper = new Helper();
        if($id != "undefined")
        {
            $pp = DB::table("pinterest_pins")->where("pp_id",$id)->first();
            $scid = DB::table("service_sub_categories")->where("ssc_id",$pp->ssc_id)->first()->sc_id;
    
            $sc_image = DB::table("service_categories")->where("sc_id",$scid)->first()->sc_image;
            if(!empty($sc_image))
            {
                
                    $filename = $helper->cate_images()."/".$sc_image;
                   
                 // $filename = public_path()."/category_images//".$sc_image;
                
                        if (file_exists($filename)) 
                        {   
                            \File::delete($filename);                         
                        }
            }
            
            $url = $pp->p_imageUrl;       
            $contents = file_get_contents($url);
            $name = substr($url, strrpos($url, '/') + 1);
            \Storage::put($name, $contents);

            $image = storage_path().'/'.'app'.'/'.$name;

            
                $destination    = $helper->cate_images();
                    
            // $destination= public_path("category_images/");

            $img_name=basename($image);

            if( rename( $image , $destination.'/'.$img_name )){
             
            } 

            DB::table("service_categories")->where("sc_id",$scid)->update(array("sc_image"=>$name));
            $sc = DB::table("service_categories")->where("sc_id",$scid)->first();
        }
        else
        {   
            $notification = array(
                    'warning' => 'Not set as category image!', 
                    'alert-type' => 'warning'
                );
               return back()->with($notification);  
        }
        $notification = array(
                'success' => 'Set as category image successfully!', 
                'alert-type' => 'success'
            );
        
        return back()->with($notification);
        
        // $pp->p_imageUrl;$pp->ssc_id;
    }
    public function set_as_editor_img($id)
    {
        if($id != "undefined")
        {
            $ssc_id = DB::table("pinterest_pins")->where("pp_id",$id)->first();
            if($ssc_id->p_status!=2)
            {
                $ssc_id = $ssc_id->ssc_id;
                // dd($ssc_id);
                DB::statement("UPDATE `pinterest_pins` SET p_setas_editor_img = 0 where ssc_id = '$ssc_id' ");
                DB::table("pinterest_pins")->where("pp_id",$id)->update(array("p_setas_editor_img"=>1,'p_status'=>1));    
                $notification = array(
                        'success' => 'Set as editor image successfully!', 
                        'alert-type' => 'success'
                    );
                return back()->with($notification);
            }
        }
        else
        {
            $notification = array(
                        'warning' => 'Not set as editor image!', 
                        'alert-type' => 'warning'
                    );
                return back()->with($notification);
        }
        
    }

    public function add_new_post($ssc_id)
    {
        $page_title = "Post Management";

       $sscs = DB::table("service_sub_categories")
                   ->where('ssc_status',1)
                   ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                   ->where('service_categories.sc_status',1)
                   ->orderBy('ssc_gender','ASC')
                   ->orderBy("ssc_name","ASC")->get();
                   // dd(Carbon::now()->toDateTimeString());

        return view("pinterestboard/add_new_post",compact('ssc_id','page_title','sscs'));
    }
    public function addnew_post(Request $request)
    {
        $data['ssc_id']     = $request->ssc_id;
        $data['p_status']   = $request->p_status;
        $data['pp_creator'] = 'admin';
        $data['p_link']     = $request->p_link;
        $data['p_imageUrl'] = $request->p_imageUrl;
        $data['p_metadata'] = $request->p_metadata;
        $data['p_keywords'] = $request->p_keywords;

        $data['admin_id']   = \Auth::user()->id;
        $data['pp_datetime'] = Carbon::now()->toDateTimeString();
        $data['p_fetch_date'] = Carbon::now()->toDateTimeString();
        $data['update_date'] = Carbon::now()->toDateTimeString();


        $check_post = DB::table('pinterest_pins as pp')
                          ->join('service_sub_categories as ssc','ssc.ssc_id','=','pp.ssc_id')->where('p_imageUrl',$data['p_imageUrl'])->first();
        
        if(!empty($check_post)){
             $notification = array(
                'warning' =>  'this post is already exist under the this service sub category '.$check_post->ssc_name,
                'alert-type' => 'warning'
            );
        }
        else
        {   
             // dd($check_post);
            $result = DB::table("pinterest_pins")->insert($data);

            $notification = array(
                'success' => 'New post added successfully!', 
                'alert-type' => 'success'
            );
        }
     
        
        return back()->with($notification);
    }

    public function upload_salon(Request $request)
    {
        Session::put('activemenu','save_file_services'); 
        echo "oka";
    }

    // resize image 
    public function resize()
    {
        return view("image_resize/image_resize");
    }
    public function image_re(Request $request)
    {
        echo "safadf";
    }

    public function posts()
    {
        $page_title = "Service Sub-Category Styles";
        $posts = DB::SELECT("select count(*) as count, p_status, service_sub_categories.ssc_id, sc_name, ssc_name from `pinterest_pins` inner join `service_sub_categories` on `service_sub_categories`.`ssc_id` = `pinterest_pins`.`ssc_id` inner join `service_categories` on `service_categories`.`sc_id` = `service_sub_categories`.`sc_id` where p_status in(0,1) and `service_sub_categories`.`ssc_status` = 1 and `service_categories`.`sc_status` = 1 group by 2,3,4 order by service_sub_categories.ssc_id, p_status");

        // dd($posts);
        $DuplicateTest = $posts;
        $myfinalCategories = [];
        foreach ($DuplicateTest as $index => $SubCat) {
            
            if($index != count($DuplicateTest)-1){
                $NextSubCateogry = $DuplicateTest[$index+1];
                
            if($SubCat->ssc_id == $NextSubCateogry->ssc_id){
                if($SubCat->p_status == 0){
                    $SubCat->ApprovedPost = $NextSubCateogry->count;
                }else{
                    $SubCat->ApprovedPost = $NextSubCateogry->count;
                }
                $SubCat->unApprovedPost = $SubCat->count;
                array_push($myfinalCategories,$SubCat);
                // unset($DuplicateTest[$index+1]);
                
            }else{
                if($SubCat->p_status == 0){
                    $SubCat->unApprovedPost = $SubCat->count;
                }else{
                    $SubCat->ApprovedPost = $SubCat->count;
                }
                array_push($myfinalCategories,$SubCat);
            }
            }
           
        }
        // echo "<pre>";
        //     // // print_r($SubCat) ;
        //     print_r($myfinalCategories) ;
        //     die();
// dd($posts);
        Session::put('activemenu','service_subcategory_posts'); 
        return view('pinterestboard/posts',compact('myfinalCategories','posts','page_title'));
    }

    public function post_detail($id)
    {

        Session::put('activemenu','service_subcategory_posts');
        
        $page_title = "Service Sub-Category Styles Detail";
        // $posts = DB::table('pinterest_pins as pp')
        //                     ->select('pp.p_status','ssc.ssc_id', 'ssc_name','pp.update_date','sc.sc_name')
        //                     ->join('service_sub_categories as ssc','ssc.ssc_id','=','pp.ssc_id')
        //                     ->join('service_categories as sc','sc.sc_id','=','ssc.sc_id')
        //                     ->where('pp.ssc_id',$id)
        //                     ->where('p_status',1)
        //                     ->orderBy('update_date','DESC')
        //                     // ->toSql();
        //                     ->get();
            $posts = DB::SELECT("select count(*) as count, p_status, `ssc`.`ssc_id`, `ssc_name`, date(`pp`.`update_date`) as update_date, `sc`.`sc_name` from `pinterest_pins` as `pp` inner join `service_sub_categories` as `ssc` on `ssc`.`ssc_id` = `pp`.`ssc_id` inner join   `service_categories` as `sc` on `sc`.`sc_id` = `ssc`.`sc_id` where `pp`.`ssc_id` = '$id' and `p_status` = 1 group by date(update_date) order by `update_date` desc");
                // dd($posts);

             
        
        return view('pinterestboard/post_detail',compact('posts','page_title'));
    }

    public function update_app_data()
    {
        $page_title = "Update Salon App Data";
        
        // update local_data_services last fetched
            $qry = DB::statement("UPDATE local_data_services SET last_updated = (Select max(last_updated) from app_home_page_section WHERE status = 1) WHERE lds_id = 1");
            // @mysql_query($qry);
            
            $qry = DB::statement("UPDATE local_data_services SET last_updated = (Select max(ssc_modify_datetime) from service_sub_categories WHERE ssc_status = 1) WHERE lds_id = 3");
            // @mysql_query($qry);
            
            $qry = DB::statement("UPDATE local_data_services SET last_updated = (Select max(p_fetch_date) from pinterest_pins WHERE p_status = 1) WHERE lds_id = 4");
            
            $qry = DB::statement("update salon s JOIN postcodelatlng pc on s.sal_zip = pc.postcode set s.sal_lat = pc.latitude, s.sal_lng = pc.longitude where s.sal_lat = '' or s.sal_lng = '' ");

            // $pk_fetch_date = DB::SELECT("Select max(pk_fetch_date) as pk_fetch_date from post_keywords");
            // DB::table('orders')->where('id', DB::raw("(select max(`id`) from orders)"))->get();

            $pk_fetch_date =  DB::table('post_keywords')
                                ->where('pk_fetch_date', DB::raw("(select max(`pk_fetch_date`) from post_keywords)"))
                                ->first()->pk_fetch_date;
            // dd($pk_fetch_date);

            if(empty($pk_fetch_date) || is_null($pk_fetch_date))
            {
                $result =  DB::statement(" INSERT INTO post_keywords (pk_keyword, ssc_id, pk_fetch_date)  
                                SELECT  DISTINCT p_keywords as keyword, ssc_id, update_date from pinterest_pins pp 
                                ON DUPLICATE KEY UPDATE post_keywords.pk_fetch_date = pp.update_date ");
                // dd("if condition  = ",$result);
            }
            else
            {
                $result = DB::statement("INSERT INTO post_keywords (pk_keyword, ssc_id, pk_fetch_date)  
                            SELECT  DISTINCT p_keywords as keyword, ssc_id, update_date from pinterest_pins pp WHERE p_fetch_date >=   '$pk_fetch_date'
                            ON DUPLICATE KEY UPDATE post_keywords.pk_fetch_date = pp.update_date");
                // dd("else condition  = ",$result);
            }

            // @mysql_query($qry);
            $notification = array(
                'warning' => 'App Successfully Updated!', 
                'alert-type' => 'success'
            );

        // return back()->with($notification);
            Session::put("message","Salon App data update successfully ");

        return view('pinterestboard/update_app_data',compact('page_title','notification'));
    }
    public function category_association($sc_id, $ssc_id,$sc_name)
    {
        $sc_gender = DB::table('service_categories')->where('sc_id',$sc_id)->first()->sc_gender;
        $ssc_gender = DB::table('service_sub_categories')->where('ssc_id',$ssc_id)->first()->ssc_gender;
        
        // dd($result);
        $returnback = URL::previous();
        // dd($returnback);
        $category_association = serviceCategory::with('service_sub_category')
                            ->where('sc_status',1)
                            ->where('sc_gender',$sc_gender)
                            // ->where('ssc_gender',$ssc_gender)
                            // ->orderBy('sc_name', 'asc')
                            // ->toSql();
                            ->get(); 
        $page_title = "Category of association";
        return view('service-mgmt/category_association',compact('sc_name','returnback','ssc_id','page_title','category_association'));
    }

    public function updateSalonSerSubCate(Request $request)
    {
        $sscids = $request->sscids;
        $returnback = $request->returnback;
        
        $arr=explode(",",$request->ssc_id);
        $ssc_name = $arr[0];
        $ssc_id = $arr[1];

        // dd('sscids = ',$sscids,"ssc_id  =  ",$ssc_id);
        if($ssc_name == 'sc_id')
        {
            $result = DB::statement("INSERT INTO sal_ser_sub_categories (sal_id, ssc_id ) SELECT distinct sal_id,".$sscids." FROM `sal_ser_sub_categories` ssc where ssc_id in(select ssc_id from service_sub_categories where sc_id = ".$ssc_id." ) ON DUPLICATE KEY UPDATE sal_ser_sub_categories.sal_id = sal_ser_sub_categories.sal_id");
            
        }
        elseif($ssc_name == 'ssc_id')
        {
            $result = DB::statement("INSERT INTO sal_ser_sub_categories (sal_id, ssc_id ) 
            SELECT distinct sal_id, ".$sscids." FROM `sal_ser_sub_categories` as ssc where ssc_id = ".$ssc_id." ON DUPLICATE KEY UPDATE sal_ser_sub_categories.sal_id = sal_ser_sub_categories.sal_id");
        }
        // INSERT INTO sal_ser_sub_categories (sal_id, ssc_id ) 
        //     SELECT distinct sal_id as sal_id, ssc_id FROM `sal_ser_sub_categories` ssc where ssc_id = 43 
        //     ON DUPLICATE KEY UPDATE sal_ser_sub_categories.sal_id = sal_ser_sub_categories.sal_id
            
        // $result1 = DB::statement("INSERT INTO sal_ser_sub_categories (sal_id, ssc_id ) 
        //      SELECT " . $sal_id . ", tag_id FROM service_tags where ser_id  in (" . implode(",", $request['sal_type_status']) . ""
        //                     . ") ON DUPLICATE KEY UPDATE sal_id = 
        //               sal_id, salon_tags.tag_id=salon_tags.tag_id ");
        
    
        
        $notification = array(
            'warning' => 'category for association successfully updated !', 
            'alert-type' => 'success'
        );
        Session::put("returnback",$returnback);
        Session::put("message","category for association successfully updated !");
        return back()->with($notification);
        // dd($result);
        // dd($request->ssc_id);
    }


    public function userapp_activity(Request $request)
    {   $endDate    = '';
        $startDate  = '';
        $page_title = "Userapp Activities ";
        if(!empty($request->startDate) && !empty($request->endDate)){
            $endDate = $request->endDate;
            $startDate = $request->startDate;
            // dd($endDate);
        }else{
            $endDate = date('Y-m-j');
            $startDate = date("Y-m-j", strtotime( '-10 days' ));
        }
        $pu =  DB::select("SELECT date(pu.pu_created_datetime) as pu_datetime, date(c.cust_datetime) as cust_datetime, count(c.cust_id) as customer_count, count(pu.pu_created_datetime) as pu_count FROM `public_users` pu LEFT JOIN customers c on  date(c.cust_datetime) = date(pu.pu_created_datetime) where date(pu.pu_created_datetime) BETWEEN '$startDate' and '$endDate' GROUP BY  date(pu.pu_created_datetime) ORDER By pu_datetime DESC ");

        // $last = Carbon::now()->toDateString();
        // $first = Carbon::now()->subDays(10)->toDateString();

        $pu = DB::table('public_users')
                          ->select(DB::raw(" DATE(`pu_created_datetime`) as pu_datetime, count(pu_created_datetime) as pu_count"))
                          ->whereBetween(DB::raw(" DATE(`pu_created_datetime`)"), [$startDate,$endDate])
                          ->groupBy(DB::raw('DATE(`pu_created_datetime`)'))
                          ->orderBy(DB::raw('DATE(`pu_created_datetime`)'),'DESC')
                          ->get()->toArray();
        // dd($pu[0]->pu_datetime);
        $custm = DB::table('customers')
                         ->select(DB::raw(" DATE(`cust_datetime`) as cu_datetime, count(cust_datetime) as cu_count"))
                         ->whereBetween(DB::raw(" DATE(`cust_datetime`)"), [$startDate,$endDate])
                         ->groupBy(DB::raw('DATE(`cust_datetime`)'))
                         ->orderBy(DB::raw('DATE(`cust_datetime`)'),'DESC')
                         ->get()->toArray();

        $last_ten_days = $this->dateRange($startDate,$endDate);
        arsort($last_ten_days);
        $pu_count = '';
        $cu_count = '';

        $i = 0;
        $j = 0;
        $array1= array();
        foreach ($last_ten_days as $key => $value) {
            $datetime = $value;
            if(isset($pu[$i])){
                if($datetime === $pu[$i]->pu_datetime){
                    $pu_count = $pu[$i]->pu_count;
                    $i++;
                }else{

                   $pu_count = "0"; 
                }
            }else{
                $pu_count = "0";
            }
            if(isset($custm[$j])){
                if($datetime === $custm[$j]->cu_datetime){
                    $cu_count = $custm[$j]->cu_count;
                    ++$j;
                }else{
                    $cu_count = "0";
                }
            }else{
                $cu_count = '0';       
            }
            array_push($array1,['datetime' => $datetime,'pu_count' => $pu_count,'cu_count' => $cu_count ]);
        }
        if(!empty($array1)){
            $public_user = $array1;
        }else{
            $public_user = '';
        }
      // dd( $public_user);

        return view("appuser_activity",compact('public_user','startDate','endDate','page_title'));
    }


    function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {
        $dates = [];
        $current = strtotime( $first );
        $last = strtotime( $last );

        while( $current <= $last ) {
            $dates[] = date( $format, $current );
            $current = strtotime( $step, $current );
        }

        return $dates;
    }

}









