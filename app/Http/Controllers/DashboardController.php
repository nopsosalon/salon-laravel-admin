<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Carbon\Carbon;
use Config;
use Auth;
use App;
use App\Helpers\Helper;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    
            $this->middleware('auth');
            $dbc = new Helper();
            $dbc->setDBConnection();
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {
        $dates = [];
        $current = strtotime( $first );
        $last = strtotime( $last );

        while( $current <= $last ) {
            $dates[] = date( $format, $current );
            $current = strtotime( $step, $current );
        }

        return $dates;
    }

    public function index()
    {   
        $arrays = [];
        $last = Carbon::now()->toDateString();
        $first = Carbon::now()->subDays(10)->toDateString();

        $pu = DB::table('public_users')->select(DB::raw(" DATE(`pu_created_datetime`) as pu_datetime, count(pu_created_datetime) as pu_count"))->whereBetween(DB::raw(" DATE(`pu_created_datetime`)"), [Carbon::now()->subDays(10)->toDateString(),Carbon::now()->toDateString()])->groupBy(DB::raw('DATE(`pu_created_datetime`)'))->orderBy(DB::raw('DATE(`pu_created_datetime`)'),'DESC')
        // ->toSql();
        ->get()->toArray();
        // dd($pu);
        // dd($pu[0]->pu_datetime);
        $custm = DB::table('customers')->select(DB::raw(" DATE(`cust_datetime`) as cu_datetime, count(cust_datetime) as cu_count"))->whereBetween(DB::raw(" DATE(`cust_datetime`)"), [Carbon::now()->subDays(10)->toDateString(),Carbon::now()->toDateString()])->groupBy(DB::raw('DATE(`cust_datetime`)'))->orderBy(DB::raw('DATE(`cust_datetime`)'),'DESC')
            // ->toSql();
        ->get()->toArray();
            // dd($custm);
        $data = '';
        $last_ten_days = $this->dateRange($first,$last);
        arsort($last_ten_days);

        $pu_count = '';
        $cu_count = '';
        $i = 0;
        $j = 0;
        $array1= array();
        foreach ($last_ten_days as $key => $value) {
            $datetime = $value;
            if(isset($pu[$i])){
                if($datetime === $pu[$i]->pu_datetime){
                    $pu_count = $pu[$i]->pu_count;
                    $i++;
                }else{

                   $pu_count = "0"; 
                }
            }else{
                $pu_count = "0";
            }
            if(isset($custm[$j])){
                if($datetime === $custm[$j]->cu_datetime){
                    $cu_count = $custm[$j]->cu_count;
                    ++$j;
                }else{
                    $cu_count = "0";
                }
            }else{
                $cu_count = '0';       
            }
            array_push($array1,['datetime' => $datetime,'pu_count' => $pu_count,'cu_count' => $cu_count ]);
        }
        if(!empty($array1)){
            $public_user = $array1;
        }else{
            $public_user = '';
        }

        $totalpin = '';
        $tsalon = DB::table('salon')->count();
        $totech = DB::table('technicians')->count();
        $customers = DB::table('customers')->count();
        $data = date('Y-m-d',strtotime("-1 days"));
        // $totalpin = DB::table("pinterest_pins")->whereDate('p_fetch_date',$data)->get();
        $date = date('Y-m-d');
       
        $ssc = DB::table('service_sub_categories')->orderBy("ssc_name","ASC")
                        // ->toSql();
                        ->get();
                        // dd($ssc);
        $ssc = '';
        
        $total_salons = DB::table('salon')->count();
        $coun = DB::SELECT("SELECT count(*), pp.ssc_id, ssc_name, date(p_fetch_date) FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) > NOW() - INTERVAL 24 HOUR group by 2,3,4");
        
                // DB::SELECT("SELECT count(*) as count, pp.ssc_id, ssc_name, date(p_fetch_date) as p_fetch_date FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) > NOW() - INTERVAL 24 HOUR group by 2,3,4");
        // $pins = DB::SELECT("SELECT pp.ssc_id, ssc_name, (SELECT count(pp_id) from `pinterest_pins` pp where pp.ssc_id = ssc.ssc_id and pp.p_status = 1) as approved_post, (SELECT count(pp_id) from `pinterest_pins` pp where pp.ssc_id = ssc.ssc_id and pp.p_status = 0 ) as unapproved_post FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) > NOW() - INTERVAL 24 HOUR group by pp.ssc_id order by unapproved_post DESC LIMIT 15");
        // dd($pins);


        $pins = DB::SELECT("SELECT count(*) as count, pp.ssc_id, ssc_name FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) > NOW() - INTERVAL 24 HOUR group by 2,3 order by count DESC LIMIT 15");

        $app_requests = DB::SELECT("SELECT app.app_created, c.cust_name, s.sal_name
                        FROM appointments app
                        INNER JOIN customers c ON c.cust_id = app.cust_id
                        INNER JOIN salon s ON s.sal_id   = app.sal_id order by app.app_created DESC LIMIT 10
                        ");
        // dd($app_requests);
        $posts = DB::SELECT("select count(*) as count, p_status, service_sub_categories.ssc_id, sc_name, ssc_name from `pinterest_pins` inner join `service_sub_categories` on `service_sub_categories`.`ssc_id` = `pinterest_pins`.`ssc_id` inner join `service_categories` on `service_categories`.`sc_id` = `service_sub_categories`.`sc_id` where p_status in(0,1) and `service_sub_categories`.`ssc_status` = 1 and `service_categories`.`sc_status` = 1 group by 2,3,4 order by service_sub_categories.ssc_id, p_status LIMIT 20");


        $DuplicateTest = $posts;
        $myfinalCategories = [];
        foreach ($DuplicateTest as $index => $SubCat) {
            
            if($index != count($DuplicateTest)-1){
                $NextSubCateogry = $DuplicateTest[$index+1];
                
            if($SubCat->ssc_id == $NextSubCateogry->ssc_id){
                if($SubCat->p_status == 0){
                    $SubCat->ApprovedPost = $NextSubCateogry->count;
                }else{
                    $SubCat->ApprovedPost = $NextSubCateogry->count;
                }
                $SubCat->unApprovedPost = $SubCat->count;
                array_push($myfinalCategories,$SubCat);
                // unset($DuplicateTest[$index+1]);
                
            }else{
                if($SubCat->p_status == 0){
                    $SubCat->unApprovedPost = $SubCat->count;
                }else{
                    $SubCat->ApprovedPost = $SubCat->count;
                }
                array_push($myfinalCategories,$SubCat);
            }
            }
           
        }
        // echo "<pre>";
        //     // // print_r($SubCat) ;
        //     print_r($myfinalCategories) ;
        //     die();
        $total_posts = DB::SELECT("select count(*)
        , p_status, service_sub_categories.ssc_id, sc_name, ssc_name from `pinterest_pins` inner join `service_sub_categories` on `service_sub_categories`.`ssc_id` = `pinterest_pins`.`ssc_id` inner join `service_categories` on `service_categories`.`sc_id` = `service_sub_categories`.`sc_id` where p_status in(0,1) and `service_sub_categories`.`ssc_status` = 1 and `service_categories`.`sc_status` = 1 group by 2,3,4 order by service_sub_categories.ssc_id, p_status");

        // dd($posts);
        $totaol_apr = DB::table('app_requests')->count();

        $active_bt = DB::select( DB::raw("SELECT * FROM beauty_tips where bt_status = 1 and date(bt_modified_date) > NOW() - INTERVAL 24 HOUR"));
        $t_bt = DB::table('beauty_tips')->where('bt_status',1)->get()->count();

        Session::put("activemenu","dashboard");
        return view('dashboard',compact('public_user','t_bt','active_bt','customers','myfinalCategories','posts','total_posts','totaol_apr','app_requests','totalpin','tsalon','totech','ssc','date','pins'), ['total_salons' => $total_salons]);

// SELECT count(*), pp.ssc_id, ssc_name, date(p_fetch_date) FROM `pinterest_pins` pp, service_sub_categories ssc where ssc.ssc_id = pp.ssc_id and date(p_fetch_date) BETWEEN '2018-05-26' and '2018-06-01' group by 2,3,4

    }
}
