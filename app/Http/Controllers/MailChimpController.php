<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Config;
use \DrewM\MailChimp\MailChimp;
use \DrewM\MailChimp\Batch;
use \DrewM\MailChimp\Webhook;
use Carbon\Carbon;
use DB;


class MailChimpController extends Controller
{
    public $campaign_id = '50c1cc1381';
    public $apikey = '5c9da73d2c30caa708d80afae0aed959-us18';
    public $mailchimp;
    public $listId = '08331600cf'; 


    public function __construct(\Mailchimp $mailchimp)
    {
      $this->middleware('auth');
      $this->mailchimp = $mailchimp;
      $ssl = false;

    }
    
    public function manageMailChimp()
    {
        return view('newsletter/sendmail');
    }

    public function getList_status()
    {
      $MailChimp = new Mailchimp("5c9da73d2c30caa708d80afae0aed959-us18"); 
      $result = $MailChimp->get('/lists/'.$this->listId.'/members/');
      // print($result["members"][1]["status"]);
      $re = $result['members'];
      foreach ($re as $key => $value) {
        $result = \DB::statement("INSERT INTO mailchimp_email_status(email_address,status)values('".$value['email_address']."','".$value['status']."') ON DUPLICATE KEY UPDATE mailchimp_email_status.email_address = '".$value['email_address']."', mailchimp_email_status.status = '".$value['status']."' ");
      }  
      // dd($re);
      $page_title = "Mail status";
      return view('pinterestboard/mailstatus',compact('re','page_title'));
      
    }
    public function subscribe(Request $request)
    {
      // $MailChimp = new Mailchimp("5c9da73d2c30caa708d80afae0aed959-us18"); 
      // $result = $MailChimp->get('/lists/'.$this->listId.'/members/');
      // // print($result["members"][1]["status"]);
      // $re = $result['members'];
      // foreach ($re as $key => $value) {
      //   echo "<pre>";
      //   print_r($value);
      //   echo "<br>";
      //   echo $value['email_address'];
      //   echo "<br>";
      //   echo $value['status'];
      //   echo "</pre>";

      //   // $result = \DB::statement("INSERT INTO mailchimp_email_status(email_address,status)values('".$value['email_address']."','".$value['status']."') ON DUPLICATE KEY UPDATE mailchimp_email_status.email_address = '".$value['email_address']."', mailchimp_email_status.status = '".$value['status']."' ");
      // }  
      // die();
      // dd($result);
      ///
      
       $data = [
          'email'     => $request->email,
          'status'    => 'subscribed'
         ];
         
      
                $apiKey = $this->apikey;
                $listId = $this->listId;

                $memberId = md5(strtolower($data['email']));
                $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
                $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;

                $json = json_encode([
                        'email_address' => $data['email'],
                        'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
                ]);

                $ch = curl_init($url);

                curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);

                $notification = array(
                'success' => 'User subscribed successfully!', 
                'alert-type' => 'success'
            );
        
            return back()->with($notification);
                // return back()

        


      // $mailchimp = new \Mailchimp($this->apikey);
      // // dd($mailchimp->users);
     
      // // dd($this->mailchimp->call('get', '/lists/'.$this->listId.'/members'));

      // try {
      // $this->mailchimp->lists->status($this->listId,['email' => $request->email]);
      //      return redirect()->back()->with('success','Email Subscribed successfully');
      //   } 
      //   catch (\Mailchimp_List_AlreadySubscribed $e) {
      //       return redirect()->back()->with('error','Email is Already Subscribed');
      //   } 
      //   catch (\Mailchimp_Error $e) {
 
      //       return redirect()->back()->with('error','Error from MailChimp');
      //   }

      // send mails 
        //require_once('mailchimpint/mcapi/inc/MCAPI.class.php');
      //   $apiKey = $this->apikey;
      //   $to_emails = array('fahimalyani73@gmail.com', 'fahimalyani73@gmail.com');
      //   $to_names = array('fahim1', 'fahim2');

      //   $message = array(
      //       'html'=>'Yo, this is the <b>html</b> portion',
      //       'text'=>'Yo, this is the *text* portion',
      //       'subject'=>'This is the subject',
      //       'from_name'=>'Me!',
      //       'from_email'=>'',
      //       'to_email'=>$to_emails,
      //       'to_name'=>$to_names
      //   );

      //   $tags = array('WelcomeEmail');

      //   $params = array(
      //       'apikey'=>$apiKey,
      //       'message'=>$message,
      //       'track_opens'=>true,
      //       'track_clicks'=>false,
      //       'tags'=>$tags
      //   );

      //   $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
      //   $url = 'https://'. $dataCenter . '.api.mailchimp.com/3.0/SendEmail';
      //   // dd($url);
      //   // $url = "http://us5.sts.mailchimp.com/1.0/SendEmail";
      //   $ch = curl_init();
      //   curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($params));
      //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      //   $result = curl_exec($ch);
      //   echo $result;
      //   curl_close ($ch);
      //    dd($result);
      //   $data = json_decode($result);
      //   dd($data->status);
      //   echo "Status = ".$data->status."\n";
         
      // //send mail using mailchimp api core php
      //   $apiKey = $this->apikey;
      //   $campaignId = $this->campaign_id;

      //   $memberId = md5(strtolower("fahimalyani73@gmail.com"));
      //   $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
      //   $url = 'https://'. $dataCenter . '.api.mailchimp.com/3.0/campaigns/' . $campaignId .'/actions/test';

      //   $jsonEmail = '{"test_emails":["fahimalyani73@gmail.com"],"send_type":"html"}';

      //   $ch = curl_init($url);
      //   dd($ch);
      //   curl_setopt($ch, CURLOPT_USERPWD, 'apikey:'.$apiKey);
      //   curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
      //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      //   curl_setopt($ch, CURLOPT_TIMEOUT, 10);
      //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      //   curl_setopt($ch, CURLOPT_POST, true);
      //   curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonEmail);
      //   $result = curl_exec($ch);
      //   curl_close($ch);

      //   dd($result);

      // //delete subscribed 
      //   $email = 'admin@gmail.com';
      //   $list_id = $this->listId;
      //   $api_key = $this->apikey;
      //   $data_center = substr($api_key,strpos($api_key,'-')+1);
      //   $url = 'https://'. $data_center .'.api.mailchimp.com/3.0/lists';
      //   $ch = curl_init($url);
      //   curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $api_key);
      //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      //   curl_setopt($ch, CURLOPT_TIMEOUT, 10);
      //   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      //   $result = curl_exec($ch);
      //   dd($result);
      //   $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      //   curl_close($ch);
      //   dd($status_code);
      // // subscribed 
      //   $email = 'fahim@gmail.com';
      //   $list_id = $this->listId;
      //   $api_key = $this->apikey;
      //   $data_center = substr($api_key,strpos($api_key,'-')+1);
      //   $url = 'https://'. $data_center .'.api.mailchimp.com/3.0/lists/'. $list_id .'/members';
      //   $json = json_encode([
      //       'email_address' => $email,
      //       'status'        => 'subscribed', //pass 'subscribed' or 'pending'
      //   ]);
      //   $ch = curl_init($url);
      //   curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $api_key);
      //   curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
      //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      //   curl_setopt($ch, CURLOPT_TIMEOUT, 10);
      //   curl_setopt($ch, CURLOPT_POST, 1);
      //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      //   curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
      //   $result = curl_exec($ch);
        
      //   $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      //   curl_close($ch);
      //   dd($status_code);


          ///////
        // $MailChimp = new \DrewM\MailChimp\MailChimp($this->apikey);
        // $MailChimp->verify_ssl = false;
        // $campaigns = $MailChimp->get('campaigns');
        
        // $result = $MailChimp->post('campaigns/'.$campaigns['campaigns'][0]['50c1cc1381'].'/actions/send');

        //     if ($MailChimp->success()) {
        //          print_r($result);   
        //          die(); 
        //      } else {
        //       echo $MailChimp->getLastError();
        //       die();
        //     }
        //     dd("oka");
        //     //////////
        // $email = 'abc1@gmail.com';
        //     $list_id = '08331600cf';
        //     $api_key = '5c9da73d2c30caa708d80afae0aed959-us18';
        //     $data_center = substr($api_key,strpos($api_key,'-')+1);
        //     $url = 'https://'. $data_center .'.api.mailchimp.com/3.0/campaigns/50c1cc1381/actions/send';  
        //     // dd($url);
        //     $json = json_encode([
        //         'to_email' => 'text@text.com',
        //         'from_email'        => 'abc@gmail.com',
        //         'subject'        => 'test subject',
        //         'test_emails' => array('test@test.com'),
        //         'message' => 'text ' //pass 'subscribed' or 'pending'
        //     ]);
        //     $ch = curl_init($url);
        //     // dd($ch);
        //     curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $api_key);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        //     curl_setopt($ch, CURLOPT_POST, 1);
        //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //     curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        //     $result = curl_exec($ch);
        //     dd($result);
        //     $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //     curl_close($ch);
        //     dd($status_code);


        // $stringData =  array(
        //                     'apikey'        => $this->apikey,
        //                     'email_address' => $request->email,
        //                     'status'        => 'subscribed',
        //                     'merge_fields'  =>  array(
        //                         'FNAME'     =>  utf8_encode('FNAME'),
        //                         'LNAME'     =>  utf8_encode('LNAME')
        //                     )
        //                 );
        // dd($stringData);
      //   dd(\Mailchimp::getLists());
      //   dd(\Mailchimp::api('GET', '/campaigns', $data = [])); 
      // // array("field1"=>"value1","field2"=>"value2" )
      //       dd($da);
          
      //       $result = $MailChimp->post("lists/$this->listId/members", [
      //                       'email_address' => 'text@text.com',
      //                       'status'        => 'subscribed',
      //                   ]);
      //       // dd($result);

      //       if ($MailChimp->success()) {
      //           print_r($result);
      //           die();   
      //       } else {
      //           echo $MailChimp->getLastError();
      //           die();
      //       }

      //           $apikey = '5c9da73d2c30caa708d80afae0aed959-us18';
      //           $to_emails = array('fahimalyani73@gmail.com', 'your_mom@example.com');
      //           $to_names = array('You', 'Your Mom');
                 
      //           $message = array(
      //               'html'=>'Yo, this is the <b>html</b> portion',
      //               'text'=>'Yo, this is the *text* portion',
      //               'subject'=>'This is the subject',
      //               'from_name'=>'Me!',
      //               'from_email'=>'verifed@example.com',
      //               'to_email'=>$to_emails,
      //               'to_name'=>$to_names
      //           );
                 
      //           $tags = array('WelcomeEmail');
                 
      //           $params = array(
      //               'apikey'=>$apikey,
      //               'message'=>$message,
      //               'track_opens'=>true,
      //               'track_clicks'=>false,
      //               'tags'=>$tags
      //           );
      //            // dd($params);
      //           $url = "http://us1.sts.mailchimp.com/3.0/SendEmail";
                 
      //           $ch = curl_init();
      //           curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($params));
      //           curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      //           curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      //            // dd($ch);
      //           $result = curl_exec($ch);
      //           dd($result);
      //           echo $result;
      //           curl_close ($ch);
                 
      //           $data = json_decode($result);
      //           dd($data);
      //           dd("Status = ".$data->status."\n");

      //   dd(Mailchimp::getLists());
      //   dd("get api ",Mailchimp::api('post', '/campaign-folders', $data = ['admin@gmail.com'])); //  check the specific user status 

      // dd("get api ",Mailchimp::api('GET', '/ping', $data = [])); //  check the specific user status 
        
       
       // dd(Mailchimp::getLists()); activity
       

        $this->validate($request, [
	    	'email' => 'required|email',
        ]);
// $mc = new \NZTim\Mailchimp\Mailchimp('5c9da73d2c30caa708d80afae0aed959-us18');
// dd($mc);
        // core php start code here 
            // $email = 'abc1@gmail.com';
            // $list_id = '08331600cf';
            // $api_key = '5c9da73d2c30caa708d80afae0aed959-us18';
            // $data_center = substr($api_key,strpos($api_key,'-')+1);
            // $url = 'https://'. $data_center .'.api.mailchimp.com/3.0/lists/'. $list_id .'/members';
            // // dd($url);
            // $json = json_encode([
            //     'email_address' => $email,
            //     'status'        => 'subscribed', //pass 'subscribed' or 'pending'
            // ]);
            // $ch = curl_init($url);
            // // dd($ch);
            // curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $api_key);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            // curl_setopt($ch, CURLOPT_POST, 1);
            // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            // $result = curl_exec($ch);
            // dd($result);
            // $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            // curl_close($ch);
            // dd($status_code);

        // end core php code here 

        // dd(\NZTim\Mailchimp\Mailchimp::getLists());
// dd($this->mailchimp->lists);

        try 
        {
            

            $this->mailchimp
            ->lists
            ->subscribe(
                $this->listId,
                ['email' => $request->input('email')]
            );
           
            return redirect()->back()->with('success','Email Subscribed successfully');
        } 
        catch (\Mailchimp_List_AlreadySubscribed $e) {
            return redirect()->back()->with('error','Email is Already Subscribed');
        } 
        catch (\Mailchimp_Error $e) {
 
            return redirect()->back()->with('error','Error from MailChimp');
        }
    }


    public function sendCompaign(Request $request)
    {

    	$this->validate($request, [
	    	// 'subject' => 'required',
	    	'to_email' => 'required',
	    	// 'from_email' => 'required',
	    	// 'message' => 'required',
        ]);
        try {
	        $options = [
	        'list_id'   => $this->listId,
	        // 'subject' => $request->input('subject'),
          'subject' => 'nopso salon app ',
	        'from_name' => 'salon app',
	        // 'from_email' => $request->input('from_email'),
          'from_email' => 'nopso@gmail.com',
	        'to_name' => $request->input('to_email')
	        ];
	        $content = [
	        // 'html' => $request->input('message'),
            'html' => 'message ',
	        // 'text' => strip_tags($request->input('message'))
            'text' => strip_tags('message')
	        ];

	
          $campaign = $this->mailchimp->campaigns->create('regular', $options, $content);

	        $this->mailchimp->campaigns->send($campaign['id']);

        	return redirect()->back()->with('success','send campaign successfully');
        	
        } catch (Exception $e) {
        	return redirect()->back()->with('error','Error from MailChimp');
        }
    }
}



