<?php

namespace App\Http\Controllers;

//AYrnuOGV91nZr_s7pF4sKrwJdWMUFSxYdrC7sGFE6NycygAsVwAAAAA
use DB;
use Illuminate\Http\Request;

class AddBoardController extends Controller {

    public function index() {
        $Nextpage = "";
        //  dd('bus');
        for ($i = 0; $i < 3; $i++) {
            $url_board = "https://api.pinterest.com/v1/me/following/boards/?access_token=AVUChBlf-sUV1v5qZqQvGu6_xtb4FSxiwd0Pz21E6NycygAsVwAAAAA&fields=id%2Cname%2Curl%2Ccounts%2Ccreated_at%2Ccreator%2Cdescription%2Cimage%2Cprivacy%2Creason";
            // $url_board = "http://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.quotes%20WHERE%20symbol%3D%27WRC%27&format=json&diagnostics=true&env=store://datatables.org/alltableswithkeys&callback";


            if ($i == 0) {

                $url_board = "https://api.pinterest.com/v1/me/following/boards/?access_token=AVUChBlf-sUV1v5qZqQvGu6_xtb4FSxiwd0Pz21E6NycygAsVwAAAAA&fields=id%2Cname%2Curl%2Ccounts%2Ccreated_at%2Ccreator%2Cdescription%2Cimage%2Cprivacy%2Creason";
            } else {

                $url_board = $Nextpage;
                //   dd($url_board, $i);
            }

        
       //  dd($result);
            $response = file_get_contents($url_board);
            //   $type="board";
            // $result = DB::statement("INSERT INTO track_data (Json, type) VALUES ('" . $response . "' , '"  . $type . "') ");
            //dd('What');
           echo '' . $response;
            $apiData = json_decode($response, true);
            $page = $apiData['page'];
            if (isset($apiData['page'])) {
                //  $currentPage=$apiData['page'];
                //check if next page exist
                if (isset($page['next'])) {
                    //dd('again set');
                    $Nextpage = $page['next'];
                    //  dd($currentPage);
                } else {
                    $Nextpage = "";
                }
            } else {
                $Nextpage = "";
            }
            // dd($Nextpage);
            //dd($apiData);
            $boards = $apiData['data'];
         //   dd($boards,'dd');
            foreach ($boards as $post) {
                $name = $post['name'];
                $creator_id = $post['creator']['id'];
                $url = $post['url'];
                $created_at = $post['created_at'];
                $privacy = $post['privacy'];
                $reason = $post['reason'];
                //  $image_url=$post['image']['url'];
                $p_id = $post['id'];
                $description = $post['description'];

                $b_total_pins = $post['counts']['pins'];

                // dd('till now');
                // dd($p_id);
                $description = "";
                echo '---outer-----';
                $ssc_id = 1;
                try {
                    
                 $result = DB::statement("INSERT INTO pinterest_boards (pb_name, ssc_id, pb_creator,  pb_url, pb_created_at, pb_privacy, pb_description, b_total_pins, p_board_id) VALUES ('" . $name . "' , '" . $ssc_id . "', '" . $creator_id . "', '" . $url . "', '"
                                . $created_at . "', '" . $privacy . "', '" . $description . "', '" . $b_total_pins . "', '" . $p_id . "')  ON DUPLICATE KEY UPDATE b_total_pins = " . $b_total_pins . " ");
      
                } catch (\Exception $e) {
                    echo 'Exception in inserting data to db';
                }
                     }
            // dd('Boards inserted');
        }
    }

    public function store(Request $request) {
        $board = Board::create($request->all());
        return response()->json($board, 201);
    }

    public function update(Request $request, Board $board) {
        $board->update($request->all());

        return response()->json($board, 200);
    }

    public function delete(Board $baord) {
        $baord->delete();

        return response()->json(null, 204);
    }

}
