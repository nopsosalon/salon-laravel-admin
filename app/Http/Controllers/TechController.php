<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Model\salon_services_sa_import;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use App\Model\serviceCategory;
use App\app_requests_action;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Carbon\Carbon;
use DateTime;
use Config;
use Auth;
use App;
use DB;
use File;

class TechController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }

    public function technicians(Request $request){
        $salons = DB::table('salon')->select('sal_name','sal_id','sal_email')->orderBy('sal_name','ASC')->get();
        
        $sal_id = '';
    	$tech_name 	= '';
    	$tech_status = '';
    	if($request->tech_name){
    		$tech_name = $request->tech_name;
    	}
    	if($request->tech_status){
    		$tech_status = $request->tech_status;
    	}
        if($request->sal_id){
            $sal_id = $request->sal_id;
        }

        // dd($sal_id);
    	// dd('tech_name = '.$tech_name." = tech_status = ".$tech_status);

    	$helper = new Helper();
        $display_tech_img = $helper->display_tech_img();
        $page_title = "";

        $data = DB::table('technicians')->join('salon','salon.sal_id','=','technicians.sal_id');
        if(!is_null($tech_name) && $tech_name != ''){
        	$data = $data->where('tech_name',$tech_name);
        }
        if(!is_null($sal_id) && $sal_id != ''){
            if($sal_id!='all_active'){
                $data = $data->where('technicians.sal_id',$sal_id);
            }
        }
        if(!is_null($tech_status) && $tech_status != ''){
        	if($tech_status!='all_active'){
        		if($tech_status=='1')
        		{
        			$data = $data->where('tech_status',1);
        		}
        		elseif($tech_status=='2'){
        			// dd($tech_status);
        			$data = $data->where('tech_status',0);
        		}
        	}
        }

        $technicians= $data->paginate(50);
        // dd($technicians);

    	return view('technicians/technicians',compact('sal_id','salons','technicians','page_title','tech_name','tech_status','display_tech_img'));
    }
}
