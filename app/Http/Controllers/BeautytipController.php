<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use File;
use App\Model\salon_services_sa_import;
use App\app_requests_action;
use Carbon\Carbon;
use DateTime;
use App\Model\serviceCategory;
use Config;
use Auth;
use App;
use App\Helpers\Helper;
use View;


class BeautytipController extends Controller
{
    //
    public function __construct()
    {
	    $this->middleware('auth');
	    $dbc = new Helper();
	    $dbc->setDBConnection(); 

    }
    public function beautytip_listing(Request $request, $id,$urls){
      $add_btips_listingUrl = \Request::url();
      $btc_id = $id;
      Session::put("activemenu","beautyTips_category");
      $page_title = 'Beauty Tips ';
      $ssc_id = '';
      $bt_status = 'all_status';
      $bt_title = '';
      $data = DB::table('beauty_tips')->where('btc_id',$btc_id);
      if(!empty($request->bt_title) && $bt_title !== ''){
        $data = $data->where('bt_title',$request->bt_title);
      }
      if(!empty($request->bt_status) && $request->bt_status !== 'all_status'){
        if($request->bt_status == 1){
          $bt_status = $request->bt_status;
          $data = $data->where('bt_status',1);
        }
        else{
          $bt_status = $request->bt_status;
          $data = $data->where('bt_status',0);
        }
      }

      $beauty_tips = $data->orderBy('bt_id','DESC')->paginate(40);
      // dd($bt_status);
      // $beauty_tips = DB::table('beauty_tips')->where('btc_id',$btc_id)->paginate(10);
     
    	return view('beautytip/beautytip_listing',compact('add_btips_listingUrl','btc_id','page_title','ssc_id','beauty_tips'));
    }

    public function add_beautytips(Request $request){
      Session::put("activemenu","beautyTips_category");
    	$returnback = $request->urls_back;
      $btc_id = $request->btc_id;
      $ssc_id = null;
    	$page_title = "";
      $bt_id = '';
      $add_btips_listingUrl = $request->add_btips_listingUrl;

      $btc_tips = DB::table('beauty_tip_categories')->orderBy('btc_name','ASC')->get();
      // dd($btc_tips);
      if($returnback==null){
        $returnback = \URL::previous();
      }
      
       $sscs = DB::table("service_sub_categories")
                   ->where('ssc_status',1)
                   ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                   ->where('service_categories.sc_status',1)
                   ->orderBy('ssc_gender','ASC')
                   ->orderBy("ssc_name","ASC")->get();
// dd('oka');
        return view("beautytip/add_beautytips",compact('add_btips_listingUrl','btc_tips','bt_id','btc_id','ssc_id','page_title','sscs','returnback'));
    }
    public function edit_beautyTips(Request $request, $id,$btc_id){
      Session::put("activemenu","beautyTips_category");
      $returnback = $request->urls_back;
      $ssc_id = '';
      $ssc_name = '';
      $page_title = "";

      $btc_tips = DB::table('beauty_tip_categories')->orderBy('btc_name','ASC')->get();

      if($returnback==null){
        $returnback = \Request::url();
      }
    
      $beauty_tips = DB::table('beauty_tips')->where('bt_id',$id)->first();
      $ssc_id = $beauty_tips->ssc_id;
      $bt_id = $beauty_tips->bt_id;

      $ssc_names = DB::SELECT("select ssc_name from service_sub_categories where ssc_id in($ssc_id) "); 
      foreach ($ssc_names as $key => $value) {
        $ssc_name = $ssc_name.','.$value->ssc_name;
      }
      $ssc_name = trim($ssc_name,",");

      $sscs = DB::table("service_sub_categories")
                   ->where('ssc_status',1)
                   ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                   ->where('service_categories.sc_status',1)
                   ->orderBy('ssc_gender','ASC')
                   ->orderBy("ssc_name","ASC")->get();
      // dd($ssc_name);
      return view("beautytip/add_beautytips",compact('ssc_name','btc_tips','btc_id','ssc_id','bt_id','page_title','beauty_tips','sscs','returnback'));
    }

    public function add_beauty_tips(Request $request){
      // dd('oka');
     // dd($request->bt_short_description_english);
      $sscids = $request->ssc_id;
      $ssc_ids = '';
      $ssc_name = '';
      // dd($sscids);
      foreach ($sscids as $key => $value) {
        $ssc_ids = $ssc_ids.','.$value;
      }
      $ssc_ids = trim($ssc_ids,",");

      $ssc_names = DB::SELECT("select ssc_name from service_sub_categories where ssc_id in($ssc_ids) "); 
      foreach ($ssc_names as $key => $value) {
        $ssc_name = $ssc_name.','.$value->ssc_name;
      }
      $ssc_name = trim($ssc_name,",");
      // dd($request->bt_description);
      Session::put("activemenu","beautyTips_category");
      $bt_id = $request->bt_id;
      $btc_id = $request->btc_id;

        $bt_description = str_replace("<pre>","<p>",$request->bt_description);
        $bt_description = str_replace("</pre>","</p>",$bt_description);

        $bt_description_urdu = str_replace("<pre>","<p>",$request->bt_description_urdu);
        $bt_description_urdu = str_replace("</pre>","</p>",$bt_description_urdu);

        $bt_description_urdu = preg_replace('/\s+/', '  ',$bt_description_urdu);
        $bt_title_urdu = preg_replace('/\s+/', '  ',$request->bt_title_urdu);
        $bt_short_description_urdu = preg_replace('/\s+/', '  ',$request->bt_short_description_urdu);
        
        $add_btips_listingUrl = $request->add_btips_listingUrl;     

      if($request->save == 'Update' && $bt_id != ''){
        
        $result = DB::table('beauty_tips')->where('bt_id',$bt_id)->update(array(
                'bt_title'              => $request->bt_title,
                'ssc_id'                => $ssc_ids,
                'bt_status'             => $request->bt_status,
                'bt_URL'                => $request->bt_URL,
                'bt_imageUrl'           => $request->bt_imageUrl,
                'bt_title_urdu'         => $bt_title_urdu == null ?  '' : $bt_title_urdu,
                'bt_description'         => $bt_description,
                'bt_modified_date'      => Carbon::now()->toDateTimeString(),
                'btc_id'                => $request->btc_id,
                'bt_description_urdu'   => $bt_description_urdu == null ?  '' : $bt_description_urdu,
                'bt_type'               => $request->bt_type,

// $salon['sal_city']=$request->input('sal_city')== null ?  '' : $request->input('sal_city');


                'bt_short_description_english'   => $request->bt_short_description_english,

                'bt_short_description_urdu' => $bt_short_description_urdu == null ?  '' : $bt_short_description_urdu,
                'bt_keywords'               => $request->bt_keywords,


              ));
          $notification = array(
                'success' => 'Beatuty Tips Updated Successfully!', 
                'alert-type' => 'success'
            );

      }else{
        $data['bt_title']               = $request->bt_title;
        $data['ssc_id']                 = $ssc_ids;
        $data['bt_status']              = $request->bt_status;
        $data['bt_URL']                 = $request->bt_URL;
        $data['bt_imageUrl']            = $request->bt_imageUrl;
        $data['bt_datetime']            = Carbon::now()->toDateTimeString();
        $data['bt_title_urdu']          = $bt_title_urdu == null ?  '' : $bt_title_urdu;
        $data['bt_description']         = $bt_description;
        $data['bt_modified_date']       = Carbon::now()->toDateTimeString();
        $data['btc_id']                 = $request->btc_id;
        $data['bt_description_urdu']    = $bt_description_urdu == null ?  '' : $bt_description_urdu;
        $data['bt_type']                = $request->bt_type;

        $data['bt_short_description_english'] = $request->bt_short_description_english;
        $data['bt_short_description_urdu']    = $bt_short_description_urdu == null ?  '' : $bt_short_description_urdu;
     
        $data['bt_keywords']                = $request->bt_keywords;



        $result = DB::table('beauty_tips')->insert($data);
        $bt_id = DB::getPdo()->lastInsertId();

        $notification = array(
                'success' => 'Beatuty Tips Add Successfully!', 
                'alert-type' => 'success'
            );

        return redirect()->to($add_btips_listingUrl)->with($notification);
      }
      

      $returnback = $request->urls_back;
      $ssc_id = null;
      $page_title = "";

      $btc_tips = DB::table('beauty_tip_categories')->orderBy('btc_name','ASC')->get();

      if($returnback==null){
        $returnback = \Request::url();
      }
      $beauty_tips = DB::table('beauty_tips')->where('bt_id',$bt_id)->first();
      $ssc_id = $beauty_tips->ssc_id;
      $bt_id = $beauty_tips->bt_id;
      $sscs = DB::table("service_sub_categories")
                   ->where('ssc_status',1)
                   ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                   ->where('service_categories.sc_status',1)
                   ->orderBy('ssc_gender','ASC')
                   ->orderBy("ssc_name","ASC")->get();
// dd($ssc_name);
      return view("beautytip/add_beautytips",compact('ssc_name','btc_tips','btc_id','ssc_id','bt_id','page_title','beauty_tips','sscs','returnback'))->with($notification);
        // return back()->with($notification);
      
    }

    public function beautyTips_category(Request $request){
      Session::put("activemenu","beautyTips_category");
      $helper = new Helper();

        $cate_images = $helper->display_beautytips_images();
        $page_title = "";
        // dd($request->btc_status);
        $btc_name = '';
        $btc_status = 'all_active';
        if(!empty($request->btc_name) && !is_null($request->btc_name)){
          $btc_name = $request->btc_name;
        }
        if(!empty($request->btc_status) && !is_null($request->btc_status)){
          $btc_status = $request->btc_status;
        }

        $data = DB::table('beauty_tip_categories');
       if(!empty($btc_name) && $btc_name !== '')
       {
          $data = $data->where('btc_name',$btc_name);
       }
       if(!empty($btc_status) && $btc_status!=='all_active'){
        if($btc_status==1){
          $data = $data->where('btc_status',1);
        }
        else{
          $data = $data->where('btc_status',0);
        }
       }
       $beauty_tip_cat = $data->orderBy('btc_id','DESC')->paginate(40);
       // dd($beauty_tip_cat);
       $count = $data->count();
  
        return view('beautytip/beautyTips_category',compact('count','cate_images','page_title','btc_name','btc_status'), ['beauty_tip_cat' => $beauty_tip_cat]);
   
    }
    public function add_beautyTips_category($urls){
      Session::put("activemenu","beautyTips_category");
      $page_title = '';
      $returnback = $urls;
      return View::make('beautytip/add_beautytips_category',compact('page_title','returnback'));
    }
    public function store_beautytips_category(Request $request){
      Session::put("activemenu","beautyTips_category");
      $helper = new Helper();
      $data['btc_name'] = $request->btc_name;
      $data['btc_status'] = $request->btc_status;
      $data['btc_image'] = $request->btc_image;
      $data['btc_created_date'] = Carbon::now()->toDateTimeString();
      $data['btc_updated_date'] = Carbon::now()->toDateTimeString();

      if (Input::hasFile('btc_image')) {
           $destinationPath = $helper->store_beautytips_images();
           $data['btc_image'] = time() . '.' . $request->btc_image->getClientOriginalExtension();
           // if(Session::get('change_db_connection')=="3"){
           //        $helper->uploadimageFTP('/beautytips_images/'.$data['btc_image'],$request->btc_image);
           //    }
           $request->btc_image->move($destinationPath, $data['btc_image']);    
        }

      $result = DB::table('beauty_tip_categories')->insert($data);

      $notification = array(
                'success' => 'Beauty Tips Category Added Successfully !', 
                'alert-type' => 'success'
            );
      return redirect('beautyTips_category')->with($notification);
        // return back()->with($notification);
    }
    public function edit_beautytip_cat($id,$urls){
      Session::put("activemenu","beautyTips_category");
      $returnback = $urls;
      $page_title = '';

      $data = DB::table('beauty_tip_categories')->where('btc_id',$id)->first();
      return view('beautytip/edit_beautytip_cat',compact('data','page_title','returnback'));
    }
    public function update_beautyTips_cat(Request $request, $id){
      Session::put("activemenu","beautyTips_category");
      $helper = new Helper();
      $data['btc_image'] = '';
      $files = DB::table('beauty_tip_categories')->where('btc_id' , $id)->first();
      if (Input::hasFile('btc_image')) {
              $file = $files->btc_image; 
                  $filename = $helper->display_beautytips_images()."/".$file;
              if (file_exists($filename)) 
              {   
                  \File::delete($filename);                         
              }

             $destinationPath = $helper->store_beautytips_images();
             $data['btc_image'] = time() . '.' . $request->btc_image->getClientOriginalExtension();
             // if(Session::get('change_db_connection')=="3"){
             //      $helper->uploadimageFTP('/beautytips_images/'.$data['btc_image'],$request->btc_image);
             //  }
             $request->btc_image->move($destinationPath, $data['btc_image']);    
        }
        if(empty($data['btc_image']) OR $data['btc_image'] == ''){
          $data['btc_image'] = $files->btc_image;
        }

      DB::table('beauty_tip_categories')->where('btc_id',$id)->update(array(
        'btc_name'          => $request->btc_name,
        'btc_status'        => $request->btc_status,
        'btc_image'         =>  $data['btc_image'],
        'btc_updated_date'  => Carbon::now()->toDateTimeString()
      ));
      
      $notification = array(
                'success' => 'Beauty Tips Category Updated Successfully !', 
                'alert-type' => 'success'
            );
        return back()->with($notification);
    }

    public function beautytip_search(Request $request){
      $add_btips_listingUrl = $request->add_btips_listingUrl;
      $btc_id = $request->btc_id;
      Session::put("activemenu","beautyTips_category");
      $page_title = 'Beauty Tips ';
      $ssc_id = '';
      $bt_status = 'all_status';
      $bt_title = '';
      $bt_type = 'all_type';
      $data = DB::table('beauty_tips')->where('btc_id',$btc_id);

      if(!empty($request->bt_type) && $request->bt_type !== 'all_type'){
        if($request->bt_type == 1){
          $bt_type = $request->bt_type;
          $data = $data->where('bt_type',1);
        }
        elseif($request->bt_type == 2){
          $bt_type = $request->bt_type;
          $data = $data->where('bt_type',2);
        }
      }

      if(!empty($request->bt_status) && $request->bt_status !== 'all_status'){
        if($request->bt_status == 1){
          $bt_status = $request->bt_status;
          $data = $data->where('bt_status',1);
        }
        else{
          $bt_status = $request->bt_status;
          $data = $data->where('bt_status',0);
        }
      }
      if(!empty($request->bt_title) && $request->bt_title !== ''){
        $bt_title = $request->bt_title;
        $data = $data->where('bt_title','LIKE','%'.$request->bt_title.'%');
      }
      
      $beauty_tips = $data->orderBy('bt_id','DESC')->paginate(40);
      // dd($beauty_tips);
      // $beauty_tips = DB::table('beauty_tips')->where('btc_id',$btc_id)->paginate(10);
     
      return view('beautytip/beautytip_listing',compact('bt_type','add_btips_listingUrl','bt_title','bt_status','btc_id','page_title','ssc_id','beauty_tips'));
    }

    public function active_inactive(Request $request){
      $bt_status = $request->bt_status;
      $bt_id = '';
      $bt_id = $request->bt_id;
      $bt_id = join(',', $bt_id);
      // dd($bt_id);
      if($request->bt_status == 'Beautytip-Enabled'){
          DB::statement("update beauty_tips set bt_status = 1 where bt_id in($bt_id)");
          return back();
          dd('enabled bt_status ',$request->bt_id);
    }
    if($request->bt_status == 'Beautytip-Disbaled'){
      DB::statement("update beauty_tips set bt_status = 0 where bt_id in($bt_id)");
      return back();
          dd('enabled disabled ',$request->bt_id);
    }
  }

  public function active_beauty_tips(Request $request){
    $page_title = "Beauty Tips ";
    $bt_status = '';

    $data = DB::table('beauty_tips')->orderBy('bt_modified_date','ASC');

    if($request->bt_status !== 'all_active'){
      
      if($request->bt_status === '1'){
        $bt_status = $request->bt_status;
        $data = $data->where('bt_status',1);
      }
      if($request->bt_status === '2'){
        $bt_status = $request->bt_status;
        $data = $data->where('bt_status',0);
      }
    }
    $beautytips = $data->get();
    // dd($beautytips);
    return view('beautytip/beautytips',compact('bt_status','beautytips','page_title'));
    dd($beautytips);
  }
  public function beautytips_detail($id){
    $return_back =  \URL::previous();
    $page_title = "Beauty Tip Detail";
    $beautytip = DB::table('beauty_tips')->where('bt_id',$id)->first();
    return view('beautytip/beautytip_detail',compact('return_back','page_title','beautytip'));
  }

  public function setas_featured_image($id)
    {
      // dd($id);
        
        if($id !== "undefined" AND $id !== null AND $id !== '')
        {
            DB::table('beauty_tips')->where('bt_is_featured','1')->update([
                'bt_is_featured' => 0,
              ]);
            DB::table('beauty_tips')->where('bt_id',$id)->update([
                'bt_is_featured' => 1,
              ]);
        }
        else
        {   
            $notification = array(
                    'warning' => 'Not set as category image!', 
                    'alert-type' => 'warning'
                );
               return back()->with($notification);  
        }
        $notification = array(
                'success' => 'Set as category image successfully!', 
                'alert-type' => 'success'
            );
        return back()->with($notification);
    }


}
