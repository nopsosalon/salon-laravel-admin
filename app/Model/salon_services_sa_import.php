<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class salon_services_sa_import extends Model
{
    //
    protected $table = "salon_services_sa_import";
    protected $primaryKey = '';

    protected $fillable = ['sal_id','sser_name','sser_rate','sser_time','sser_id','sal_name','ssc_name'];
    public $timestamps  = false;
}
