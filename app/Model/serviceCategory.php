<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\serviceSubCategory;

class serviceCategory extends Model
{
    // 
    protected $table = 'service_categories';
    protected $primaryKey = 'sc_id';

    public function service_sub_category()
    {
        return $this->hasMany('App\Model\serviceSubCategory','sc_id')->where('ssc_status',1);
    }
    
}
