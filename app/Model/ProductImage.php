<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Product;

class ProductImage extends Model
{
    //
    protected $table = 'product_images';
    protected $primaryKey = 'pi_id';
    protected $guarded = [];
    public $timestamps = false;

    public function products(){
    	return $this->belongsTo('App\Model\Product','p_id');
    }
}
