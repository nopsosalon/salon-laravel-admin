<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    //

    protected $table = 'states';
    protected $primaryKey = 'id';

    public function getCity(){
    	return $this->hasMany('App\Model\cities','state_id');
    }
}
