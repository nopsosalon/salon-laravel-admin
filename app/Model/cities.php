<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class cities extends Model
{
    //
    protected $table = 'cities';
    protected $primaryKey = 'id';

    public function getStates(){
    	$this->belongsTo('App\Model\States','id');
    }
}
